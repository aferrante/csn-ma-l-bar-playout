object NCAAMatchupManualDlg: TNCAAMatchupManualDlg
  Left = 415
  Top = 266
  Width = 656
  Height = 212
  Caption = 'NCAA Football Matchups - Manual Entry'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 24
    Top = 16
    Width = 598
    Height = 105
    BevelWidth = 2
    TabOrder = 0
    object Label12: TLabel
      Left = 21
      Top = 17
      Width = 97
      Height = 16
      Caption = 'Matchup Text:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Edit1: TEdit
      Left = 21
      Top = 38
      Width = 556
      Height = 28
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      MaxLength = 50
      ParentFont = False
      TabOrder = 0
    end
  end
  object BitBtn4: TBitBtn
    Left = 214
    Top = 132
    Width = 99
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkOK
  end
  object BitBtn1: TBitBtn
    Left = 336
    Top = 132
    Width = 99
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Kind = bkCancel
  end
end
