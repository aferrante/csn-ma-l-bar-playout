unit PlaybackControlsForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls;

type
  TPlaybackControls = class(TForm)
    PlayoutPanel: TPanel;
    AutoPlaybackEnableCheckbox: TCheckBox;
    LoadPlaylistBtn: TBitBtn;
    PlayNextBtn: TBitBtn;
    PlaySelectedBtn: TBitBtn;
    UnloadCurrentPlaylistBtn: TBitBtn;
    TriggerTickerBtn: TBitBtn;
    AbortPlaylistBtn: TBitBtn;
    ResetTickerBtn: TBitBtn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PlaybackControls: TPlaybackControls;

implementation

{$R *.dfm}

end.
