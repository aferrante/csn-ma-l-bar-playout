object AuthoringInterface: TAuthoringInterface
  Left = 901
  Top = 313
  Width = 186
  Height = 140
  Caption = 'AuthoringInterface'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object AuthoringControlPort: TServerSocket
    Active = False
    Port = 3395
    ServerType = stNonBlocking
    OnAccept = AuthoringControlPortAccept
    OnClientConnect = AuthoringControlPortClientConnect
    OnClientDisconnect = AuthoringControlPortClientDisconnect
    OnClientRead = AuthoringControlPortClientRead
    OnClientError = AuthoringControlPortClientError
    Left = 75
    Top = 32
  end
end
