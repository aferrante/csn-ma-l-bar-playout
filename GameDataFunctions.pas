// This unit contains functions related to getting game data from Sportsticker and Stats Inc.
// Original Rev: 12/21/2009
unit GameDataFunctions;

interface

uses  SysUtils,
      Globals,
      Main, //Main form
      DataModule, //Data module
      EngineIntf,
      DateUtils;

  //utility functions
  function GetLastName(FullName: String): String;
  function FormatGameClock(TimeMin: SmallInt; TimeSec: SmallInt): String;
  function GetGamePhaseRec(League: String; GPhase: SmallInt): GamePhaseRec;
  function GetGameData (League: String; GCode: String): GameRec;
  function GetFootballGameSituationData(CurrentGameData: GameRec): FootballSituationRec;
  function GetTeamMnemonicFromID (League: String; TeamID: LongInt): String;

  //Get value of symbolic name
  function GetValueOfSymbol(PlaylistID, PlaylistType: SmallInt; Symbolname: String; CurrentEntryIndex: SmallInt;
               CurrentGameData: GameRec; TickerMode: SmallInt; WeatherLocationCode: String): SymbolValueRec;

  //Function to pull weather forecast data for the home stadium
  function GetWeatherForecastData(League: String; HomeStatsID: LongInt): WeatherForecastRec;

  //Function to get weather for LBar
  function GetLBarWeatherForecastData(LocationCode: String): LBarWeatherForecastRec;

  //Function to parse team records
  function ParseTeamRecord(RecordStringIn: String): TeamRecordRec;

  //General stats functions
  function GetBoxscoreData(League: String; GameCode: String): BoxscoreRec;

  //Baseball stats functions
  function GetStartingPitcherStats(GameCode: String): StartingPitcherStatsRec;
  function GetGameFinalPitcherStats(GameCode: String): FinalPitcherStatsRec;
  function GetGameFinalBatterStats(GameCode: String; TeamID: LongInt; StatNum: SmallInt): FinalBatterStatsRec;

  //Basketball stats functions
  function GetGameFinalBBallStats(League, GameCode: String; TeamID: LongInt; StatNum: SmallInt): FinalBBallStatsRec;

  //Hockey Stats functions
  function GetGameFinalSkaterStats(GameCode: String; TeamID: LongInt; StatNum: SmallInt): FinalSkaterStatsRec;
  function GetGameFinalGoalieStats(GameCode: String; TeamID: LongInt; StatNum: SmallInt): FinalGoalieStatsRec;

  //Football stats functions - all stats returned in 1 record
  function GetGameFinalFBStats(League, GameCode: String): FinalFBStatsRec;

  //Fantasy Stats functions
  function GetFantasyFootballPassingStats: FantasyFootballPassingStatsRec;
  function GetFantasyFootballRushingStats: FantasyFootballRushingStatsRec;
  function GetFantasyFootballReceivingStats: FantasyFootballReceivingStatsRec;
  function GetFantasyBaseballPitchingStats: FantasyBaseballPitchingStatsRec;
  function GetFantasyBaseballBattingStats: FantasyBaseballBattingStatsRec;
  function GetFantasyBasketballStats(StatType: String): FantasyBasketballStatsRec;
  function GetFantasyHockeyPointsStats: FantasyHockeyPointsStatsRec;
  function GetFantasyHockeyGoalsStats: FantasyHockeyGoalsStatsRec;
  function GetFantasyHockeyAssistsStats: FantasyHockeyAssistsStatsRec;
  //Version 3.1.0
  function GetFantasyStats_General(StoredProcedureName, DataIdentifier, LeagueIdentifier, DataFieldName: String): FantasyGeneralStatsRec;
  function GetLeaderStats_General(StoredProcedureName, DataIdentifier, LeagueIdentifier, DataFieldName: String): SeasonLeadersGeneralStatsRec;

  //For L-Bar weather data
  function GetLBarWeatherIconFilepath(IconCode: String): String;

  //Added for Version 2.0
  function GetHeadshotElementPath(HeadshotPath: String): String;
  function GetCamioTeamLogoPath(League: String; StatsIncID: LongInt): string;
  function GetCamioLeagueLogoPath(League: String): string;

implementation

//Added for Version 2.0
//Take the headshot path and create the path to the companion elements required for compositing
function GetHeadshotElementPath(HeadshotPath: String): String;
var
  Cursor, CharPos: SmallInt;
  Basefilename: String;
  LeagueStr, TeamStr: String;
  OutPath: String;
begin
  //Extract the base name
  BaseFilename := ExtractFilename(StringReplace(HeadshotPath, '/', '\', [rfReplaceAll]));

  //Get the league string
  CharPos := Pos('_', BaseFilename);
  if (CharPos > 0) then LeagueStr := '_' + Copy(BaseFilename, 1, CharPos-1);

  //Remove league & underscore from base filename
  if ((Length(Basefilename)-CharPos) > 0) then
    Basefilename := Copy(BaseFilename, CharPos+1, Length(Basefilename)-CharPos);

  //Get the team string
  CharPos := Pos('_', BaseFilename);
  if (CharPos > 0) then TeamStr := Copy(BaseFilename, 1, CharPos-1);

  OutPath := StringReplace(Base_Element_Path, '\', '/', [rfReplaceAll]) + LeagueStr + '/' + TeamStr + '/' + StringReplace(End_Element_Path, '\', '/', [rfReplaceAll]) + '/';

  //Return
  GetHeadshotElementPath := OutPath;
end;

//Utility function to parse the player last name out of a full name string
function GetLastName(FullName: String): String;
var
  i: SmallInt;
  OutStr: String;
  Cursor: SmallInt;
begin
  OutStr := FullName;
  Cursor := Pos(',', FullName);
  if (Cursor > 1) then
  begin
    OutStr := '';
    for i := 1 to Cursor-1 do
      OutStr := OutStr + FullName[i];
  end;
  GetLastName := OutStr;
end;

//Function to parse team records
function ParseTeamRecord(RecordStringIn: String): TeamRecordRec;
var
  Tokens: Array[1..5] of String;
  i, Cursor, TokenIndex: SmallInt;
  ParamVal: SmallInt;
  OutRec: TeamRecordRec;
begin
  //Parse the string for the team record
  if (Length(RecordStringIn) >= 3) then
  begin
    //Parse for tokens
    for i := 1 to 4 do Tokens[i] := '';
    Cursor := 1;
    TokenIndex :=1;
    repeat
      if (RecordStringIn[Cursor] = '-') then
      begin
        Inc(Cursor);
        Inc(TokenIndex);
      end;
      Tokens[TokenIndex] := Tokens[TokenIndex] + RecordStringIn[Cursor];
      Inc(Cursor);
    until (Cursor > Length(RecordStringIn)) OR (TokenIndex = 5);
  end;
  //Set output record
  OutRec.Param[1] := Tokens[1];
  OutRec.Param[2] := Tokens[2];
  OutRec.Param[3] := Tokens[3];
  OutRec.Param[4] := Tokens[4];
  ParseTeamRecord := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
//Function to format the game clock into minutes and seconds
////////////////////////////////////////////////////////////////////////////////
function FormatGameClock(TimeMin: SmallInt; TimeSec: SmallInt): String;
var
  SecsStr: String;
  OutStr: String;
begin
  //Check for manual game as indicated by min, sec = -1
  if (TimeSec = -1) AND (TimeMin = -1) then OutStr := ''
  else begin
    if (TimeSec < 10) then SecsStr := '0' + IntToStr(TimeSec)
    else SecsStr := IntToStr(TimeSec);
    if (TimeMin > 0) then
      OutStr := IntToStr(TimeMin) + ':' + SecsStr
    else
      OutStr := ':' + SecsStr;
  end;
  FormatGameClock := OutStr;
end;

////////////////////////////////////////////////////////////////////////////////
//Function to take a league and a game phase code, and return a game phase record
////////////////////////////////////////////////////////////////////////////////
function GetGamePhaseRec(League: String; GPhase: SmallInt): GamePhaseRec;
var
  i: SmallInt;
  GamePhaseRecPtr: ^GamePhaseRec; //Pointer to game phase record type
  OutRec: GamePhaseRec;
  FoundRecord: Boolean;
begin
  FoundRecord := FALSE;
  OutRec.League := ' ';
  OutRec.ST_Phase_Code := 0;
  OutRec.SI_Phase_Code := 0;
  OutRec.Display_Period := ' ';
  OutRec.End_Is_Half := FALSE;
  OutRec.Display_Half := ' ';
  OutRec.Display_Final := ' ';
  OutRec.Display_Extended1 := ' ';
  OutRec.Display_Extended2 := ' ';
  OutRec.Game_OT := FALSE;
  if (Game_Phase_Collection.Count > 0) then
  begin
    i := 0;
    repeat
      GamePhaseRecPtr := Game_Phase_Collection.At(i);
      if (Trim(GamePhaseRecPtr^.League) = Trim(League)) AND (GamePhaseRecPtr^.SI_Phase_Code = GPhase) then
      begin
        OutRec.League := Trim(League);
        OutRec.ST_Phase_Code := GPhase;
        OutRec.SI_Phase_Code := GPhase;
        OutRec.Label_Period := Trim(GamePhaseRecPtr^.Label_Period);
        OutRec.Display_Period := Trim(GamePhaseRecPtr^.Display_Period);
        OutRec.End_Is_Half := GamePhaseRecPtr^.End_Is_Half;
        OutRec.Display_Half := GamePhaseRecPtr^.Display_Half;
        OutRec.Display_Half := Trim(GamePhaseRecPtr^.Display_Half);
        OutRec.Display_Final := Trim(GamePhaseRecPtr^.Display_Final);
        OutRec.Display_Extended1 := Trim(GamePhaseRecPtr^.Display_Extended1);
        OutRec.Display_Extended2 := Trim(GamePhaseRecPtr^.Display_Extended2);
        OutRec.Game_OT := GamePhaseRecPtr^.Game_OT;
        FoundRecord := TRUE;
      end;
      Inc(i);
    until (i = Game_Phase_Collection.Count) OR (FoundRecord = TRUE);
  end;
  GetGamePhaseRec := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
//Function to run stored procedures to get game information
////////////////////////////////////////////////////////////////////////////////
function GetGameData (League: String; GCode: String): GameRec;
var
  OutRec: GameRec;
begin
  //Check if valid league first
  if (League <> 'NONE') then
  begin
    try
      //Execute required stored procedure
      dmMain.GameDataQuery.Active := FALSE;
      dmMain.GameDataQuery.SQL.Clear;
      dmMain.GameDataQuery.SQL.Add('/* */sp_GetSI' + League + 'GameInfoCSN' + ' ' + QuotedStr(GCode));
      dmMain.GameDataQuery.Open;
      //Check for record count > 0 and process
      if (dmMain.GameDataQuery.RecordCount > 0) then
      begin
        OutRec.League := dmMain.GameDataQuery.FieldByName('League').AsString;
        //Get extra fields if it's an MLB game
        if (League = 'MLB') then
        begin
          OutRec.SubLeague := dmMain.GameDataQuery.FieldByName('SubLeague').AsString;
          OutRec.DoubleHeader := dmMain.GameDataQuery.FieldByName('DoubleHeader').AsBoolean;
          OutRec.DoubleHeaderGameNumber := dmMain.GameDataQuery.FieldByName('DoubleHeaderGameNumber').AsInteger;
          OutRec.TimeMin := 0;
          OutRec.TimeSec := 0;
          OutRec.Count := dmMain.GameDataQuery.FieldByName('Count').AsString;;
          OutRec.Situation := dmMain.GameDataQuery.FieldByName('Situation').AsString;
          OutRec.Batter := dmMain.GameDataQuery.FieldByName('Batter').AsString;
          OutRec.Baserunners[1] := dmMain.GameDataQuery.FieldByName('FirstBaserunner').AsString;
          OutRec.Baserunners[2] := dmMain.GameDataQuery.FieldByName('SecondBaserunner').AsString;
          OutRec.Baserunners[3] := dmMain.GameDataQuery.FieldByName('ThirdBaserunner').AsString;
        end
        else if (League = 'CFB') OR (League = 'CFL') OR (League = 'NFL')then
        begin
          OutRec.SubLeague := ' ';
          OutRec.DoubleHeader := FALSE;
          OutRec.DoubleHeaderGameNumber := 0;
          OutRec.TimeMin := dmMain.GameDataQuery.FieldByName('TimeMin').AsInteger;
          OutRec.TimeSec := dmMain.GameDataQuery.FieldByName('TimeSec').AsInteger;
          OutRec.Count := '000';
          OutRec.Situation := dmMain.GameDataQuery.FieldByName('Situation').AsString;;
          OutRec.Batter := '';
          OutRec.Baserunners[1] := '';
          OutRec.Baserunners[2] := '';
          OutRec.Baserunners[3] := '';
        end
        else begin
          OutRec.SubLeague := ' ';
          OutRec.DoubleHeader := FALSE;
          OutRec.DoubleHeaderGameNumber := 0;
          OutRec.TimeMin := dmMain.GameDataQuery.FieldByName('TimeMin').AsInteger;
          OutRec.TimeSec := dmMain.GameDataQuery.FieldByName('TimeSec').AsInteger;
          OutRec.Count := '000';
          OutRec.Situation := 'FFF';
          OutRec.Batter := '';
          OutRec.Baserunners[1] := '';
          OutRec.Baserunners[2] := '';
          OutRec.Baserunners[3] := '';
        end;
        //Set team strength variables for NHL
        if (League = 'NHL') then
        begin
          if (ANSIUpperCase(dmMain.GameDataQuery.FieldByName('VStrength').AsString) = 'Powerplay') then
            OutRec.Visitor_Param1 := 1
          else
            OutRec.Visitor_Param1 := 0;
          if (ANSIUpperCase(dmMain.GameDataQuery.FieldByName('HStrength').AsString) = 'Powerplay') then
            OutRec.Home_Param1 := 1
          else
            OutRec.Home_Param1 := 0;
          OutRec.Visitor_Param2 := 0;
          OutRec.Home_Param2 := 0;
        end
        //Set hits & errors for MLB linescore
        else if (League = 'MLB') then
        begin
          OutRec.Visitor_Param1 := StrToIntDef(dmMain.GameDataQuery.FieldByName('VHits').AsString, 0);
          OutRec.Visitor_Param2 := StrToIntDef(dmMain.GameDataQuery.FieldByName('VErrors').AsString, 0);
          OutRec.Home_Param1 := StrToIntDef(dmMain.GameDataQuery.FieldByName('HHits').AsString, 0);
          OutRec.Home_Param2 := StrToIntDef(dmMain.GameDataQuery.FieldByName('HErrors').AsString, 0);
        end
        //Set to defaults for all other sports
        else begin
          OutRec.Visitor_Param1 := 0;
          OutRec.Visitor_Param2 := 0;
          OutRec.Home_Param1 := 0;
          OutRec.Home_Param2 := 0;
        end;

        //If college, get ranks
        if (League = 'NCAAB') OR (League = 'CBK') then
        begin
          if (UseManualRankings_NCAAB) then
          begin
            OutRec.VRank := dmMain.GameDataQuery.FieldByName('VManualRank').AsString;
            if (Trim(OutRec.VRank) = '0') then OutRec.VRank := '';
            OutRec.HRank := dmMain.GameDataQuery.FieldByName('HManualRank').AsString;
            if (Trim(OutRec.HRank) = '0') then OutRec.HRank := '';
          end
          else begin
            //For NCAA Tournament
            if (UseSeedingForNCAABGames) then
            begin
              OutRec.VRank := dmMain.GameDataQuery.FieldByName('VSeed').AsString;
              if (Trim(OutRec.VRank) = '0') then OutRec.VRank := '';
              OutRec.HRank := dmMain.GameDataQuery.FieldByName('HSeed').AsString;
              if (Trim(OutRec.HRank) = '0') then OutRec.HRank := '';
            end
            else begin
              OutRec.VRank := dmMain.GameDataQuery.FieldByName('VRank').AsString;
              if (Trim(OutRec.VRank) = '0') then OutRec.VRank := '';
              OutRec.HRank := dmMain.GameDataQuery.FieldByName('HRank').AsString;
              if (Trim(OutRec.HRank) = '0') then OutRec.HRank := '';
            end;
          end;
        end
        else if (League = 'NCAAF') OR (League = 'CFB') then
        begin
          if (UseManualRankings_NCAAF) then
          begin
            OutRec.VRank := dmMain.GameDataQuery.FieldByName('VManualRank').AsString;
            if (Trim(OutRec.VRank) = '0') then OutRec.VRank := '';
            OutRec.HRank := dmMain.GameDataQuery.FieldByName('HManualRank').AsString;
            if (Trim(OutRec.HRank) = '0') then OutRec.HRank := '';
          end
          else begin
            OutRec.VRank := dmMain.GameDataQuery.FieldByName('VRank').AsString;
            if (Trim(OutRec.VRank) = '0') then OutRec.VRank := '';
            OutRec.HRank := dmMain.GameDataQuery.FieldByName('HRank').AsString;
            if (Trim(OutRec.HRank) = '0') then OutRec.HRank := '';
          end;
        end
        else if (League = 'NCAAW') OR (League = 'WCBK') then
        begin
          if (UseManualRankings_NCAAW) then
          begin
            OutRec.VRank := dmMain.GameDataQuery.FieldByName('VManualRank').AsString;
            if (Trim(OutRec.VRank) = '0') then OutRec.VRank := '';
            OutRec.HRank := dmMain.GameDataQuery.FieldByName('HManualRank').AsString;
            if (Trim(OutRec.HRank) = '0') then OutRec.HRank := '';
          end
          else begin
            OutRec.VRank := dmMain.GameDataQuery.FieldByName('VRank').AsString;
            if (Trim(OutRec.VRank) = '0') then OutRec.VRank := '';
            OutRec.HRank := dmMain.GameDataQuery.FieldByName('HRank').AsString;
            if (Trim(OutRec.HRank) = '0') then OutRec.HRank := '';
          end;
        end
        else begin
          OutRec.VRank := ' ';
          OutRec.HRank := ' ';
        end;

        //Set remaining parameters
        OutRec.League := League;
        OutRec.GameID := GCode;
        OutRec.HStatsIncID := dmMain.GameDataQuery.FieldByName('HId').AsInteger;
        OutRec.VStatsIncID := dmMain.GameDataQuery.FieldByName('VId').AsInteger;
        OutRec.HName := dmMain.GameDataQuery.FieldByName('HName').AsString;
        OutRec.VName := dmMain.GameDataQuery.FieldByName('VName').AsString;
        OutRec.HMnemonic := dmMain.GameDataQuery.FieldByName('HMnemonic').AsString;
        OutRec.VMnemonic := dmMain.GameDataQuery.FieldByName('VMnemonic').AsString;
        OutRec.HScore := dmMain.GameDataQuery.FieldByName('HScore').AsInteger;
        OutRec.VScore := dmMain.GameDataQuery.FieldByName('VScore').AsInteger;
        OutRec.HRecord := dmMain.GameDataQuery.FieldByName('HRecord').AsString;
        OutRec.VRecord := dmMain.GameDataQuery.FieldByName('VRecord').AsString;
        OutRec.HStreak := dmMain.GameDataQuery.FieldByName('HStreak').AsString;
        OutRec.VStreak := dmMain.GameDataQuery.FieldByName('VStreak').AsString;
        OutRec.Phase := dmMain.GameDataQuery.FieldByName('Phase').AsInteger;
        //Set the game state
        if (Trim(dmMain.GameDataQuery.FieldByName('State').AsString) = 'Pre-Game') then
          OutRec.State := 1
        else if (Trim(dmMain.GameDataQuery.FieldByName('State').AsString) = 'In-Progress') then
          OutRec.State := 2
        else if (Trim(dmMain.GameDataQuery.FieldByName('State').AsString) = 'Final') then
          OutRec.State := 3
        else if (Trim(dmMain.GameDataQuery.FieldByName('State').AsString) = 'Postponed') then
          OutRec.State := 4
        else if (Trim(dmMain.GameDataQuery.FieldByName('State').AsString) = 'Delayed') then
          OutRec.State := 5
        else if (Trim(dmMain.GameDataQuery.FieldByName('State').AsString) = 'Suspended') then
          OutRec.State := 6
        else if (Trim(dmMain.GameDataQuery.FieldByName('State').AsString) = 'Cancelled') then
          OutRec.State := 7
        else
          OutRec.State := -1;
        //Set state for in-progress check
        OutRec.StateForInProgressCheck := OutRec.State;  
        OutRec.Description := OutRec.League + ': ' + OutRec.VName + ' @ ' + OutRec.HName + '  (' +
          DateToStr(dmMain.GameDataQuery.FieldByName('Date').AsDateTime) + ')';
        OutRec.Date := dmMain.GameDataQuery.FieldByName('Date').AsDateTime;
        OutRec.StartTime := dmMain.GameDataQuery.FieldByName('StartTime').AsDateTime;
        OutRec.DataFound := TRUE;
      end
      else OutRec.DataFound := FALSE;
    except
      if (ErrorLoggingEnabled = True) then
        EngineInterface.WriteToErrorLog('Error occurred in GetGameData function for full game data');
      //Clear data found flag
      OutRec.DataFound := FALSE;
    end;
  end;
  //Return
  GetGameData := OutRec;
end;

function GetFootballGameSituationData(CurrentGameData: GameRec): FootballSituationRec;
var
  i: SmallInt;
  OutRec: FootballSituationRec;
  VTeamID, HTeamID, PossessionTeamID: Double;
  PossessionYardsFromGoal: SmallInt;
  Down, Distance: SmallInt;
  TeamRecPtr: ^TeamRec;
  StrCursor, SubStrCounter: SmallInt;
  Substrings: Array[1..4] of String;
begin
  //Init
  HTeamID := 0;
  VTeamID := 0;
  StrCursor := 1;
  SubStrCounter := 1;
  for i := 1 to 4 do Substrings[i] := '';

  if (Length(CurrentGameData.Situation) > 0) AND (CurrentGameData.Situation <> ' - - - ') then
  begin
    //Parse out situation string to components
    While (StrCursor <= Length(CurrentGameData.Situation)) do
    begin
      //Get substrings for data fields
      While (CurrentGameData.Situation[StrCursor] <> '-') and (StrCursor <= Length(CurrentGameData.Situation)) do
      begin
        SubStrings[SubStrCounter] := SubStrings[SubStrCounter] + CurrentGameData.Situation[StrCursor];
        Inc(StrCursor);
      end;
      //Go to next field
      Inc(StrCursor);
      Inc(SubStrCounter);
    end;

    //Set values
    //Do ID of team with possession
    if (Trim(SubStrings[1]) <> '') then
    begin
      try
        PossessionTeamID := Trunc(StrToFloat(Substrings[1]));
      except
        PossessionTeamID := 0;
        EngineInterface.WriteToErrorLog('Error occurred while trying to convert team ID for football possession');
      end;
    end
    else PossessionTeamID := 0;

    //Get yards from goal
    if (Trim(SubStrings[2]) <> '') then
    begin
      try
        PossessionYardsFromGoal := StrToInt(Substrings[2]);
      except
        PossessionYardsFromGoal := 0;
        EngineInterface.WriteToErrorLog('Error occurred while trying to convert field position for football possession');
      end;
    end
    else PossessionYardsFromGoal := 0;

    //Get Down
    if (Trim(SubStrings[3]) <> '') then
    begin
      try
        Down := StrToInt(Substrings[3]);
      except
        Down := 0;
        EngineInterface.WriteToErrorLog('Error occurred while trying to down for football possession');
      end;
    end
    else Down := 0;

    //Get Distance
    if (Trim(SubStrings[4]) <> '') then
    begin
      try
        Distance := StrToInt(Substrings[4]);
      except
        Distance := 0;
        EngineInterface.WriteToErrorLog('Error occurred while trying to down for football possession');
      end;
    end
    else Distance := 0;

    //Get team IDs of visiting & home teams
    if (Team_Collection.Count > 0) then
    begin
      for i := 0 to Team_Collection.Count-1 do
      begin
        TeamRecPtr := Team_Collection.At(i);
        if (CurrentGameData.VMnemonic = TeamRecPtr^.DisplayName1) AND
           (CurrentGameData.League = TeamRecPtr^.League) then
          VTeamID := TeamRecPtr^.StatsIncId;
      end;
      for i := 0 to Team_Collection.Count-1 do
      begin
        TeamRecPtr := Team_Collection.At(i);
        if (CurrentGameData.HMnemonic = TeamRecPtr^.DisplayName1) AND
           (CurrentGameData.League = TeamRecPtr^.League) then
          HTeamID := TeamRecPtr^.StatsIncId;
      end;
    end;
    //Check to find team with possession
    if (VTeamID = PossessionTeamID) then
    begin
      OutRec.VisitorHasPossession := TRUE;
      OutRec.HomeHasPossession := FALSE;
    end
    else if (HTeamID = PossessionTeamID) then
    begin
      OutRec.VisitorHasPossession := FALSE;
      OutRec.HomeHasPossession := TRUE;
    end
    else begin
      OutRec.VisitorHasPossession := FALSE;
      OutRec.HomeHasPossession := FALSE;
    end;
    //Set remaining values
    OutRec.YardsFromGoal := PossessionYardsFromGoal;
    OutRec.Down := Down;
    OutRec.Distance := Distance;
  end
  else begin
    OutRec.VisitorHasPossession := FALSE;
    OutRec.HomeHasPossession := FALSE;
    OutRec.YardsFromGoal := -1;
    OutRec.Down := 0;
    OutRec.Distance := 0;
  end;
  //Set return value
  GetFootballGameSituationData := OutRec;
end;

//Function to get team mnemonic from team ID and league - used for Fantasy Stats
function GetTeamMnemonicFromID (League: String; TeamID: LongInt): String;
var
  i: SmallInt;
  TeamRecPtr: ^TeamRec;
  OutStr: String;
  FoundMatch: Boolean;
begin
  //Init
  i := 0;
  FoundMatch := FALSE;
  OutStr := '';
  //Look for match
  if (Team_Collection.Count > 0) then
  repeat
    TeamRecPtr := Team_Collection.At(i);
    if (((TeamRecPtr^.League = 'MLB') AND ((League = 'AL') OR (League = 'NL'))) OR (TeamRecPtr^.League = League)) AND
       (TeamRecPtr^.StatsIncID = TeamID) then
    begin
      OutStr := TeamRecPtr^.DisplayName1;
      FoundMatch := TRUE;
    end;
    inc(i);
  until (i = Team_Collection.Count) OR (FoundMatch);
  GetTeamMnemonicFromID := OutStr;
end;

//Function to take the integer for down (down & distance) & return with suffix
function IntToDown(Down: SmallInt): String;
var
  OutStr: String;
begin
  Case Down of
   1: OutStr := '1st';
   2: OutStr := '2nd';
   3: OutStr := '3rd';
   4: OutStr := '4th';
  end;
  IntToDown := OutStr; 
end;

////////////////////////////////////////////////////////////////////////////////
// Function to take a symbolic name and return the value of the string to be
// sent to the engine
////////////////////////////////////////////////////////////////////////////////
function GetValueOfSymbol(PlaylistID, PlaylistType: SmallInt; Symbolname: String;
                          CurrentEntryIndex: SmallInt; CurrentGameData: GameRec;
                          TickerMode: SmallInt; WeatherLocationCode: String): SymbolValueRec;
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  OutStr, OutStr2, OutStr3: String;
  TextFieldBias: SmallInt;
  GamePhaseData: GamePhaseRec;
  BaseName: String;
  CharPos: SmallInt;
  Suffix: String;
  SwitchPosMixed, SwitchPosUpper: SmallInt;
  SwitchPosStartBlack, SwitchPosEndBlack: SmallInt;
  TempStr: String;
  Days: array[1..7] of string;
  TeamRecord: TeamRecordRec;
  StartingPitcherStats: StartingPitcherStatsRec;
  FinalPitcherStats: FinalPitcherStatsRec;
  FinalBatterStats: FinalBatterStatsRec;
  FinalBBallStats: FinalBBallStatsRec;
  FinalSkaterStats: FinalSkaterStatsRec;
  FinalGoalieStats: FinalGoalieStatsRec;
  FootballSituation: FootballSituationRec;
//  FinalFBStats: FinalFBStatsRec;
  OKToDisplay: Boolean;
  LineWrap: Boolean;
  Index: SmallInt;
  TeamNameMnemonicSuffix: String;
  PlayerNameSuffix: String;
  TempFantasyGeneralStatsRec: FantasyGeneralStatsRec;
  TempSeasonLeadersGeneralStatsRec: SeasonLeadersGeneralStatsRec;
begin
  //Init
  OutStr := SymbolName;
  BaseName := '';
  Suffix := '';
  LongTimeFormat := 'h:mm AM/PM';
  OKToDisplay := TRUE;
  LineWrap := FALSE;

  TextFieldBias := 0;

  //Set suffixes based on CSN or SNY formatting
  if (FormatForSNY) then
  begin
    TeamNameMnemonicSuffix := '  ';
    PlayerNameSuffix := ': ';
    ABBV_Rushes := 'Rush';
    ABBV_Receptions := 'Rec';
    ABBV_Yards := 'Yds';
    ABBV_TDs := 'TD';
    ABBV_INTs := 'Int';
    ABBV_Points := 'Pts';
    ABBV_Rebounds := 'Reb';
    ABBV_Assists := 'Ast';
    ABBV_Steals := 'Stl';
    ABBV_Blocks := 'Blk';
    ABBV_PPG := 'PPG';
    ABBV_RPG := 'RPG';
    ABBV_APG := 'APG';
    ABBV_SPG := 'SPG';
    ABBV_BPG := 'BPG';
    ABBV_Goals := 'Goal';
    ABBV_Saves := 'Save';
  end
  else begin
    TeamNameMnemonicSuffix := ': ';
    PlayerNameSuffix := ' ';
    ABBV_Rushes := 'RUSH';
    ABBV_Receptions := 'REC';
    ABBV_Yards := 'YDS';
    ABBV_TDs := 'TD';
    ABBV_INTs := 'INT';
    ABBV_Points := 'PTS';
    ABBV_Rebounds := 'REB';
    ABBV_Assists := 'AST';
    ABBV_Steals := 'STL';
    ABBV_Blocks := 'BLK';
    ABBV_PPG := 'PPG';
    ABBV_RPG := 'RPG';
    ABBV_APG := 'APG';
    ABBV_SPG := 'SPG';
    ABBV_BPG := 'BPG';
    ABBV_Goals := 'GOAL';
    ABBV_Saves := 'SAVE';
  end;

  //Get info on current record
  if (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
   Case PlaylistID of
     PLAYLIST_1: TickerRecPtr := Ticker_Collection.At(CurrentEntryIndex);
     PLAYLIST_2: TickerRecPtr := Ticker_Collection2.At(CurrentEntryIndex);
   end;
  end;

  //Extract out base symbol name and any suffix
  CharPos := Pos('|', SymbolName);
  if (CharPos > 0) then
  begin
    //Get base name
    if (CharPos > 1) then
      for i := 1 to CharPos-1 do BaseName := BaseName + SymbolName[i];
    //Get suffix
    if (Length(Symbolname) > CharPos) then
      for i := CharPos+1 to Length(Symbolname) do Suffix := Suffix + SymbolName[i];
    //Set new base name
    SymbolName := BaseName;
  end;

  //Blank
  if (SymbolName = '$Blank') then OutStr := ' '
  //Sponsor logo
  else if (SymbolName = '$Sponsor_Logo') then
  begin
    if (CurrentEntryIndex <> NOENTRYINDEX) then
    begin
      Case  PlaylistType of
        TICKER: OutStr := Sponsor_Logo_Base_Path + '\' + TickerRecPtr^.SponsorLogo_Name;
      end;
      //Format for Channel Box
      OutStr := StringReplace(OutStr, '\', '/', [rfReplaceAll]);
    end
    else OutStr := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
  end
  //League
  else if (SymbolName = '$League') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      //TICKER: OutStr := UpperCase(TickerRecPtr^.Subleague_Mnemonic_Standard);
      TICKER: OutStr := UpperCase(TickerRecPtr^.League);
    end;
  end

  //User-defined text fields
  else if (SymbolName = '$Text_1') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[1+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_2') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[2+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_3') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[3+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_4') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[4+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_5') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[5+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_6') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[6+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_7') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[7+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_8') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[8+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_9') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[9+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_10') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[10];
    end;
  end
  else if (SymbolName = '$Text_11') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[11];
    end;
  end
  else if (SymbolName = '$Text_12') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[12];
    end;
  end
  else if (SymbolName = '$Text_13') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[13];
    end;
  end
  else if (SymbolName = '$Text_14') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[14];
    end;
  end
  else if (SymbolName = '$Text_15') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[15+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_16') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[16+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_17') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[17+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_18') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[18+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_19') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[19+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_20') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[20+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_21') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[21+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_22') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[22+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_23') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[23+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_24') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[24+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_25') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[25+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_26') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[26+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_27') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[27+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_28') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[28+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_29') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[29+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_30') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[30+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_31') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[31+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_32') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[32+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_33') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[33+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_34') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[34+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_35') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[35+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_36') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[36+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_37') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[37+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_38') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[38+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_39') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[39+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_40') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[40+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_41') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[41+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_42') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[42+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_43') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[43+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_44') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[44+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_45') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[45+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_46') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[46+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_47') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[47+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_48') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[48+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_49') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[49+TextFieldBias];
    end;
  end
  else if (SymbolName = '$Text_50') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
    Case PlaylistType of
      TICKER: OutStr := TickerRecPtr^.UserData[50+TextFieldBias];
    end;
  end

  //Includes AM/PM + timezone suffix
  else if (SymbolName = '$Start_Time') then
  begin
    //Check for doubleheader in baseball
    if (CurrentGameData.StartTime - Now > 1) then
    begin
      Days[1] := 'Sun';
      Days[2] := 'Mon';
      Days[3] := 'Tues';
      Days[4] := 'Wed';
      Days[5] := 'Thur';
      Days[6] := 'Fri';
      Days[7] := 'Sat';
      //Option to show day of week AND start time
      if (ShowTimeAndDay) then
      begin
        OutStr := Days[DayOfWeek(CurrentGameData.Date)];
        //Check for doubleheader in baseball, game #2
        if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
          OutStr := OutStr + ' GM 2'
        //Start time offset added for Version 1.0.6  03/31/08
        else OutStr := OutStr + ' ' + TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset) + ' ' + TimeZoneSuffix;
        if (FormatForSNY) then
        begin
          OutStr := StringReplace(OutStr, ' AM', 'am', []);
          OutStr := StringReplace(OutStr, ' PM', 'pm', []);
        end;
      end
      else begin
        OutStr := Days[DayOfWeek(CurrentGameData.Date)];
      end;
    end
    else begin
      //Check for doubleheader in baseball, game #2
      if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
        OutStr := 'GM 2'
      //Start time offset added for Version 1.0.6  03/31/08
      else OutStr := TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset) + ' ' + TimeZoneSuffix;
      if (FormatForSNY) then
      begin
        OutStr := StringReplace(OutStr, ' AM', 'am', []);
        OutStr := StringReplace(OutStr, ' PM', 'pm', []);
      end;
    end;
    OutStr := Trim(OutStr);
  end

  //Remove timezone only (includes AM/PM)
  else if (SymbolName = '$Start_Time_No_Timezone') then
  begin
    //Check for doubleheader in baseball
    if (CurrentGameData.StartTime - Now > 1) then
    begin
      Days[1] := 'Sun';
      Days[2] := 'Mon';
      Days[3] := 'Tues';
      Days[4] := 'Wed';
      Days[5] := 'Thur';
      Days[6] := 'Fri';
      Days[7] := 'Sat';
      //Option to show day of week AND start time
      if (ShowTimeAndDay) then
      begin
        OutStr := Days[DayOfWeek(CurrentGameData.Date)];
        //Check for doubleheader in baseball, game #2
        if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
          OutStr := OutStr + ' ' + 'GM 2'
        //Start time offset added for Version 1.0.6  03/31/08
        else OutStr := OutStr + ' ' + TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset);
        if (FormatForSNY) then
        begin
          OutStr := StringReplace(OutStr, ' AM', 'am', []);
          OutStr := StringReplace(OutStr, ' PM', 'pm', []);
        end;
      end
      else begin
        OutStr := Days[DayOfWeek(CurrentGameData.Date)];
      end;
    end
    else begin
      if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
        OutStr := 'GM 2'
      //Start time offset added for Version 1.0.6  03/31/08
      else OutStr := TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset);
      if (FormatForSNY) then
      begin
        OutStr := StringReplace(OutStr, ' AM', 'am', []);
        OutStr := StringReplace(OutStr, ' PM', 'pm', []);
      end;
    end;
    OutStr := Trim(OutStr);
  end

  //Remove AM/PM suffix
  else if (SymbolName = '$Start_Time_No_Suffix') then
  begin
    //Check for doubleheader in baseball
    if (CurrentGameData.StartTime - Now > 1) then
    begin
      Days[1] := 'Sun';
      Days[2] := 'Mon';
      Days[3] := 'Tues';
      Days[4] := 'Wed';
      Days[5] := 'Thur';
      Days[6] := 'Fri';
      Days[7] := 'Sat';
      //Option to show day of week AND start time
      if (ShowTimeAndDay) then
      begin
        OutStr := Days[DayOfWeek(CurrentGameData.Date)];
        //Check for doubleheader in baseball, game #2
        if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
          OutStr := OutStr + ' GM 2'
        //Start time offset added for Version 1.0.6  03/31/08
        else begin
          OutStr := OutStr + ' ' + TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset) + ' ' + TimeZoneSuffix;;
          OutStr := StringReplace(OutStr, 'AM', '', [rfReplaceAll]);
          OutStr := StringReplace(OutStr, 'PM', '', [rfReplaceAll]);
        end;
      end
      else begin
        OutStr := Days[DayOfWeek(CurrentGameData.Date)];
      end;
    end
    else begin
      if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
        OutStr := 'GM 2'
      else begin
        OutStr := TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset) + ' ' + TimeZoneSuffix;;
        OutStr := StringReplace(OutStr, 'AM', '', [rfReplaceAll]);
        OutStr := StringReplace(OutStr, 'PM', '', [rfReplaceAll]);
      end;
    end;
    OutStr := Trim(OutStr);
  end

  //No AM/PM or timezone suffixes
  else if (SymbolName = '$Start_Time_No_Suffix_No_Timezone') then
  begin
    //Check for doubleheader in baseball
    if (CurrentGameData.StartTime - Now > 1) then
    begin
      Days[1] := 'Sun';
      Days[2] := 'Mon';
      Days[3] := 'Tues';
      Days[4] := 'Wed';
      Days[5] := 'Thur';
      Days[6] := 'Fri';
      Days[7] := 'Sat';
      //Option to show day of week AND start time
      if (ShowTimeAndDay) then
      begin
        OutStr := Days[DayOfWeek(CurrentGameData.Date)];
        //Check for doubleheader in baseball, game #2
        if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
          OutStr := OutStr + ' GM 2'
        //Start time offset added for Version 1.0.6  03/31/08
        else begin
          OutStr := OutStr + ' ' + TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset);
          OutStr := StringReplace(OutStr, 'AM', '', [rfReplaceAll]);
          OutStr := StringReplace(OutStr, 'PM', '', [rfReplaceAll]);
        end;
      end
      else begin
        OutStr := Days[DayOfWeek(CurrentGameData.Date)];
      end;
    end
    else begin
      if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
        OutStr := 'GM 2'
      else begin
        OutStr := TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset);
        OutStr := StringReplace(OutStr, 'AM', '', [rfReplaceAll]);
        OutStr := StringReplace(OutStr, 'PM', '', [rfReplaceAll]);
      end;
    end;
    OutStr := Trim(OutStr);
  end

  //Timezone suffix only
  else if (SymbolName = '$Timezone') then
  begin
    //Check for doubleheader in baseball
    if (CurrentGameData.StartTime - Now > 1) then
    begin
      OutStr := '';
    end
    else begin
      if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
        OutStr := ''
      else OutStr := TimeZoneSuffix;
    end;
    OutStr := Trim(OutStr);
  end

  //AM/PM suffix only
  else if (SymbolName = '$AMPM_StartTime_Suffix') then
  begin
    //Check for doubleheader in baseball
    if (CurrentGameData.StartTime - Now > 1) then
      OutStr := ''
    else begin
      if (CurrentGameData.DoubleHeader = TRUE) AND (CurrentGameData.DoubleHeaderGameNumber = 2) then
        OutStr := ''
      else begin
        if (Pos('AM', TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset)) > 0) then OutStr := 'AM'
        else if (Pos('PM', TimeToStr(CurrentGameData.StartTime + GameStartTimeOffset)) > 0) then OutStr := 'PM'
        else OutStr := '';
      end;
    end;
    OutStr := Trim(OutStr);
    if (FormatForSNY) then
    begin
      OutStr := StringReplace(OutStr, 'AM', 'am', []);
      OutStr := StringReplace(OutStr, 'PM', 'pm', []);
    end;
  end

  //Team ranks - will only be applicable for NCAAB & NCAAF
  else if (SymbolName = '$Visitor_Rank') then OutStr := CurrentGameData.VRank
  else if (SymbolName = '$Home_Rank') then OutStr := CurrentGameData.HRank

  //Standard team name
  else if (SymbolName = '$Visitor_Name') then
  begin
    OutStr := UpperCase(CurrentGameData.VName);
    //Set to gold color if game is final
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := '[f 2][c 19]' + OutStr
    //If NFL or NCAAF append possession indicator
    else if ((CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'NCAAF')) AND
      (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) then
    begin
      if (GetFootballGameSituationData(CurrentGameData).VisitorHasPossession = TRUE) then
        OutStr := OutStr + FOOTBALLPOSSESSION;
    end;
  end

  //Visiting team name with leading indicator
  else if (SymbolName = '$Visitor_Name_Leader_Indicator') then
  begin
    OutStr := UpperCase(CurrentGameData.VName);
    //Set to gold color if game is final; if in progress and team leading, add indicator
    if (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := LEADERINDICATOR + OutStr
    else if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := '[f 2][c 19]' + OutStr;
  end

  //Visiting team leading indicator
  else if (SymbolName = '$Visitor_Leader_Indicator') then
  begin
    //If in progress and team leading, set indicator
    if (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := LEADERINDICATOR
    else OutStr := '';
  end

  else if (SymbolName = '$Home_Name') then
  begin
    OutStr := UpperCase(CurrentGameData.HName);
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := '[f 2][c 19]' + OutStr
    //If NFL or NCAAF append possession indicator
    else if ((CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'NCAAF')) AND
      (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0)then
    begin
      if (GetFootballGameSituationData(CurrentGameData).HomeHasPossession = TRUE) then
        OutStr := OutStr + FOOTBALLPOSSESSION;
    end;
  end

  //Home team name with leading indicator
  else if (SymbolName = '$Home_Name_Leader_Indicator') then
  begin
    OutStr := UpperCase(CurrentGameData.HName);
    //Set to gold color if game is final; if in progress and team leading, add indicator
    if (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := LEADERINDICATOR + OutStr
    else if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := '[f 2][c 19]' + OutStr;
  end

  //Home team leading indicator
  else if (SymbolName = '$Home_Leader_Indicator') then
  begin
    //If in progress and team leading, set indicator
    if (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := LEADERINDICATOR
    else OutStr := '';
  end

  //Visitor mnemonic
  else if (SymbolName = '$Visitor_Mnemonic') then
  begin
    OutStr := UpperCase(CurrentGameData.VMnemonic);
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := '[f 2][c 19]' + OutStr
    //If NFL or NCAAF append possession indicator
    else if ((CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'NCAAF')) AND
      (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) then
    begin
      if (GetFootballGameSituationData(CurrentGameData).VisitorHasPossession = TRUE) then
        OutStr := OutStr + FOOTBALLPOSSESSION;
    end;
  end

  //Visitor mnemonic with leader indicator
  else if (SymbolName = '$Visitor_Mnemonic_Leader_Indicator') then
  begin
    OutStr := UpperCase(CurrentGameData.VMnemonic);
    //Set to gold color if game is final; if in progress and team leading, add indicator
    if (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := LEADERINDICATOR + OutStr
    else if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := '[f 2][c 19]' + OutStr;
  end

  //Home mnemonic
  else if (SymbolName = '$Home_Mnemonic') then
  begin
    OutStr := UpperCase(CurrentGameData.HMnemonic);
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := '[f 2][c 19]' + OutStr
    //If NFL or NCAAF append possession indicator
    else if ((CurrentGameData.League = 'NFL') OR (CurrentGameData.League = 'NCAAF')) AND
      (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) then
    begin
      if (GetFootballGameSituationData(CurrentGameData).HomeHasPossession = TRUE) then
        OutStr := OutStr + FOOTBALLPOSSESSION;
    end;
  end

  //Home mnemonic with leader indicator
  else if (SymbolName = '$Home_Mnemonic_Leader_Indicator') then
  begin
    OutStr := UpperCase(CurrentGameData.HMnemonic);
    //Set to gold color if game is final; if in progress and team leading, add indicator
    if (CurrentGameData.State = INPROGRESS) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := LEADERINDICATOR + OutStr
    else if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := '[f 2][c 19]' + OutStr;
  end

  else if (SymbolName = '$Matchup_TeamNames') then
  begin
    OutStr := UpperCase(CurrentGameData.VName) + '  at  '+ UpperCase(CurrentGameData.HName);
  end
  else if (SymbolName = '$Matchup_TeamNames_Ranked') then
  begin
    OutStr := '[f 3][c 20]' + Trim(CurrentGameData.VRank) + '[f 2][c 1] ' + UpperCase(CurrentGameData.VName) + '  at  '+
      '[f 3][c 20]' + Trim(CurrentGameData.HRank) + '[f 2][c 1] ' + UpperCase(CurrentGameData.HName);
  end
  else if (SymbolName = '$Matchup_TeamMnemonics') then
  begin
    OutStr := UpperCase(CurrentGameData.VMnemonic) + '  at  '+ UpperCase(CurrentGameData.HMnemonic);
  end
  else if (SymbolName = '$Matchup_TeamMnemonics_Ranked') then
  begin
    OutStr := '[f 3][c 20]' + Trim(CurrentGameData.VRank) + '[f 2][c 1] ' + UpperCase(CurrentGameData.VMnemonic) + '  at  '+
      '[f 3][c 20]' + Trim(CurrentGameData.HRank) + '[f 2][c 1] ' + UpperCase(CurrentGameData.HMnemonic);
  end

  //Added for Version 2.0
  //Standard team name + score
  else if (SymbolName = '$Visitor_Name_Score') then
  begin
    OutStr := UpperCase(CurrentGameData.VName);
    if (CurrentGameData.Phase > 0) or ((CurrentGameData.League = 'MLB') and (CurrentGameData.Phase >= 0)) then
      OutStr := OutStr + '  ' + IntToStr(CurrentGameData.VScore);
  end
  //Standard team name + score
  else if (SymbolName = '$Home_Name_Score') then
  begin
    OutStr := UpperCase(CurrentGameData.HName);
    if (CurrentGameData.Phase > 0) or ((CurrentGameData.League = 'MLB') and (CurrentGameData.Phase >= 0)) then
      OutStr := OutStr + '  ' + IntToStr(CurrentGameData.HScore);
  end
  else if (SymbolName = '$Visitor_Mnemonic_Score') then
  begin
    OutStr := UpperCase(CurrentGameData.VMnemonic);
    if (CurrentGameData.Phase > 0) or ((CurrentGameData.League = 'MLB') and (CurrentGameData.Phase >= 0)) then
      OutStr := OutStr + '  ' + IntToStr(CurrentGameData.VScore);
  end
  //Standard team name + score
  else if (SymbolName = '$Home_Mnemonic_Score') then
  begin
    OutStr := UpperCase(CurrentGameData.HMnemonic);
    if (CurrentGameData.Phase > 0) or ((CurrentGameData.League = 'MLB') and (CurrentGameData.Phase >= 0)) then
      OutStr := OutStr + '  ' + IntToStr(CurrentGameData.HScore);
  end
  
  //Scores
  else if (SymbolName = '$Visitor_Score') then
  begin
    if (CurrentGameData.Phase > 0) or ((CurrentGameData.League = 'MLB') and (CurrentGameData.Phase >= 0)) then
      OutStr := IntToStr(CurrentGameData.VScore)
    else OutStr := '';
  end
  else if (SymbolName = '$Visitor_Score_Highlighted_Final') then
  begin
    if (CurrentGameData.Phase > 0) or ((CurrentGameData.League = 'MLB') and (CurrentGameData.Phase >= 0)) then
    begin
      OutStr := IntToStr(CurrentGameData.VScore);
      if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
        OutStr := '[f 2][c 19]' + OutStr;
    end
    else OutStr := '';
  end
  else if (SymbolName = '$Home_Score') then
  begin
    if (CurrentGameData.Phase > 0) or ((CurrentGameData.League = 'MLB') and (CurrentGameData.Phase >= 0)) then
      OutStr := IntToStr(CurrentGameData.HScore)
    else OutStr := '';
  end
  else if (SymbolName = '$Home_Score_Highlighted_Final') then
  begin
    if (CurrentGameData.Phase > 0) or ((CurrentGameData.League = 'MLB') and (CurrentGameData.Phase >= 0)) then
    begin
      OutStr := IntToStr(CurrentGameData.HScore);
      if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
        OutStr := '[f 2][c 19]' + OutStr;
    end
    else OutStr := '';
  end

  //Baseball linescore
  else if (SymbolName = '$MLB_Visitor_Hits') then
    OutStr := IntToStr(CurrentGameData.Visitor_Param1)
  else if (SymbolName = '$MLB_Visitor_Errors') then
    OutStr := IntToStr(CurrentGameData.Visitor_Param2)
  else if (SymbolName = '$MLB_Home_Hits') then
    OutStr := IntToStr(CurrentGameData.Home_Param1)
  else if (SymbolName = '$MLB_Home_Errors') then
    OutStr := IntToStr(CurrentGameData.Home_Param2)

  //Baseball baserunner situation
  else if (SymbolName = '$MLB_Baserunner_Situation') then
  begin
    if (Trim(CurrentGameData.Situation) = 'FFF') then OutStr := #240
    else if (Trim(CurrentGameData.Situation) = 'TFF') then OutStr := #241
    else if (Trim(CurrentGameData.Situation) = 'FTF') then OutStr := #242
    else if (Trim(CurrentGameData.Situation) = 'TTF') then OutStr := #243
    else if (Trim(CurrentGameData.Situation) = 'FFT') then OutStr := #244
    else if (Trim(CurrentGameData.Situation) = 'TFT') then OutStr := #245
    else if (Trim(CurrentGameData.Situation) = 'FTT') then OutStr := #246
    else if (Trim(CurrentGameData.Situation) = 'TTT') then OutStr := #247;
  end

  //MLB Team and # outs
  else if (SymbolName = '$MLB_Outs') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    if (Length(CurrentGameData.Count) >= 3) then
    begin
      if (FormatForSNY) then
      begin
        if (CurrentGameData.Count[3] = '0') then TempStr := '0 Outs'
        else if (CurrentGameData.Count[3] = '1') then TempStr := '1 Out'
        else if (CurrentGameData.Count[3] = '2') then TempStr := '2 Outs'
        else if (CurrentGameData.Count[3] = '3') then TempStr := '3 Outs';
      end
      else begin
        if (CurrentGameData.Count[3] = '0') then TempStr := '0 outs'
        else if (CurrentGameData.Count[3] = '1') then TempStr := '1 out'
        else if (CurrentGameData.Count[3] = '2') then TempStr := '2 outs'
        else if (CurrentGameData.Count[3] = '3') then TempStr := '3 outs';
      end;
      //Visitor at bat
      if ((GamePhaseData.SI_Phase_Code MOD 2) = 0) then
      begin
        OutStr :=  '[f 2][c 19]' + UpperCase(CurrentGameData.VMnemonic);
        if (FormatForSNY) then OutStr := OutStr + '  '
        else OutStr := OutStr + ': ';
        OutStr := OutStr + '[f 2][c 1]' + TempStr;
      end
      //Home team at bat
      else begin
        OutStr :=  '[f 2][c 19]' + UpperCase(CurrentGameData.HMnemonic);
        if (FormatForSNY) then OutStr := OutStr + '  '
        else OutStr := OutStr + ': ';
        OutStr := OutStr + '[f 2][c 1]' + TempStr;
      end;
    end;
  end

  //MLB batter & baserunners
  else if (SymbolName = '$MLB_Batter') then
  begin
    if (Trim(CurrentGameData.Batter) <> '') then
      OutStr := GetLastName(Trim(CurrentGameData.Batter)) + ' at bat'
    else OutStr := '';
  end
  else if (SymbolName = '$MLB_Batter_Abbreviated') then
  begin
    if (Trim(CurrentGameData.Batter) <> '') then
      OutStr := 'AB: ' + GetLastName(Trim(CurrentGameData.Batter))
    else OutStr := '';
  end
  else if (SymbolName = '$MLB_Baserunner1') then
  begin
    if (Trim(CurrentGameData.Baserunners[1]) <> '') then
      OutStr := Trim(CurrentGameData.Baserunners[1]) + ' on 1st'
    else OutStr := '';
  end
  else if (SymbolName = '$MLB_Baserunner2') then
  begin
    if (Trim(CurrentGameData.Baserunners[2]) <> '') then
      OutStr := Trim(CurrentGameData.Baserunners[2]) + ' on 2nd'
    else OutStr := '';
  end
  else if (SymbolName = '$MLB_Baserunner3') then
  begin
    if (Trim(CurrentGameData.Baserunners[3]) <> '') then
      OutStr := Trim(CurrentGameData.Baserunners[3]) + ' on 3rd'
    else OutStr := '';
  end

  //Game clock
  else if (SymbolName = '$Game_Clock') then
  begin
    if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) then
      OutStr := ''
    //Special case for baseball
    else begin
      TempStr := CurrentGameData.League;
      if (TempStr = 'MLB') OR (TempStr = 'ML') OR (TempStr = 'NL') OR (TempStr = 'AL') OR (TempStr = 'IL') OR (TempStr = 'GPFT') OR
         (TempStr = 'CAC') then
        OutStr := ' '
      else
        OutStr := FormatGameClock(CurrentGameData.TimeMin, CurrentGameData.TimeSec)
    end;
  end

  //Game phase
  else if (SymbolName = '$Game_Phase') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //Pre-Game
    if (CurrentGameData.State = PREGAME) then OutStr := ' '
    //In-Progress
    else if (CurrentGameData.State = INPROGRESS) then
    begin
      if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = FALSE) then
      begin
        TempStr := UpperCase(CurrentGameData.Subleague);
        if (TempStr = 'MLB') OR (TempStr = 'ML') OR (TempStr = 'AL') OR (TempStr = 'NL') OR (TempStr = 'IL') OR (TempStr = 'GRAPEFRUIT') OR (TempStr = 'CACTUS') then
        begin
          OutStr := GamePhaseData.Display_Period;
          if not(FormatForSNY) then OutStr := UpperCase(OutStr);
        end
        else begin
          OutStr := 'End ' + GamePhaseData.Display_Period;
          if not(FormatForSNY) then OutStr := UpperCase(OutStr);
        end;
      end
      else if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = TRUE) then
      begin
        OutStr := GamePhaseData.Display_Half;
        if not(FormatForSNY) then OutStr := UpperCase(OutStr);
      end
      else begin
        OutStr := GamePhaseData.Display_Period;
        if not(FormatForSNY) then OutStr := UpperCase(OutStr);
      end;
    end
    //Final
    else if (CurrentGameData.State = FINAL) then
    begin
      OutStr := GamePhaseData.Display_Final;
      if (FormatForSNY) then OutStr := '[f 2][c 19]' + OutStr;
      if not(FormatForSNY) then OutStr := UpperCase(OutStr);
    end
    //Add special cases for MLB
    //Postponed
    else if (CurrentGameData.State = POSTPONED) then
    begin
      OutStr := 'PPD';
    end
    //Delayed
    else if (CurrentGameData.State = DELAYED) then
    begin
      OutStr := 'DLY';
    end
    //Suspended
    else if (CurrentGameData.State = SUSPENDED) then
    begin
      OutStr := 'SUSP';
    end
    //Cancelled
    else if (CurrentGameData.State = CANCELLED) then
    begin
      OutStr := 'CANC';
    end
    else OutStr := '';
  end

  //Game phase with final highlighted in gold
  else if (SymbolName = '$Game_Phase_Highlighted_Final') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //Pre-Game
    if (CurrentGameData.State = PREGAME) then OutStr := ' '
    //In-Progress
    else if (CurrentGameData.State = INPROGRESS) then
    begin
      if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = FALSE) then
      begin
        TempStr := UpperCase(CurrentGameData.Subleague);
        if (TempStr = 'MLB') OR (TempStr = 'ML') OR (TempStr = 'AL') OR (TempStr = 'NL') OR (TempStr = 'IL') OR (TempStr = 'GRAPEFRUIT') OR (TempStr = 'CACTUS') then
        begin
          OutStr := GamePhaseData.Display_Period;
          if not(FormatForSNY) then OutStr := UpperCase(OutStr);
        end
        else begin
          OutStr := 'End ' + GamePhaseData.Display_Period;
          if not(FormatForSNY) then OutStr := UpperCase(OutStr);
        end;
      end
      else if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = TRUE) then
      begin
        OutStr := GamePhaseData.Display_Half;
        if not(FormatForSNY) then OutStr := UpperCase(OutStr);
      end
      else begin
        OutStr := GamePhaseData.Display_Period;
        if not(FormatForSNY) then OutStr := UpperCase(OutStr);
      end;
    end
    //Final
    else if (CurrentGameData.State = FINAL) then
    begin
      if (FormatForSNY) then
        OutStr := '[f 2][c 19]' + GamePhaseData.Display_Final
      else
        OutStr := '[f 2][c 19]' + UpperCase(GamePhaseData.Display_Final);
    end
    //Add special cases for MLB
    //Postponed
    else if (CurrentGameData.State = POSTPONED) then
    begin
      OutStr := 'PPD';
    end
    //Delayed
    else if (CurrentGameData.State = DELAYED) then
    begin
      OutStr := 'DLY';
    end
    //Suspended
    else if (CurrentGameData.State = SUSPENDED) then
    begin
      OutStr := 'SUSP';
    end
    //Cancelled
    else if (CurrentGameData.State = CANCELLED) then
    begin
      OutStr := 'CANC';
    end
    else OutStr := '';
  end

  //Game clock + phase
  else if (SymbolName = '$Game_Clock_Phase') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //Pre-Game
    if (CurrentGameData.State = PREGAME) then
    begin
      OutStr := TimeToStr(CurrentGameData.StartTime) + ' ' + TimeZoneSuffix;
    end
    //In-Progress
    else if (CurrentGameData.State = INPROGRESS) AND (CurrentEntryIndex <> NOENTRYINDEX) then
    begin
      if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = FALSE) then
      begin
        TempStr := UpperCase(CurrentGameData.Subleague);
        if (TempStr = 'MLB') OR (TempStr = 'ML') OR (TempStr = 'AL') OR (TempStr = 'NL') OR (TempStr = 'IL') OR (TempStr = 'GRAPEFRUIT') OR (TempStr = 'CACTUS') then
        begin
          OutStr := GamePhaseData.Display_Period;
          if not(FormatForSNY) then OutStr := UpperCase(OutStr);
        end
        else begin
          OutStr := 'End ' + GamePhaseData.Display_Period;
          if not(FormatForSNY) then OutStr := UpperCase(OutStr);
        end;
      end
      else if (CurrentGameData.TimeMin = 0) AND (CurrentGameData.TimeSec = 0) AND
         (GamePhaseData.End_Is_Half = TRUE) then
      begin
        OutStr := GamePhaseData.Display_Half;
        if not(FormatForSNY) then OutStr := UpperCase(OutStr);
      end
      else begin
        if (FormatForSNY) then
          OutStr := FormatGameClock(CurrentGameData.TimeMin, CurrentGameData.TimeSec) + '  ' +
            GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase).Display_Period
        else
          OutStr := FormatGameClock(CurrentGameData.TimeMin, CurrentGameData.TimeSec) + '  ' +
            UpperCase(GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase).Display_Period);
      end;
    end
    //Final
    else if (CurrentGameData.State = FINAL) then
    begin
      OutStr := GamePhaseData.Display_Final;
      if not(FormatForSNY) then OutStr := UpperCase(OutStr);
      //Check for doubleheader in baseball and append suffix if applicable
      if (CurrentGameData.DoubleHeader = TRUE) then
      begin
        //End of game, so display game number in note field
        if (CurrentGameData.DoubleHeaderGameNumber = 1) then
          OutStr := OutStr + ' - GM 1'
        else if (CurrentGameData.DoubleHeaderGameNumber = 2) then
          OutStr := OutStr + ' - GM 2'
      end;
      //if (FormatForSNY) then OutStr := '[c 19]' + OutStr;
    end
    //Add special cases for MLB
    //Postponed
    else if (CurrentGameData.State = POSTPONED) then
    begin
      OutStr := 'PPD';
    end
    //Delayed
    else if (CurrentGameData.State = DELAYED) then
    begin
      OutStr := 'DLY';
    end
    //Suspended
    else if (CurrentGameData.State = SUSPENDED) then
    begin
      OutStr := 'SUSP';
    end
    //Cancelled
    else if (CurrentGameData.State = CANCELLED) then
    begin
      OutStr := 'CANC';
    end
    else OutStr := '';
  end

  //Game inning (baseball only)
  else if (SymbolName = '$Game_Inning') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //Remove top/bottom inning indicator characters
    GamePhaseData.Display_Period := StringReplace(GamePhaseData.Display_Period, '�', '', [rfReplaceAll]);
    GamePhaseData.Display_Period := StringReplace(GamePhaseData.Display_Period, '�', '', [rfReplaceAll]);

    //In-Game
    if (CurrentGameData.State = INPROGRESS) then
      OutStr := GamePhaseData.Display_Period
    //Add special cases for MLB
    //Postponed
    else if (CurrentGameData.State = POSTPONED) then
    begin
      OutStr := 'PPD';
    end
    //Delayed
    else if (CurrentGameData.State = DELAYED) then
    begin
      OutStr := 'DLY';
    end
    //Suspended
    else if (CurrentGameData.State = SUSPENDED) then
    begin
      OutStr := 'SPD';
    end
    else
      OutStr := ' ';
  end

  //Top/bottom of inning indicator
  else if (SymbolName = '$Inning_Indicator') then
  begin
    GamePhaseData := GetGamePhaseRec(CurrentGameData.League, CurrentGameData.Phase);
    //In-game only; check for upper-ASCII character as first in string
    if (CurrentGameData.State = 2) AND
       (Length(GamePhaseData.Display_Period) > 0) AND
       (Ord(GamePhaseData.Display_Period[1]) > 128) then
      OutStr := GamePhaseData.Display_Period[1]
    else OutStr := '';
  end

  //Football field position
  else if (SymbolName = '$Football_Field_Position') then
  begin
    FootballSituation := GetFootballGameSituationData(CurrentGameData);
    if (FootballSituation.VisitorHasPossession = TRUE) then
    begin
      //Normalize - template goes from 0 to 100
      OutStr := IntToStr(100-FootballSituation.YardsFromGoal);
    end
    else if (GetFootballGameSituationData(CurrentGameData).HomeHasPossession = TRUE) then
    begin
      //Normalize - template goes from 0 to 100; add "-" for home team
      OutStr := '-' + IntToStr(100-FootballSituation.YardsFromGoal);
    end
  end

  //Football down & distance
  else if (SymbolName = '$Football_Down_Distance') then
  begin
    FootballSituation := GetFootballGameSituationData(CurrentGameData);
    if ((FootballSituation.Down > 0) AND (FootballSituation.Distance > 0)) then
    begin
      if (FootballSituation.VisitorHasPossession = TRUE) then
      begin
        OutStr := '[c 19]' + CurrentGameData.VMnemonic + ': ';
        OutStr := OutStr + '[c 1]' + IntToDown(FootballSituation.Down) + ' & ' + IntToStr(FootballSituation.Distance);
        if (FootballSituation.YardsFromGoal  > 50) then
          OutStr :=  OutStr + ' on ' + IntToStr(100-FootballSituation.YardsFromGoal) + ' '
        else if (FootballSituation.YardsFromGoal  > 0) AND (FootballSituation.YardsFromGoal  <= 50) then
          OutStr :=  OutStr + ' on ' + IntToStr(FootballSituation.YardsFromGoal) + ' '
        else OutStr := ' ';
      end
      else if (FootballSituation.HomeHasPossession = TRUE) then
      begin
        OutStr := '[c 19]' + CurrentGameData.HMnemonic + ': ';
        OutStr := OutStr + '[c 1]' + IntToDown(FootballSituation.Down) + ' & ' + IntToStr(FootballSituation.Distance);
        if (FootballSituation.YardsFromGoal  > 50) then
          OutStr :=  OutStr + ' on ' + IntToStr(100-FootballSituation.YardsFromGoal) + ' '
        else if (FootballSituation.YardsFromGoal  > 0) AND (FootballSituation.YardsFromGoal  <= 50) then
          OutStr :=  OutStr + ' on ' + IntToStr(FootballSituation.YardsFromGoal) + ' '
        else OutStr := ' ';
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //////////////////////////////////////////////////////////////////////////////
  // WEATHER (PRE-CSNMA)
  //////////////////////////////////////////////////////////////////////////////
  //Weather related functions; store current league and home Stats ID in global variables to prevent
  //separate queries for each variable in Weather template
  else if (SymbolName = '$Weather_Stadium_Name') then
  begin
    //if (CurrentWeatherForecastRec.CurrentLeague <> CurrentGameData.League) OR
    //   (CurrentWeatherForecastRec.CurrentHomeStatsID <> CurrentGameData.HStatsIncID) then
      CurrentWeatherForecastRec := GetWeatherForecastData(CurrentGameData.League, CurrentGameData.HStatsIncID);
    OutStr := '[c 19]' + CurrentWeatherForecastRec.Weather_Stadium_Name;
  end
  else if (SymbolName = '$Weather_Current_Temperature') then
  begin
    //if (CurrentWeatherForecastRec.CurrentLeague <> CurrentGameData.League) OR
    //   (CurrentWeatherForecastRec.CurrentHomeStatsID <> CurrentGameData.HStatsIncID) then
      CurrentWeatherForecastRec := GetWeatherForecastData(CurrentGameData.League, CurrentGameData.HStatsIncID);
    OutStr := '[c 1]' + CurrentWeatherForecastRec.Weather_Current_Temperature + ' F';
  end
  else if (SymbolName = '$Weather_High_Temperature') then
  begin
    //if (CurrentWeatherForecastRec.CurrentLeague <> CurrentGameData.League) OR
    //   (CurrentWeatherForecastRec.CurrentHomeStatsID <> CurrentGameData.HStatsIncID) then
      CurrentWeatherForecastRec := GetWeatherForecastData(CurrentGameData.League, CurrentGameData.HStatsIncID);
    OutStr := '[c 1]' + CurrentWeatherForecastRec.Weather_High_Temperature + ' F';
  end
  else if (SymbolName = '$Weather_Low_Temperature') then
  begin
    //if (CurrentWeatherForecastRec.CurrentLeague <> CurrentGameData.League) OR
    //   (CurrentWeatherForecastRec.CurrentHomeStatsID <> CurrentGameData.HStatsIncID) then
      CurrentWeatherForecastRec := GetWeatherForecastData(CurrentGameData.League, CurrentGameData.HStatsIncID);
    OutStr := '[c 1]' + CurrentWeatherForecastRec.Weather_Low_Temperature + ' F';
  end
  else if (SymbolName = '$Weather_Current_Conditions') then
  begin
    //if (CurrentWeatherForecastRec.CurrentLeague <> CurrentGameData.League) OR
    //   (CurrentWeatherForecastRec.CurrentHomeStatsID <> CurrentGameData.HStatsIncID) then
      CurrentWeatherForecastRec := GetWeatherForecastData(CurrentGameData.League, CurrentGameData.HStatsIncID);
    OutStr := '[c 1]' + CurrentWeatherForecastRec.Weather_Current_Conditions;
  end
  else if (SymbolName = '$Weather_Current_Temp_Conditions') then
  begin
    //if (CurrentWeatherForecastRec.CurrentLeague <> CurrentGameData.League) OR
    //   (CurrentWeatherForecastRec.CurrentHomeStatsID <> CurrentGameData.HStatsIncID) then
      CurrentWeatherForecastRec := GetWeatherForecastData(CurrentGameData.League, CurrentGameData.HStatsIncID);
    OutStr := '[c 1]' + CurrentWeatherForecastRec.Weather_Current_Temperature + ' F  ' +
              CurrentWeatherForecastRec.Weather_Current_Conditions;
  end
  else if (SymbolName = '$Weather_Forecast_Icon') then
  begin
    //if (CurrentWeatherForecastRec.CurrentLeague <> CurrentGameData.League) OR
    //   (CurrentWeatherForecastRec.CurrentHomeStatsID <> CurrentGameData.HStatsIncID) then
      //Icon character codes are stored as strings in SQL DB
      CurrentWeatherForecastRec := GetWeatherForecastData(CurrentGameData.League, CurrentGameData.HStatsIncID);
    //Get icon based on time of day
    if (CurrentGameData.StartTime > EncodeTime(20, 0, 0, 0)) then
      OutStr := Char(StrToInt(CurrentWeatherForecastRec.Weather_Forecast_Icon_Night))
    else
      OutStr := Char(StrToInt(CurrentWeatherForecastRec.Weather_Forecast_Icon_Day));
  end

  //////////////////////////////////////////////////////////////////////////////
  //TEAM RECORDS - PRE-GAME
  //////////////////////////////////////////////////////////////////////////////
  //Team records for supported leagues
  //These leagues - show only wins & losses
  else if (SymbolName = '$Team_Record_WL_Visitor_Mnemonic') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.VRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) + ')';
  end
  else if (SymbolName = '$Team_Record_WL_Visitor_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.VRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) + ')';
  end
  else if (SymbolName = '$Team_Record_WL_Home_Mnemonic') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.HRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) + ')';
  end
  else if (SymbolName = '$Team_Record_WL_Home_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.HRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) + ')';
  end
  //These leagues show wins, losses, ties
  else if (SymbolName = '$Team_Record_WLT_Visitor_Mnemonic') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.VRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
      '-' + Trim(TeamRecord.Param[3]) + ')';
  end
  else if (SymbolName = '$Team_Record_WLT_Visitor_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.VRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
      '-' + Trim(TeamRecord.Param[3]) + ')';
  end
  else if (SymbolName = '$Team_Record_WLT_Home_Mnemonic') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.HRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
      '-' + Trim(TeamRecord.Param[3]) + ')';
  end
  else if (SymbolName = '$Team_Record_WLT_Home_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.HRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
      '-' + Trim(TeamRecord.Param[3]) + ')';
  end
  //NHL - Show wins, losses, OT losses & total points
  else if (SymbolName = '$Team_Record_NHL_Visitor_Mnemonic') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.VRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    if (FormatForSNY) then
      OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
        '-' + Trim(TeamRecord.Param[3]) + ', ' + Trim(TeamRecord.Param[4]) + ' Pts' + ')'
    else
      OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
        '-' + Trim(TeamRecord.Param[3]) + ', ' + Trim(TeamRecord.Param[4]) + ' pts' + ')';
  end
  else if (SymbolName = '$Team_Record_NHL_Visitor_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.VRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    if (FormatForSNY) then
      OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
        '-' + Trim(TeamRecord.Param[3]) + ', ' + Trim(TeamRecord.Param[4]) + ' Pts' + ')'
    else
      OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
        '-' + Trim(TeamRecord.Param[3]) + ', ' + Trim(TeamRecord.Param[4]) + ' pts' + ')';
  end
  else if (SymbolName = '$Team_Record_NHL_Home_Mnemonic') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.HRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    if (FormatForSNY) then
      OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
        '-' + Trim(TeamRecord.Param[3]) + ', ' + Trim(TeamRecord.Param[4]) + ' Pts' + ')'
    else
      OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
        '-' + Trim(TeamRecord.Param[3]) + ', ' + Trim(TeamRecord.Param[4]) + ' pts' + ')';
  end
  else if (SymbolName = '$Team_Record_NHL_Home_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.HRecord);
    OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    if (FormatForSNY) then
      OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
        '-' + Trim(TeamRecord.Param[3]) + ', ' + Trim(TeamRecord.Param[4]) + ' Pts' + ')'
    else
      OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
        '-' + Trim(TeamRecord.Param[3]) + ', ' + Trim(TeamRecord.Param[4]) + ' pts' + ')';
  end


  //////////////////////////////////////////////////////////////////////////////
  //TEAM RECORDS - POST-GAME
  //////////////////////////////////////////////////////////////////////////////
  else if (SymbolName = '$Team_Record_Final_WL_Visitor_Mnemonic') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.VRecord);
    //Check for visitor is winner
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic)
    else
      OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VMnemonic);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) + ')';
  end
  else if (SymbolName = '$Team_Record_Final_WL_Visitor_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.VRecord);
    //Check for visitor is winner
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName)
    else
      OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VName);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) + ')';
  end
  else if (SymbolName = '$Team_Record_Final_WL_Home_Mnemonic') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.HRecord);
    //Check for home is winner
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic)
    else
      OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HMnemonic);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) + ')';
  end
  else if (SymbolName = '$Team_Record_Final_WL_Home_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.HRecord);
    //Check for home is winner
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName)
    else
      OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HName);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) + ')';
  end
  //These leagues show wins, losses, ties
  else if (SymbolName = '$Team_Record_Final_WLT_Visitor_Mnemonic') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.VRecord);
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic)
    else
      OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VMnemonic);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
      '-' + Trim(TeamRecord.Param[3]) + ')';
  end
  else if (SymbolName = '$Team_Record_Final_WLT_Visitor_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.VRecord);
    //Check for visitor is winner
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName)
    else
      OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VName);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
      '-' + Trim(TeamRecord.Param[3]) + ')';
  end
  else if (SymbolName = '$Team_Record_Final_WLT_Home_Mnemonic') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.HRecord);
    //Check for home is winner
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic)
    else
      OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HMnemonic);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
      '-' + Trim(TeamRecord.Param[3]) + ')';
  end
  else if (SymbolName = '$Team_Record_Final_WLT_Home_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.HRecord);
    //Check for home is winner
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName)
    else
      OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HName);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
      '-' + Trim(TeamRecord.Param[3]) + ')';
  end
  //NHL - Show wins, losses, OT losses & total points
  else if (SymbolName = '$Team_Record_Final_NHL_Visitor_Mnemonic') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.VRecord);
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic)
    else
      OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VMnemonic);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    if (FormatForSNY) then
      OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
        '-' + Trim(TeamRecord.Param[3]) + ', ' + Trim(TeamRecord.Param[4]) + ' Pts' + ')'
    else
      OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
        '-' + Trim(TeamRecord.Param[3]) + ', ' + Trim(TeamRecord.Param[4]) + ' pts' + ')';
  end
  else if (SymbolName = '$Team_Record_Final_NHL_Visitor_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.VRecord);
    //Check for visitor is winner
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName)
    else
      OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VName);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    if (FormatForSNY) then
      OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
        '-' + Trim(TeamRecord.Param[3]) + ', ' + Trim(TeamRecord.Param[4]) + ' Pts' + ')'
    else
      OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
        '-' + Trim(TeamRecord.Param[3]) + ', ' + Trim(TeamRecord.Param[4]) + ' pts' + ')';
  end
  else if (SymbolName = '$Team_Record_Final_NHL_Home_Mnemonic') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.HRecord);
    //Check for home is winner
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic)
    else
      OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HMnemonic);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    if (FormatForSNY) then
      OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
        '-' + Trim(TeamRecord.Param[3]) + ', ' + Trim(TeamRecord.Param[4]) + ' Pts' + ')'
    else
      OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
        '-' + Trim(TeamRecord.Param[3]) + ', ' + Trim(TeamRecord.Param[4]) + ' pts' + ')';
  end
  else if (SymbolName = '$Team_Record_Final_NHL_Home_Name') then
  begin
    TeamRecord := ParseTeamRecord(CurrentGameData.HRecord);
    //Check for home is winner
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName)
    else
      OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HName);
    OutStr := OutStr + TeamNameMnemonicSuffix;
    if (FormatForSNY) then
      OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
        '-' + Trim(TeamRecord.Param[3]) + ', ' + Trim(TeamRecord.Param[4]) + ' Pts' + ')'
    else
      OutStr := OutStr + '[f 2][c 1]' + '(' + Trim(TeamRecord.Param[1]) + '-' + Trim(TeamRecord.Param[2]) +
        '-' + Trim(TeamRecord.Param[3]) + ', ' + Trim(TeamRecord.Param[4]) + ' pts' + ')';
  end

  //////////////////////////////////////////////////////////////////////////////
  //TEAM STREAKS
  //////////////////////////////////////////////////////////////////////////////
  //Team Streaks
  else if (SymbolName = '$Team_Streak_Visitor_Mnemonic') then
  begin
    if (Trim(CurrentGameData.VStreak) <> '') then
    begin
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic);
      OutStr := OutStr + TeamNameMnemonicSuffix;
      OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentGameData.VStreak);
    end
    else OutStr := ' ';
  end
  else if (SymbolName = '$Team_Streak_Home_Mnemonic') then
  begin
    if (Trim(CurrentGameData.HStreak) <> '') then
    begin
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic);
      OutStr := OutStr + TeamNameMnemonicSuffix;
      OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentGameData.HStreak);
    end
    else OutStr := ' ';
  end
  else if (SymbolName = '$Team_Streak_Visitor_Name') then
  begin
    if (Trim(CurrentGameData.VStreak) <> '') then
    begin
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName);
      OutStr := OutStr + TeamNameMnemonicSuffix;
      OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentGameData.VStreak);
    end
    else OutStr := ' ';
  end
  else if (SymbolName = '$Team_Streak_Home_Name') then
  begin
    if (Trim(CurrentGameData.HStreak) <> '') then
    begin
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName);
      OutStr := OutStr + TeamNameMnemonicSuffix;
      OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentGameData.HStreak);
    end
    else OutStr := ' ';
  end

  //////////////////////////////////////////////////////////////////////////////
  //MLB Pitcher Stat functions
  //////////////////////////////////////////////////////////////////////////////
  //Starting pitchers
  else if (SymbolName = '$Starting_Pitcher_Visitor') then
  begin
    if (CurrentStartingPitcherStatsRec.CurrentGameID <> CurrentGameData.GameID) then
    CurrentStartingPitcherStatsRec := GetStartingPitcherStats(CurrentGameData.GameID);
    if (Trim(CurrentStartingPitcherStatsRec.Visitor_Pitcher_Name_Last) <> '') then
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + TeamNameMnemonicSuffix +
                '[f 2][c 1]' + Trim(CurrentStartingPitcherStatsRec.Visitor_Pitcher_Name_Last)
    else OutStr := ' ';
  end
  else if (SymbolName = '$Starting_Pitcher_Home') then
  begin
    if (CurrentStartingPitcherStatsRec.CurrentGameID <> CurrentGameData.GameID) then
    CurrentStartingPitcherStatsRec := GetStartingPitcherStats(CurrentGameData.GameID);
    if (Trim(CurrentStartingPitcherStatsRec.Home_Pitcher_Name_Last) <> '') then
      OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + TeamNameMnemonicSuffix +
                '[f 2][c 1]' + Trim(CurrentStartingPitcherStatsRec.Home_Pitcher_Name_Last)
    else OutStr := ' ';
  end

  //Final pitcher record - Visitor
  else if (SymbolName = '$Final_Pitcher_Record_Visitor') then
  begin
    if (CurrentFinalPitcherStatsRec.CurrentGameID <> CurrentGameData.GameID) then
    CurrentFinalPitcherStatsRec := GetGameFinalPitcherStats(CurrentGameData.GameID);
    //Check for visitor is winner
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
    begin
      if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) <> '') then
      begin
        if (FormatForSNY) then
        begin
          OutStr := '[f 2][c 19]W - ' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) + '[c 1]' +
                    ' (' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Wins) + '-' +
                    Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Losses) + ')';
        end
        else begin
          OutStr := '[f 2][c 1]W - ' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) +
                    ' (' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Wins) + '-' +
                    Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Losses) + ')'
        end;
      end
      else OutStr := ' ';
    end
    //Check for visitor is loser
    else if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore < CurrentGameData.HScore) then
    begin
      if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Name_Last) <> '') then
        OutStr := '[f 2][c 1]L - ' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Name_Last) +
                  ' (' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Wins) + '-' +
                  Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Losses) + ')'
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Final pitcher record - Home
  else if (SymbolName = '$Final_Pitcher_Record_Home') then
  begin
    if (CurrentFinalPitcherStatsRec.CurrentGameID <> CurrentGameData.GameID) then
    CurrentFinalPitcherStatsRec := GetGameFinalPitcherStats(CurrentGameData.GameID);
    //Check for home is winner
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
    begin
      if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) <> '') then
      begin
        if (FormatForSNY) then
        begin
          OutStr := '[f 2][c 19]W - ' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) + '[c 1]' +
                    ' (' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Wins) + '-' +
                    Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Losses) + ')';
        end
        else begin
          OutStr := '[f 2][c 1]W - ' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) +
                    ' (' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Wins) + '-' +
                    Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Losses) + ')'
        end;
      end  
      else OutStr := ' ';
    end
    //Check for home is loser
    else if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore < CurrentGameData.VScore) then
    begin
      if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Name_Last) <> '') then
        OutStr := '[f 2][c 1]L - ' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Name_Last) +
                  ' (' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Wins) + '-' +
                  Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Losses) + ')'
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Final pitcher record - Winning Pitcher
  else if (SymbolName = '$Final_Pitcher_Record_Winner') then
  begin
    if (CurrentFinalPitcherStatsRec.CurrentGameID <> CurrentGameData.GameID) then
    CurrentFinalPitcherStatsRec := GetGameFinalPitcherStats(CurrentGameData.GameID);
    //Check for winner
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) <> '') then
      begin
        if (FormatForSNY) then
        begin
          OutStr := '[f 2][c 19]W - ' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) + '[c 1]' +
                    ' (' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Wins) + '-' +
                    Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Losses) + ')';
        end
        else begin
          OutStr := '[f 2][c 1]W - ' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) +
                    ' (' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Wins) + '-' +
                    Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Losses) + ')'
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Final pitcher record - Losing Pitcher
  else if (SymbolName = '$Final_Pitcher_Record_Loser') then
  begin
    if (CurrentFinalPitcherStatsRec.CurrentGameID <> CurrentGameData.GameID) then
    CurrentFinalPitcherStatsRec := GetGameFinalPitcherStats(CurrentGameData.GameID);
    //Check for loser
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Name_Last) <> '') then
        OutStr := '[f 2][c 1]L - ' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Name_Last) +
                  ' (' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Wins) + '-' +
                  Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Losses) + ')'
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Final pitcher record (saves) - Saving Pitcher
  else if (SymbolName = '$Final_Pitcher_Record_Save') then
  begin
    if (CurrentFinalPitcherStatsRec.CurrentGameID <> CurrentGameData.GameID) then
    CurrentFinalPitcherStatsRec := GetGameFinalPitcherStats(CurrentGameData.GameID);
    //Check for home is winner
    if (CurrentGameData.State = FINAL) AND (CurrentFinalPitcherStatsRec.NeedToAddSavingPitcher) then
    begin
      if (Trim(CurrentFinalPitcherStatsRec.Saving_Pitcher_Name_Last) <> '') then
      begin
        if (FormatForSNY) then
        begin
          OutStr := '[f 2][c 19]S - ' + Trim(CurrentFinalPitcherStatsRec.Saving_Pitcher_Name_Last) + '[c 1]' +
                    ' (' + Trim(CurrentFinalPitcherStatsRec.Saving_Pitcher_Saves) + ')';
        end
        else begin
          OutStr := '[f 2][c 1]S - ' + Trim(CurrentFinalPitcherStatsRec.Saving_Pitcher_Name_Last) +
                    ' (' + Trim(CurrentFinalPitcherStatsRec.Saving_Pitcher_Saves) + ')';
        end;
        //Disabled for version 3.1.1. 04/06/11
        //If saving pitcher is the home team pitcher, move to second line using pipe delimiter
        //if (CurrentGameData.HScore > CurrentGameData.VScore) AND ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
        //  OutStr := '|' + OutStr;
      end
      //Set text value as flag to skip record
      else begin
        OutStr := ' ';
        OKToDisplay := FALSE;
      end;
    end
    //Set text value as flag to skip record
    else begin
      OutStr := ' ';
      OKToDisplay := FALSE;
    end;
  end

  //////////////////////////////////////////////////////////////////////////////
  //MLB Hero Notes Functions
  //////////////////////////////////////////////////////////////////////////////
  //Final pitcher stats - Visitor
  else if (SymbolName = '$Final_Pitcher_Stats_Visitor') or (SymbolName = '$Final_Pitcher_Stats_Visitor_NoLineBreak') then
  begin
    if (CurrentFinalPitcherStatsRec.CurrentGameID <> CurrentGameData.GameID) then
    CurrentFinalPitcherStatsRec := GetGameFinalPitcherStats(CurrentGameData.GameID);
    //Check for visitor is winner
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore > CurrentGameData.HScore) then
    begin
      if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) <> '') then
      begin
        //Modified to show "CG" instead of "IP" if complete game pitched
        if (FormatForSNY) then
        begin
          OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + TeamNameMnemonicSuffix +
                    '[f 2][c 1]' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last);
          if (StrToFloatDef(Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_IP), 0.0) >= 9.0) then
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + 'CG'
          else
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_IP) + ' IP';
        end
        else
          OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + TeamNameMnemonicSuffix +
                    '[f 2][c 1]' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) +
                    '[f 2][c 1]' + PlayerNameSuffix + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_IP) + ' IP';
        if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Hits) <> '0') then
        begin
          if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Hits) = '1') then
            OutStr := OutStr + ', ' + 'H'
          else
            OutStr := OutStr + ', ' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Hits) + ' H';
        end;
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) and (Pos('NoLineBreak', SymbolName) = 0) then
          OutStr := OutStr + '|';
        //Add stats
        //Removed runs at request of SNY Version 3.1.1 04/05/11
        if not (FormatForSNY) then
        begin
          if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Runs) <> '0') and
             (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Runs) <> '') then
          begin
            if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
            if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Runs) = '1') then
              OutStr := OutStr + 'R'
            else
              OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Runs) + ' R';
          end;
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_EarnedRuns) <> '0') and
           (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_EarnedRuns) <> '') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_EarnedRuns) = '1') then
            OutStr := OutStr + 'ER'
          else
            OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_EarnedRuns) + ' ER';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_BB) <> '0') and
           (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_BB) <> '') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_BB) = '1') then
            OutStr := OutStr + 'BB'
          else
            OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_BB) + ' BB';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_K) <> '0') and
           (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_K) <> '') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_K) = '1') then
            OutStr := OutStr + 'K'
          else
            OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_K) + ' K';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_HomeRuns) <> '0') and
           (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_HomeRuns) <> '') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_HomeRuns) = '1') then
            OutStr := OutStr + 'HR'
          else
            OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_HomeRuns) + ' HR';
        end;
      end
      else OutStr := ' ';
    end
    //Check for visitor is loser
    else if (CurrentGameData.State = FINAL) AND (CurrentGameData.VScore < CurrentGameData.HScore) then
    begin
      if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Name_Last) <> '') then
      begin
        //Modified to show "CG" instead of "IP" if complete game pitched
        if (FormatForSNY) then
        begin
          OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VMnemonic) + TeamNameMnemonicSuffix +
                    '[f 2][c 1]' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Name_Last);
          if (StrToFloatDef(Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_IP), 0.0) >= 9.0) then
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + 'CG'
          else
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_IP) + ' IP';
        end
        else
          OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VMnemonic) + TeamNameMnemonicSuffix +
                    '[f 2][c 1]' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Name_Last) +
                    '[f 2][c 1]' + PlayerNameSuffix + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_IP) + ' IP';
        if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Hits) <> '0') and
           (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Hits) <> '') then
        begin
          if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Hits) = '1') then
            OutStr := OutStr + ', ' + 'H'
          else
            OutStr := OutStr + ', ' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Hits) + ' H';
        end;
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) and (Pos('NoLineBreak', SymbolName) = 0) then
          OutStr := OutStr + '|';
        //Add stats
        //Removed runs at request of SNY Version 3.1.1 04/05/11
        if not (FormatForSNY) then
        begin
          if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Runs) <> '0') and
             (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Runs) <> '') then
          begin
            if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
            if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Runs) = '1') then
              OutStr := OutStr + 'R'
            else
              OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Runs) + ' R';
          end;
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_EarnedRuns) <> '0') and
           (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_EarnedRuns) <> '') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_EarnedRuns) = '1') then
            OutStr := OutStr + 'ER'
          else
            OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_EarnedRuns) + ' ER';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_BB) <> '0') and
           (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_BB) <> '') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_BB) = '1') then
            OutStr := OutStr + 'BB'
          else
            OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_BB) + ' BB';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_K) <> '0') and
           (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_K) <> '') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_K) = '1') then
            OutStr := OutStr + 'K'
          else
            OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_K) + ' K';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_HomeRuns) <> '0') and
           (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_HomeRuns) <> '') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_HomeRuns) = '1') then
            OutStr := OutStr + 'HR'
          else
            OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_HomeRuns) + ' HR';
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Final pitcher stats - Home
  else if (SymbolName = '$Final_Pitcher_Stats_Home') or (SymbolName = '$Final_Pitcher_Stats_Home_NoLineBreak')then
  begin
    if (CurrentFinalPitcherStatsRec.CurrentGameID <> CurrentGameData.GameID) then
    CurrentFinalPitcherStatsRec := GetGameFinalPitcherStats(CurrentGameData.GameID);
    //Check for home is winner
    if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore > CurrentGameData.VScore) then
    begin
      if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) <> '') then
      begin
        //Modified to show "CG" instead of "IP" if complete game pitched
        if (FormatForSNY) then
        begin
          OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + TeamNameMnemonicSuffix +
                    '[f 2][c 1]' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last);
          if (StrToFloatDef(Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_IP), 0.0) >= 9.0) then
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + 'CG'
          else
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_IP) + ' IP';
        end
        else
          OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + TeamNameMnemonicSuffix +
                    '[f 2][c 1]' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Name_Last) +
                    '[f 2][c 1]' + PlayerNameSuffix + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_IP) + ' IP';
        if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Hits) <> '0') and
           (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Hits) <> '') then
        begin
          if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Hits) = '1') then
            OutStr := OutStr + ', ' + 'H'
          else
            OutStr := OutStr + ', ' + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Hits) + ' H';
        end;
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) and (Pos('NoLineBreak', SymbolName) = 0) then
          OutStr := OutStr + '|';
        //Add stats
        //Removed runs at request of SNY Version 3.1.1 04/05/11
        if not (FormatForSNY) then
        begin
          if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Runs) <> '0') and
             (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Runs) <> '') then
          begin
            if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
            if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Runs) = '1') then
              OutStr := OutStr + 'R'
            else
              OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_Runs) + ' R';
          end;
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_EarnedRuns) <> '0') and
           (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_EarnedRuns) <> '') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_EarnedRuns) = '1') then
            OutStr := OutStr + 'ER'
          else
            OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_EarnedRuns) + ' ER';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_BB) <> '0') and
           (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_BB) <> '') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_BB) = '1') then
            OutStr := OutStr + 'BB'
          else
            OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_BB) + ' BB';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_K) <> '0') and
           (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_K) <> '') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_K) = '1') then
            OutStr := OutStr + 'K'
          else
            OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_K) + ' K';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_HomeRuns) <> '0') and
           (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_HomeRuns) <> '') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          if (Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_HomeRuns) = '1') then
            OutStr := OutStr + 'HR'
          else
            OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Winning_Pitcher_HomeRuns) + ' HR';
        end;
      end
      else OutStr := ' ';
    end
    //Check for visitor is loser
    else if (CurrentGameData.State = FINAL) AND (CurrentGameData.HScore < CurrentGameData.VScore) then
    begin
      if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Name_Last) <> '') then
      begin
        //Modified to show "CG" instead of "IP" if complete game pitched
        if (FormatForSNY) then
        begin
          OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HMnemonic) + TeamNameMnemonicSuffix +
                    '[f 2][c 1]' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Name_Last);
          if (StrToFloatDef(Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_IP), 0.0) >= 9.0) then
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + 'CG'
          else
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_IP) + ' IP';
        end
        else
          OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HMnemonic) + TeamNameMnemonicSuffix +
                    '[f 2][c 1]' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Name_Last) +
                    '[f 2][c 1]' + PlayerNameSuffix + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_IP) + ' IP';
        if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Hits) <> '0') and
           (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Hits) <> '') then
        begin
          if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Hits) = '1') then
            OutStr := OutStr + ', ' + 'H'
          else
            OutStr := OutStr + ', ' + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Hits) + ' H';
        end;
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) and (Pos('NoLineBreak', SymbolName) = 0) then
          OutStr := OutStr + '|';
        //Add stats
        //Removed runs at request of SNY Version 3.1.1 04/05/11
        if not (FormatForSNY) then
        begin
          if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Runs) <> '0') and
             (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Runs) <> '') then
          begin
            if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
            if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Runs) = '1') then
              OutStr := OutStr + 'R'
            else
              OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_Runs) + ' R';
          end;
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_EarnedRuns) <> '0') and
           (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_EarnedRuns) <> '') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_EarnedRuns) = '1') then
            OutStr := OutStr + 'ER'
          else
            OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_EarnedRuns) + ' ER';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_BB) <> '0') and
           (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_BB) <> '') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_BB) = '1') then
            OutStr := OutStr + 'BB'
          else
            OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_BB) + ' BB';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_K) <> '0') and
           (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_K) <> '') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_K) = '1') then
            OutStr := OutStr + 'K'
          else
            OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_K) + ' K';
        end;
        if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_HomeRuns) <> '0') and
           (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_HomeRuns) <> '') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          if (Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_HomeRuns) = '1') then
            OutStr := OutStr + 'HR'
          else
            OutStr := OutStr + Trim(CurrentFinalPitcherStatsRec.Losing_Pitcher_HomeRuns) + ' HR';
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Final batter stats - Visitor (can be 1st or 2nd in list)
  else if (SymbolName = '$Final_Batter_Stats1_Visitor') OR (SymbolName = '$Final_Batter_Stats2_Visitor') or
          (SymbolName = '$Final_Batter_Stats1_Visitor_NoLineBreak') OR (SymbolName = '$Final_Batter_Stats2_Visitor_NoLineBreak')then
  begin
    if (SymbolName = '$Final_Batter_Stats1_Visitor') or (SymbolName = '$Final_Batter_Stats1_Visitor_NoLineBreak') then
      FinalBatterStats := GetGameFinalBatterStats(CurrentGameData.GameID, CurrentGameData.VStatsIncID, 1)
    else if (SymbolName = '$Final_Batter_Stats2_Visitor') or (SymbolName = '$Final_Batter_Stats2_Visitor_NoLineBreak') then
      FinalBatterStats := GetGameFinalBatterStats(CurrentGameData.GameID, CurrentGameData.VStatsIncID, 2);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(FinalBatterStats.Name_Last) <> '') then
      begin
        if (CurrentGameData.VScore > CurrentGameData.HScore) then
          OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + TeamNameMnemonicSuffix +
                    '[f 2][c 1]' + Trim(FinalBatterStats.Name_Last) +
                     //Add hits, at-bats
                    '[f 2][c 1]' + PlayerNameSuffix + Trim(FinalBatterStats.Hits) + '-' + Trim(FinalBatterStats.AtBats)
        else
          OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VMnemonic) + TeamNameMnemonicSuffix +
                    '[f 2][c 1]' + Trim(FinalBatterStats.Name_Last) +
                     //Add hits, at-bats
                    '[f 2][c 1]' + PlayerNameSuffix + Trim(FinalBatterStats.Hits) + '-' + Trim(FinalBatterStats.AtBats);
        //Add doubles
        if (Trim(FinalBatterStats.Doubles) = '1') then
          OutStr := OutStr + ', ' + '2B'
        else if (Trim(FinalBatterStats.Doubles) > '1') then
          OutStr := OutStr + ', ' + Trim(FinalBatterStats.Doubles) + ' 2B';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) and (Pos('NoLineBreak', SymbolName) = 0) then
          OutStr := OutStr + '|';
        //Add triples
        if (Trim(FinalBatterStats.Triples) = '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + '3B';
        end
        else if (Trim(FinalBatterStats.Triples) > '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(FinalBatterStats.Triples) + ' 3B';
        end;
        //Add home-runs
        if (Trim(FinalBatterStats.HomeRuns) = '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + 'HR';
        end
        else if (Trim(FinalBatterStats.HomeRuns) > '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(FinalBatterStats.HomeRuns) + ' HR';
        end;
        //Add RBI
        if (Trim(FinalBatterStats.RBI) = '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + 'RBI';
        end
        else if (Trim(FinalBatterStats.RBI) > '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(FinalBatterStats.RBI) + ' RBI';
        end;
        //Add stolen bases
        if (Trim(FinalBatterStats.StolenBases) = '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + 'SB';
        end
        else if (Trim(FinalBatterStats.StolenBases) > '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(FinalBatterStats.StolenBases) + ' SB';
        end;
        //Add runs
        if (Trim(FinalBatterStats.Runs) = '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + 'R';
        end
        else if (Trim(FinalBatterStats.Runs) > '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(FinalBatterStats.Runs) + ' R';
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Final batter stats - Home (can be 1st or 2nd in list)
  else if (SymbolName = '$Final_Batter_Stats1_Home') OR (SymbolName = '$Final_Batter_Stats2_Home') or
          (SymbolName = '$Final_Batter_Stats1_Home_NoLineBreak') OR (SymbolName = '$Final_Batter_Stats2_Home_NoLineBreak')then
  begin
    if (SymbolName = '$Final_Batter_Stats1_Home') or (SymbolName = '$Final_Batter_Stats1_Home_NoLineBreak') then
      FinalBatterStats := GetGameFinalBatterStats(CurrentGameData.GameID, CurrentGameData.HStatsIncID, 1)
    else if (SymbolName = '$Final_Batter_Stats2_Home') or (SymbolName = '$Final_Batter_Stats2_Home_NoLineBreak') then
      FinalBatterStats := GetGameFinalBatterStats(CurrentGameData.GameID, CurrentGameData.HStatsIncID, 2);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(FinalBatterStats.Name_Last) <> '') then
      begin
        if (CurrentGameData.HScore > CurrentGameData.VScore) then
          OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + TeamNameMnemonicSuffix +
                    '[f 2][c 1]' + Trim(FinalBatterStats.Name_Last) +
                     //Add hits, at-bats
                    '[f 2][c 1]' + PlayerNameSuffix + Trim(FinalBatterStats.Hits) + '-' + Trim(FinalBatterStats.AtBats)
        else
          OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HMnemonic) + TeamNameMnemonicSuffix +
                    '[f 2][c 1]' + Trim(FinalBatterStats.Name_Last) +
                     //Add hits, at-bats
                    '[f 2][c 1]' + PlayerNameSuffix + Trim(FinalBatterStats.Hits) + '-' + Trim(FinalBatterStats.AtBats);
        //Add doubles
        if (Trim(FinalBatterStats.Doubles) = '1') then
          OutStr := OutStr + ', ' + '2B'
        else if (Trim(FinalBatterStats.Doubles) > '1') then
          OutStr := OutStr + ', ' + Trim(FinalBatterStats.Doubles) + ' 2B';
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) and (Pos('NoLineBreak', SymbolName) = 0) then
          OutStr := OutStr + '|';
        //Add triples
        if (Trim(FinalBatterStats.Triples) = '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + '3B';
        end
        else if (Trim(FinalBatterStats.Triples) > '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(FinalBatterStats.Triples) + ' 3B';
        end;
        //Add home-runs
        if (Trim(FinalBatterStats.HomeRuns) = '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + 'HR';
        end
        else if (Trim(FinalBatterStats.HomeRuns) > '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(FinalBatterStats.HomeRuns) + ' HR';
        end;
        //Add RBI
        if (Trim(FinalBatterStats.RBI) = '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + 'RBI';
        end
        else if (Trim(FinalBatterStats.RBI) > '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(FinalBatterStats.RBI) + ' RBI';
        end;
        //Add stolen bases
        if (Trim(FinalBatterStats.StolenBases) = '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + 'SB';
        end
        else if (Trim(FinalBatterStats.StolenBases) > '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(FinalBatterStats.StolenBases) + ' SB';
        end;
        //Add runs
        if (Trim(FinalBatterStats.Runs) = '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + 'R';
        end
        else if (Trim(FinalBatterStats.Runs) > '1') then
        begin
          if (OutStr[Length(OutStr)] <> '|') then OutStr := OutStr + ', ';
          OutStr := OutStr + Trim(FinalBatterStats.Runs) + ' R';
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end


  //////////////////////////////////////////////////////////////////////////////
  //Basketball Hero Notes Functions
  //////////////////////////////////////////////////////////////////////////////
  //Basketball stats - Visitor (can be 1st or 2nd in list)
  else if (SymbolName = '$Final_BBall_Stats1_Visitor_Mnemonic') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Mnemonic') OR
          (SymbolName = '$Final_BBall_Stats1_Visitor_Mnemonic_NoLineBreak') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Mnemonic_NoLineBreak') OR
          (SymbolName = '$Final_BBall_Stats1_Visitor_Name') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Name') OR
          (SymbolName = '$Final_BBall_Stats1_Visitor_Name_NoLineBreak') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Name_NoLineBreak') then
  begin
    if (SymbolName = '$Final_BBall_Stats1_Visitor_Mnemonic') OR (SymbolName = '$Final_BBall_Stats1_Visitor_Name') or
       (SymbolName = '$Final_BBall_Stats1_Visitor_Mnemonic_NoLineBreak') OR (SymbolName = '$Final_BBall_Stats1_Visitor_Name_NoLineBreak') then
      FinalBBallStats := GetGameFinalBBallStats(CurrentGameData.League, CurrentGameData.GameID, CurrentGameData.VStatsIncID, 1)
    else if (SymbolName = '$Final_BBall_Stats2_Visitor_Mnemonic') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Name') or
            (SymbolName = '$Final_BBall_Stats2_Visitor_Mnemonic_NoLineBreak') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Name_NoLineBreak') then
      FinalBBallStats := GetGameFinalBBallStats(CurrentGameData.League, CurrentGameData.GameID, CurrentGameData.VStatsIncID, 2);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(FinalBBallStats.Name_Last) <> '') then
      begin
        if (CurrentGameData.VScore > CurrentGameData.HScore) then
        begin
          if (SymbolName = '$Final_BBall_Stats1_Visitor_Mnemonic') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Mnemonic') or
             (SymbolName = '$Final_BBall_Stats1_Visitor_Mnemonic_NoLineBreak') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Mnemonic_NoLineBreak') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + TeamNameMnemonicSuffix
          else if (SymbolName = '$Final_BBall_Stats1_Visitor_Name') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Name') or
                  (SymbolName = '$Final_BBall_Stats1_Visitor_Name_NoLineBreak') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Name_NoLineBreak') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName) + TeamNameMnemonicSuffix;
        end
        else begin
          if (SymbolName = '$Final_BBall_Stats1_Visitor_Mnemonic') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Mnemonic') or
             (SymbolName = '$Final_BBall_Stats1_Visitor_Mnemonic_NoLineBreak') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Mnemonic_NoLineBreak')then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VMnemonic) + TeamNameMnemonicSuffix
          else if (SymbolName = '$Final_BBall_Stats1_Visitor_Name') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Name') or
                  (SymbolName = '$Final_BBall_Stats1_Visitor_Name_NoLineBreak') OR (SymbolName = '$Final_BBall_Stats2_Visitor_Name_NoLineBreak')
          then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VName) + TeamNameMnemonicSuffix;
        end;
        OutStr := OutStr + '[f 2][c 1]' + Trim(FinalBBallStats.Name_Last);
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) and (Pos('NoLineBreak', SymbolName) = 0) then
          OutStr := OutStr + PlayerNameSuffix + '|'
        else
          OutStr := OutStr + PlayerNameSuffix;
        //Add points & rebounds
        OutStr := OutStr + '[f 2][c 1]' + Trim(FinalBBallStats.Points) + ' ' + ABBV_Points;
        //Add rebounds
        if (Trim(FinalBBallStats.Rebounds) <> '') AND (FinalBBallStats.Rebounds <> '0') then
          OutStr := OutStr + ', ' + Trim(FinalBBallStats.Rebounds) + ' ' + ABBV_Rebounds;
        //Add assists
        if (Trim(FinalBBallStats.Assists) <> '') AND (FinalBBallStats.Assists <> '0') then
          OutStr := OutStr + ', ' + Trim(FinalBBallStats.Assists) + ' ' + ABBV_Assists;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Basketball stats - Home (can be 1st or 2nd in list)
  else if (SymbolName = '$Final_BBall_Stats1_Home_Mnemonic') OR (SymbolName = '$Final_BBall_Stats2_Home_Mnemonic') OR
          (SymbolName = '$Final_BBall_Stats1_Home_Mnemonic_NoLineBreak') OR (SymbolName = '$Final_BBall_Stats2_Home_Mnemonic_NoLineBreak') OR
          (SymbolName = '$Final_BBall_Stats1_Home_Name') OR (SymbolName = '$Final_BBall_Stats2_Home_Name') OR
          (SymbolName = '$Final_BBall_Stats1_Home_Name_NoLineBreak') OR (SymbolName = '$Final_BBall_Stats2_Home_Name_NoLineBreak') then
  begin
    if (SymbolName = '$Final_BBall_Stats1_Home_Mnemonic') OR (SymbolName = '$Final_BBall_Stats1_Home_Name') or
       (SymbolName = '$Final_BBall_Stats1_Home_Mnemonic_NoLineBreak') OR (SymbolName = '$Final_BBall_Stats1_Home_Name_NoLineBreak')then
      FinalBBallStats := GetGameFinalBBallStats(CurrentGameData.League, CurrentGameData.GameID, CurrentGameData.HStatsIncID, 1)
    else if (SymbolName = '$Final_BBall_Stats2_Home_Mnemonic') OR (SymbolName = '$Final_BBall_Stats2_Home_Name') or
            (SymbolName = '$Final_BBall_Stats2_Home_Mnemonic_NoLineBreak') OR (SymbolName = '$Final_BBall_Stats2_Home_Name_NoLineBreak') then
      FinalBBallStats := GetGameFinalBBallStats(CurrentGameData.League, CurrentGameData.GameID, CurrentGameData.HStatsIncID, 2);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(FinalBBallStats.Name_Last) <> '') then
      begin
        if (CurrentGameData.HScore > CurrentGameData.VScore) then
        begin
          if (SymbolName = '$Final_BBall_Stats1_Home_Mnemonic') OR (SymbolName = '$Final_BBall_Stats2_Home_Mnemonic') or
             (SymbolName = '$Final_BBall_Stats1_Home_Mnemonic_NoLineBreak') OR (SymbolName = '$Final_BBall_Stats2_Home_Mnemonic_NoLineBreak') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + TeamNameMnemonicSuffix
          else if (SymbolName = '$Final_BBall_Stats1_Home_Name') OR (SymbolName = '$Final_BBall_Stats2_Home_Name') or
                  (SymbolName = '$Final_BBall_Stats1_Home_Name_NoLineBreak') OR (SymbolName = '$Final_BBall_Stats2_Home_Name_NoLineBreak')then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName) + TeamNameMnemonicSuffix;
        end
        else begin
          if (SymbolName = '$Final_BBall_Stats1_Home_Mnemonic') OR (SymbolName = '$Final_BBall_Stats2_Home_Mnemonic') or
             (SymbolName = '$Final_BBall_Stats1_Home_Mnemonic_NoLineBreak') OR (SymbolName = '$Final_BBall_Stats2_Home_Mnemonic_NoLineBreak')then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HMnemonic) + TeamNameMnemonicSuffix
          else if (SymbolName = '$Final_BBall_Stats1_Home_Name') OR (SymbolName = '$Final_BBall_Stats2_Home_Name') or
                  (SymbolName = '$Final_BBall_Stats1_Home_Name_NoLineBreak') OR (SymbolName = '$Final_BBall_Stats2_Home_Name_NoLineBreak') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HName) + TeamNameMnemonicSuffix;
        end;
        OutStr := OutStr + '[f 2][c 1]' + Trim(FinalBBallStats.Name_Last);
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) and (Pos('NoLineBreak', SymbolName) = 0) then
          OutStr := OutStr + PlayerNameSuffix + '|'
        else
          OutStr := OutStr + PlayerNameSuffix;
        //Add points & rebounds
        OutStr := OutStr + '[f 2][c 1]' + Trim(FinalBBallStats.Points) + ' ' + ABBV_Points;
        if (Trim(FinalBBallStats.Rebounds) <> '') AND (FinalBBallStats.Rebounds <> '0') then
          OutStr := OutStr + ', ' + Trim(FinalBBallStats.Rebounds) + ' ' + ABBV_Rebounds;
        //Add assists
        if (Trim(FinalBBallStats.Assists) <> '') AND (FinalBBallStats.Assists <> '0') then
          OutStr := OutStr + ', ' + Trim(FinalBBallStats.Assists) + ' ' + ABBV_Assists;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //////////////////////////////////////////////////////////////////////////////
  //NHL Hero Notes Functions
  //////////////////////////////////////////////////////////////////////////////
  //Skater stats - Visitor (can be 1st or 2nd in list)
  else if (SymbolName = '$Final_Skater_Stats1_Visitor') OR (SymbolName = '$Final_Skater_Stats2_Visitor') then
  begin
    if (SymbolName = '$Final_Skater_Stats1_Visitor') then
      FinalSkaterStats := GetGameFinalSkaterStats(CurrentGameData.GameID, CurrentGameData.VStatsIncID, 1)
    else if (SymbolName = '$Final_Skater_Stats2_Visitor') then
      FinalSkaterStats := GetGameFinalSkaterStats(CurrentGameData.GameID, CurrentGameData.VStatsIncID, 2);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(FinalSkaterStats.Name_Last) <> '') then
      begin
        if (CurrentGameData.VScore > CurrentGameData.HScore) then
        begin
          OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + TeamNameMnemonicSuffix +
                    '[f 2][c 1]' + Trim(FinalSkaterStats.Name_Last);
          //Add goals & assists
          if (Trim(FinalSkaterStats.Goals) = '1') then
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + ABBV_Goals
          else
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + Trim(FinalSkaterStats.Goals) + ' ' + ABBV_Goals + 's';
          if (Trim(FinalSkaterStats.Assists) <> '') AND (FinalSkaterStats.Assists <> '0') then
          begin
            if (Trim(FinalSkaterStats.Assists) = '1') then
              OutStr := OutStr + ', ' + ABBV_Assists
            else
              OutStr := OutStr + ', ' + Trim(FinalSkaterStats.Assists) + ' ' + ABBV_Assists + 's';
          end;
        end
        else begin
          OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VMnemonic) + TeamNameMnemonicSuffix +
                    '[f 2][c 1]' + Trim(FinalSkaterStats.Name_Last);
           //Add goals & assists
          if (Trim(FinalSkaterStats.Goals) = '1') then
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + ABBV_Goals
          else
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + Trim(FinalSkaterStats.Goals) + ' ' + ABBV_Goals + 's';
          if (Trim(FinalSkaterStats.Assists) <> '') AND (FinalSkaterStats.Assists <> '0') then
          begin
            if (Trim(FinalSkaterStats.Assists) = '1') then
              OutStr := OutStr + ', ' + ABBV_Assists
            else
              OutStr := OutStr + ', ' + Trim(FinalSkaterStats.Assists) + ' ' + ABBV_Assists + 's';
          end;
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Skater stats - Home (can be 1st or 2nd in list)
  else if (SymbolName = '$Final_Skater_Stats1_Home') OR (SymbolName = '$Final_Skater_Stats2_Home') then
  begin
    if (SymbolName = '$Final_Skater_Stats1_Home') then
      FinalSkaterStats := GetGameFinalSkaterStats(CurrentGameData.GameID, CurrentGameData.HStatsIncID, 1)
    else if (SymbolName = '$Final_Skater_Stats2_Home') then
      FinalSkaterStats := GetGameFinalSkaterStats(CurrentGameData.GameID, CurrentGameData.HStatsIncID, 2);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(FinalSkaterStats.Name_Last) <> '') then
      begin
        if (CurrentGameData.HScore > CurrentGameData.VScore) then
        begin
          OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + TeamNameMnemonicSuffix +
                    '[f 2][c 1]' + Trim(FinalSkaterStats.Name_Last);
           //Add goals & assists
          if (Trim(FinalSkaterStats.Goals) = '1') then
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + ABBV_Goals
          else
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + Trim(FinalSkaterStats.Goals) + ' ' + ABBV_Goals + 's';
          if (Trim(FinalSkaterStats.Assists) <> '') AND (FinalSkaterStats.Assists <> '0') then
          begin
            if (Trim(FinalSkaterStats.Assists) = '1') then
              OutStr := OutStr + ', ' + ABBV_Assists
            else
              OutStr := OutStr + ', ' + Trim(FinalSkaterStats.Assists) + ' ' + ABBV_Assists + 's';
          end;
        end
        else begin
          OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HMnemonic) + TeamNameMnemonicSuffix +
                    '[f 2][c 1]' + Trim(FinalSkaterStats.Name_Last);
           //Add goals & assists
          if (Trim(FinalSkaterStats.Goals) = '1') then
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + ABBV_Goals
          else
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + Trim(FinalSkaterStats.Goals) + ' ' + ABBV_Goals + 's';
          if (Trim(FinalSkaterStats.Assists) <> '') AND (FinalSkaterStats.Assists <> '0') then
          begin
            if (Trim(FinalSkaterStats.Assists) = '1') then
              OutStr := OutStr + ', ' + ABBV_Assists
            else
              OutStr := OutStr + ', ' + Trim(FinalSkaterStats.Assists) + ' ' + ABBV_Assists + 's';
          end;
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Goalie stats - Visitor (can be 1st or 2nd in list)
  else if (SymbolName = '$Final_Goalie_Stats1_Visitor') OR (SymbolName = '$Final_Goalie_Stats2_Visitor') then
  begin
    if (SymbolName = '$Final_Goalie_Stats1_Visitor') then
      FinalGoalieStats := GetGameFinalGoalieStats(CurrentGameData.GameID, CurrentGameData.VStatsIncID, 1)
    else if (SymbolName = '$Final_Goalie_Stats2_Visitor') then
      FinalGoalieStats := GetGameFinalGoalieStats(CurrentGameData.GameID, CurrentGameData.VStatsIncID, 2);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(FinalGoalieStats.Name_Last) <> '') then
      begin
        if (CurrentGameData.VScore > CurrentGameData.HScore) then
        begin
          OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + TeamNameMnemonicSuffix +
                    '[f 2][c 1]' + Trim(FinalGoalieStats.Name_Last);
          //Add saves
          if (Trim(FinalGoalieStats.Saves) = '1') then
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + ABBV_Saves
          else
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + Trim(FinalGoalieStats.Saves) + ' ' + ABBV_Saves + 's';
        end
        else begin
          OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VMnemonic) + TeamNameMnemonicSuffix +
                    '[f 2][c 1]' + Trim(FinalGoalieStats.Name_Last);
          //Add saves
          if (Trim(FinalGoalieStats.Saves) = '1') then
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + ABBV_Saves
          else
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + Trim(FinalGoalieStats.Saves) + ' ' + ABBV_Saves + 's';
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Goalie stats - Home (can be 1st or 2nd in list)
  else if (SymbolName = '$Final_Goalie_Stats1_Home') OR (SymbolName = '$Final_Goalie_Stats2_Home') then
  begin
    if (SymbolName = '$Final_Goalie_Stats1_Home') then
      FinalGoalieStats := GetGameFinalGoalieStats(CurrentGameData.GameID, CurrentGameData.HStatsIncID, 1)
    else if (SymbolName = '$Final_Skater_Stats2_Home') then
      FinalGoalieStats := GetGameFinalGoalieStats(CurrentGameData.GameID, CurrentGameData.HStatsIncID, 2);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(FinalGoalieStats.Name_Last) <> '') then
      begin
        if (CurrentGameData.HScore > CurrentGameData.VScore) then
        begin
          OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + TeamNameMnemonicSuffix +
                    '[f 2][c 1]' + Trim(FinalGoalieStats.Name_Last);
          //Add saves
          if (Trim(FinalGoalieStats.Saves) = '1') then
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + ABBV_Saves
          else
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + Trim(FinalGoalieStats.Saves) + ' ' + ABBV_Saves + 's';
        end
        else begin
          OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HMnemonic) + TeamNameMnemonicSuffix +
                    '[f 2][c 1]' + Trim(FinalGoalieStats.Name_Last);
          //Add saves
          if (Trim(FinalGoalieStats.Saves) = '1') then
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + ABBV_Saves
          else
            OutStr := OutStr + '[f 2][c 1]' + PlayerNameSuffix + Trim(FinalGoalieStats.Saves) + ' ' + ABBV_Saves + 's';
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //////////////////////////////////////////////////////////////////////////////
  //NFL Hero Notes functions
  //////////////////////////////////////////////////////////////////////////////
  //Quarterback stats - Visitor
  else if (SymbolName = '$Final_QB_Stats_Visitor_Mnemonic') or
          (SymbolName = '$Final_QB_Stats_Visitor_Name') or
          (SymbolName = '$Final_QB_Stats_Visitor_Mnemonic_NoLineBreak') or
          (SymbolName = '$Final_QB_Stats_Visitor_Name_NoLineBreak') then
  begin
    //Only request data if new game or league
    if (CurrentFBStatsRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentFBStatsRec.CurrentLeague <> CurrentGameData.League) then
      CurrentFBStatsRec := GetGameFinalFBStats(CurrentGameData.League, CurrentGameData.GameID);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(CurrentFBStatsRec.VPassingName_Last) <> '') then
      begin
        if (CurrentGameData.VScore > CurrentGameData.HScore) then
        begin
          if (SymbolName = '$Final_QB_Stats_Visitor_Mnemonic') or
             (SymbolName = '$Final_QB_Stats_Visitor_Mnemonic_NoLineBreak') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + TeamNameMnemonicSuffix
          else if (SymbolName = '$Final_QB_Stats_Visitor_Name') or
                  (SymbolName = '$Final_QB_Stats_Visitor_Name_NoLineBreak') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName) + TeamNameMnemonicSuffix;
        end
        else if (CurrentGameData.VScore < CurrentGameData.HScore) then
        begin
          if (SymbolName = '$Final_QB_Stats_Visitor_Mnemonic') or
             (SymbolName = '$Final_QB_Stats_Visitor_Mnemonic_NoLineBreak') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VMnemonic) + TeamNameMnemonicSuffix
          else if (SymbolName = '$Final_QB_Stats_Visitor_Name') or
                  (SymbolName = '$Final_QB_Stats_Visitor_Name_NoLineBreak') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VName) + TeamNameMnemonicSuffix;
        end;
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.VPassingName_Last) + PlayerNameSuffix;
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) and not
           ((SymbolName = '$Final_QB_Stats_Visitor_Mnemonic_NoLineBreak') or
            (SymbolName = '$Final_QB_Stats_Visitor_Name_NoLineBreak'))then
          OutStr := OutStr + '|';
        //Add attempts & completions
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.VPassingCompletions) +
                  '-' + Trim(CurrentFBStatsRec.VPassingAttempts);
        OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.VPassingYards) + ' ' + ABBV_Yards;
        //Add TDs if applicable
        if (Trim(CurrentFBStatsRec.VPassingTouchDowns) <> '') AND (Trim(CurrentFBStatsRec.VPassingTouchDowns) <> '0') then
        begin
          if (Trim(CurrentFBStatsRec.VPassingTouchDowns) = '1') then
            OutStr := OutStr + ', ' + ABBV_TDs
          else
            OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.VPassingTouchdowns) + ' ' + ABBV_TDs;
        end;
        //Add INTs if applicable
        if (Trim(CurrentFBStatsRec.VPassingInterceptions) <> '') AND (Trim(CurrentFBStatsRec.VPassingInterceptions) <> '0') then
        begin
          if (Trim(CurrentFBStatsRec.VPassingInterceptions) = '1') then
            OutStr := OutStr + ', ' + ABBV_INTs
          else
            OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.VPassingInterceptions) + ' ' + ABBV_INTs;
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Quarterback stats - Home
  else if (SymbolName = '$Final_QB_Stats_Home_Mnemonic') or
          (SymbolName = '$Final_QB_Stats_Home_Name') or
          (SymbolName = '$Final_QB_Stats_Home_Mnemonic_NoLineBreak') or
          (SymbolName = '$Final_QB_Stats_Home_Name_NoLineBreak') then
  begin
    //Only request data if new game or league
    if (CurrentFBStatsRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentFBStatsRec.CurrentLeague <> CurrentGameData.League) then
      CurrentFBStatsRec := GetGameFinalFBStats(CurrentGameData.League, CurrentGameData.GameID);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(CurrentFBStatsRec.HPassingName_Last) <> '') then
      begin
        if (CurrentGameData.HScore > CurrentGameData.VScore) then
        begin
          if (SymbolName = '$Final_QB_Stats_Home_Mnemonic') or
             (SymbolName = '$Final_QB_Stats_Home_Mnemonic_NoLineBreak') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + TeamNameMnemonicSuffix
          else if (SymbolName = '$Final_QB_Stats_Home_Name') or
                  (SymbolName = '$Final_QB_Stats_Home_Name_NoLineBreak')
          then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName) + TeamNameMnemonicSuffix;
        end
        else if (CurrentGameData.HScore < CurrentGameData.VScore) then
        begin
          if (SymbolName = '$Final_QB_Stats_Home_Mnemonic') or
             (SymbolName = '$Final_QB_Stats_Home_Mnemonic_NoLineBreak') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HMnemonic) + TeamNameMnemonicSuffix
          else if (SymbolName = '$Final_QB_Stats_Home_Name') or
                  (SymbolName = '$Final_QB_Stats_Home_Name_NoLineBreak') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HName) + TeamNameMnemonicSuffix;
        end;
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.HPassingName_Last) + PlayerNameSuffix;
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) and not
           ((SymbolName = '$Final_QB_Stats_Home_Mnemonic_NoLineBreak') or
            (SymbolName = '$Final_QB_Stats_Home_Name_NoLineBreak')) then
          OutStr := OutStr + '|';
        //Add attempts & completions
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.HPassingCompletions) +
                  '-' + Trim(CurrentFBStatsRec.HPassingAttempts);
        OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.HPassingYards) + ' ' + ABBV_Yards;
        //Add TDs if applicable
        if (Trim(CurrentFBStatsRec.HPassingTouchDowns) <> '') AND (Trim(CurrentFBStatsRec.HPassingTouchDowns) <> '0') then
        begin
          if (Trim(CurrentFBStatsRec.HPassingTouchDowns) = '1') then
            OutStr := OutStr + ', ' + ABBV_TDs
          else
            OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.HPassingTouchdowns) + ' ' + ABBV_TDs;
        end;
        //Add INTs if applicable
        if (Trim(CurrentFBStatsRec.HPassingInterceptions) <> '') AND (Trim(CurrentFBStatsRec.HPassingInterceptions) <> '0') then
        begin
          if (Trim(CurrentFBStatsRec.HPassingInterceptions) = '1') then
            OutStr := OutStr + ', ' + ABBV_INTs
          else
            OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.HPassingInterceptions) + ' ' + ABBV_INTs;
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Running back stats - Visitor
  else if (SymbolName = '$Final_RB_Stats_Visitor_Mnemonic') or
          (SymbolName = '$Final_RB_Stats_Visitor_Name') or
          (SymbolName = '$Final_RB_Stats_Visitor_Mnemonic_NoLineBreak') or
          (SymbolName = '$Final_RB_Stats_Visitor_Name_NoLineBreak') then
  begin
    //Only request data if new game or league
    if (CurrentFBStatsRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentFBStatsRec.CurrentLeague <> CurrentGameData.League) then
      CurrentFBStatsRec := GetGameFinalFBStats(CurrentGameData.League, CurrentGameData.GameID);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(CurrentFBStatsRec.VRushingName_Last) <> '') then
      begin
        if (CurrentGameData.VScore > CurrentGameData.HScore) then
        begin
          if (SymbolName = '$Final_RB_Stats_Visitor_Mnemonic') or
             (SymbolName = '$Final_RB_Stats_Visitor_Mnemonic_NoLineBreak') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + TeamNameMnemonicSuffix
          else if (SymbolName = '$Final_RB_Stats_Visitor_Name') or
                  (SymbolName = '$Final_RB_Stats_Visitor_Name_NoLineBreak') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName) + TeamNameMnemonicSuffix;
        end
        else if (CurrentGameData.VScore < CurrentGameData.HScore) then
        begin
          if (SymbolName = '$Final_RB_Stats_Visitor_Mnemonic') or
             (SymbolName = '$Final_RB_Stats_Visitor_Mnemonic_NoLineBreak') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VMnemonic) + TeamNameMnemonicSuffix
          else if (SymbolName = '$Final_RB_Stats_Visitor_Name') or
                  (SymbolName = '$Final_RB_Stats_Visitor_Name_NoLineBreak') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VName) + TeamNameMnemonicSuffix;
        end;
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.VRushingName_Last) + PlayerNameSuffix;
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) and not
           ((SymbolName = '$Final_RB_Stats_Visitor_Mnemonic_NoLineBreak') or
            (SymbolName = '$Final_RB_Stats_Visitor_Name_NoLineBreak')) then
          OutStr := OutStr + '|';
        //Add attempts
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.VRushingAttempts) + ' ' + ABBV_Rushes;
        OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.VRushingYards) + ' ' + ABBV_Yards;
        //Add TDs if applicable
        if (Trim(CurrentFBStatsRec.VRushingTouchdowns) <> '') AND (Trim(CurrentFBStatsRec.VRushingTouchdowns) <> '0') then
        begin
          if (Trim(CurrentFBStatsRec.VRushingTouchDowns) = '1') then
            OutStr := OutStr + ', ' + ABBV_TDs
          else
            OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.VRushingTouchdowns) + ' ' + ABBV_TDs;
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Running back stats - Home
  else if (SymbolName = '$Final_RB_Stats_Home_Mnemonic') or
          (SymbolName = '$Final_RB_Stats_Home_Name') or
          (SymbolName = '$Final_RB_Stats_Home_Mnemonic_NoLineBreak') or
          (SymbolName = '$Final_RB_Stats_Home_Name_NoLineBreak') then
  begin
    //Only request data if new game or league
    if (CurrentFBStatsRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentFBStatsRec.CurrentLeague <> CurrentGameData.League) then
      CurrentFBStatsRec := GetGameFinalFBStats(CurrentGameData.League, CurrentGameData.GameID);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(CurrentFBStatsRec.HRushingName_Last) <> '') then
      begin
        if (CurrentGameData.HScore > CurrentGameData.VScore) then
        begin
          if (SymbolName = '$Final_RB_Stats_Home_Mnemonic') or
             (SymbolName = '$Final_RB_Stats_Home_Mnemonic_NoLineBreak') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + TeamNameMnemonicSuffix
          else if (SymbolName = '$Final_RB_Stats_Home_Name') or
                  (SymbolName = '$Final_RB_Stats_Home_Name_NoLineBreak') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName) + TeamNameMnemonicSuffix;
        end
        else if (CurrentGameData.HScore < CurrentGameData.VScore) then
        begin
          if (SymbolName = '$Final_RB_Stats_Home_Mnemonic') or
             (SymbolName = '$Final_RB_Stats_Home_Mnemonic_NoLineBreak') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HMnemonic) + TeamNameMnemonicSuffix
          else if (SymbolName = '$Final_RB_Stats_Home_Name') or
                  (SymbolName = '$Final_RB_Stats_Home_Name_NoLineBreak') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HName) + TeamNameMnemonicSuffix;
        end;
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.HRushingName_Last) + PlayerNameSuffix;
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) and not
           ((SymbolName = '$Final_RB_Stats_Home_Mnemonic_NoLineBreak') or
            (SymbolName = '$Final_RB_Stats_Home_Name_NoLineBreak'))
        then
          OutStr := OutStr + '|';
        //Add attempts
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.HRushingAttempts) + ' ' + ABBV_Rushes;
        OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.HRushingYards) + ' ' + ABBV_Yards;
        //Add TDs if applicable
        if (Trim(CurrentFBStatsRec.HRushingTouchdowns) <> '') AND (Trim(CurrentFBStatsRec.HRushingTouchdowns) <> '0') then
        begin
          if (Trim(CurrentFBStatsRec.HRushingTouchDowns) = '1') then
            OutStr := OutStr + ', ' + ABBV_TDs
          else
            OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.HRushingTouchdowns) + ' ' + ABBV_TDs;
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Wide Receiver stats - Visitor
  else if (SymbolName = '$Final_WR_Stats_Visitor_Mnemonic') or
          (SymbolName = '$Final_WR_Stats_Visitor_Name') or
          (SymbolName = '$Final_WR_Stats_Visitor_Mnemonic_NoLineBreak') or
          (SymbolName = '$Final_WR_Stats_Visitor_Name_NoLineBreak') then
  begin
    //Only request data if new game or league
    if (CurrentFBStatsRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentFBStatsRec.CurrentLeague <> CurrentGameData.League) then
      CurrentFBStatsRec := GetGameFinalFBStats(CurrentGameData.League, CurrentGameData.GameID);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(CurrentFBStatsRec.VReceivingName_Last) <> '') then
      begin
        if (CurrentGameData.VScore > CurrentGameData.HScore) then
        begin
          if (SymbolName = '$Final_WR_Stats_Visitor_Mnemonic') or
             (SymbolName = '$Final_WR_Stats_Visitor_Mnemonic_NoLineBreak') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VMnemonic) + TeamNameMnemonicSuffix
          else if (SymbolName = '$Final_WR_Stats_Visitor_Name') or
                  (SymbolName = '$Final_WR_Stats_Visitor_Name_NoLineBreak') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.VName) + TeamNameMnemonicSuffix;
        end
        else if (CurrentGameData.VScore < CurrentGameData.HScore) then
        begin
          if (SymbolName = '$Final_WR_Stats_Visitor_Mnemonic') or
             (SymbolName = '$Final_WR_Stats_Visitor_Mnemonic_NoLineBreak') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VMnemonic) + TeamNameMnemonicSuffix
          else if (SymbolName = '$Final_WR_Stats_Visitor_Name') or
                  (SymbolName = '$Final_WR_Stats_Visitor_Name_NoLineBreak') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.VName) + TeamNameMnemonicSuffix;
        end;
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.VReceivingName_Last) + PlayerNameSuffix;
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) and not
           ((SymbolName = '$Final_WR_Stats_Visitor_Mnemonic_NoLineBreak') or
            (SymbolName = '$Final_WR_Stats_Visitor_Name_NoLineBreak')) then
          OutStr := OutStr + '|';
        //Add attempts
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.VReceivingReceptions) + ' ' + ABBV_Receptions;
        OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.VReceivingYards) + ' ' + ABBV_Yards;
        //Add TDs if applicable
        if (Trim(CurrentFBStatsRec.VReceivingTouchdowns) <> '') AND (Trim(CurrentFBStatsRec.VReceivingTouchdowns) <> '0') then
        begin
          if (Trim(CurrentFBStatsRec.VReceivingTouchDowns) = '1') then
            OutStr := OutStr + ', ' + ABBV_TDs
          else
            OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.VReceivingTouchdowns) + ' ' + ABBV_TDs;
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //Wide Receiver stats - Home
  else if (SymbolName = '$Final_WR_Stats_Home_Mnemonic') or
          (SymbolName = '$Final_WR_Stats_Home_Name') or
          (SymbolName = '$Final_WR_Stats_Home_Mnemonic_NoLineBreak') or
          (SymbolName = '$Final_WR_Stats_Home_Name_NoLineBreak') then
  begin
    //Only request data if new game or league
    if (CurrentFBStatsRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentFBStatsRec.CurrentLeague <> CurrentGameData.League) then
      CurrentFBStatsRec := GetGameFinalFBStats(CurrentGameData.League, CurrentGameData.GameID);
    //Check for game state final
    if (CurrentGameData.State = FINAL) then
    begin
      if (Trim(CurrentFBStatsRec.HReceivingName_Last) <> '') then
      begin
        if (CurrentGameData.HScore > CurrentGameData.VScore) then
        begin
          if (SymbolName = '$Final_WR_Stats_Home_Mnemonic') or
             (SymbolName = '$Final_WR_Stats_Home_Mnemonic_NoLineBreak') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HMnemonic) + TeamNameMnemonicSuffix
          else if (SymbolName = '$Final_WR_Stats_Home_Name') or
                  (SymbolName = '$Final_WR_Stats_Home_Name_NoLineBreak') then
            OutStr := '[f 2][c 19]' + Trim(CurrentGameData.HName) + TeamNameMnemonicSuffix;
        end
        else if (CurrentGameData.HScore < CurrentGameData.VScore) then
        begin
          if (SymbolName = '$Final_WR_Stats_Home_Mnemonic') or
             (SymbolName = '$Final_WR_Stats_Home_Mnemonic_NoLineBreak') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HMnemonic) + TeamNameMnemonicSuffix
          else if (SymbolName = '$Final_WR_Stats_Home_Name') or
                  (SymbolName = '$Final_WR_Stats_Home_Name_NoLineBreak') then
            OutStr := '[f 2][c 1]' + Trim(CurrentGameData.HName) + TeamNameMnemonicSuffix;
        end;
        //Build stats part of string
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.HReceivingName_Last) + PlayerNameSuffix;
        //Line-wrap
        if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) and not
           ((SymbolName = '$Final_WR_Stats_Home_Mnemonic_NoLineBreak') or
            (SymbolName = '$Final_WR_Stats_Home_Name_NoLineBreak')) then
          OutStr := OutStr + '|';
        //Add attempts
        OutStr := OutStr + '[f 2][c 1]' + Trim(CurrentFBStatsRec.HReceivingReceptions) + ' ' + ABBV_Receptions;
        OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.HReceivingYards) + ' ' + ABBV_Yards;
        //Add TDs if applicable
        if (Trim(CurrentFBStatsRec.HReceivingTouchdowns) <> '') AND (Trim(CurrentFBStatsRec.HReceivingTouchdowns) <> '0') then
        begin
          if (Trim(CurrentFBStatsRec.HReceivingTouchDowns) = '1') then
            OutStr := OutStr + ', ' + ABBV_TDs
          else
            OutStr := OutStr + ', ' + Trim(CurrentFBStatsRec.HReceivingTouchdowns) + ' ' + ABBV_TDs;
        end;
      end
      else OutStr := ' ';
    end
    else OutStr := ' ';
  end

  //////////////////////////////////////////////////////////////////////////////
  //NFL Fantasy Stats
  //////////////////////////////////////////////////////////////////////////////
  //QB fantasy stats
  else if (SymbolName = '$NFL_Fantasy_Stats_QB') or (SymbolName = '$NFL_Fantasy_Stats_QB_Next') then
  begin
    //Only request data if not for current date
    if (CurrentFantasyFootballPassingStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyFootballPassingStatsRec := GetFantasyFootballPassingStats;
    with CurrentFantasyFootballPassingStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);
        //Increment index if "Next" record
        if (Pos('_Next', SymbolName) <> 0) then Inc(Index);

        //Format data string
        if (FormatForSNY) then
        begin
          //Build string
          OutStr := '[f 2][c 19]' + IntToStr(Index) + '. ';
          //Build stats part of string
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          OutStr := OutStr + '(' + Trim(GetTeamMnemonicFromID('NFL', TeamID[Index])) + ') ';
          //Add attempts & completions
          //OutStr := OutStr + '[f 2][c 1]' + Trim(Completions[Index]) +
          //          '-' + Trim(Attempts[Index]);
          //OutStr := OutStr + ', ' + Trim(PassingYards[Index]) + ' ' + ABBV_Yards;
          OutStr := OutStr + Trim(PassingYards[Index]) + ' ' + ABBV_Yards;
          //Add TDs if applicable
          if (Trim(PassingTDs[Index]) <> '') AND (Trim(PassingTDs[Index]) <> '0') then
          begin
            if (Trim(PassingTDs[Index]) = '1') then
              OutStr := OutStr + ', ' + ABBV_TDs
            else
              OutStr := OutStr + ', ' + Trim(PassingTDs[Index]) + ' ' + ABBV_TDs;
          end;
        end
        else begin
          //Build string
          OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('NFL', TeamID[Index])) + ': ';
          //Build stats part of string
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          //Line-wrap
          if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
            OutStr := OutStr + '|';
          //Add attempts & completions
          OutStr := OutStr + '[f 2][c 1]' + Trim(Completions[Index]) +
                    '-' + Trim(Attempts[Index]);
          OutStr := OutStr + ', ' + Trim(PassingYards[Index]) + ' ' + ABBV_Yards;
          //Add TDs if applicable
          if (Trim(PassingTDs[Index]) <> '') AND (Trim(PassingTDs[Index]) <> '0') then
          OutStr := OutStr + ', ' + Trim(PassingTDs[Index]) + ' ' + ABBV_TDs;
        end;
      end
      else OutStr := '';
    end;
  end

  //RB fantasy stats
  else if (SymbolName = '$NFL_Fantasy_Stats_RB') or (SymbolName = '$NFL_Fantasy_Stats_RB_Next') then
  begin
    //Only request data if not for current date
    if (CurrentFantasyFootballRushingStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyFootballRushingStatsRec := GetFantasyFootballRushingStats;
    with CurrentFantasyFootballRushingStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);
        //Increment index if "Next" record
        if (Pos('_Next', SymbolName) <> 0) then Inc(Index);
        //Format data string
        if (FormatForSNY) then
        begin
          //Build string
          OutStr := '[f 2][c 19]' + IntToStr(Index) + '. ';
          //Build stats part of string
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          OutStr := OutStr + '(' + Trim(GetTeamMnemonicFromID('NFL', TeamID[Index])) + ') ';
          //Rushes
          //OutStr := OutStr + '[f 2][c 1]' + Trim(Rushes[Index]) + ' ' + ABBV_Rushes;
          //Yards
          //OutStr := OutStr + ', ' + Trim(Yards[Index]) + ' ' + ABBV_Yards;
          OutStr := OutStr + Trim(Yards[Index]) + ' ' + ABBV_Yards;
          //Add TDs if applicable
          if (Trim(TDs[Index]) <> '') AND (Trim(TDs[Index]) <> '0') then
          begin
            if (Trim(TDs[Index]) = '1') then
              OutStr := OutStr + ', ' + ABBV_TDs
            else
              OutStr := OutStr + ', ' + Trim(TDs[Index]) + ' ' + ABBV_TDs;
          end;
        end
        else begin
          OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('NFL', TeamID[Index])) + ': ';
          //Build stats part of string
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          //Line-wrap
          if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
            OutStr := OutStr + '|';
          //Rushes
          OutStr := OutStr + '[f 2][c 1]' + Trim(Rushes[Index]) + ' ' + ABBV_Rushes;
          //Yards
          OutStr := OutStr + ', ' + Trim(Yards[Index]) + ' ' + ABBV_Yards;
          //Add TDs if applicable
          if (Trim(TDs[Index]) <> '') AND (Trim(TDs[Index]) <> '0') then
          OutStr := OutStr + ', ' + Trim(TDs[Index]) + ' ' + ABBV_TDs;
        end;
      end
      else OutStr := '';
    end;
  end

  //WR fantasy stats
  else if (SymbolName = '$NFL_Fantasy_Stats_WR') or (SymbolName = '$NFL_Fantasy_Stats_WR_Next') then
  begin
    //Only request data if not for current date
    if (CurrentFantasyFootballReceivingStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyFootballReceivingStatsRec := GetFantasyFootballReceivingStats;
    with CurrentFantasyFootballReceivingStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);
        //Increment index if "Next" record
        if (Pos('_Next', SymbolName) <> 0) then Inc(Index);
        //Format data string
        if (FormatForSNY) then
        begin
          //Build string
          OutStr := '[f 2][c 19]' + IntToStr(Index) + '. ';
          //Build stats part of string
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          OutStr := OutStr + '(' + Trim(GetTeamMnemonicFromID('NFL', TeamID[Index])) + ') ';
          //Receptions
          //OutStr := OutStr + '[f 2][c 1]' + Trim(Receptions[Index]) + ' ' + ABBV_Receptions;
          //Yards
          //OutStr := OutStr + ', ' + Trim(Yards[Index]) + ' ' + ABBV_Yards;
          OutStr := OutStr + Trim(Yards[Index]) + ' ' + ABBV_Yards;
          //Add TDs if applicable
          if (Trim(TDs[Index]) <> '') AND (Trim(TDs[Index]) <> '0') then
          begin
            if (Trim(TDs[Index]) = '1') then
              OutStr := OutStr + ', ' + ABBV_TDs
            else
              OutStr := OutStr + ', ' + Trim(TDs[Index]) + ' ' + ABBV_TDs;
          end;
        end
        else begin
          //Build string
          OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('NFL', TeamID[Index])) + ': ';
          //Build stats part of string
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          //Line-wrap
          if ((TickerDisplayMode = 3) OR (TickerDisplayMode = 4)) then
            OutStr := OutStr + '|';
          //Receptions
          OutStr := OutStr + '[f 2][c 1]' + Trim(Receptions[Index]) + ' ' + ABBV_Receptions;
          //Yards
          OutStr := OutStr + ', ' + Trim(Yards[Index]) + ' ' + ABBV_Yards;
          //Add TDs if applicable
          if (Trim(TDs[Index]) <> '') AND (Trim(TDs[Index]) <> '0') then
          OutStr := OutStr + ', ' + Trim(TDs[Index]) + ' ' + ABBV_TDs;
        end;
      end
      else OutStr := '';
    end;
  end

  //////////////////////////////////////////////////////////////////////////////
  //MLB Fantasy Stats
  //////////////////////////////////////////////////////////////////////////////
  //Batters
  else if (SymbolName = '$MLB_Fantasy_Stats_Batters') or (SymbolName = '$MLB_Fantasy_Stats_Batters_Next') then
  begin
    //Only request data if not for current date
    if (CurrentFantasyBaseballBattingStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyBaseballBattingStatsRec := GetFantasyBaseballBattingStats;
    with CurrentFantasyBaseballBattingStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);
        //Increment index if "Next" record
        if (Pos('_Next', SymbolName) <> 0) then Inc(Index);
        //Format data string
        if (FormatForSNY) then
        begin
          //Build string
          OutStr := '[f 2][c 19]' + IntToStr(Index) + '. ';
          //Build stats part of string
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          OutStr := OutStr + '(' + Trim(GetTeamMnemonicFromID('NFL', TeamID[Index])) + ') ';
          //Hits-AtBats
          OutStr := OutStr + '[f 2][c 1]' + Trim(Hits[Index]) + '-' +
            Trim(AtBats[Index]);
          //Add HomeRuns if applicable
          if (Trim(HomeRuns[Index]) <> '') AND (Trim(HomeRuns[Index]) <> '0') then
          begin
            if (Trim(HomeRuns[Index]) = '1') then
              OutStr := OutStr + ', ' + 'HR'
            else
              OutStr := OutStr + ', ' + Trim(HomeRuns[Index]) + ' HR';
          end;
          //Add RBI if applicable
          if (Trim(RBI[Index]) <> '') AND (Trim(RBI[Index]) <> '0') then
          begin
            if (Trim(RBI[Index]) = '1') then
              OutStr := OutStr + ', ' + 'RBI'
            else
              OutStr := OutStr + ', ' + Trim(RBI[Index]) + ' RBI';
          end;
          //Add Stolen Bases if applicable
          if (Trim(StolenBases[Index]) <> '') AND (Trim(StolenBases[Index]) <> '0') then
          begin
            if (Trim(StolenBases[Index]) = '1') then
              OutStr := OutStr + ', ' + 'SB'
            else
              OutStr := OutStr + ', ' + Trim(StolenBases[Index]) + ' SB'
          end;
        end
        else begin
          //Build string
          OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('MLB', TeamID[Index])) + ': ';
          //Build stats part of string
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          //Hits-AtBats
          OutStr := OutStr + '[f 2][c 1]' + Trim(Hits[Index]) + '-' +
            Trim(AtBats[Index]);
          //Add HomeRuns if applicable
          if (Trim(HomeRuns[Index]) <> '') AND (Trim(HomeRuns[Index]) <> '0') then
          OutStr := OutStr + ', ' + Trim(HomeRuns[Index]) + ' HR';
          //Add RBI if applicable
          if (Trim(RBI[Index]) <> '') AND (Trim(RBI[Index]) <> '0') then
          OutStr := OutStr + ', ' + Trim(RBI[Index]) + ' RBI';
          //Add Stolen Bases if applicable
          if (Trim(StolenBases[Index]) <> '') AND (Trim(StolenBases[Index]) <> '0') then
          OutStr := OutStr + ', ' + Trim(StolenBases[Index]) + ' SB';
        end;
      end
      else OutStr := '';
    end;
  end

  //Pitchers
  else if (SymbolName = '$MLB_Fantasy_Stats_Pitchers') or (SymbolName = '$MLB_Fantasy_Stats_Pitchers_Next') then
  begin
    //Only request data if not for current date
    if (CurrentFantasyBaseballPitchingStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyBaseballPitchingStatsRec := GetFantasyBaseballPitchingStats;
    with CurrentFantasyBaseballPitchingStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);
        //Increment index if "Next" record
        if (Pos('_Next', SymbolName) <> 0) then Inc(Index);
        //Format data string
        if (FormatForSNY) then
        begin
          //Build string
          OutStr := '[f 2][c 19]' + IntToStr(Index) + '. ';
          //Build stats part of string
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          OutStr := OutStr + '(' + Trim(GetTeamMnemonicFromID('NFL', TeamID[Index])) + ') ';
          //Show record
          OutStr := OutStr + '[f 2][c 1]' + Trim(Wins[Index]) + '-' + Trim(Losses[Index]);
          //Add Strikouts
          //OutStr := OutStr + ', ' + Trim(StrikeOuts[Index]) + ' K';
          //Add Saves if applicable
          if (Trim(Saves[Index]) <> '') AND (Trim(Saves[Index]) <> '0') then
          OutStr := OutStr + ', ' + Trim(Saves[Index]) + ' SV';
          //Add Earned Runs if applicable
          if (Trim(EarnedRuns[Index]) <> '') AND (Trim(EarnedRuns[Index]) <> '0') then
          OutStr := OutStr + ', ' + Trim(EarnedRuns[Index]) + ' ER';
        end
        else begin
          //Build string
          OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('MLB', TeamID[Index])) + ': ';
          //Build stats part of string
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          //Wins
          OutStr := OutStr + '[f 2][c 1]' + Trim(Wins[Index]) + ' W';
          //Add Strikouts
          OutStr := OutStr + ', ' + Trim(StrikeOuts[Index]) + ' K';
          //Add Saves if applicable
          if (Trim(Saves[Index]) <> '') AND (Trim(Saves[Index]) <> '0') then
          OutStr := OutStr + ', ' + Trim(Saves[Index]) + ' SV';
          //Add Earned Runs if applicable
          if (Trim(EarnedRuns[Index]) <> '') AND (Trim(EarnedRuns[Index]) <> '0') then
          OutStr := OutStr + ', ' + Trim(EarnedRuns[Index]) + ' ER';
        end;
      end
      else OutStr := '';
    end;
  end

  //For specific MLB fantasy stats
  else if (SymbolName = '$MLB_Fantasy_Stats_Hits') or
          (SymbolName = '$MLB_Fantasy_Stats_Hits_Next') or
          (SymbolName = '$MLB_Fantasy_Stats_HRs') or
          (SymbolName = '$MLB_Fantasy_Stats_HRs_Next') or
          (SymbolName = '$MLB_Fantasy_Stats_RBIs') or
          (SymbolName = '$MLB_Fantasy_Stats_RBIs_Next') or
          (SymbolName = '$MLB_Fantasy_Stats_Runs_Scored') or
          (SymbolName = '$MLB_Fantasy_Stats_Runs_Scored_Next') or
          (SymbolName = '$MLB_Fantasy_Stats_Stolen_Bases') or
          (SymbolName = '$MLB_Fantasy_Stats_Stolen_Bases_Next') or
          (SymbolName = '$MLB_Fantasy_Stats_Wins') or
          (SymbolName = '$MLB_Fantasy_Stats_Wins_Next') or
          (SymbolName = '$MLB_Fantasy_Stats_Saves') or
          (SymbolName = '$MLB_Fantasy_Stats_Saves_Next') or
          (SymbolName = '$MLB_Fantasy_Stats_Strikeouts') or
          (SymbolName = '$MLB_Fantasy_Stats_Strikeouts_Next') or
          (SymbolName = '$MLB_Fantasy_Stats_Innings_Pitched') or
          (SymbolName = '$MLB_Fantasy_Stats_Innings_Pitched_Next') then
  begin
    //Get the latest data
    if ((SymbolName = '$MLB_Fantasy_Stats_Hits') or (SymbolName = '$MLB_Fantasy_Stats_Hits_Next')) and
       (CurrentFantasyBaseballHitsStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyBaseballHitsStatsRec := GetFantasyStats_General('p_SI_Get_MLB_Daily_Leaders', 'Hits', 'MLB', 'H')
    else if ((SymbolName = '$MLB_Fantasy_Stats_HRs') or (SymbolName = '$MLB_Fantasy_Stats_HRs_Next')) and
       (CurrentFantasyBaseballHomeRunsStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyBaseballHomeRunsStatsRec := GetFantasyStats_General('p_SI_Get_MLB_Daily_Leaders', 'Home Runs', 'MLB', 'HR')
    else if ((SymbolName = '$MLB_Fantasy_Stats_RBIs') or (SymbolName = '$MLB_Fantasy_Stats_RBIs_Next')) and
       (CurrentFantasyBaseballRBIsStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyBaseballRBIsStatsRec := GetFantasyStats_General('p_SI_Get_MLB_Daily_Leaders', 'RBIs', 'MLB', 'RBI')
    else if ((SymbolName = '$MLB_Fantasy_Stats_Runs_Scored') or (SymbolName = '$MLB_Fantasy_Stats_Runs_Scored_Next')) and
       (CurrentFantasyBaseballRunsScoredStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyBaseballRunsScoredStatsRec := GetFantasyStats_General('p_SI_Get_MLB_Daily_Leaders', 'Runs', 'MLB', 'R')
    else if ((SymbolName = '$MLB_Fantasy_Stats_Stolen_Bases') or (SymbolName = '$MLB_Fantasy_Stats_Stolen_Bases_Next')) and
       (CurrentFantasyBaseballStolenBasesStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyBaseballStolenBasesStatsRec := GetFantasyStats_General('p_SI_Get_MLB_Daily_Leaders', 'Stolen Bases', 'MLB', 'SB')
    else if ((SymbolName = '$MLB_Fantasy_Stats_Wins') or (SymbolName = '$MLB_Fantasy_Stats_Wins_Next')) and
       (CurrentFantasyBaseballWinsStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyBaseballWinsStatsRec := GetFantasyStats_General('p_SI_Get_MLB_Daily_Leaders', 'Wins', 'MLB', 'W')
    else if ((SymbolName = '$MLB_Fantasy_Stats_Saves') or (SymbolName = '$MLB_Fantasy_Stats_Saves_Next')) and
       (CurrentFantasyBaseballSavesStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyBaseballSavesStatsRec := GetFantasyStats_General('p_SI_Get_MLB_Daily_Leaders', 'Saves', 'MLB', 'S')
    else if ((SymbolName = '$MLB_Fantasy_Stats_Strikeouts') or (SymbolName = '$MLB_Fantasy_Stats_Strikeouts_Next')) and
       (CurrentFantasyBaseballStrikeOutsStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyBaseballStrikeoutsStatsRec := GetFantasyStats_General('p_SI_Get_MLB_Daily_Leaders', 'Strikeouts', 'MLB', 'K')
    else if ((SymbolName = '$MLB_Fantasy_Stats_Innings_Pitched') or (SymbolName = '$MLB_Fantasy_Stats_Innings_Pitched_Next')) and
       (CurrentFantasyBaseballInningsPitchedStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyBaseballInningsPitchedStatsRec := GetFantasyStats_General('p_SI_Get_MLB_Daily_Leaders', 'Innings Pitched', 'MLB', 'IP');

    //Setup temporary stats record
    if ((SymbolName = '$MLB_Fantasy_Stats_Hits') or (SymbolName = '$MLB_Fantasy_Stats_Hits_Next')) then
      TempFantasyGeneralStatsRec := CurrentFantasyBaseballHitsStatsRec
    else if ((SymbolName = '$MLB_Fantasy_Stats_HRs') or (SymbolName = '$MLB_Fantasy_Stats_HRs_Next')) then
      TempFantasyGeneralStatsRec := CurrentFantasyBaseballHomeRunsStatsRec
    else if ((SymbolName = '$MLB_Fantasy_Stats_RBIs') or (SymbolName = '$MLB_Fantasy_Stats_RBIs_Next')) then
      TempFantasyGeneralStatsRec := CurrentFantasyBaseballRBIsStatsRec
    else if ((SymbolName = '$MLB_Fantasy_Stats_Runs_Scored') or (SymbolName = '$MLB_Fantasy_Stats_Runs_Scored_Next')) then
      TempFantasyGeneralStatsRec := CurrentFantasyBaseballRunsScoredStatsRec
    else if ((SymbolName = '$MLB_Fantasy_Stats_Stolen_Bases') or (SymbolName = '$MLB_Fantasy_Stats_Stolen_Bases_Next')) then
      TempFantasyGeneralStatsRec := CurrentFantasyBaseballStolenBasesStatsRec
    else if ((SymbolName = '$MLB_Fantasy_Stats_Wins') or (SymbolName = '$MLB_Fantasy_Stats_Wins_Next')) then
      TempFantasyGeneralStatsRec := CurrentFantasyBaseballWinsStatsRec
    else if ((SymbolName = '$MLB_Fantasy_Stats_Saves') or (SymbolName = '$MLB_Fantasy_Stats_Saves_Next')) then
      TempFantasyGeneralStatsRec := CurrentFantasyBaseballSavesStatsRec
    else if ((SymbolName = '$MLB_Fantasy_Stats_Strikeouts') or (SymbolName = '$MLB_Fantasy_Stats_Strikeouts_Next')) then
      TempFantasyGeneralStatsRec := CurrentFantasyBaseballStrikeoutsStatsRec
    else if ((SymbolName = '$MLB_Fantasy_Stats_Innings_Pitched') or (SymbolName = '$MLB_Fantasy_Stats_Innings_Pitched_Next')) then
      TempFantasyGeneralStatsRec := CurrentFantasyBaseballInningsPitchedStatsRec;

    //Update record
    with TempFantasyGeneralStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);
        //Increment index if "Next" record
        if (Pos('_Next', SymbolName) <> 0) then Inc(Index);
        //Format data string
        if (FormatForSNY) then
        begin
          //Build string - CURRENTLY HARD-WIRED FOR MLB
          OutStr := '[f 2][c 19]' + IntToStr(Index) + '. ';
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          OutStr := OutStr + '(' + Trim(GetTeamMnemonicFromID(League, TeamID[Index])) + ') ';
          OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(StatValue[Index]);
        end
        else begin
          //Build string
          OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID(League, TeamID[Index])) + ': ';
          //Build stats part of string
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(StatValue[Index]);
        end;
      end
      else OutStr := '';
    end;
  end

  //////////////////////////////////////////////////////////////////////////////
  //NHL Fantasy Stats
  //////////////////////////////////////////////////////////////////////////////
  //Points
  else if (SymbolName = '$NHL_Fantasy_Stats_Points') or (SymbolName = '$NHL_Fantasy_Stats_Points_Next') then
  begin
    //Only request data if not for current date
    if (CurrentFantasyHockeyPointsStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyHockeyPointsStatsRec := GetFantasyHockeyPointsStats;
    with CurrentFantasyHockeyPointsStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);
        //Increment index if "Next" record
        if (Pos('_Next', SymbolName) <> 0) then Inc(Index);
        //Format data string
        if (FormatForSNY) then
        begin
          //Build string
          OutStr := '[f 2][c 19]' + IntToStr(Index) + '. ';
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          OutStr := OutStr + '(' + Trim(GetTeamMnemonicFromID('NHL', TeamID[Index])) + ') ';
          //Add points
          OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Points[Index]); // + ' ' + ABBV_Points;
        end
        else begin
          //Build string
          OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('NHL', TeamID[Index])) + ': ';
          //Build stats part of string
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          //Add points
          OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Points[Index]); // + ' ' + ABBV_Points;
        end;
      end
      else OutStr := '';
    end;
  end

  //Goals
  else if (SymbolName = '$NHL_Fantasy_Stats_Goals') or (SymbolName = '$NHL_Fantasy_Stats_Goals_Next') then
  begin
    //Only request data if not for current date
    if (CurrentFantasyHockeyGoalsStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyHockeyGoalsStatsRec := GetFantasyHockeyGoalsStats;
    with CurrentFantasyHockeyGoalsStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);
        //Increment index if "Next" record
        if (Pos('_Next', SymbolName) <> 0) then Inc(Index);
        //Format data string
        if (FormatForSNY) then
        begin
          //Build string
          OutStr := '[f 2][c 19]' + IntToStr(Index) + '. ';
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          OutStr := OutStr + '(' + Trim(GetTeamMnemonicFromID('NHL', TeamID[Index])) + ') ';
          //Add goals
          if (Trim(Goals[Index]) = '1') then
            OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Goals[Index]) // + ' ' + ABBV_Goals
          else
            OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Goals[Index]); // + ' ' + ABBV_Goals + 's';
        end
        else begin
          //Build string
          OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('NHL', TeamID[Index])) + ': ';
          //Build stats part of string
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          //Add goals
          if (Trim(Goals[Index]) = '1') then
            OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Goals[Index]) // + ' ' + ABBV_Goals
          else
            OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Goals[Index]); // + ' ' + ABBV_Goals + 's';
        end;
      end
      else OutStr := '';
    end;
  end

  //Assists
  else if (SymbolName = '$NHL_Fantasy_Stats_Assists') or (SymbolName = '$NHL_Fantasy_Stats_Assists_Next') then
  begin
    //Only request data if not for current date
    if (CurrentFantasyHockeyAssistsStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyHockeyAssistsStatsRec := GetFantasyHockeyAssistsStats;
    with CurrentFantasyHockeyAssistsStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);
        //Increment index if "Next" record
        if (Pos('_Next', SymbolName) <> 0) then Inc(Index);

        //Format data string
        if (FormatForSNY) then
        begin
          //Build string
          OutStr := '[f 2][c 19]' + IntToStr(Index) + '. ';
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          OutStr := OutStr + '(' + Trim(GetTeamMnemonicFromID('NHL', TeamID[Index])) + ') ';
          //Add assists
          if (Trim(Assists[Index]) = '1') then
            OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Assists[Index]) // + ' ' + ABBV_Assists
          else
            OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Assists[Index]); // + ' ' + ABBV_Assists + 's';
        end
        else begin
          //Build string
          OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('NHL', TeamID[Index])) + ': ';
          //Build stats part of string
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          //Add assists
          if (Trim(Assists[Index]) = '1') then
            OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Assists[Index]) // + ' ' + ABBV_Assists
          else
            OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Assists[Index]); // + ' ' + ABBV_Assists + 's';
        end;
      end
      else OutStr := '';
    end;
  end

  //////////////////////////////////////////////////////////////////////////////
  //NBA Fantasy Stats
  //////////////////////////////////////////////////////////////////////////////
  //Points
  else if (SymbolName = '$NBA_Fantasy_Stats_Points') or (SymbolName = '$NBA_Fantasy_Stats_Points_Next') then
  begin
    if (CurrentFantasyBasketballPointsStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyBasketballPointsStatsRec := GetFantasyBasketballStats('Points');
    with CurrentFantasyBasketballPointsStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);
        //Increment index if "Next" record
        if (Pos('_Next', SymbolName) <> 0) then Inc(Index);

        //Format data string
        if (FormatForSNY) then
        begin
          //Build string
          OutStr := '[f 2][c 19]' + IntToStr(Index) + '. ';
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          OutStr := OutStr + '(' + Trim(GetTeamMnemonicFromID('NBA', TeamID[Index])) + ') ';
          //Add points
          OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Points[Index]) + ' ' + ABBV_Points;
        end
        else begin
          //Build string
          OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('NBA', TeamID[Index])) + ': ';
          //Build stats part of string
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          //Add points
          OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Points[Index]) + ' ' + ABBV_Points;
        end;
      end
      else OutStr := '';
    end;
  end

  //Rebounds
  else if (SymbolName = '$NBA_Fantasy_Stats_Rebounds') or (SymbolName = '$NBA_Fantasy_Stats_Rebounds_Next') then
  begin
    if (CurrentFantasyBasketballReboundsStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyBasketballReboundsStatsRec := GetFantasyBasketballStats('Rebounds');
    with CurrentFantasyBasketballReboundsStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);
        //Increment index if "Next" record
        if (Pos('_Next', SymbolName) <> 0) then Inc(Index);

        //Format data string
        if (FormatForSNY) then
        begin
          //Build string
          OutStr := '[f 2][c 19]' + IntToStr(Index) + '. ';
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          OutStr := OutStr + '(' + Trim(GetTeamMnemonicFromID('NBA', TeamID[Index])) + ') ';
          //Add rebounds
          if (Trim(Rebounds[Index]) = '1') then
            OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Rebounds[Index]) // + ' ' + ABBV_Rebounds
          else
            OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Rebounds[Index]); // + ' ' + ABBV_Rebounds + 's';
        end
        else begin
          //Build string
          OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('NBA', TeamID[Index])) + ': ';
          //Build stats part of string
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          //Add rebounds
          if (Trim(Rebounds[Index]) = '1') then
            OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Rebounds[Index]) // + ' ' + ABBV_Rebounds
          else
            OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Rebounds[Index]); // + ' ' + ABBV_Rebounds + 's';
        end;
      end
      else OutStr := '';
    end;
  end

  //Assists
  else if (SymbolName = '$NBA_Fantasy_Stats_Assists') or (SymbolName = '$NBA_Fantasy_Stats_Assists_Next') then
  begin
    if (CurrentFantasyBasketballAssistsStatsRec.CurrentDate <> Trunc(Now)) then
      CurrentFantasyBasketballAssistsStatsRec := GetFantasyBasketballStats('Assists');
    with CurrentFantasyBasketballAssistsStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);
        //Increment index if "Next" record
        if (Pos('_Next', SymbolName) <> 0) then Inc(Index);

        //Format data string
        if (FormatForSNY) then
        begin
          //Build string
          OutStr := '[f 2][c 19]' + IntToStr(Index) + '. ';
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          OutStr := OutStr + '(' + Trim(GetTeamMnemonicFromID('NBA', TeamID[Index])) + ') ';
          //Add assists
          if (Trim(Assists[Index]) = '1') then
            OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Assists[Index]) // + ' ' + ABBV_Assists)
          else
            OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Assists[Index]); // + ' ' + ABBV_Assists + 's';
        end
        else begin
          //Build string
          OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID('NBA', TeamID[Index])) + ': ';
          //Build stats part of string
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          //Add assists
          if (Trim(Assists[Index]) = '1') then
            OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Assists[Index]) // + ' ' + ABBV_Assists)
          else
            OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(Assists[Index]); // + ' ' + ABBV_Assists + 's';
        end;
      end
      else OutStr := '';
    end;
  end

  //////////////////////////////////////////////////////////////////////////////
  //Boxscore stats
  //////////////////////////////////////////////////////////////////////////////
  else if (SymbolName = '$Box_V1') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.VPeriod1);
  end
  else if (SymbolName = '$Box_V2') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.VPeriod2);
  end
  else if (SymbolName = '$Box_V3') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.VPeriod3);
  end
  else if (SymbolName = '$Box_V4') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.VPeriod4);
  end
  else if (SymbolName = '$Box_V5') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.VPeriod5);
  end
  else if (SymbolName = '$Box_V6') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.VPeriod6);
  end
  else if (SymbolName = '$Box_V7') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.VPeriod7);
  end
  else if (SymbolName = '$Box_H1') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.HPeriod1);
  end
  else if (SymbolName = '$Box_H2') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.HPeriod2);
  end
  else if (SymbolName = '$Box_H3') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.HPeriod3);
  end
  else if (SymbolName = '$Box_H4') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.HPeriod4);
  end
  else if (SymbolName = '$Box_H5') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.HPeriod5);
  end
  else if (SymbolName = '$Box_H6') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.HPeriod6);
  end
  else if (SymbolName = '$Box_H7') then
  begin
    if (CurrentBoxscoreRec.CurrentGameID <> CurrentGameData.GameID) OR
       (CurrentBoxscoreRec.CurrentLeague <> CurrentGameData.League) then
      CurrentBoxscoreRec := GetBoxscoreData(CurrentGameData.League, CurrentGameData.GameID);
    OutStr := '[c 1]' + Trim(CurrentBoxscoreRec.HPeriod7);
  end

  ///////////////////////////
  //SEASON LEADERS
  ///////////////////////////
  else if (SymbolName = '$NFL_Season_Leaders_QB_Yds') or
          (SymbolName = '$NFL_Season_Leaders_QB_Yds_Next') or
          (SymbolName = '$NFL_Season_Leaders_QB_TDs') or
          (SymbolName = '$NFL_Season_Leaders_QB_TDs_Next') or
          (SymbolName = '$NFL_Season_Leaders_RB_Yds') or
          (SymbolName = '$NFL_Season_Leaders_RB_Yds_Next') or
          (SymbolName = '$NFL_Season_Leaders_RB_TDs') or
          (SymbolName = '$NFL_Season_Leaders_RB_TDs_Next') or
          (SymbolName = '$NFL_Season_Leaders_WR_Yds') or
          (SymbolName = '$NFL_Season_Leaders_WR_Yds_Next') or
          (SymbolName = '$NFL_Season_Leaders_WR_TDs') or
          (SymbolName = '$NFL_Season_Leaders_WR_TDs_Next') or
          (SymbolName = '$NFL_Season_Leaders_FGs') or
          (SymbolName = '$NFL_Season_Leaders_FGs_Next') or

          (SymbolName = '$MLB_Season_Leaders_AL_Hits') or
          (SymbolName = '$MLB_Season_Leaders_AL_Hits_Next') or
          (SymbolName = '$MLB_Season_Leaders_AL_HRs') or
          (SymbolName = '$MLB_Season_Leaders_AL_HRs_Next') or
          (SymbolName = '$MLB_Season_Leaders_AL_RBIs') or
          (SymbolName = '$MLB_Season_Leaders_AL_RBIs_Next') or
          (SymbolName = '$MLB_Season_Leaders_AL_Runs') or
          (SymbolName = '$MLB_Season_Leaders_AL_Runs_Next') or
          (SymbolName = '$MLB_Season_Leaders_AL_SBs') or
          (SymbolName = '$MLB_Season_Leaders_AL_SBs_Next') or

          (SymbolName = '$MLB_Season_Leaders_AL_Strikeouts') or
          (SymbolName = '$MLB_Season_Leaders_AL_Strikeouts_Next') or
          (SymbolName = '$MLB_Season_Leaders_AL_IP') or
          (SymbolName = '$MLB_Season_Leaders_AL_IP_Next') or
          (SymbolName = '$MLB_Season_Leaders_AL_Wins') or
          (SymbolName = '$MLB_Season_Leaders_AL_Wins_Next') or
          (SymbolName = '$MLB_Season_Leaders_AL_Saves') or
          (SymbolName = '$MLB_Season_Leaders_AL_Saves_Next') or
          (SymbolName = '$MLB_Season_Leaders_AL_ERA') or
          (SymbolName = '$MLB_Season_Leaders_AL_ERA_Next') or

          (SymbolName = '$MLB_Season_Leaders_NL_Hits') or
          (SymbolName = '$MLB_Season_Leaders_NL_Hits_Next') or
          (SymbolName = '$MLB_Season_Leaders_NL_HRs') or
          (SymbolName = '$MLB_Season_Leaders_NL_HRs_Next') or
          (SymbolName = '$MLB_Season_Leaders_NL_RBIs') or
          (SymbolName = '$MLB_Season_Leaders_NL_RBIs_Next') or
          (SymbolName = '$MLB_Season_Leaders_NL_Runs') or
          (SymbolName = '$MLB_Season_Leaders_NL_Runs_Next') or
          (SymbolName = '$MLB_Season_Leaders_NL_SBs') or
          (SymbolName = '$MLB_Season_Leaders_NL_SBs_Next') or

          (SymbolName = '$MLB_Season_Leaders_NL_Strikeouts') or
          (SymbolName = '$MLB_Season_Leaders_NL_Strikeouts_Next') or
          (SymbolName = '$MLB_Season_Leaders_NL_IP') or
          (SymbolName = '$MLB_Season_Leaders_NL_IP_Next') or
          (SymbolName = '$MLB_Season_Leaders_NL_Wins') or
          (SymbolName = '$MLB_Season_Leaders_NL_Wins_Next') or
          (SymbolName = '$MLB_Season_Leaders_NL_Saves') or
          (SymbolName = '$MLB_Season_Leaders_NL_Saves_Next') or
          (SymbolName = '$MLB_Season_Leaders_NL_ERA') or
          (SymbolName = '$MLB_Season_Leaders_NL_ERA_Next') or

          (SymbolName = '$NHL_Season_Leaders_GPG') or
          (SymbolName = '$NHL_Season_Leaders_GPG_Next') or
          (SymbolName = '$NHL_Season_Leaders_APG') or
          (SymbolName = '$NHL_Season_Leaders_APG_Next') or
          (SymbolName = '$NHL_Season_Leaders_PPG') or
          (SymbolName = '$NHL_Season_Leaders_PPG_Next') or

          (SymbolName = '$NHL_Season_Leaders_Saves') or
          (SymbolName = '$NHL_Season_Leaders_Saves_Next') or
          (SymbolName = '$NHL_Season_Leaders_GAA') or
          (SymbolName = '$NHL_Season_Leaders_GAA_Next') or

          (SymbolName = '$NBA_Season_Leaders_PPG') or
          (SymbolName = '$NBA_Season_Leaders_PPG_Next') or
          (SymbolName = '$NBA_Season_Leaders_RPG') or
          (SymbolName = '$NBA_Season_Leaders_RPG_Next') or
          (SymbolName = '$NBA_Season_Leaders_APG') or
          (SymbolName = '$NBA_Season_Leaders_APG_Next') or
          (SymbolName = '$NBA_Season_Leaders_SPG') or
          (SymbolName = '$NBA_Season_Leaders_SPG_Next') or
          (SymbolName = '$NBA_Season_Leaders_BPG') or
          (SymbolName = '$NBA_Season_Leaders_BPG_Next') then
  begin
    //Get the latest data
    //NFL
    if (SymbolName = '$NFL_Season_Leaders_QB_Yds') or (SymbolName = '$NFL_Season_Leaders_QB_Yds_Next') and
       (Current_NFL_QB_Leaders_Yds.CurrentDate <> Trunc(Now)) then
      Current_NFL_QB_Leaders_Yds := GetLeaderStats_General('p_SI_Get_NFL_Leaders', 'Passing Yds', 'NFL', 'Yds')

    else if (SymbolName = '$NFL_Season_Leaders_QB_TDs') or (SymbolName = '$NFL_Season_Leaders_QB_TDs_Next') and
       (Current_NFL_QB_Leaders_TDs.CurrentDate <> Trunc(Now)) then
      Current_NFL_QB_Leaders_TDs := GetLeaderStats_General('p_SI_Get_NFL_Leaders', 'Passing Touchdowns', 'NFL', 'TD')

    else if (SymbolName = '$NFL_Season_Leaders_RB_Yds') or (SymbolName = '$NFL_Season_Leaders_RB_Yds_Next') and
       (Current_NFL_RB_Leaders_Yds.CurrentDate <> Trunc(Now)) then
      Current_NFL_RB_Leaders_Yds := GetLeaderStats_General('p_SI_Get_NFL_Leaders', 'Yds Gained', 'NFL', 'Yds')

    else if (SymbolName = '$NFL_Season_Leaders_RB_TDs') or (SymbolName = '$NFL_Season_Leaders_RB_TDs_Next') and
       (Current_NFL_RB_Leaders_TDs.CurrentDate <> Trunc(Now)) then
      Current_NFL_RB_Leaders_TDs := GetLeaderStats_General('p_SI_Get_NFL_Leaders', 'Rushing Touchdowns',  'NFL', 'TD')

    else if (SymbolName = '$NFL_Season_Leaders_WR_Yds') or (SymbolName = '$NFL_Season_Leaders_WR_Yds_Next') and
       (Current_NFL_WR_Leaders_Yds.CurrentDate <> Trunc(Now)) then
      Current_NFL_WR_Leaders_Yds := GetLeaderStats_General('p_SI_Get_NFL_Leaders', 'Receiving Yds', 'NFL', 'Yds')

    else if (SymbolName = '$NFL_Season_Leaders_WR_TDs') or (SymbolName = '$NFL_Season_Leaders_WR_TDs_Next') and
       (Current_NFL_WR_Leaders_TDs.CurrentDate <> Trunc(Now)) then
      Current_NFL_WR_Leaders_TDs := GetLeaderStats_General('p_SI_Get_NFL_Leaders', 'Receiving Touchdowns', 'NFL', 'TD')

    else if (SymbolName = '$NFL_Season_Leaders_FGs') or (SymbolName = '$NFL_Season_Leaders_FGs_Next') and
       (Current_NFL_Field_Goal_Leaders_FGM.CurrentDate <> Trunc(Now)) then
      Current_NFL_Field_Goal_Leaders_FGM := GetLeaderStats_General('p_SI_Get_NFL_Leaders', 'Field Goals Made', 'NFL', 'FGM')

    //MLB
    else if (SymbolName = '$MLB_Season_Leaders_AL_Hits') or (SymbolName = '$MLB_Season_Leaders_AL_Hits_Next') and
       (Current_MLB_AL_Leaders_Hits.CurrentDate <> Trunc(Now)) then
      Current_MLB_AL_Leaders_Hits := GetLeaderStats_General('p_SI_Get_MLB_Leaders', 'Hits', 'AL', 'H')

    else if (SymbolName = '$MLB_Season_Leaders_AL_HRs') or (SymbolName = '$MLB_Season_Leaders_AL_HRs_Next') and
       (Current_MLB_AL_Leaders_HR.CurrentDate <> Trunc(Now)) then
      Current_MLB_AL_Leaders_HR := GetLeaderStats_General('p_SI_Get_MLB_Leaders', 'Home Runs', 'AL', 'HR')

    else if (SymbolName = '$MLB_Season_Leaders_AL_RBIs') or (SymbolName = '$MLB_Season_Leaders_AL_RBIs_Next') and
       (Current_MLB_AL_Leaders_RBIs.CurrentDate <> Trunc(Now)) then
      Current_MLB_AL_Leaders_RBIs := GetLeaderStats_General('p_SI_Get_MLB_Leaders', 'Runs Batted In', 'AL', 'RBI')

    else if (SymbolName = '$MLB_Season_Leaders_AL_Runs') or (SymbolName = '$MLB_Season_Leaders_AL_Runs_Next') and
       (Current_MLB_AL_Leaders_Runs.CurrentDate <> Trunc(Now)) then
      Current_MLB_AL_Leaders_Runs := GetLeaderStats_General('p_SI_Get_MLB_Leaders', 'Runs Scored', 'AL', 'R')

    else if (SymbolName = '$MLB_Season_Leaders_AL_SBs') or (SymbolName = '$MLB_Season_Leaders_AL_SBs_Next') and
       (Current_MLB_AL_Leaders_Stolen_Bases.CurrentDate <> Trunc(Now)) then
      Current_MLB_AL_Leaders_Stolen_Bases := GetLeaderStats_General('p_SI_Get_MLB_Leaders', 'Stolen Bases', 'AL', 'SB')

    else if (SymbolName = '$MLB_Season_Leaders_AL_Strikeouts') or (SymbolName = '$MLB_Season_Leaders_AL_Strikeouts_Next') and
       (Current_MLB_AL_Leaders_Strikeouts.CurrentDate <> Trunc(Now)) then
      Current_MLB_AL_Leaders_Strikeouts := GetLeaderStats_General('p_SI_Get_MLB_Leaders', 'Pitcher Strikeouts', 'AL', 'K')

    else if (SymbolName = '$MLB_Season_Leaders_AL_IP') or (SymbolName = '$MLB_Season_Leaders_AL_IP_Next') and
       (Current_MLB_AL_Leaders_Innings_Pitched.CurrentDate <> Trunc(Now)) then
      Current_MLB_AL_Leaders_Innings_Pitched := GetLeaderStats_General('p_SI_Get_MLB_Leaders', 'Innings Pitched', 'AL', 'IP')

    else if (SymbolName = '$MLB_Season_Leaders_AL_Wins') or (SymbolName = '$MLB_Season_Leaders_AL_Wins_Next') and
       (Current_MLB_AL_Leaders_Wins.CurrentDate <> Trunc(Now)) then
      Current_MLB_AL_Leaders_Wins := GetLeaderStats_General('p_SI_Get_MLB_Leaders', 'Wins', 'AL', 'W')

    else if (SymbolName = '$MLB_Season_Leaders_AL_Saves') or (SymbolName = '$MLB_Season_Leaders_AL_Saves_Next') and
       (Current_MLB_AL_Leaders_Saves.CurrentDate <> Trunc(Now)) then
      Current_MLB_AL_Leaders_Saves := GetLeaderStats_General('p_SI_Get_MLB_Leaders', 'Saves', 'AL', 'Svs')

    else if (SymbolName = '$MLB_Season_Leaders_AL_ERA') or (SymbolName = '$MLB_Season_Leaders_AL_ERA_Next') and
       (Current_MLB_AL_Leaders_ERA.CurrentDate <> Trunc(Now)) then
      Current_MLB_AL_Leaders_ERA := GetLeaderStats_General('p_SI_Get_MLB_Leaders', 'Earned Run Average', 'AL', 'ERA')

    else if (SymbolName = '$MLB_Season_Leaders_NL_Hits') or (SymbolName = '$MLB_Season_Leaders_NL_Hits_Next') and
       (Current_MLB_NL_Leaders_Hits.CurrentDate <> Trunc(Now)) then
      Current_MLB_NL_Leaders_Hits := GetLeaderStats_General('p_SI_Get_MLB_Leaders', 'Hits', 'NL', 'H')

    else if (SymbolName = '$MLB_Season_Leaders_NL_HRs') or (SymbolName = '$MLB_Season_Leaders_NL_HRs_Next') and
       (Current_MLB_NL_Leaders_HR.CurrentDate <> Trunc(Now)) then
      Current_MLB_NL_Leaders_HR := GetLeaderStats_General('p_SI_Get_MLB_Leaders', 'Home Runs', 'NL', 'HR')

    else if (SymbolName = '$MLB_Season_Leaders_NL_RBIs') or (SymbolName = '$MLB_Season_Leaders_NL_RBIs_Next') and
       (Current_MLB_NL_Leaders_RBIs.CurrentDate <> Trunc(Now)) then
      Current_MLB_NL_Leaders_RBIs := GetLeaderStats_General('p_SI_Get_MLB_Leaders', 'Runs Batted In', 'NL', 'RBI')

    else if (SymbolName = '$MLB_Season_Leaders_NL_Runs') or (SymbolName = '$MLB_Season_Leaders_NL_Runs_Next') and
       (Current_MLB_NL_Leaders_Runs.CurrentDate <> Trunc(Now)) then
      Current_MLB_NL_Leaders_Runs := GetLeaderStats_General('p_SI_Get_MLB_Leaders', 'Runs Scored', 'NL', 'R')

    else if (SymbolName = '$MLB_Season_Leaders_NL_SBs') or (SymbolName = '$MLB_Season_Leaders_NL_SBs_Next') and
       (Current_MLB_NL_Leaders_Stolen_Bases.CurrentDate <> Trunc(Now)) then
      Current_MLB_NL_Leaders_Stolen_Bases := GetLeaderStats_General('p_SI_Get_MLB_Leaders', 'Stolen Bases', 'NL', 'SB')

    else if (SymbolName = '$MLB_Season_Leaders_NL_Strikeouts') or (SymbolName = '$MLB_Season_Leaders_NL_Strikeouts_Next') and
       (Current_MLB_NL_Leaders_Strikeouts.CurrentDate <> Trunc(Now)) then
      Current_MLB_NL_Leaders_Strikeouts := GetLeaderStats_General('p_SI_Get_MLB_Leaders', 'Pitcher Strikeouts', 'NL', 'K')

    else if (SymbolName = '$MLB_Season_Leaders_NL_IP') or (SymbolName = '$MLB_Season_Leaders_NL_IP_Next') and
       (Current_MLB_NL_Leaders_Innings_Pitched.CurrentDate <> Trunc(Now)) then
      Current_MLB_NL_Leaders_Innings_Pitched := GetLeaderStats_General('p_SI_Get_MLB_Leaders', 'Innings Pitched', 'NL', 'IP')

    else if (SymbolName = '$MLB_Season_Leaders_NL_Wins') or (SymbolName = '$MLB_Season_Leaders_NL_Wins_Next') and
       (Current_MLB_NL_Leaders_Wins.CurrentDate <> Trunc(Now)) then
      Current_MLB_NL_Leaders_Wins := GetLeaderStats_General('p_SI_Get_MLB_Leaders', 'Wins', 'NL', 'W')

    else if (SymbolName = '$MLB_Season_Leaders_NL_Saves') or (SymbolName = '$MLB_Season_Leaders_NL_Saves_Next') and
       (Current_MLB_NL_Leaders_Saves.CurrentDate <> Trunc(Now)) then
      Current_MLB_NL_Leaders_Saves := GetLeaderStats_General('p_SI_Get_MLB_Leaders', 'Saves', 'NL', 'Svs')

    else if (SymbolName = '$MLB_Season_Leaders_NL_ERA') or (SymbolName = '$MLB_Season_Leaders_NL_ERA_Next') and
       (Current_MLB_NL_Leaders_ERA.CurrentDate <> Trunc(Now)) then
      Current_MLB_NL_Leaders_ERA := GetLeaderStats_General('p_SI_Get_MLB_Leaders', 'Earned Run Average', 'NL', 'ERA')

    //NHL
    else if (SymbolName = '$NHL_Season_Leaders_GPG') or (SymbolName = '$NHL_Season_Leaders_GPG_Next') and
       (Current_NHL_Leaders_GPG.CurrentDate <> Trunc(Now)) then
      Current_NHL_Leaders_GPG := GetLeaderStats_General('p_SI_Get_NHL_Leaders', 'Goals Per Game', 'NHL', 'G/PG')

    else if (SymbolName = '$NHL_Season_Leaders_APG') or (SymbolName = '$NHL_Season_Leaders_APG_Next') and
       (Current_NHL_Leaders_APG.CurrentDate <> Trunc(Now)) then
      Current_NHL_Leaders_APG := GetLeaderStats_General('p_SI_Get_NHL_Leaders', 'Assists Per Game', 'NHL', 'Ast/G')

    else if (SymbolName = '$NHL_Season_Leaders_PPG') or (SymbolName = '$NHL_Season_Leaders_PPG_Next') and
       (Current_NHL_Leaders_PPG.CurrentDate <> Trunc(Now)) then
      Current_NHL_Leaders_PPG := GetLeaderStats_General('p_SI_Get_NHL_Leaders', 'Points Per Game', 'NHL', 'Pts/G')

    else if (SymbolName = '$NHL_Season_Leaders_Saves') or (SymbolName = '$NHL_Season_Leaders_Saves_Next') and
       (Current_NHL_Leaders_Saves.CurrentDate <> Trunc(Now)) then
      Current_NHL_Leaders_Saves := GetLeaderStats_General('p_SI_Get_NHL_Leaders', 'Saves', 'NHL', 'Sv')

    else if (SymbolName = '$NHL_Season_Leaders_GAA') or (SymbolName = '$NHL_Season_Leaders_GAA_Next') and
       (Current_NHL_Leaders_GAA.CurrentDate <> Trunc(Now)) then
      Current_NHL_Leaders_GAA := GetLeaderStats_General('p_SI_Get_NHL_Leaders', 'Goals-Against Avg.', 'NHL', 'GAA')

    //NBA
    else if (SymbolName = '$NBA_Season_Leaders_PPG') or (SymbolName = '$NBA_Season_Leaders_PPG_Next') and
       (Current_NBA_Leaders_PPG.CurrentDate <> Trunc(Now)) then
      Current_NBA_Leaders_PPG := GetLeaderStats_General('p_SI_Get_NBA_Leaders', 'Points per Game', 'NBA', 'Pts/G')

    else if (SymbolName = '$NBA_Season_Leaders_RPG') or (SymbolName = '$NBA_Season_Leaders_RPG_Next') and
       (Current_NBA_Leaders_RPG.CurrentDate <> Trunc(Now)) then
      Current_NBA_Leaders_RPG := GetLeaderStats_General('p_SI_Get_NBA_Leaders', 'Rebounds per Game', 'NBA', 'Reb/G')

    else if (SymbolName = '$NBA_Season_Leaders_APG') or (SymbolName = '$NBA_Season_Leaders_APG_Next') and
       (Current_NBA_Leaders_APG.CurrentDate <> Trunc(Now)) then
      Current_NBA_Leaders_APG := GetLeaderStats_General('p_SI_Get_NBA_Leaders', 'Assists per Game', 'NBA', 'Ast/G')

    else if (SymbolName = '$NBA_Season_Leaders_SPG') or (SymbolName = '$NBA_Season_Leaders_SPG_Next') and
       (Current_NBA_Leaders_SPG.CurrentDate <> Trunc(Now)) then
      Current_NBA_Leaders_SPG := GetLeaderStats_General('p_SI_Get_NBA_Leaders', 'Steals per Game', 'NBA', 'Stl/G')

    else if (SymbolName = '$NBA_Season_Leaders_BPG') or (SymbolName = '$NBA_Season_Leaders_BPG_Next') and
       (Current_NBA_Leaders_BPG.CurrentDate <> Trunc(Now)) then
      Current_NBA_Leaders_BPG := GetLeaderStats_General('p_SI_Get_NBA_Leaders', 'Blocked Shots per Game', 'NBA', 'Blk/G');
////
    //Setup temporary stats record
    //NFL
    if (SymbolName = '$NFL_Season_Leaders_QB_Yds') or (SymbolName = '$NFL_Season_Leaders_QB_Yds_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_NFL_QB_Leaders_Yds
    else if (SymbolName = '$NFL_Season_Leaders_QB_TDs') or (SymbolName = '$NFL_Season_Leaders_QB_TDs_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_NFL_QB_Leaders_TDs
    else if (SymbolName = '$NFL_Season_Leaders_RB_Yds') or (SymbolName = '$NFL_Season_Leaders_RB_Yds_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_NFL_RB_Leaders_Yds
    else if (SymbolName = '$NFL_Season_Leaders_RB_TDs') or (SymbolName = '$NFL_Season_Leaders_RB_TDs_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_NFL_RB_Leaders_TDs
    else if (SymbolName = '$NFL_Season_Leaders_WR_Yds') or (SymbolName = '$NFL_Season_Leaders_WR_Yds_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_NFL_WR_Leaders_Yds
    else if (SymbolName = '$NFL_Season_Leaders_WR_TDs') or (SymbolName = '$NFL_Season_Leaders_WR_TDs_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_NFL_WR_Leaders_TDs
    else if (SymbolName = '$NFL_Season_Leaders_FGs') or (SymbolName = '$NFL_Season_Leaders_FGs_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_NFL_Field_Goal_Leaders_FGM

    //MLB
    else if (SymbolName = '$MLB_Season_Leaders_AL_Hits') or (SymbolName = '$MLB_Season_Leaders_AL_Hits_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_MLB_AL_Leaders_Hits
    else if (SymbolName = '$MLB_Season_Leaders_AL_HRs') or (SymbolName = '$MLB_Season_Leaders_AL_HRs_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_MLB_AL_Leaders_HR
    else if (SymbolName = '$MLB_Season_Leaders_AL_RBIs') or (SymbolName = '$MLB_Season_Leaders_AL_RBIs_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_MLB_AL_Leaders_RBIs
    else if (SymbolName = '$MLB_Season_Leaders_AL_Runs') or (SymbolName = '$MLB_Season_Leaders_AL_Runs_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_MLB_AL_Leaders_Runs
    else if (SymbolName = '$MLB_Season_Leaders_AL_SBs') or (SymbolName = '$MLB_Season_Leaders_AL_SBs_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_MLB_AL_Leaders_Stolen_Bases
    else if (SymbolName = '$MLB_Season_Leaders_AL_Strikeouts') or (SymbolName = '$MLB_Season_Leaders_AL_Strikeouts_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_MLB_AL_Leaders_Strikeouts
    else if (SymbolName = '$MLB_Season_Leaders_AL_IP') or (SymbolName = '$MLB_Season_Leaders_AL_IP_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_MLB_AL_Leaders_Innings_Pitched
    else if (SymbolName = '$MLB_Season_Leaders_AL_Wins') or (SymbolName = '$MLB_Season_Leaders_AL_Wins_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_MLB_AL_Leaders_Wins
    else if (SymbolName = '$MLB_Season_Leaders_AL_Saves') or (SymbolName = '$MLB_Season_Leaders_AL_Saves_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_MLB_AL_Leaders_Saves
    else if (SymbolName = '$MLB_Season_Leaders_AL_ERA') or (SymbolName = '$MLB_Season_Leaders_AL_ERA_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_MLB_AL_Leaders_ERA
    else if (SymbolName = '$MLB_Season_Leaders_NL_Hits') or (SymbolName = '$MLB_Season_Leaders_NL_Hits_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_MLB_NL_Leaders_Hits
    else if (SymbolName = '$MLB_Season_Leaders_NL_HRs') or (SymbolName = '$MLB_Season_Leaders_NL_HRs_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_MLB_NL_Leaders_HR
    else if (SymbolName = '$MLB_Season_Leaders_NL_RBIs') or (SymbolName = '$MLB_Season_Leaders_NL_RBIs_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_MLB_NL_Leaders_RBIs
    else if (SymbolName = '$MLB_Season_Leaders_NL_Runs') or (SymbolName = '$MLB_Season_Leaders_NL_Runs_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_MLB_NL_Leaders_Runs
    else if (SymbolName = '$MLB_Season_Leaders_NL_SBs') or (SymbolName = '$MLB_Season_Leaders_NL_SBs_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_MLB_NL_Leaders_Stolen_Bases
    else if (SymbolName = '$MLB_Season_Leaders_NL_Strikeouts') or (SymbolName = '$MLB_Season_Leaders_NL_Strikeouts_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_MLB_NL_Leaders_Strikeouts
    else if (SymbolName = '$MLB_Season_Leaders_NL_IP') or (SymbolName = '$MLB_Season_Leaders_NL_IP_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_MLB_NL_Leaders_Innings_Pitched
    else if (SymbolName = '$MLB_Season_Leaders_NL_Wins') or (SymbolName = '$MLB_Season_Leaders_NL_Wins_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_MLB_NL_Leaders_Wins
    else if (SymbolName = '$MLB_Season_Leaders_NL_Saves') or (SymbolName = '$MLB_Season_Leaders_NL_Saves_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_MLB_NL_Leaders_Saves
    else if (SymbolName = '$MLB_Season_Leaders_NL_ERA') or (SymbolName = '$MLB_Season_Leaders_NL_ERA_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_MLB_NL_Leaders_ERA

    //NHL
    else if (SymbolName = '$NHL_Season_Leaders_GPG') or (SymbolName = '$NHL_Season_Leaders_GPG_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_NHL_Leaders_GPG
    else if (SymbolName = '$NHL_Season_Leaders_APG') or (SymbolName = '$NHL_Season_Leaders_APG_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_NHL_Leaders_APG
    else if (SymbolName = '$NHL_Season_Leaders_PPG') or (SymbolName = '$NHL_Season_Leaders_PPG_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_NHL_Leaders_PPG
    else if (SymbolName = '$NHL_Season_Leaders_Saves') or (SymbolName = '$NHL_Season_Leaders_Saves_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_NHL_Leaders_Saves
    else if (SymbolName = '$NHL_Season_Leaders_GAA') or (SymbolName = '$NHL_Season_Leaders_GAA_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_NHL_Leaders_GAA

    //NBA
    else if (SymbolName = '$NBA_Season_Leaders_PPG') or (SymbolName = '$NBA_Season_Leaders_PPG_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_NBA_Leaders_PPG
    else if (SymbolName = '$NBA_Season_Leaders_RPG') or (SymbolName = '$NBA_Season_Leaders_RPG_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_NBA_Leaders_RPG
    else if (SymbolName = '$NBA_Season_Leaders_APG') or (SymbolName = '$NBA_Season_Leaders_APG_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_NBA_Leaders_APG
    else if (SymbolName = '$NBA_Season_Leaders_SPG') or (SymbolName = '$NBA_Season_Leaders_SPG_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_NBA_Leaders_SPG
    else if (SymbolName = '$NBA_Season_Leaders_BPG') or (SymbolName = '$NBA_Season_Leaders_BPG_Next') then
      TempSeasonLeadersGeneralStatsRec := Current_NBA_Leaders_BPG;

    //Update record
    with TempSeasonLeadersGeneralStatsRec do
    begin
      if (DataFound) then
      begin
        //Force array index value to 1 if not specified - only show first value in authoring mode
        if (CurrentEntryIndex = NOENTRYINDEX) then
          Index := 1
        else
          Index := StrToIntDef(TickerRecPtr^.UserData[1], 1);
        //Increment index if "Next" record
        if (Pos('_Next', SymbolName) <> 0) then Inc(Index);
        //Format data string
        if (FormatForSNY) then
        begin
          //Build string
          OutStr := '[f 2][c 19]' + IntToStr(Index) + '. ';
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          OutStr := OutStr + '(' + Trim(GetTeamMnemonicFromID(League, TeamID[Index])) + ') ';
          OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(StatValue[Index]);
        end
        else begin
          //Build string
          OutStr := '[f 2][c 19]' + Trim(GetTeamMnemonicFromID(League, TeamID[Index])) + ': ';
          //Build stats part of string
          OutStr := OutStr + '[f 2][c 1]' + Trim(Name_Last[Index]) + ' ';
          OutStr := OutStr + '[f 2][c 1]' + ' ' + Trim(StatValue[Index]);
        end;
      end
      else OutStr := '';
    end;
  end
  /////////////////////////////////////
  // End Season Leaders
  /////////////////////////////////////

  /////////////////////////////////////
  // Start CSNMA headshot functions
  /////////////////////////////////////
  else if (SymbolName = '$Team_Logo_1') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
      OutStr := TickerRecPtr^.UserData[21+TextFieldBias];
  end
  else if (SymbolName = '$Team_Logo_2') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
      OutStr := TickerRecPtr^.UserData[22+TextFieldBias];
  end
  else if (SymbolName = '$League_Logo_1') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
      OutStr := TickerRecPtr^.UserData[21+TextFieldBias];
  end
  else if (SymbolName = '$League_Logo_2') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
      OutStr := TickerRecPtr^.UserData[22+TextFieldBias];
  end
  else if (SymbolName = '$Player_Headshot_1') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
      OutStr := TickerRecPtr^.UserData[21+TextFieldBias];
  end
  else if (SymbolName = '$Player_Headshot_2') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
      OutStr := TickerRecPtr^.UserData[22+TextFieldBias];
  end
  //Added for Version 2.0
  else if (SymbolName = '$Full_Promo_Graphic') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
      OutStr := TickerRecPtr^.UserData[21+TextFieldBias];
  end
  //Get filenames for images required to composite headshots
  else if (SymbolName = '$Headshot_Backplate_1') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
      //Call function to build element path and add element filename
      OutStr := GetHeadshotElementPath(Trim(TickerRecPtr^.UserData[21+TextFieldBias])) + Headshot_Backplate_Filename;
  end
  else if (SymbolName = '$Headshot_Backplate_2') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
      //Call function to build element path and add element filename
      OutStr := GetHeadshotElementPath(Trim(TickerRecPtr^.UserData[22+TextFieldBias])) + Headshot_Backplate_Filename;
  end
  else if (SymbolName = '$Team_Logo_Headshot_1') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
      //Call function to build element path and add element filename; check if flag - if so, use alternate element
      if (Pos('FLAG', TickerRecPtr^.UserData[21+TextFieldBias]) > 0) then
        OutStr := GetHeadshotElementPath(Trim(TickerRecPtr^.UserData[21+TextFieldBias])) + Flag_Logo_Headshot_Filename
      else
        OutStr := GetHeadshotElementPath(Trim(TickerRecPtr^.UserData[21+TextFieldBias])) + Team_Logo_Headshot_Filename;
  end
  else if (SymbolName = '$Team_Logo_Headshot_2') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
      //Call function to build element path and add element filename; check if flag - if so, use alternate element
      if (Pos('FLAG', TickerRecPtr^.UserData[22+TextFieldBias]) > 0) then
        OutStr := GetHeadshotElementPath(Trim(TickerRecPtr^.UserData[22+TextFieldBias])) + Flag_Logo_Headshot_Filename
      else
        OutStr := GetHeadshotElementPath(Trim(TickerRecPtr^.UserData[22+TextFieldBias])) + Team_Logo_Headshot_Filename;
  end
  else if (SymbolName = '$Team_Color_Headshot_1') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
      //Call function to build element path and add element filename
      OutStr := GetHeadshotElementPath(Trim(TickerRecPtr^.UserData[21+TextFieldBias])) + Team_Color_Filename;
  end
  else if (SymbolName = '$Team_Color_Headshot_2') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
      //Call function to build element path and add element filename
      OutStr := GetHeadshotElementPath(Trim(TickerRecPtr^.UserData[22+TextFieldBias])) + Team_Color_Filename;
  end
  //Added for automated team logos
  else if (SymbolName = '$Team_Logo_Visitor') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
      //Call function to build element path and add element filename
      OutStr := StringReplace(Base_Element_Path, '\', '/', [rfReplaceAll]) + GetCamioTeamLogoPath(CurrentGameData.League, CurrentGameData.VStatsIncID) +
        '/' + StringReplace(End_Element_Path, '\', '/', [rfReplaceAll]) + '/' + Team_Chip_A_Filename;
  end
  else if (SymbolName = '$Team_Logo_Home') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
      //Call function to build element path and add element filename
      OutStr := StringReplace(Base_Element_Path, '\', '/', [rfReplaceAll]) + GetCamioTeamLogoPath(CurrentGameData.League, CurrentGameData.HStatsIncID) +
        '/' + StringReplace(End_Element_Path, '\', '/', [rfReplaceAll]) + '/' + Team_Chip_A_Filename;
  end
  else if (SymbolName = '$Team_Logo_Visitor_Small') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
      //Call function to build element path and add element filename
      OutStr := StringReplace(Base_Element_Path, '\', '/', [rfReplaceAll]) + GetCamioTeamLogoPath(CurrentGameData.League, CurrentGameData.VStatsIncID) +
        '/' + StringReplace(End_Element_Path, '\', '/', [rfReplaceAll]) + '/' + Team_Chip_B_Filename;
  end
  else if (SymbolName = '$Team_Logo_Home_Small') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
      //Call function to build element path and add element filename
      OutStr := StringReplace(Base_Element_Path, '\', '/', [rfReplaceAll]) + GetCamioTeamLogoPath(CurrentGameData.League, CurrentGameData.HStatsIncID) +
        '/' + StringReplace(End_Element_Path, '\', '/', [rfReplaceAll]) + '/' + Team_Chip_B_Filename;
  end
  else if (SymbolName = '$League_Logo_Auto') AND (CurrentEntryIndex <> NOENTRYINDEX) then
  begin
      //Call function to build element path and add element filename
      OutStr := StringReplace(Base_Element_Path, '\', '/', [rfReplaceAll]) + GetCamioLeagueLogoPath(TickerRecPtr^.League) + '/' +
      StringReplace(GetCamioLeagueLogoPath(TickerRecPtr^.League), '_', '', [rfReplaceAll]) + '/' + StringReplace(End_Element_Path, '\', '/', [rfReplaceAll]) + '/' + Team_Chip_A_Filename;
  end

  /////////////////////////////////////
  // End CSNMA headshot functions
  /////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  // CSNMA L-BAR Weather Functions
  //////////////////////////////////////////////////////////////////////////////
  //Weather related functions; store current location ID in global variables to prevent
  //separate queries for each variable in Weather template
  else if (SymbolName = '$LBar_Weather_Location_Code') then
  begin
    //if (CurrentLBarWeatherForecastRec.LocationCode <> WeatherLocationCode) then
      CurrentLBarWeatherForecastRec := GetLBarWeatherForecastData(WeatherLocationCode);
    OutStr := CurrentLBarWeatherForecastRec.LocationCode;
  end
  else if (SymbolName = '$LBar_Weather_Day_Tomorrow') then
  begin
    Case (DayOfWeek(Now)+1) of
      2: OutStr := 'MONDAY';
      3: OutStr := 'TUESDAY';
      4: OutStr := 'WEDNESDAY';
      5: OutStr := 'THURSDAY';
      6: OutStr := 'FRIDAY';
      7: OutStr := 'SATURDAY';
      8: OutStr := 'SUNDAY';
    end;
  end
  else if (SymbolName = '$LBar_Weather_Day_After_Tomorrow') then
  begin
    Case (DayOfWeek(Now)+2) of
      3: OutStr := 'TUESDAY';
      4: OutStr := 'WEDNESDAY';
      5: OutStr := 'THURSDAY';
      6: OutStr := 'FRIDAY';
      7: OutStr := 'SATURDAY';
      8: OutStr := 'SUNDAY';
      9: OutStr := 'MONDAY';
    end;
  end
  else if (SymbolName = '$LBar_Weather_City') then
  begin
    //if (CurrentLBarWeatherForecastRec.LocationCode <> WeatherLocationCode) then
      CurrentLBarWeatherForecastRec := GetLBarWeatherForecastData(WeatherLocationCode);
    OutStr := ANSIUpperCase(CurrentLBarWeatherForecastRec.Weather_City);
  end
  //Today
  else if (SymbolName = '$LBar_Weather_Temperature_Current') then
  begin
    //if (CurrentLBarWeatherForecastRec.LocationCode <> WeatherLocationCode) then
      CurrentLBarWeatherForecastRec := GetLBarWeatherForecastData(WeatherLocationCode);
    if (CurrentLBarWeatherForecastRec.Weather_Temperature_Current = '-') then
      OutStr := '-'
    else
      OutStr := CurrentLBarWeatherForecastRec.Weather_Temperature_Current + DEGREE_SYMBOL;
  end
  else if (SymbolName = '$LBar_Weather_Icon_Current') then
  begin
    //if (CurrentLBarWeatherForecastRec.LocationCode <> WeatherLocationCode) then
      CurrentLBarWeatherForecastRec := GetLBarWeatherForecastData(WeatherLocationCode);
    //Need new code here
    if (HourOf(Now) >= 18) then
      OutStr := GetLBarWeatherIconFilepath(CurrentLBarWeatherForecastRec.Weather_Icon_Tonight)
    else
      OutStr := GetLBarWeatherIconFilepath(CurrentLBarWeatherForecastRec.Weather_Icon_Today);
  end
  else if (SymbolName = '$LBar_Weather_Icon_Today') then
  begin
    //if (CurrentLBarWeatherForecastRec.LocationCode <> WeatherLocationCode) then
      CurrentLBarWeatherForecastRec := GetLBarWeatherForecastData(WeatherLocationCode);
    //Need new code here
    OutStr := GetLBarWeatherIconFilepath(CurrentLBarWeatherForecastRec.Weather_Icon_Today);
  end
  else if (SymbolName = '$LBar_Weather_Icon_Tonight') then
  begin
    //if (CurrentLBarWeatherForecastRec.LocationCode <> WeatherLocationCode) then
      CurrentLBarWeatherForecastRec := GetLBarWeatherForecastData(WeatherLocationCode);
    //Need new code here
    OutStr := GetLBarWeatherIconFilepath(CurrentLBarWeatherForecastRec.Weather_Icon_Tonight);
  end
  else if (SymbolName = '$LBar_Weather_Temperature_Today_High') then
  begin
    //if (CurrentLBarWeatherForecastRec.LocationCode <> WeatherLocationCode) then
      CurrentLBarWeatherForecastRec := GetLBarWeatherForecastData(WeatherLocationCode);
    if (CurrentLBarWeatherForecastRec.Weather_Temperature_Today_High = '-') then
      OutStr := '-'
    else
      OutStr := CurrentLBarWeatherForecastRec.Weather_Temperature_Today_High + DEGREE_SYMBOL;
  end
  else if (SymbolName = '$LBar_Weather_Temperature_Today_Low') then
  begin
    //if (CurrentLBarWeatherForecastRec.LocationCode <> WeatherLocationCode) then
      CurrentLBarWeatherForecastRec := GetLBarWeatherForecastData(WeatherLocationCode);
    if (CurrentLBarWeatherForecastRec.Weather_Temperature_Today_Low = '-') then
      OutStr := '-'
    else
      OutStr := CurrentLBarWeatherForecastRec.Weather_Temperature_Today_Low + DEGREE_SYMBOL;
  end
  else if (SymbolName = '$LBar_Weather_Icon_Tomorrow') then
  begin
    //if (CurrentLBarWeatherForecastRec.LocationCode <> WeatherLocationCode) then
      CurrentLBarWeatherForecastRec := GetLBarWeatherForecastData(WeatherLocationCode);
    OutStr := GetLBarWeatherIconFilepath(CurrentLBarWeatherForecastRec.Weather_Icon_Tomorrow);
  end
  else if (SymbolName = '$LBar_Weather_Temperature_Tomorrow_High') then
  begin
    //if (CurrentLBarWeatherForecastRec.LocationCode <> WeatherLocationCode) then
      CurrentLBarWeatherForecastRec := GetLBarWeatherForecastData(WeatherLocationCode);
    if (CurrentLBarWeatherForecastRec.Weather_Temperature_Tomorrow_High = '-') then
      OutStr := '-'
    else
      OutStr := CurrentLBarWeatherForecastRec.Weather_Temperature_Tomorrow_High + DEGREE_SYMBOL;
  end
  else if (SymbolName = '$LBar_Weather_Temperature_Tomorrow_Low') then
  begin
    //if (CurrentLBarWeatherForecastRec.LocationCode <> WeatherLocationCode) then
      CurrentLBarWeatherForecastRec := GetLBarWeatherForecastData(WeatherLocationCode);
    if (CurrentLBarWeatherForecastRec.Weather_Temperature_Tomorrow_Low = '-') then
      OutStr := '-'
    else
      OutStr := CurrentLBarWeatherForecastRec.Weather_Temperature_Tomorrow_Low + DEGREE_SYMBOL;
  end
  else if (SymbolName = '$LBar_Weather_Icon_Day_After_Tomorrow') then
  begin
    //if (CurrentLBarWeatherForecastRec.LocationCode <> WeatherLocationCode) then
      CurrentLBarWeatherForecastRec := GetLBarWeatherForecastData(WeatherLocationCode);
    OutStr := GetLBarWeatherIconFilepath(CurrentLBarWeatherForecastRec.Weather_Icon_Day_After_Tomorrow);
  end
  else if (SymbolName = '$LBar_Weather_Temperature_Day_After_Tomorrow_High') then
  begin
    //if (CurrentLBarWeatherForecastRec.LocationCode <> WeatherLocationCode) then
      CurrentLBarWeatherForecastRec := GetLBarWeatherForecastData(WeatherLocationCode);
    if (CurrentLBarWeatherForecastRec.Weather_Temperature_Day_After_Tomorrow_High = '-') then
      OutStr := '-'
    else
      OutStr := CurrentLBarWeatherForecastRec.Weather_Temperature_Day_After_Tomorrow_High + DEGREE_SYMBOL;
  end
  else if (SymbolName = '$LBar_Weather_Temperature_Day_After_Tomorrow_Low') then
  begin
    //if (CurrentLBarWeatherForecastRec.LocationCode <> WeatherLocationCode) then
      CurrentLBarWeatherForecastRec := GetLBarWeatherForecastData(WeatherLocationCode);
    if (CurrentLBarWeatherForecastRec.Weather_Temperature_Day_After_Tomorrow_Low = '-') then
      OutStr := '-'
    else
      OutStr := CurrentLBarWeatherForecastRec.Weather_Temperature_Day_After_Tomorrow_Low + DEGREE_SYMBOL;
  end;

  //Check if need to force to upper case
  if (ForceUpperCase) then OutStr := ANSIUpperCase(OutStr);

  //Remove any CAL formatting strings
  OutStr := StringReplace(OutStr, '[c 1]', '', [rfReplaceAll]);
  OutStr := StringReplace(OutStr, '[c 19]', '', [rfReplaceAll]);
  OutStr := StringReplace(OutStr, '[c 20]', '', [rfReplaceAll]);
  OutStr := StringReplace(OutStr, '[f 2]', '', [rfReplaceAll]);
  OutStr := StringReplace(OutStr, '[f 3]', '', [rfReplaceAll]);
  OutStr := StringReplace(OutStr, '[r -20]', '', [rfReplaceAll]);
  OutStr := StringReplace(OutStr, '[r 40]', '', [rfReplaceAll]);

  //Return
  GetValueOfSymbol.SymbolValue := OutStr;
  GetValueOfSymbol.OKToDisplay := OKToDisplay;
end;

//Function to pull weather forecast data for the home stadium
function GetWeatherForecastData(League: String; HomeStatsID: LongInt): WeatherForecastRec;
var
  OutRec: WeatherForecastRec;
begin
  try
    //Run stored procedure to get weather data
    with dmMain.WeatherQuery do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add ('sp_GetWeatherData ' + QuotedStr(League) + ',' + IntToStr(HomeStatsID));
      Active := TRUE;
      if (RecordCount > 0) then
      begin
        OutRec.Weather_Stadium_Name := FieldByName('StadiumName').AsString;
        OutRec.Weather_Current_Temperature := FieldByName('CurrentTemperature').AsString;
        OutRec.Weather_High_Temperature := FieldByName('HighTemperature').AsString;
        OutRec.Weather_Low_Temperature := FieldByName('LowTemperature').AsString;
        OutRec.Weather_Current_Conditions := FieldByName('Condition').AsString;
        OutRec.Weather_Forecast_Icon_Day := FieldByName('DayIcon').AsString;
        OutRec.Weather_Forecast_Icon_Night := FieldByName('NightIcon').AsString;
      end;
      Active := FALSE;
    end;
  except
  end;
  GetWeatherForecastData := OutRec;
end;

//Function to take a league mnemonic and Stats Inc. team ID and return the team logo path
function GetCamioTeamLogoPath(League: String; StatsIncID: LongInt): string;
var
  i: SmallInt;
  FoundTeam: Boolean;
  TeamPtr: ^TeamRec; //Pointer to team collection object
  OutStr: String;
begin
  if (Team_Collection.Count > 0) then
  begin
    OutStr := '';
    FoundTeam := FALSE;
    i := 0;
    Repeat
    TeamPtr := Team_Collection.At(i);
    if (TeamPtr^.League = League) and (TeamPtr^.StatsIncID = StatsIncID) then
    begin
      OutStr := TeamPtr^.CamioTeamLogoPath;
      FoundTeam := TRUE;
    end;
    inc(i);
    Until ((FoundTeam) or (i = Team_Collection.Count));
  end;
  GetCamioTeamLogoPath := OutStr;
end;

//Function to take a league mnemonic and return the league logo path
function GetCamioLeagueLogoPath(League: String): string;
var
  i: SmallInt;
  FoundLeague: Boolean;
  CategoryRecPtr: ^CategoryRec;
  OutStr: String;
begin
  if (Categories_Collection.Count > 0) then
  begin
    OutStr := '';
    Foundleague := FALSE;
    i := 0;
    Repeat
    CategoryRecPtr := Categories_Collection.At(i);
    if (CategoryRecPtr^.Category_SPort = League)then
    begin
      OutStr := CategoryRecPtr^.CamioFolderName;
      FoundLeague := TRUE;
    end;
    inc(i);
    Until ((FoundLeague) or (i = Categories_Collection.Count));
  end;
  GetCamioLeagueLogoPath := OutStr;
end;

//Function to take a weather icon numerical code and return the logo file path
function GetLBarWeatherIconFilepath(IconCode: String): String;
var
  i: SmallInt;
  LBarWeatherIconRecPtr: ^LbarWeatherIconRec;
  IconVal: SmallInt;
  OutVal: String;
begin
  //Default to weather not available icon
  OutVal := Weather_Icon_Base_Path + '\' + '44_Not-Available.tga';
  if (LBar_Weather_Icon_Collection.Count > 0) then
  begin
    for i := 0 to LBar_Weather_Icon_Collection.Count-1 do
    begin
      //Convert icon code to integer value; default to "N/A"
      IconVal := StrToIntDef(IconCode, NOT_AVAILABLE);
      LBarWeatherIconRecPtr := LBar_Weather_Icon_Collection.At(i);
      if (LBarWeatherIconRecPtr^.IconCharacterValue = IconVal) then
        OutVal := Weather_Icon_Base_Path + '\' + LBarWeatherIconRecPtr^.IconFilename + '.tga';
    end;
  end;
  OutVal := StringReplace(OutVal, '\', '/', [rfReplaceAll]);
  GetLBarWeatherIconFilepath := OutVal;
end;

//Function to pull weather forecast data for the home stadium
function GetLBarWeatherForecastData(LocationCode: String): LBarWeatherForecastRec;
var
  OutRec: LBarWeatherForecastRec;
begin
  try
    //Run stored procedure to get weather data
    with dmMain.WeatherQuery do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add ('sp_GetLBarWeatherData ' + QuotedStr(LocationCode));
      Active := TRUE;
      if (RecordCount > 0) then
      begin
        OutRec.LocationCode := FieldByName('LocationCode').AsString;
        OutRec.Weather_City := FieldByName('Weather_City').AsString;
        OutRec.Weather_Temperature_Current := FieldByName('Weather_Temperature_Current').AsString;
        OutRec.Weather_Icon_Today := FieldByName('Weather_Icon_Today').AsString;
        OutRec.Weather_Icon_Tonight := FieldByName('Weather_Icon_Tonight').AsString;
        OutRec.Weather_Temperature_Today_High := FieldByName('Weather_Temperature_Today_High').AsString;
        OutRec.Weather_Temperature_Today_Low := FieldByName('Weather_Temperature_Today_Low').AsString;
        OutRec.Weather_Icon_Tomorrow := FieldByName('Weather_Icon_Tomorrow').AsString;
        OutRec.Weather_Temperature_Tomorrow_High := FieldByName('Weather_Temperature_Tomorrow_High').AsString;
        OutRec.Weather_Temperature_Tomorrow_Low := FieldByName('Weather_Temperature_Tomorrow_Low').AsString;
        OutRec.Weather_Icon_Day_After_Tomorrow := FieldByName('Weather_Icon_Day_After_Tomorrow').AsString;
        OutRec.Weather_Temperature_Day_After_Tomorrow_High := FieldByName('Weather_Temperature_Day_After_Tomorrow_High').AsString;;
        OutRec.Weather_Temperature_Day_After_Tomorrow_Low := FieldByName('Weather_Temperature_Day_After_Tomorrow_Low').AsString;
      end;
      Active := FALSE;
    end;
  except
  end;
  GetLBarWeatherForecastData := OutRec;
end;

//Function to get starting pitchers
function GetStartingPitcherStats(GameCode: String): StartingPitcherStatsRec;
var
  OutRec: StartingPitcherStatsRec;
begin
  //Init
  OutRec.Home_Pitcher_Name_Last := '';
  OutRec.Home_Pitcher_Name_First := '';
  OutRec.Visitor_Pitcher_Name_Last := '';
  OutRec.Visitor_Pitcher_Name_First := '';
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */SELECT * FROM dbo.tf_SI_Get_MLB_Game_Starting_Pitchers(' + GameCode + ')');
      Open;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        OutRec.CurrentGameID := GameCode;
        OutRec.Home_Pitcher_Name_Last := FieldByName('HomeLastName').AsString;
        OutRec.Home_Pitcher_Name_First := FieldByName('HomeFirstName').AsString;
        OutRec.Home_Pitcher_Wins := '0';
        OutRec.Home_Pitcher_Losses := '0';
        OutRec.Visitor_Pitcher_Name_Last := FieldByName('AwayLastName').AsString;
        OutRec.Visitor_Pitcher_Name_First := FieldByName('AwayFirstName').AsString;
        OutRec.Visitor_Pitcher_Wins := '0';
        OutRec.Visitor_Pitcher_Losses := '0';
      end;
    end;
  except

  end;
  GetStartingPitcherStats := OutRec;
end;

//Function to get winning, losing and saving pitchers
function GetGameFinalPitcherStats(GameCode: String): FinalPitcherStatsRec;
var
  OutRec: FinalPitcherStatsRec;
begin
  //Init
  OutRec.Winning_Pitcher_Name_Last := '';
  OutRec.Winning_Pitcher_Name_First := '';
  OutRec.Losing_Pitcher_Name_Last := '';
  OutRec.Losing_Pitcher_Name_First := '';
  OutRec.Saving_Pitcher_Name_Last := '';
  OutRec.Saving_Pitcher_Name_First := '';
  //Init
  OutRec.NeedToAddSavingPitcher := FALSE;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */SELECT * FROM dbo.tf_SI_Get_MLB_Game_Pitcher_Stats(' + GameCode + ') ' +
        'WHERE (IsWinningPitcher = ' + QuotedStr('true') + ' or IsLosingPitcher = ' + QuotedStr ('true') +
          ' or IsSavingPitcher = ' + QuotedStr('true') + ') ORDER BY TeamId ASC, LastName ASC');
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        OutRec.CurrentGameID := GameCode;
        OutRec.NeedToAddSavingPitcher := FALSE;
        repeat
          if (FieldByName('IsWinningPitcher').AsBoolean = TRUE) then
          begin
            OutRec.Winning_Pitcher_Name_Last := FieldByName('LastName').AsString;
            OutRec.Winning_Pitcher_Name_First := FieldByName('FirstName').AsString;
            OutRec.Winning_Pitcher_Wins := FieldByName('Wins').AsString;
            OutRec.Winning_Pitcher_Losses := FieldByName('Losses').AsString;
            OutRec.Winning_Pitcher_IP := FieldByName('InningsPitched').AsString;
            OutRec.Winning_Pitcher_K := FieldByName('Strikeouts').AsString;
            OutRec.Winning_Pitcher_BB := FieldByName('Walks').AsString;
            OutRec.Winning_Pitcher_Hits := FieldByName('Hits').AsString;
            OutRec.Winning_Pitcher_Runs := FieldByName('Runs').AsString;
            OutRec.Winning_Pitcher_EarnedRuns := FieldByName('EarnedRuns').AsString;
            OutRec.Winning_Pitcher_HomeRuns := FieldByName('HomeRunsAllowed').AsString;
          end
          else if (FieldByName('IsLosingPitcher').AsBoolean = TRUE) then
          begin
            OutRec.Losing_Pitcher_Name_Last := FieldByName('LastName').AsString;
            OutRec.Losing_Pitcher_Name_First := FieldByName('FirstName').AsString;
            OutRec.Losing_Pitcher_Wins := FieldByName('Wins').AsString;
            OutRec.Losing_Pitcher_Losses := FieldByName('Losses').AsString;
            OutRec.Losing_Pitcher_IP := FieldByName('InningsPitched').AsString;
            OutRec.Losing_Pitcher_K := FieldByName('Strikeouts').AsString;
            OutRec.Losing_Pitcher_BB := FieldByName('Walks').AsString;
            OutRec.Losing_Pitcher_Hits := FieldByName('Hits').AsString;
            OutRec.Losing_Pitcher_Runs := FieldByName('Runs').AsString;
            OutRec.Losing_Pitcher_EarnedRuns := FieldByName('EarnedRuns').AsString;
            OutRec.Losing_Pitcher_HomeRuns := FieldByName('HomeRunsAllowed').AsString;
          end
          else if (FieldByName('IsSavingPitcher').AsBoolean = TRUE) then
          begin
            OutRec.Saving_Pitcher_Name_Last := FieldByName('LastName').AsString;
            OutRec.Saving_Pitcher_Name_First := FieldByName('FirstName').AsString;
            OutRec.Saving_Pitcher_Saves := FieldByName('Saves').AsString;
            //Set flag to indicate saving pitcher needs to be added
            OutRec.NeedToAddSavingPitcher := TRUE;
          end;
          //Go to next record
          Next;
        until (dmMain.SportbaseQuery2.EOF);
      end;
    end;
  except

  end;
  //Return
  GetGameFinalPitcherStats := OutRec;
end;

//Function to get batter stats
function GetGameFinalBatterStats(GameCode: String; TeamID: LongInt; StatNum: SmallInt): FinalBatterStatsRec;
var
  i: SmallInt;
  OutRec: FinalBatterStatsRec;
begin
  //Init
  OutRec.Name_Last := '';
  OutRec.name_First := '';
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/**/SELECT * FROM dbo.tf_SI_Get_MLB_Game_Player_Stats(' + GameCode + ') ' +
        'WHERE TeamID = ' + IntToStr(TeamID) + ' ' +
        'ORDER BY Runs DESC, Hits DESC');
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Go to specified record number
        if (StatNum > 1) then for i := 1 to StatNum-1 do Next;
        //Set data values
        OutRec.Name_Last := FieldByName('LastName').AsString;
        OutRec.Name_First := FieldByName('FirstName').AsString;
        OutRec.Atbats := FieldByName('AtBats').AsString;
        OutRec.Hits := FieldByName('Hits').AsString;
        OutRec.Runs := FieldByName('Runs').AsString;
        OutRec.Doubles := FieldByName('Doubles').AsString;
        OutRec.Triples := FieldByName('Triples').AsString;
        OutRec.HomeRuns := FieldByName('HomeRuns').AsString;
        OutRec.RBI := FieldByName('RBI').AsString;
        OutRec.StolenBases := FieldByName('StolenBases').AsString;
      end;
    end;
  except

  end;
  //Return
  GetGameFinalBatterStats := OutRec;
end;

//Basketball stats
function GetGameFinalBBallStats(League, GameCode: String; TeamID: LongInt; StatNum: SmallInt): FinalBBallStatsRec;
var
  i: SmallInt;
  OutRec: FinalBBallStatsRec;
  LeagueStr: String;
begin
  //Init
  OutRec.Name_Last := '';
  OutRec.name_First := '';
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      if (League = 'NBA') then LeagueStr := 'NBA'
      else if (League = 'NCAAB') then LeagueStr := 'CBK'
      else if (League = 'NCAAW') then LeagueStr := 'WCBK';
      SQL.Add('/**/SELECT TOP (3) WITH TIES * FROM dbo.tf_SI_Get_' + LeagueStr + '_Game_Player_Stats(' + GameCode + ') ' +
        'WHERE TeamID = ' + IntToStr(TeamID) + ' ' +
        'ORDER BY Points DESC, ReboundsTotal DESC, Assists DESC');
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Go to specified record number
        if (StatNum > 1) then for i := 1 to StatNum-1 do Next;
        //Set data values
        OutRec.Name_Last := FieldByName('LastName').AsString;
        OutRec.Name_First := FieldByName('FirstName').AsString;
        OutRec.Points := FieldByName('Points').AsString;
        OutRec.Rebounds := FieldByName('ReboundsTotal').AsString;
        OutRec.Assists := FieldByName('Assists').AsString;
      end;
    end;
  except

  end;
  //Return
  GetGameFinalBBallStats := OutRec;
end;

//Football stats function - all stats returned in 1 record
function GetGameFinalFBStats(League, GameCode: String): FinalFBStatsRec;
var
  i: SmallInt;
  OutRec: FinalFBStatsRec;
  LeagueStr: String;
begin
  //Init
  OutRec.VPassingName_Last := '';
  OutRec.VPassingName_First := '';
  OutRec.HPassingName_Last := '';
  OutRec.HPassingName_First := '';
  OutRec.VRushingName_Last := '';
  OutRec.VRushingName_First := '';
  OutRec.HRushingName_Last := '';
  OutRec.HRushingName_First := '';
  OutRec.VReceivingName_Last := '';
  OutRec.VReceivingName_First := '';
  OutRec.HReceivingName_Last := '';
  OutRec.HReceivingName_First := '';
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      if (League = 'NCAAF') then LeagueStr := 'CFB'
      else if (League = 'NFL') then LeagueStr := 'NFL';
      SQL.Add('/**/p_SI_Get_' + LeagueStr + '_InGameStats ' + GameCode);
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set data values
        //Psssing stats
        OutRec.CurrentLeague := League;
        OutRec.CurrentGameID := GameCode;
        OutRec.VPassingName_Last := FieldByName('VPassingLastName').AsString;
        OutRec.VPassingName_First := FieldByName('VPassingFirstName').AsString;
        OutRec.VPassingAttempts := FieldByName('VPassingAttempts').AsString;
        OutRec.VPassingCompletions := FieldByName('VPassingCompletions').AsString;
        OutRec.VPassingYards := FieldByName('VPassingYards').AsString;
        OutRec.VPassingTouchdowns := FieldByName('VPassingTDs').AsString;
        OutRec.VPassingInterceptions := FieldByName('VPassingInterceptions').AsString;
        OutRec.HPassingName_Last := FieldByName('HPassingLastName').AsString;
        OutRec.HPassingName_First := FieldByName('HPassingFirstName').AsString;
        OutRec.HPassingAttempts := FieldByName('HPassingAttempts').AsString;
        OutRec.HPassingCompletions := FieldByName('HPassingCompletions').AsString;
        OutRec.HPassingYards := FieldByName('HPassingYards').AsString;
        OutRec.HPassingTouchdowns := FieldByName('HPassingTDs').AsString;
        OutRec.HPassingInterceptions := FieldByName('HPassingInterceptions').AsString;
        //Rushing stats
        OutRec.VRushingName_Last := FieldByName('VRushingLastName').AsString;
        OutRec.VRushingName_First := FieldByName('VRushingFirstName').AsString;
        OutRec.VRushingAttempts := FieldByName('VRushingAttempts').AsString;
        OutRec.VRushingYards := FieldByName('VRushingYards').AsString;
        OutRec.VRushingTouchdowns := FieldByName('VRushingTDs').AsString;
        OutRec.HRushingName_Last := FieldByName('HRushingLastName').AsString;
        OutRec.HRushingName_First := FieldByName('HRushingFirstName').AsString;
        OutRec.HRushingAttempts := FieldByName('HRushingAttempts').AsString;
        OutRec.HRushingYards := FieldByName('HRushingYards').AsString;
        OutRec.HRushingTouchdowns := FieldByName('HRushingTDs').AsString;
        //Receiving stats
        OutRec.VReceivingName_Last := FieldByName('VReceivingLastName').AsString;
        OutRec.VReceivingName_First := FieldByName('VReceivingFirstName').AsString;
        OutRec.VReceivingReceptions := FieldByName('VReceivingReceptions').AsString;
        OutRec.VReceivingYards := FieldByName('VReceivingYards').AsString;
        OutRec.VReceivingTouchdowns := FieldByName('VReceivingTDs').AsString;
        OutRec.HReceivingName_Last := FieldByName('HReceivingLastName').AsString;
        OutRec.HReceivingName_First := FieldByName('HReceivingFirstName').AsString;
        OutRec.HReceivingReceptions := FieldByName('HReceivingReceptions').AsString;
        OutRec.HReceivingYards := FieldByName('HReceivingYards').AsString;
        OutRec.HReceivingTouchdowns := FieldByName('HReceivingTDs').AsString;
      end;
    end;
  except

  end;
  //Return
  GetGameFinalFBStats := OutRec;
end;

//Hockey Stats functions
//NHL Player Stats
function GetGameFinalSkaterStats(GameCode: String; TeamID: LongInt; StatNum: SmallInt): FinalSkaterStatsRec;
var
  i: SmallInt;
  OutRec: FinalSkaterStatsRec;
  LeagueStr: String;
begin
  //Init
  OutRec.Name_Last := '';
  OutRec.name_First := '';
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/**/SELECT TOP (3) WITH TIES * FROM dbo.tf_SI_Get_NHL_Game_Skater_Stats(' + GameCode + ') ' +
        'WHERE TeamID = ' + IntToStr(TeamID) + ' ' +
        'ORDER BY Goals DESC, ASSISTS DESC, SOG DESC');
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Go to specified record number
        if (StatNum > 1) then for i := 1 to StatNum-1 do Next;
        //Set data values
        OutRec.Name_Last := FieldByName('LastName').AsString;
        OutRec.Name_First := FieldByName('FirstName').AsString;
        OutRec.Goals := FieldByName('Goals').AsString;
        OutRec.Assists := FieldByName('Assists').AsString;
        OutRec.SOG := FieldByName('SOG').AsString;
      end;
    end;
  except

  end;
  //Return
  GetGameFinalSkaterStats := OutRec;
end;

//NHL Goalie Stats
function GetGameFinalGoalieStats(GameCode: String; TeamID: LongInt; StatNum: SmallInt): FinalGoalieStatsRec;
var
  i: SmallInt;
  OutRec: FinalGoalieStatsRec;
  LeagueStr: String;
begin
  //Init
  OutRec.Name_Last := '';
  OutRec.name_First := '';
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/**/SELECT * FROM dbo.tf_SI_Get_NHL_Game_Goalie_Stats(' + GameCode + ') ' +
        'WHERE TeamID = ' + IntToStr(TeamID)+ ' ' +
        'ORDER BY SAVES DESC');
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Go to specified record number
        if (StatNum > 1) then for i := 1 to StatNum-1 do Next;
        //Set data values
        OutRec.Name_Last := FieldByName('LastName').AsString;
        OutRec.Name_First := FieldByName('FirstName').AsString;
        OutRec.Shots := FieldByName('Shots').AsString;
        OutRec.Saves := FieldByName('Saves').AsString;
        OutRec.GoalsAgainst := FieldByName('GoalsAgainst').AsString;
      end;
    end;
  except

  end;
  //Return
  GetGameFinalGoalieStats := OutRec;
end;

//Function to return the boxscore data for a specified league and game; covers
//all leagues other than MLB
function GetBoxscoreData(League: String; GameCode: String): BoxscoreRec;
var
  i: SmallInt;
  LeagueStr: String;
  OutRec: BoxscoreRec;
begin
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      //Build query based on league
      if (League = 'NFL') then
        SQL.Add('/* */p_SI_Get_NFL_Game_Boxscore ' + GameCode)
      else if (League = 'NHL') then
        SQL.Add('/* */p_SI_Get_NHL_Game_Boxscore ' + GameCode)
      else if (League = 'NBA') then
        SQL.Add('/* */p_SI_Get_NBA_Game_Boxscore ' + GameCode)
      else if (League = 'MLS') then
        SQL.Add('/* */p_SI_Get_MLS_Game_Boxscore ' + GameCode)
      else if (League = 'NCAAF') then
        SQL.Add('/* */p_SI_Get_CFB_Game_Boxscore ' + GameCode)
      else if (League = 'NCAAB') then
        SQL.Add('/* */p_SI_Get_CBK_Game_Boxscore ' + GameCode)
      else if (League = 'NCAAW') then
        SQL.Add('/* */p_SI_Get_WCBK_Game_Boxscore ' + GameCode);
      Open;
      First;

      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        OutRec.CurrentLeague := League;
        OutRec.CurrentGameID := GameCode;
        //Set data values based on league; not - MLB not included here - use simple line score
        //handled in other functions
        if (League = 'MLS') OR (League = 'NCAAB') OR (League = 'NCAAW') then
        begin
          //Determine number of periods
          OutRec.VNumPeriods := 0;
          if (Trim(FieldByName('VHalf1').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VHalf2').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VHalf3').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VHalf4').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VHalf5').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          //Set scoring by period
          OutRec.VScore := Trim(FieldByName('VScore').AsString);
          OutRec.VPeriod1 := Trim(FieldByName('VHalf1').AsString);
          OutRec.VPeriod2 := Trim(FieldByName('VHalf2').AsString);
          OutRec.VPeriod3 := Trim(FieldByName('VHalf3').AsString);
          OutRec.VPeriod4 := Trim(FieldByName('VHalf4').AsString);
          OutRec.VPeriod5 := Trim(FieldByName('VHalf5').AsString);
          OutRec.VPeriod6 := '';
          OutRec.VPeriod7 := '';
          //Determine number of periods
          OutRec.HNumPeriods := 0;
          if (Trim(FieldByName('HHalf1').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HHalf2').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HHalf3').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HHalf4').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HHalf5').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          //Set scoring by period
          OutRec.HScore := Trim(FieldByName('HScore').AsString);
          OutRec.HPeriod1 := Trim(FieldByName('HHalf1').AsString);
          OutRec.HPeriod2 := Trim(FieldByName('HHalf2').AsString);
          OutRec.HPeriod3 := Trim(FieldByName('HHalf3').AsString);
          OutRec.HPeriod4 := Trim(FieldByName('HHalf4').AsString);
          OutRec.HPeriod5 := Trim(FieldByName('HHalf5').AsString);
          OutRec.HPeriod6 := '';
          OutRec.HPeriod7 := '';
        end
        else if (League = 'NBA') OR (League = 'NFL') OR (League = 'NCAAF') then
        begin
          //Determine number of periods
          OutRec.VNumPeriods := 0;
          if (Trim(FieldByName('VQuarter1').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VQuarter2').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VQuarter3').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VQuarter4').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VQuarter5').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VQuarter6').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VQuarter7').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          //Set scoring by period
          OutRec.VScore := Trim(FieldByName('VScore').AsString);
          OutRec.VPeriod1 := Trim(FieldByName('VQuarter1').AsString);
          OutRec.VPeriod2 := Trim(FieldByName('VQuarter2').AsString);
          OutRec.VPeriod3 := Trim(FieldByName('VQuarter3').AsString);
          OutRec.VPeriod4 := Trim(FieldByName('VQuarter4').AsString);
          OutRec.VPeriod5 := Trim(FieldByName('VQuarter5').AsString);
          OutRec.VPeriod6 := Trim(FieldByName('VQuarter6').AsString);
          OutRec.VPeriod7 := Trim(FieldByName('VQuarter7').AsString);
          //Determine number of periods
          OutRec.HNumPeriods := 0;
          if (Trim(FieldByName('HQuarter1').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HQuarter2').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HQuarter3').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HQuarter4').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HQuarter5').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HQuarter6').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HQuarter7').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          //Set scoring by period
          OutRec.HScore := Trim(FieldByName('HScore').AsString);
          OutRec.HPeriod1 := Trim(FieldByName('HQuarter1').AsString);
          OutRec.HPeriod2 := Trim(FieldByName('HQuarter2').AsString);
          OutRec.HPeriod3 := Trim(FieldByName('HQuarter3').AsString);
          OutRec.HPeriod4 := Trim(FieldByName('HQuarter4').AsString);
          OutRec.HPeriod5 := Trim(FieldByName('HQuarter5').AsString);
          OutRec.HPeriod6 := Trim(FieldByName('HQuarter6').AsString);
          OutRec.HPeriod7 := Trim(FieldByName('HQuarter7').AsString);
        end
        else if (League = 'NHL') then
        begin
          //Determine number of periods
          OutRec.VNumPeriods := 0;
          if (Trim(FieldByName('VPeriod1').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VPeriod2').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VPeriod3').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VPeriod4').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          if (Trim(FieldByName('VSOScore').AsString) <> '') then
            Inc(OutRec.VNumPeriods);
          //Set scoring by period
          OutRec.VScore := Trim(FieldByName('VScore').AsString);
          OutRec.VPeriod1 := Trim(FieldByName('VPeriod1').AsString);
          OutRec.VPeriod2 := Trim(FieldByName('VPeriod2').AsString);
          OutRec.VPeriod3 := Trim(FieldByName('VPeriod3').AsString);
          OutRec.VPeriod4 := Trim(FieldByName('VPeriod4').AsString);
          OutRec.VPeriod5 := Trim(FieldByName('VSOScore').AsString);
          OutRec.VPeriod6 := '';
          OutRec.VPeriod7 := '';
          //Determine number of periods
          OutRec.HNumPeriods := 0;
          if (Trim(FieldByName('HPeriod1').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HPeriod2').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HPeriod3').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HPeriod4').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          if (Trim(FieldByName('HSOScore').AsString) <> '') then
            Inc(OutRec.HNumPeriods);
          //Set scoring by period
          OutRec.HScore := Trim(FieldByName('HScore').AsString);
          OutRec.HPeriod1 := Trim(FieldByName('HPeriod1').AsString);
          OutRec.HPeriod2 := Trim(FieldByName('HPeriod2').AsString);
          OutRec.HPeriod3 := Trim(FieldByName('HPeriod3').AsString);
          OutRec.HPeriod4 := Trim(FieldByName('HPeriod4').AsString);
          OutRec.HPeriod5 := Trim(FieldByName('HSOScore').AsString);
          OutRec.HPeriod6 := '';
          OutRec.HPeriod7 := '';
        end
      end;
    end;
  except

  end;
  //Return
  GetBoxscoreData := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
// FANTASY STATS FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Generic function
function GetFantasyStats_General(StoredProcedureName, DataIdentifier, LeagueIdentifier, DataFieldName: String): FantasyGeneralStatsRec;
var
  i, MaxCount: SmallInt;
  OutRec: FantasyGeneralStatsRec;
begin
  //Init
  OutRec.DataFound := FALSE;
  for i := 1 to 10 do
  begin
    OutRec.TeamID[i] := 0;
    OutRec.Name_Last[i] := '';
    OutRec.Name_First[i] := '';
    OutRec.Rank[i] := '';
    OutRec.StatValue[i] := '';
  end;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */dbo.' + StoredProcedureName + ' ' + QuotedStr(DataIdentifier) + ', 0, ' + QuotedStr(''));
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set flag
        OutRec.DataFound := TRUE;
        OutRec.League := LeagueIdentifier;
        //Set max record count
        if (RecordCount < 10) then MaxCount := RecordCount
        else MaxCount := 10;
        //Set output data
        OutRec.CurrentDate := Trunc(Now);
        for i := 1 to MaxCount do
        begin
          OutRec.TeamID[i] := FieldByName('TeamID').AsInteger;
          OutRec.Name_Last[i] := FieldByName('LastName').AsString;
          OutRec.Name_First[i] := FieldByName('FirstName').AsString;
          OutRec.Rank[i] := FieldByName('Rank').AsString;
          OutRec.StatValue[i] := FieldByName(DataFieldName).AsString;
          //Next record
          Next;
        end;
      end;
    end;
  except

  end;
  GetFantasyStats_General := OutRec;
end;

//NFL Functions
//Passing
function GetFantasyFootballPassingStats: FantasyFootballPassingStatsRec;
var
  i, MaxCount: SmallInt;
  OutRec: FantasyFootballPassingStatsRec;
begin
  //Init
  OutRec.DataFound := FALSE;
  for i := 1 to 10 do
  begin
    OutRec.TeamID[i] := 0;
    OutRec.Name_Last[i] := '';
    OutRec.Name_First[i] := '';
    OutRec.Rank[i] := '';
    OutRec.Attempts[i] := '';
    OutRec.Completions[i] := '';
    OutRec.PassingYards[i] := '';
    OutRec.PassingTDs[i] := '';
  end;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */dbo.p_SI_Get_NFL_Weekly_Leaders ' + QuotedStr('Top QB'));
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set flag
        OutRec.DataFound := TRUE;
        //Set max record count
        if (RecordCount < 10) then MaxCount := RecordCount
        else MaxCount := 10;
        //Set output data
        OutRec.CurrentDate := Trunc(Now);
        for i := 1 to MaxCount do
        begin
          OutRec.TeamID[i] := FieldByName('TeamID').AsInteger;
          OutRec.Name_Last[i] := FieldByName('LastName').AsString;
          OutRec.Name_First[i] := FieldByName('FirstName').AsString;
          OutRec.Rank[i] := FieldByName('Rank').AsString;
          OutRec.Attempts[i] := FieldByName('Att').AsString;
          OutRec.Completions[i] := FieldByName('Comp').AsString;
          OutRec.PassingYards[i] := FieldByName('PassYards').AsString;
          OutRec.PassingTDs[i] := FieldByName('PassTD').AsString;
          //Next record
          Next;
        end;
      end;
    end;
  except

  end;
  GetFantasyFootballPassingStats := OutRec;
end;

//Rushing
function GetFantasyFootballRushingStats: FantasyFootballRushingStatsRec;
var
  i, MaxCount: SmallInt;
  OutRec: FantasyFootballRushingStatsRec;
begin
  //Init
  OutRec.DataFound := FALSE;
  for i := 1 to 10 do
  begin
    OutRec.TeamID[i] := 0;
    OutRec.Name_Last[i] := '';
    OutRec.Name_First[i] := '';
    OutRec.Rank[i] := '';
    OutRec.Rushes[i] := '';
    OutRec.Yards[i] := '';
    OutRec.TDs[i] := '';
  end;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */dbo.p_SI_Get_NFL_Weekly_Leaders ' + QuotedStr('Top RB'));
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set flag
        OutRec.DataFound := TRUE;
        //Set max record count
        if (RecordCount < 10) then MaxCount := RecordCount
        else MaxCount := 10;
        //Set output data
        OutRec.CurrentDate := Trunc(Now);
        for i := 1 to MaxCount do
        begin
          OutRec.TeamID[i] := FieldByName('TeamID').AsInteger;
          OutRec.Name_Last[i] := FieldByName('LastName').AsString;
          OutRec.Name_First[i] := FieldByName('FirstName').AsString;
          OutRec.Rank[i] := FieldByName('Rank').AsString;
          OutRec.Rushes[i] := FieldByName('Rush').AsString;
          OutRec.Yards[i] := FieldByName('RushYards').AsString;
          OutRec.TDs[i] := FieldByName('RushTD').AsString;
          //Next record
          Next;
        end;
      end;
    end;
  except

  end;
  GetFantasyFootballRushingStats := OutRec;
end;

//Receiving
function GetFantasyFootballReceivingStats: FantasyFootballReceivingStatsRec;
var
  i, MaxCount: SmallInt;
  OutRec: FantasyFootballReceivingStatsRec;
begin
  //Init
  OutRec.DataFound := FALSE;
  for i := 1 to 10 do
  begin
    OutRec.TeamID[i] := 0;
    OutRec.Name_Last[i] := '';
    OutRec.Name_First[i] := '';
    OutRec.Rank[i] := '';
    OutRec.Receptions[i] := '';
    OutRec.Yards[i] := '';
    OutRec.TDs[i] := '';
  end;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */dbo.p_SI_Get_NFL_Weekly_Leaders ' + QuotedStr('Top WR'));
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set flag
        OutRec.DataFound := TRUE;
        //Set max record count
        if (RecordCount < 10) then MaxCount := RecordCount
        else MaxCount := 10;
        //Set output data
        OutRec.CurrentDate := Trunc(Now);
        for i := 1 to MaxCount do
        begin
          OutRec.TeamID[i] := FieldByName('TeamID').AsInteger;
          OutRec.Name_Last[i] := FieldByName('LastName').AsString;
          OutRec.Name_First[i] := FieldByName('FirstName').AsString;
          OutRec.Rank[i] := FieldByName('Rank').AsString;
          OutRec.Receptions[i] := FieldByName('Receptions').AsString;
          OutRec.Yards[i] := FieldByName('RecYards').AsString;
          OutRec.TDs[i] := FieldByName('RecTD').AsString;
          //Next record
          Next;
        end;
      end;
    end;
  except

  end;
  GetFantasyFootballReceivingStats := OutRec;
end;

//MLB Functions
//Pitching
function GetFantasyBaseballPitchingStats: FantasyBaseballPitchingStatsRec;
var
  i, MaxCount: SmallInt;
  OutRec: FantasyBaseballPitchingStatsRec;
begin
  //Init
  OutRec.DataFound := FALSE;
  for i := 1 to 10 do
  begin
    OutRec.TeamID[i] := 0;
    OutRec.Name_Last[i] := '';
    OutRec.Name_First[i] := '';
    OutRec.Rank[i] := '';
    OutRec.Hits[i] := '';
    OutRec.Walks[i] := '';
    OutRec.Wins[i] := '';
    OutRec.Losses[i] := '';
    OutRec.Saves[i] := '';
    OutRec.Strikeouts[i] := '';
    OutRec.EarnedRuns[i] := '';
  end;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */[dbo].[p_SI_Get_MLB_Daily_Leaders] ' + QuotedStr('Pitcher Stats') + ', 0,' +
        QuotedStr(''));
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set flag
        OutRec.DataFound := TRUE;
        //Set max record count
        if (RecordCount < 10) then MaxCount := RecordCount
        else MaxCount := 10;
        //Set output data
        OutRec.CurrentDate := Trunc(Now);
        for i := 1 to MaxCount do
        begin
          OutRec.TeamID[i] := FieldByName('TeamID').AsInteger;
          OutRec.Name_Last[i] := FieldByName('LastName').AsString;
          OutRec.Name_First[i] := FieldByName('FirstName').AsString;
          OutRec.Rank[i] := IntToStr(i);
          OutRec.Hits[i] := FieldByName('Hits').AsString;
          OutRec.Walks[i] := FieldByName('Walks').AsString;
          OutRec.Wins[i] := FieldByName('Wins').AsString;
          OutRec.Losses[i] := FieldByName('Losses').AsString;
          OutRec.Saves[i] := FieldByName('Saves').AsString;
          OutRec.Strikeouts[i] := FieldByName('Strikeouts').AsString;
          OutRec.EarnedRuns[i] := FieldByName('EarnedRuns').AsString;
          //Next record
          Next;
        end;
      end;
    end;
  except

  end;
  GetFantasyBaseballPitchingStats := OutRec;
end;

//Batting
function GetFantasyBaseballBattingStats: FantasyBaseballBattingStatsRec;
var
  i, MaxCount: SmallInt;
  OutRec: FantasyBaseballBattingStatsRec;
begin
  //Init
  OutRec.DataFound := FALSE;
  for i := 1 to 10 do
  begin
    OutRec.TeamID[i] := 0;
    OutRec.Name_Last[i] := '';
    OutRec.Name_First[i] := '';
    OutRec.Rank[i] := '';
    OutRec.AtBats[i] := '';
    OutRec.Hits[i] := '';
    OutRec.RBI[i] := '';
    OutRec.Doubles[i] := '';
    OutRec.Triples[i] := '';
    OutRec.HomeRuns[i] := '';
    OutRec.Walks[i] := '';
    OutRec.StolenBases[i] := '';
  end;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */[dbo].[p_SI_Get_MLB_Daily_Leaders] ' + QuotedStr('Batter Stats') + ', 0,' +
        QuotedStr(''));
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set flag
        OutRec.DataFound := TRUE;
        //Set max record count
        if (RecordCount < 10) then MaxCount := RecordCount
        else MaxCount := 10;
        //Set output data
        OutRec.CurrentDate := Trunc(Now);
        for i := 1 to MaxCount do
        begin
          OutRec.TeamID[i] := FieldByName('TeamID').AsInteger;
          OutRec.Name_Last[i] := FieldByName('LastName').AsString;
          OutRec.Name_First[i] := FieldByName('FirstName').AsString;
          OutRec.Rank[i] := IntToStr(i);
          OutRec.AtBats[i] := FieldByName('AtBats').AsString;
          OutRec.Hits[i] := FieldByName('Hits').AsString;
          OutRec.RBI[i] := FieldByName('RBI').AsString;
          OutRec.Doubles[i] := FieldByName('Doubles').AsString;
          OutRec.Triples[i] := FieldByName('Triples').AsString;
          OutRec.HomeRuns[i] := FieldByName('HomeRuns').AsString;
          OutRec.Walks[i] := FieldByName('Walks').AsString;;
          OutRec.StolenBases[i] := FieldByName('StolenBases').AsString;;
          //Next record
          Next;
        end;
      end;
    end;
  except

  end;
  GetFantasyBaseballBattingStats := OutRec;
end;

//NBA Functions
//Points
function GetFantasyBasketballStats(StatType: String): FantasyBasketballStatsRec;
var
  i, MaxCount: SmallInt;
  OutRec: FantasyBasketballStatsRec;
begin
  //Init
  OutRec.DataFound := FALSE;
  for i := 1 to 10 do
  begin
    OutRec.TeamID[i] := 0;
    OutRec.Name_Last[i] := '';
    OutRec.Name_First[i] := '';
    OutRec.Rank[i] := '';
    OutRec.Points[i] := '';
    OutRec.Rebounds[i] := '';
    OutRec.Assists[i] := '';
    OutRec.ThreePtFieldGoalsMade[i] := '';
    OutRec.Blocks[i] := '';
    OutRec.Steals[i] := '';
    OutRec.FieldGoalsMade[i] := '';
    OutRec.FieldGoalAttempts[i] := '';
  end;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */[dbo].[p_SI_Get_NBA_Daily_Leaders] ' + QuotedStr(StatType) + ', 0, ' + QuotedStr(''));
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set flag
        OutRec.DataFound := TRUE;
        //Set max record count
        if (RecordCount < 10) then MaxCount := RecordCount
        else MaxCount := 10;
        //Set output data
        OutRec.CurrentDate := Trunc(Now);
        for i := 1 to MaxCount do
        begin
          OutRec.TeamID[i] := FieldByName('TeamID').AsInteger;
          OutRec.Name_Last[i] := FieldByName('LastName').AsString;
          OutRec.Name_First[i] := FieldByName('FirstName').AsString;
          OutRec.Rank[i] := FieldByName('Rank').AsString;
          if (StatType = 'Points') then
            OutRec.Points[i] := FieldByName('Pts').AsString
          else if (StatType = 'Rebounds') then
            OutRec.Rebounds[i] := FieldByName('Reb').AsString
          else if (StatType = 'Assists') then
            OutRec.Assists[i] := FieldByName('Ast').AsString;
          //Next record
          Next;
        end;
      end;
    end;
  except

  end;
  GetFantasyBasketballStats := OutRec;
end;

//NHL Functions
//Points
function GetFantasyHockeyPointsStats: FantasyHockeyPointsStatsRec;
var
  i, MaxCount: SmallInt;
  OutRec: FantasyHockeyPointsStatsRec;
begin
  //Init
  OutRec.DataFound := FALSE;
  for i := 1 to 10 do
  begin
    OutRec.TeamID[i] := 0;
    OutRec.Name_Last[i] := '';
    OutRec.Name_First[i] := '';
    OutRec.Rank[i] := '';
    OutRec.Points[i] := '';
  end;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */[dbo].[p_SI_Get_NHL_Daily_Leaders]' + QuotedStr('Points') + ', 0, ' + QuotedStr(''));
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set flag
        OutRec.DataFound := TRUE;
        //Set max record count
        if (RecordCount < 10) then MaxCount := RecordCount
        else MaxCount := 10;
        //Set output data
        OutRec.CurrentDate := Trunc(Now);
        for i := 1 to MaxCount do
        begin
          OutRec.TeamID[i] := FieldByName('TeamID').AsInteger;
          OutRec.Name_Last[i] := FieldByName('LastName').AsString;
          OutRec.Name_First[i] := FieldByName('FirstName').AsString;
          OutRec.Rank[i] := FieldByName('Rank').AsString;
          OutRec.Points[i] := FieldByName('Pts').AsString;
          //Next record
          Next;
        end;
      end;
    end;
  except

  end;
  GetFantasyHockeyPointsStats := OutRec;
end;

//Goals
function GetFantasyHockeyGoalsStats: FantasyHockeyGoalsStatsRec;
var
  i, MaxCount: SmallInt;
  OutRec: FantasyHockeyGoalsStatsRec;
begin
  //Init
  OutRec.DataFound := FALSE;
  for i := 1 to 10 do
  begin
    OutRec.TeamID[i] := 0;
    OutRec.Name_Last[i] := '';
    OutRec.Name_First[i] := '';
    OutRec.Rank[i] := '';
    OutRec.Goals[i] := '';
  end;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */[dbo].[p_SI_Get_NHL_Daily_Leaders]' + QuotedStr('Goals') + ', 0, ' + QuotedStr(''));
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set flag
        OutRec.DataFound := TRUE;
        //Set max record count
        if (RecordCount < 10) then MaxCount := RecordCount
        else MaxCount := 10;
        //Set output data
        OutRec.CurrentDate := Trunc(Now);
        for i := 1 to MaxCount do
        begin
          OutRec.TeamID[i] := FieldByName('TeamID').AsInteger;
          OutRec.Name_Last[i] := FieldByName('LastName').AsString;
          OutRec.Name_First[i] := FieldByName('FirstName').AsString;
          OutRec.Rank[i] := FieldByName('Rank').AsString;
          OutRec.Goals[i] := FieldByName('Gls').AsString;
          //Next record
          Next;
        end;
      end;
    end;
  except

  end;
  GetFantasyHockeyGoalsStats := OutRec;
end;

//Assists
function GetFantasyHockeyAssistsStats: FantasyHockeyAssistsStatsRec;
var
  i, MaxCount: SmallInt;
  OutRec: FantasyHockeyAssistsStatsRec;
begin
  //Init
  OutRec.DataFound := FALSE;
  for i := 1 to 10 do
  begin
    OutRec.TeamID[i] := 0;
    OutRec.Name_Last[i] := '';
    OutRec.Name_First[i] := '';
    OutRec.Rank[i] := '';
    OutRec.Assists[i] := '';
  end;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQL.Add('/* */[dbo].[p_SI_Get_NHL_Daily_Leaders]' + QuotedStr('Assists') + ', 0, ' + QuotedStr(''));
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set flag
        OutRec.DataFound := TRUE;
        //Set max record count
        if (RecordCount < 10) then MaxCount := RecordCount
        else MaxCount := 10;
        //Set output data
        OutRec.CurrentDate := Trunc(Now);
        for i := 1 to MaxCount do
        begin
          OutRec.TeamID[i] := FieldByName('TeamID').AsInteger;
          OutRec.Name_Last[i] := FieldByName('LastName').AsString;
          OutRec.Name_First[i] := FieldByName('FirstName').AsString;
          OutRec.Rank[i] := FieldByName('Rank').AsString;
          OutRec.Assists[i] := FieldByName('Ast').AsString;
          //Next record
          Next;
        end;
      end;
    end;
  except

  end;
  GetFantasyHockeyAssistsStats := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
// SEASON LEADER STATS FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
function GetLeaderStats_General(StoredProcedureName, DataIdentifier, LeagueIdentifier, DataFieldName: String): SeasonLeadersGeneralStatsRec;
var
  i, MaxCount: SmallInt;
  OutRec: SeasonLeadersGeneralStatsRec;
  SQLStr: String;
begin
  //Init
  OutRec.DataFound := FALSE;
  for i := 1 to 10 do
  begin
    OutRec.League := '';
    OutRec.TeamID[i] := 0;
    OutRec.Name_Last[i] := '';
    OutRec.Name_First[i] := '';
    OutRec.Rank[i] := '';
    OutRec.StatValue[i] := '';
  end;
  try
    //Execute required stored procedure
    with dmMain.SportbaseQuery2 do
    begin
      Active := FALSE;
      SQL.Clear;
      SQLStr := '/* */dbo.' + StoredProcedureName + ' ' + QuotedStr(DataIdentifier) + ', ';
      if (Trim(LeagueIdentifier) <> '') and (Trim(LeagueIdentifier) <> 'NHL') then
        SQLStr := SQLStr + QuotedStr(LeagueIdentifier) + ', ';
      SQLStr := SQLStr + ' 0, ' + QuotedStr('');
      SQL.Add(SQLStr);
      Open;
      First;
      //Check for record count > 0 and process
      if (RecordCount > 0) then
      begin
        //Set flag
        OutRec.DataFound := TRUE;
        //Set max record count
        if (RecordCount < 10) then MaxCount := RecordCount
        else MaxCount := 10;
        //Set output data
        OutRec.CurrentDate := Trunc(Now);
        OutRec.League := LeagueIdentifier;
        for i := 1 to MaxCount do
        begin
          OutRec.TeamID[i] := FieldByName('TeamID').AsInteger;
          OutRec.Name_Last[i] := FieldByName('LastName').AsString;
          OutRec.Name_First[i] := FieldByName('FirstName').AsString;
          OutRec.Rank[i] := FieldByName('Rank').AsString;
          OutRec.StatValue[i] := FieldByName(DataFieldName).AsString;
          //Add suffix for Yards
          if (UpperCase(DataFieldName) = 'YDS') then
            OutRec.StatValue[i] := OutRec.StatValue[i] + ' Yds';
          //Next record
          Next;
        end;
      end;
    end;
  except

  end;
  GetLeaderStats_General := OutRec;
end;

end.
