unit Globals;

interface

uses
  StBase, StColl, CheckTickerSchedule;

{$H+}
type
  TickerRec = Record
    Event_Index: Smallint;
    Field_ID: SmallInt;
    Event_GUID: TGUID;
    Enabled: Boolean;
    Is_Child: Boolean;
    League: String[10];
    Subleague_Mnemonic_Standard: String[10];
    Mnemonic_LiveEvent: String[10];
    Template_ID: SmallInt;
    Record_Type: SmallInt;
    GameID: String[20];
    SponsorLogo_Name: String[50];
    SponsorLogo_Dwell: SmallInt;
    StatStoredProcedure: String[50];
    StatType: SmallInt;
    StatTeam: String[50];
    StatLeague: String[20];
    StatRecordNumber: SmallInt;
    UserData: Array[1..50] of String[255];
    StartEnableDateTime: TDateTime;
    EndEnableDateTime: TDateTime;
    Selected: Boolean;
    DwellTime: LongInt;
    Description: String[100];
    Comments: String[100];
  end;

  ColorRec = record
    Description: String;
    DBIndexValue: SmallInt;
  end;

  OverlayRec = Record
    OverlayName: String[20];
    OverlayType: SmallInt;
    OverlayText: String[10];
    OverlayFilename: String[50];
  end;

  TeamRec = record
    League: String[25];
    OldSTTeamCode: String[8];
    NewSTTeamCode: String[15];
    StatsIncID: Double;
    LongName: String[100];
    ShortName: String[100];
    BaseName: String[100];
    DisplayName1: String[50];
    DisplayName2: String[50];
    CamioTeamLogoPath: String[100];
  end;

  TeamDisplayInfoRec = record
    DisplayMnemonic: String[10];
    Displayname: String[50];
    Division: String[50];
    Conference: String[50];
  end;

  LeagueRec = record
    LeagueNameShort: String[20];
    LeagueNameLong: String[50];
    LeagueGraphicFilename: String[50];
    LeagueType: SmallInt;
  end;

  StatRec = Record
    StatLeague: String[10];
    StatType: SmallInt;
    StatDescription: String[50];
    StatHeader: String[50];
    StatAbbreviation: String[10];
    StatStoredProcedure: String[50];
    StatDataField: String[20];
    UseStatQualifier: Boolean;
    StatQualifier: String[100];
    StatQualifierValue: SmallInt;
  end;

  SelectedStatRec = record
    StatType: SmallInt;
    StatStoredProcedure: String;
    StatLeague: String;
    StatTeam: String;
  end;

  SponsorLogoRec = Record
    SponsorLogoIndex: SmallInt;
    SponsorLogoName: String[50];
    SponsorLogoFilename: String[50];
    IsDefaultLogo: Boolean;
  end;

  CurrentSponsorLogoRec = Record
    CurrentSponsorLogoName: String[50];
    CurrentSponsorLogoDwell: SmallInt;
    CurrentSponsorTemplate: SmallInt;
    LastDisplayedSponsorLogoName: String[50];
    Tagline_Top: String[100];
    Tagline_Bottom: String[100];
  end;

  PromoLogoRec = Record
    PromoLogoIndex: SmallInt;
    PromoLogoName: String[20];
    PromoLogoFilename: String[20];
  end;

  GamePhaseRec = record
    League: String[10];
    ST_Phase_Code: SmallInt;
    SI_Phase_Code: SmallInt;
    Label_Period: String[20];
    Display_Period: String[10];
    End_Is_Half: Boolean;
    Display_Half: String[20];
    Display_Final: String[20];
    Display_Extended1: String[10];
    Display_Extended2: String[10];
    Game_OT: Boolean;
  end;

  LineupText = Record
    UseLineup: Boolean;
    TextFields: Array[1..5] of String;
  end;

  EngineRec = Record
    Enabled: Boolean;
    IPAddress: String;
    Port: String;
  end;

  TemplateDefsRec = record
    Template_ID: SmallInt;
    Template_Type: SmallInt;
    AlternateModeTemplateID: SmallInt;
    Template_Description: String[100];
    Record_Type: SmallInt;
    TemplateSponsorType: SmallInt;
    Scene_ID_1: LongInt;
    Scene_ID_2: LongInt;
    Transition_In: String[100];
    Transition_Out: String[100];
    Default_Dwell: Word;
    ManualLeague: Boolean;
    EnableForOddsOnly: Boolean;
    UsesGameData: Boolean;
    RequiredGameState: SmallInt;
    Template_Has_Children: Boolean;
    Template_Is_Child: Boolean;
    Use_Alert_Background: Boolean;
    IsFantasyTemplate: Boolean;
    IsLeadersTemplate: Boolean;
    StartEnableDateTime: TDateTime;
    EndEnableDateTime: TDateTime;
  end;

  TemplateFieldsRec = record
    Template_ID: SmallInt;
    Field_ID: SmallInt;
    Field_Type: SmallInt;
    Field_Is_Manual: Boolean;
    Field_Label: String[100];
    Field_Contents: String[200];
    Field_Format_Prefix: String[50];
    Field_Format_Suffix: String[50];
    Field_Max_Length: SmallInt;
    Scene_Field_Name: String[100];
  end;

  CategoryRec = record
    Category_Type: SmallInt;
    Category_ID: SmallInt;
    Category_Name: String[50];
    Category_Label: String[10];
    Category_Description: String[100];
    Category_Is_Sport: Boolean;
    Category_Sport: String[10];
    Sport_Games_Table_Name: String[100];
    CamioFolderName: String[100];
  end;

  CategoryTemplatesRec = record
    Category_ID: SmallInt;
    Template_ID: SmallInt;
    Template_Type: SmallInt;
    Template_Description: String[100];
  end;

  SportRec = record
    Sport_Mnemonic: String[10];
    Sport_Description: String[50];
    Sport_Games_Table_Name: String[100];
    Sport_Stats_Query: String[250];
  end;

  GameRec = record
    League: String[10];
    GameID: String[15];
    DataFound: Boolean;
    SubLeague: String[50];
    DoubleHeader: Boolean;
    DoubleHeaderGameNumber: SmallInt;
    VStatsIncID: LongInt;
    HStatsIncID: LongInt;
    HName: String[50];
    VName: String[50];
    HMnemonic: String[10];
    VMnemonic: String[10];
    HRecord: String[15];
    VRecord: String[15];
    HStreak: String[15];
    VStreak: String[15];
    HRank: String[4];
    VRank: String[4];
    HScore: SmallInt;
    VScore: SmallInt;
    VOpeningOdds: Double;
    HOpeningOdds: Double;
    OpeningTotal: Double;
    VCurrentOdds: Double;
    HCurrentOdds: Double;
    CurrentTotal: Double;
    Phase: SmallInt;
    TimeMin: SmallInt;
    TimeSec: SmallInt;
    StateForInProgressCheck: SmallInt;
    State: SmallInt;
    Count: String[5];
    Situation: String[50];
    Batter: String[25];
    Baserunners: Array[1..3] of String[25];
    Description: String[100];
    Date: TDateTime;
    StartTime: TDateTime;
    Visitor_Param1, Visitor_Param2: SmallInt;
    Home_Param1, Home_Param2: SmallInt;
  end;

  RecordTypeRec = record
    Playlist_Type: SmallInt;
    Record_Type: SmallInt;
    Record_Description: String[50];
  end;

  StyleChipRec = record
    StyleChip_Index: SmallInt;
    StyleChip_Code: String[4];
    StyleChip_Description: String[50];
    StyleChip_Type: SmallInt;
    StyleChip_String: String[250];
    StyleChip_FontCode: SmallInt;
    StyleChip_CharacterCode: SmallInt;
  end;

  LeagueCodeRec = record
    League_Mnemonic_Standard: String[10];
    Subleague_Mnemonic_Standard: String[10];
    Mnemonic_LiveEvent: String[10];
    Is_Subleague: Boolean;
  end;

  AutomatedLeagueRec = record
    SI_LeagueCode: String[10];
    ST_LeagueCode: String[10];
    Display_Mnemonic: String[10];
  end;

  PlaylistInfoRec = record
    Playlist_Description: String[100];
    Start_Enable_Time: TDateTime;
    End_Enable_Time: TDateTime;
    Playlist_ID: Double;
    Station_ID: SmallInt;
    Ticker_Mode: SmallInt;
  end;

  FootballSituationRec = record
    VisitorHasPossession: Boolean;
    HomeHasPossession: Boolean;
    YardsFromGoal: SmallInt;
    Down: SmallInt;
    Distance: SmallInt;
  end;

  //New record types for Stats Inc. Phase 2 implementation
  WeatherIconRec = record
    Icon_Description: String[50];
    Icon_Name: String[50];
  end;

  WeatherForecastRec = record
    CurrentLeague: String[10];
    CurrentHomeStatsID: LongInt;
    Weather_Stadium_Name: String[50];
    Weather_Current_Temperature: String[5];
    Weather_High_Temperature: String[5];
    Weather_Low_Temperature: String[5];
    Weather_Current_Conditions: String[50];
    Weather_Forecast_Icon_Day: String[50];
    Weather_Forecast_Icon_Night: String[50];
  end;

  //For team records
  TeamRecordRec = record
    Param: Array[1..4] of String;
  end;

  //For starting pitchers
  StartingPitcherStatsRec = record
    CurrentGameID: String[15];
    Home_Pitcher_Name_Last: String[20];
    Home_Pitcher_Name_First: String[20];
    Home_Pitcher_Wins: String[5];
    Home_Pitcher_Losses: String[5];
    Visitor_Pitcher_Name_Last: String[20];
    Visitor_Pitcher_Name_First: String[20];
    Visitor_Pitcher_Wins: String[5];
    Visitor_Pitcher_Losses: String[5];
  end;

  //For game final pitcher stats
  FinalPitcherStatsRec = record
    CurrentGameID: String[15];
    NeedToAddSavingPitcher: Boolean;
    Winning_Pitcher_Name_Last: String[20];
    Winning_Pitcher_Name_First: String[20];
    Winning_Pitcher_Wins: String[5];
    Winning_Pitcher_Losses: String[5];
    Winning_Pitcher_IP: String[5];
    Winning_Pitcher_K: String[5];
    Winning_Pitcher_BB: String[5];
    Winning_Pitcher_Hits: String[5];
    Winning_Pitcher_Runs: String[5];
    Winning_Pitcher_EarnedRuns: String[5];
    Winning_Pitcher_HomeRuns: String[5];
    Losing_Pitcher_Name_Last: String[20];
    Losing_Pitcher_Name_First: String[20];
    Losing_Pitcher_Wins: String[5];
    Losing_Pitcher_Losses: String[5];
    Losing_Pitcher_IP: String[5];
    Losing_Pitcher_K: String[5];
    Losing_Pitcher_BB: String[5];
    Losing_Pitcher_Hits: String[5];
    Losing_Pitcher_Runs: String[5];
    Losing_Pitcher_EarnedRuns: String[5];
    Losing_Pitcher_HomeRuns: String[5];
    Saving_Pitcher_Name_Last: String[20];
    Saving_Pitcher_Name_First: String[20];
    Saving_Pitcher_Saves: String[5];
  end;

  FinalBatterStatsRec = record
    Name_Last: String[20];
    Name_First: String[20];
    Atbats: String[5];
    Hits: String[5];
    Runs: String[5];
    Doubles: String[5];
    Triples: String[5];
    HomeRuns: String[5];
    RBI: String[5];
    StolenBases: String[5];
  end;

  //Basketball stats function records
  FinalBBallStatsRec = record
    CurrentLeague: String[10];
    CurrentGameID: String[15];
    Name_Last: String[20];
    Name_First: String[20];
    Points: String;
    Rebounds: String;
    Assists: String;
  end;

  //Hockey stats function records
  //Skater stats
  FinalSkaterStatsRec = record
    CurrentGameID: String[15];
    Name_Last: String[20];
    Name_First: String[20];
    Goals: String;
    Assists: String;
    SOG: String;
  end;

  //Goalie stats
  FinalGoalieStatsRec = record
    CurrentGameID: String[15];
    Name_Last: String[20];
    Name_First: String[20];
    Shots: String;
    Saves: String;
    GoalsAgainst: String;
  end;

  //Football stats function records
  FinalFBStatsRec = record
    CurrentLeague: String[10];
    CurrentGameID: String[15];
    VPassingName_Last: String[20];
    VPassingName_First: String[20];
    VPassingAttempts: String;
    VPassingCompletions: String;
    VPassingYards: String;
    VPassingTouchdowns: String;
    VPassingInterceptions: String;
    HPassingName_Last: String[20];
    HPassingName_First: String[20];
    HPassingAttempts: String;
    HPassingCompletions: String;
    HPassingYards: String;
    HPassingTouchdowns: String;
    HPassingInterceptions: String;
    VRushingName_Last: String[20];
    VRushingName_First: String[20];
    VRushingAttempts: String;
    VRushingYards: String;
    VRushingTouchdowns: String;
    HRushingName_Last: String[20];
    HRushingName_First: String[20];
    HRushingAttempts: String;
    HRushingYards: String;
    HRushingTouchdowns: String;
    VReceivingName_Last: String[20];
    VReceivingName_First: String[20];
    VReceivingReceptions: String;
    VReceivingYards: String;
    VReceivingTouchdowns: String;
    HReceivingName_Last: String[20];
    HReceivingName_First: String[20];
    HReceivingReceptions: String;
    HReceivingYards: String;
    HReceivingTouchdowns: String;
  end;

  //General stats function records
  BoxscoreRec = record
    CurrentLeague: String[10];
    CurrentGameID: String[15];
    VNumPeriods: SmallInt;
    VScore: String[3];
    VPeriod1: String[3];
    VPeriod2: String[3];
    VPeriod3: String[3];
    VPeriod4: String[3];
    VPeriod5: String[3];
    VPeriod6: String[3];
    VPeriod7: String[3];
    HNumPeriods: SmallInt;
    HScore: String[3];
    HPeriod1: String[3];
    HPeriod2: String[3];
    HPeriod3: String[3];
    HPeriod4: String[3];
    HPeriod5: String[3];
    HPeriod6: String[3];
    HPeriod7: String[3];
  end;

  //////////////////////////////////////////////////////////////////////////////
  //Begin Fantasy record types
  //////////////////////////////////////////////////////////////////////////////
  //Generic
  FantasyGeneralStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    League: String[10];
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    StatValue: Array[1..10] of String;
  end;

  //NFL
  FantasyFootballPassingStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Attempts: Array[1..10] of String;
    Completions: Array[1..10] of String;
    PassingYards: Array[1..10] of String;
    PassingTDs: Array[1..10] of String;
  end;

  FantasyFootballRushingStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Rushes: Array[1..10] of String;
    Yards: Array[1..10] of String;
    TDs: Array[1..10] of String;
  end;

  FantasyFootballReceivingStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Receptions: Array[1..10] of String;
    Yards: Array[1..10] of String;
    TDs: Array[1..10] of String;
  end;

  //MLB
  FantasyBaseballPitchingStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Hits: Array[1..10] of String;
    Walks: Array[1..10] of String;
    Wins: Array[1..10] of String;
    Losses: Array[1..10] of String;
    Saves: Array[1..10] of String;
    Strikeouts: Array[1..10] of String;
    EarnedRuns: Array[1..10] of String;
  end;

  FantasyBaseballBattingStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    AtBats: Array[1..10] of String;
    Hits: Array[1..10] of String;
    RBI: Array[1..10] of String;
    Doubles: Array[1..10] of String;
    Triples: Array[1..10] of String;
    HomeRuns: Array[1..10] of String;
    Walks: Array[1..10] of String;
    StolenBases: Array[1..10] of String;
  end;

  //NBA
  FantasyBasketballStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Points: Array[1..10] of String;
    Rebounds: Array[1..10] of String;
    Assists: Array[1..10] of String;
    ThreePtFieldGoalsMade: Array[1..10] of String;
    Blocks: Array[1..10] of String;
    Steals: Array[1..10] of String;
    FieldGoalsMade: Array[1..10] of String;
    FieldGoalAttempts: Array[1..10] of String;
  end;

  //NHL
  FantasyHockeyPointsStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Points: Array[1..10] of String;
  end;

  FantasyHockeyGoalsStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Goals: Array[1..10] of String;
  end;

  FantasyHockeyAssistsStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    Assists: Array[1..10] of String;
  end;
  //////////////////////////////////////////////////////////////////////////////
  //End Fantasy record types
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  //Start Season Leaders record types
  //////////////////////////////////////////////////////////////////////////////
  //General leader type
  SeasonLeadersGeneralStatsRec = record
    CurrentDate: TDateTime;
    DataFound: Boolean;
    League: String[10];
    TeamID: Array[1..10] of LongInt;
    Name_Last: Array[1..10] of String[20];
    Name_First: Array[1..10] of String[20];
    Rank: Array[1..10] of String;
    StatValue: Array[1..10] of String;
  end;

  //////////////////////////////////////////////////////////////////////////////
  //End Season Leaders record types
  //////////////////////////////////////////////////////////////////////////////

  DefaultTemplatesRec = record
    Category_ID: Smallint;
    Category_Name: String[50];
    Template_Index: Integer;
    Template_ID: Integer;
    Template_Description: String[255];
    Enabled: Boolean;
    Selected: Boolean;
  end;

  //Aded flag to indicate data value is valid; used for symbolic names
  SymbolValueRec = record
    SymbolValue: String;
    OKToDisplay: Boolean;
  end;

  //////////////////////////////////////////////////////////////////////////////
  //For LBar
  //////////////////////////////////////////////////////////////////////////////
  //For remote control command from Authoring tool
  CommandRec = record
    Command: String;
    Param: Array[1..4] of String;
  end;

  PlayoutControllerRec = Record
    Enabled: Boolean;
    IPAddress: String[20];
    Port: Integer;
    Connected: Boolean;
    ErrorFlag: Boolean;
  end;

  SceneRec = record
    Scene_ID: LongInt;
    Scene_Description: String[100];
    Is_Base_Scene: Boolean;
    Load_At_Startup: Boolean;
    Play_At_Startup: Boolean;
    Run_Startup_Command: Boolean;
    Startup_Command: String[250];
    Scene_Status: SmallInt;
  end;

  LastTemplateRec = record
    Scene_ID: LongInt;
    //Last_Field_Scene_ID: LongInt;
    OutTransition_Command: String[250];
    LastSceneToggleState: SmallInt;
    LastFieldTypeID: SmallInt;
  end;

  LBarWeatherForecastRec = record
    LocationCode: String[30];
    Weather_City: String[50];
    Weather_Temperature_Current: String[5];
    Weather_Icon_Today: String[50];
    Weather_Icon_Tonight: String[50];
    Weather_Temperature_Today_High: String[5];
    Weather_Temperature_Today_Low: String[5];
    Weather_Icon_Tomorrow: String[50];
    Weather_Temperature_Tomorrow_High: String[5];
    Weather_Temperature_Tomorrow_Low: String[5];
    Weather_Icon_Day_After_Tomorrow: String[50];
    Weather_Temperature_Day_After_Tomorrow_High: String[5];
    Weather_Temperature_Day_After_Tomorrow_Low: String[5];
  end;

  LBarWeatherIconRec = record
    IconID: Smallint;
    IconDescription: String[50];
    IconFilename: String[50];
    IconCharacterValue: SmallInt;
  end;

  GPIRec = record
    GPI_String: String[10];
    GPI_Delay: SmallInt;
  end;

  TrafficDataRec = record
    DataStr: String[255];
  end;

  //Added for version 2.0
  TopGraphicRec = record
    GraphicIndex: SmallInt;
    GraphicFilePath: String[150];
    DwellTime: SmallInt;
  end;

var
  PrefsFile: TextFile;
  Operator: String;
  Description: String;
  User_Collection: TStCollection; //Collection used for user logins
  Ticker_Collection: TStCollection; //Collection used for zipper entries - Field 1/3
  Ticker_Collection2: TStCollection; //Collection used for zipper entries - Field 2
  Ticker_Collection3: TStCollection; //Collection used for zipper entries - sponsors
  Team_Collection: TStCollection; //Collection used for NCAA teams
  League_Collection: TStCollection; //Collection used for leagues
  Ticker_Playout_Collection: TStCollection; //Collection used for zipper playout
  Overlay_Collection: TStCollection; //Collection used for overlays
  SponsorLogo_Collection: TStCollection; //Collection used for Sponsor logos
  PromoLogo_Collection: TStCollection; //Collection used for Promo logos
  Stat_Collection: TStCollection; //Collection used for stats types
  Temp_Stat_Collection: TStCollection; //Collection used for stats types
  Game_Phase_Collection: TStCollection; //Collection used for stats types
  Template_Defs_Collection: TStCollection; //Collection used for Template definitions
  Template_Fields_Collection: TStCollection; //Collection for template fields
  Temporary_Fields_Collection, Temporary_Fields_Collection2: TStCollection; //Collection for template fields
  Categories_Collection: TStCollection; //Collection for ticker categories
  Category_Templates_Collection: TStCollection; //Collection for templates that apply to ticker categories
  RecordType_Collection: TStCollection; //Collection for record types
  StyleChip_Collection: TStCollection; //Collection for style chips
  LeagueCode_Collection: TStCollection; //Collection for league codes
  Automated_League_Collection: TStCollection; //Collection used for leagues
  Scene_Collection: TStCollection; //Collection used for weather icons
  LBar_Weather_Icon_Collection: TStCollection; //Collection used for weather icons
  Traffic_Data_Collection: TStCollection; //For traffic data
  //Added for Version 2.0
  Top_Graphic_Collection: TStCollection; //For top region marketing graphics (URLs, Images)

  DBConnectionString, DBConnectionString2, DBConnectionString3: String;
  SearchText: String;
  SpellCheckerDictionaryDir: String;
  LoginComplete: Boolean;
  LastPageIndex: SmallInt;
  LoopTicker: Boolean;
  CurrentTickerEntryIndex1, CurrentTickerEntryIndex2: SmallInt;
  CurrentBreakingNewsEntryIndex: SmallInt;
  LastOverlay: String;
  NewOverlayPending: Boolean;
  ZipperColors: Array[0..8] of ColorRec;
  GameEntryMode: SmallInt; //1 = NCAA, 2 = NFL
  MaxCharsMatchupNotes, MaxCharsGameNotes: SmallInt;
  CurrentLeague: String;
  UseXMLGTServer: Boolean;
  SocketConnected: Boolean;
  EngineParameters: EngineRec;
  EngineControlMode: SmallInt;
  SponsorLogoDwell: SmallInt;
  SponsorLogoDwellCounter: SmallInt;
  ErrorLogFile: TextFile; {File for logging hardware interface errors}
  AsRunLogFile: TextFile; {File for logging sponsors & GPIs}
  AsRunLogFileDirectoryPath: String;
  Error_Condition: Boolean; {Flag to indicate an error occurred}
  ErrorLoggingEnabled: Boolean; {Flag to indicate error logging is enabled}
  CommandLoggingEnabled: Boolean; {Flag to indicate command logging is enabled}
  CommandLogFile: TextFile; {File for logging commands}
  CommandLogFileDirectoryPath: String;
  PacketEnable: Boolean;
  TickerDisplayMode: SmallInt;
  TickerAbortFlag: Boolean;
  RunningTicker: Boolean;
  ElapsedTimeThisWheel: LongInt;
  Debug: Boolean;
  RunningThread: Boolean;
  LastDayOfWeek: SmallInt;
  InStartup: Boolean;
  GPIsEnabled: Boolean;
  GPICOMPortNum: SmallInt;
  GPIBaudRate: SmallInt;
  GPIsOKToFire: Boolean;
  RefreshingGrid: Boolean;
  LogFilePath: String;
  LogFileName: String;
  ScheduleMonitoringEnabled: Boolean;
  CurrentTickerPlaylistID: Double;
  CurrentTickerPlaylistStartTime: TDateTime;
  CurrentBreakingNewsPlaylistID: Double;
  CurrentBreakingNewsPlaylistStartTime: TDateTime;
  CurrentBreakingNewsPlaylistEndTime: TDateTime;
  TickerInLiveEventMode: Boolean;
  AsRunLoggingEnabled: Boolean;
  //For logo/clock
  LogoClockEnabled: Boolean;
  LogoClockMode: SmallInt;
  CurrentTimeZone: SmallInt;
  LoadingPlaylist: Boolean;
  CheckTickerScheduleThread: TCheckTickerSchedule;
  UpdatingTickerPlaylist: Boolean;
  UpdatingBreakingNewsPlaylist: Boolean;
  ScheduledPlaylistInfo: Array[1..3] of PlaylistInfoRec;
  ScoreLogoOnlyEnable: Boolean;
  MasterSegmentIndex: SmallInt;
  BreakingNewsIterationsCompleted: SmallInt;
  CurrentGameData, CurrentGameData2: GameRec;
  ForceUpperCase: Boolean;
  ForceUpperCaseGameInfo: Boolean;
  USEDATAPACKET: Boolean;
  LastSegmentHeading: String;
  GameStartTimeOffset: Double;
  TimeZoneSuffix: String;
  ScheduledBreakingNewsPlaylistID: Double;
  EnableTemplateFieldFormatting: Boolean;

  //For  station ID
  StationID: SmallInt;
  StationDescription: String;

  //For game finals
  UseGameFinalGlyph: Boolean;
  GameFinalGlyphCharacterID: SmallInt;

  //For sponsor logos
  CurrentSponsorInfo: Array[1..2] of CurrentSponsorLogoRec;
  EnablePersistentSponsorLogo: Boolean;
  FullSponsorPagesEnabled: Boolean;
  TailSponsorPagesEnabled: Boolean;
  BlankOutLineupForSponsors: Boolean;
  AllowAlertBackgroundsForNews: Boolean;

  //For expansion into multiple pages
  FantasyExpansionPageCount, LeadersExpansionPageCount: SmallInt;

  //Used for Phase 2 stats data to prevent multiple stored proc calls
  CurrentWeatherForecastRec: WeatherForecastRec;
  CurrentStartingPitcherStatsRec: StartingPitcherStatsRec;
  CurrentFinalPitcherStatsRec: FinalPitcherStatsRec;
  CurrentBoxscoreRec: BoxscoreRec;
  CurrentBBallStatsRec: FinalBBallStatsRec;
  CurrentFBStatsRec: FinalFBStatsRec;
  //For fantasy stats
  CurrentFantasyFootballPassingStatsRec: FantasyFootballPassingStatsRec;
  CurrentFantasyFootballRushingStatsRec: FantasyFootballRushingStatsRec;
  CurrentFantasyFootballReceivingStatsRec: FantasyFootballReceivingStatsRec;
  CurrentFantasyBaseballPitchingStatsRec: FantasyBaseballPitchingStatsRec;
  CurrentFantasyBaseballBattingStatsRec: FantasyBaseballBattingStatsRec;
  CurrentFantasyBasketballPointsStatsRec: FantasyBasketballStatsRec;
  CurrentFantasyBasketballReboundsStatsRec: FantasyBasketballStatsRec;
  CurrentFantasyBasketballAssistsStatsRec: FantasyBasketballStatsRec;
  CurrentFantasyHockeyPointsStatsRec: FantasyHockeyPointsStatsRec;
  CurrentFantasyHockeyGoalsStatsRec: FantasyHockeyGoalsStatsRec;
  CurrentFantasyHockeyAssistsStatsRec: FantasyHockeyAssistsStatsRec;

  //Version 3.1 - New Fantasy Record Types
  CurrentFantasyBaseballHitsStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballHomeRunsStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballRBIsStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballRunsScoredStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballStolenBasesStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballWinsStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballSavesStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballStrikeoutsStatsRec: FantasyGeneralStatsRec;
  CurrentFantasyBaseballInningsPitchedStatsRec: FantasyGeneralStatsRec;

  //Version 3.1 - Start season leaders record types
  Current_NFL_QB_Leaders_Yds: SeasonLeadersGeneralStatsRec;
  Current_NFL_QB_Leaders_TDs: SeasonLeadersGeneralStatsRec;
  Current_NFL_RB_Leaders_Yds: SeasonLeadersGeneralStatsRec;
  Current_NFL_RB_Leaders_TDs: SeasonLeadersGeneralStatsRec;
  Current_NFL_WR_Leaders_Yds: SeasonLeadersGeneralStatsRec;
  Current_NFL_WR_Leaders_TDs: SeasonLeadersGeneralStatsRec;
  Current_NFL_Field_Goal_Leaders_FGM: SeasonLeadersGeneralStatsRec;

  Current_MLB_AL_Leaders_Hits: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_HR: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_RBIs: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_Runs: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_Stolen_Bases: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_Strikeouts: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_Innings_Pitched: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_Wins: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_Saves: SeasonLeadersGeneralStatsRec;
  Current_MLB_AL_Leaders_ERA: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Hits: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_HR: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_RBIs: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Runs: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Stolen_Bases: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Strikeouts: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Innings_Pitched: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Wins: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_Saves: SeasonLeadersGeneralStatsRec;
  Current_MLB_NL_Leaders_ERA: SeasonLeadersGeneralStatsRec;

  Current_NHL_Leaders_GPG: SeasonLeadersGeneralStatsRec;
  Current_NHL_Leaders_APG: SeasonLeadersGeneralStatsRec;
  Current_NHL_Leaders_PPG: SeasonLeadersGeneralStatsRec;
  Current_NHL_Leaders_Saves: SeasonLeadersGeneralStatsRec;
  Current_NHL_Leaders_GAA: SeasonLeadersGeneralStatsRec;

  Current_NBA_Leaders_PPG: SeasonLeadersGeneralStatsRec;
  Current_NBA_Leaders_RPG: SeasonLeadersGeneralStatsRec;
  Current_NBA_Leaders_APG: SeasonLeadersGeneralStatsRec;
  Current_NBA_Leaders_SPG: SeasonLeadersGeneralStatsRec;
  Current_NBA_Leaders_BPG: SeasonLeadersGeneralStatsRec;
  //End season leaders record types
  
  //To enable showing time and day in schedule for games not scheduled for the current day
  ShowTimeAndDay: Boolean;

  //Format for SNY
  FormatForSNY: Boolean;

  //Abbreviations
  ABBV_Rushes: String;
  ABBV_Receptions: String;
  ABBV_Yards: String;
  ABBV_TDs: String;
  ABBV_INTs: String;
  ABBV_Points: String;
  ABBV_Rebounds: String;
  ABBV_Assists: String;
  ABBV_Steals: String;
  ABBV_Blocks: String;
  ABBV_PPG: String;
  ABBV_RPG: String;
  ABBV_APG: String;
  ABBV_SPG: String;
  ABBV_BPG: String;
  ABBV_Goals: String;
  ABBV_Saves: String;

  //For disabling automated rankings
  UseManualRankings_NCAAB, UseManualRankings_NCAAF, UseManualRankings_NCAAW: Boolean;

  //Added for NCAAB Tournament
  UseSeedingForNCAABGames: Boolean;

  //For remote control interface from Authoring tool
  AuthoringControlPortNum: String;
  AuthoringControlInterfaceEnabled: Boolean;
  AuthoringRemoteControlModeEnabled: Boolean;
  AutoSequenceEnabled: Boolean;

  //////////////////////////////////////////////////////////////////////////////
  //CSNMA L-Bar
  //////////////////////////////////////////////////////////////////////////////
  PlayoutControllerInfo: PlayoutControllerRec;

  //For remote playout mode
  InRemotePlayoutMode: Boolean;

  //For images
  League_Logo_Mapped_Drive: String;
  League_Logo_Host_Drive: String;
  League_Logo_Base_Path: String;
  Team_Logo_Mapped_Drive: String;
  Team_Logo_Host_Drive: String;
  Team_Logo_Base_Path: String;
  Player_Headshot_Mapped_Drive: String;
  Player_Headshot_Host_Drive: String;
  Player_Headshot_Base_Path: String;
  Weather_Icon_Base_Path: String;
  Sponsor_Logo_Base_Path: String;
  //Added for Version 2.0
  Full_Promo_Graphic_Mapped_Drive: String;
  Full_Promo_Graphic_Base_Path: String;
  Full_Promo_Graphic_Host_Drive: String;
  Top_Region_Image_Mapped_Drive: String;
  Top_Region_Image_Base_Path: String;
  Top_Region_Image_Host_Drive: String;
  Base_Element_Path: String;
  End_Element_Path: String;
  Headshot_Backplate_Filename: String;
  Team_Logo_Headshot_Filename: String;
  Team_Chip_A_Filename: String;
  Team_Chip_B_Filename: String;
  Team_Color_Filename: String;
  Flag_Logo_Headshot_Filename: String;

  LoadScenesAtStartup: Boolean;

  //For Field 1/3, Field 2, Sponsors
  LastTemplateInfo: Array[1..3] of LastTemplateRec;

  //For base scene commands
  BaseScene_SceneID: LongInt;
  BaseScene_Field_1_In_Command: String;
  BaseScene_Field_1_Out_Command: String;
  BaseScene_Field_2_In_Command: String;
  BaseScene_Field_2_Out_Command: String;
  BaseScene_Field_3_In_Command: String;
  BaseScene_Field_3_Out_Command: String;

  //For LBar weather data
  CurrentLBarWeatherForecastRec: LBarWeatherForecastRec;

  //For GPIs
  GPIInfo: Array[1..5] of GPIRec;
  GPIDebounceInterval: SmallInt;

  //Misc. control variables
  InitialSceneLoadComplete: Boolean;
  ClearDataPanelsOnAbort: Boolean;
  AlwaysPlayAllScenesAtStartup: Boolean;

  //For host mode crawl data response socket
  CrawlDataSocketPort: LongInt;
  CurrentTrafficDataIndex: SmallInt;
  TrafficCrawlEnabled: Boolean;
  TrafficCrawlDataIn: Boolean;

  TemplateFieldUpdatesEnabled: Boolean;

  CrawlFeedbackPortEnabled: Boolean;

  //Added for top region updates
  TopRegionToggleState: SmallInt;

  //Added for V2.0
  ClockDisplayEnabled: Boolean;
  ClockStateChanged: Boolean;

  Sponsor_Logo_Scene_ID: LongInt;
  Full_Page_Promo_Scene_ID: LongInt;
const
  //Default delay for pages
  DEFAULTDELAY = 5000;

  //For ticker playlist loading
  CLEAR_AND_LOAD_PLAYLIST_MODE = 0;
  APPEND_PLAYLIST_MODE = 1;

  //For playlist modes
  TICKER = 1;
  BREAKINGNEWS = 2;

  //Constant for cases where no entry index is specified
  NOENTRYINDEX = -1;

  //Color format strings
  BLACK = '[c 0]';
  YELLOW = '[c 16]';
  GRAY = '[c 17]';
  WHITE = '[c 18]';
  BLUE = '[c 19]';

  //Breaking news modes
  IMMEDIATE = 0;
  SEGMENT = 1;

  //For football field position
  FIELDARROWRIGHT = #180;
  FIELDARROWLEFT = #181;
  FOOTBALLPOSSESSION = #200;
  LEADERINDICATOR = #200;

  //Game States for Stats Inc.
  PREGAME = 1;
  INPROGRESS = 2;
  FINAL = 3;
  POSTPONED = 4;
  DELAYED = 5;
  SUSPENDED = 6;
  CANCELLED = 7;
  UNDEFINED = -1;

  //Sponsor templates
  ONE_LINE_SPONSOR_TAGLINE = 1;
  ONE_LINE_SPONSOR_FULL_PAGE = 51;
  ONE_LINE_LOWER_RIGHT_SPONSOR_IN = 52;
  ONE_LINE_LOWER_RIGHT_SPONSOR_OUT = 53;
  TWO_LINE_SPONSOR_TAGLINE = 1001;
  TWO_LINE_SPONSOR_FULL_PAGE = 1051;
  TWO_LINE_LOWER_RIGHT_SPONSOR_IN = 1052;
  TWO_LINE_LOWER_RIGHT_SPONSOR_OUT = 1053;

  //For remote triggering
  NEXT_RECORD = TRUE;
  SPECIFIED_RECORD = FALSE;

  //For remote communications
  SOH = #1; //Start of heading
  ETX = #3; //End of text
  EOT = #4; //End of transmission
  EOT_CB = '\\';

  //For grid refresh
  FIELD_1 = 1;
  FIELD_2 = 2;
  SPONSORS = 3;

  SELECT_ALL = TRUE;
  DESELECT_ALL = FALSE;

  //Template Field Types
  FIELD_TYPE_COMPOUND_FIELD = -1;
  FIELD_TYPE_SYMBOLIC_NAME = 1;
  FIELD_TYPE_MANUAL_TEXT = 2;
  FIELD_TYPE_SPONSOR_LOGO = 3;
  FIELD_TYPE_EXPLICIT_TEXT = 4;
  FIELD_TYPE_MANUAL_GAME_WINNER = 5;
  FIELD_TYPE_GAME_WINNER_ANIMATION = 6;
  FIELD_TYPE_WEATHER_ICON = 7;
  FIELD_TYPE_LEAGUE_HEADER = 8;
  FIELD_TYPE_NEXT_PAGE_ANIMATION = 9;
  //For logos
  FIELD_TYPE_LEAGUE_LOGO_1A = 10;
  FIELD_TYPE_LEAGUE_LOGO_1B = 11;
  FIELD_TYPE_LEAGUE_LOGO_2A = 12;
  FIELD_TYPE_LEAGUE_LOGO_2B = 13;
  FIELD_TYPE_TEAM_LOGO_1A = 14;
  FIELD_TYPE_TEAM_LOGO_1B = 15;
  FIELD_TYPE_TEAM_LOGO_2A = 16;
  FIELD_TYPE_TEAM_LOGO_2B = 17;
  FIELD_TYPE_PLAYER_HEADSHOT_1A = 18;
  FIELD_TYPE_PLAYER_HEADSHOT_1B = 19;
  FIELD_TYPE_PLAYER_HEADSHOT_2A = 20;
  FIELD_TYPE_PLAYER_HEADSHOT_2B = 21;
  //Added for Version 2.0
  FIELD_TYPE_FULL_PROMO_GRAPHIC = 22;
  FIELD_TYPE_TOP_REGION_GRAPHIC = 23;

  WEATHER_LOCATION_CODE = 22;

  //For Channel Box
  PLAYLIST_1 = 1;
  PLAYLIST_2 = 2;
  //SPONSOR_LOGO_SCENE_ID = 1002;
  SPONSOR_LOGO_FILE_UPDATE_COMMAND = 'Sponsor_Logo_File';
  SPONSOR_LOGO_IN_COMMAND = 'Sponsor_In_Btn';
  SPONSOR_LOGO_OUT_COMMAND = 'Sponsor_Out_Btn';

  //Field/panel types
  STARTUP = 0;
  FIELDS_1_2 = 1;
  FIELD_3 = 2;
  SPONSOR_LOGO = 3;
  FULL_PROMO = 4;

  //For weather
  DEGREE_SYMBOL = #176;
  NOT_AVAILABLE = 44;

  CB_DELIMITER = #167; //� symbol

  //Scene load status codes
  SCENE_NONEXISTENT = -1;
  SCENE_NOSTATUS = 0;
  SCENE_OPENED = 1;
  SCENE_LOADED = 2;
  SCENE_LOADING = 3;
  SCENE_PLAYING = 4;
  SCENE_STOPPED = 5;
  SCENE_CLOSED = 6;

  //For traffic
  //Control buttons
  CRAWL_SCENE = 2001;
  CRAWL_IN = 'Crawl_In_Btn';
  CRAWL_OUT = 'Crawl_Out_Btn';
  CRAWL_HEADER = 'Crawl_Header';
  CRAWL_TEXT = 'Crawl_Text';
  //Text colors
  TRAFFIC_GREEN = '{\\colortbl;\\red255\\green255\\blue255;\\red252\\green206\\blue4;\\red128\\green128\\blue128;\\red200\\green0\\blue0;\\red0\\green200\\blue0;}\\cf5';
  TRAFFIC_YELLOW = '{\\colortbl;\\red255\\green255\\blue255;\\red252\\green206\\blue4;\\red128\\green128\\blue128;\\red200\\green0\\blue0;\\red0\\green200\\blue0;}\\cf2';
  TRAFFIC_RED = '{\\colortbl;\\red255\\green255\\blue255;\\red252\\green206\\blue4;\\red128\\green128\\blue128;\\red200\\green0\\blue0;\\red0\\green200\\blue0;}\\cf4';
  TRAFFIC_WHITE = '{\\colortbl;\\red255\\green255\\blue255;\\red252\\green206\\blue4;\\red128\\green128\\blue128;\\red200\\green0\\blue0;\\red0\\green200\\blue0;}\\cf1';

  //Added for V1.1.0 10/21/11
  STOP_SCENES = TRUE;
  DONT_STOP_SCENES = FALSE;
  IS_STARTUP = TRUE;
  IS_NOT_STARTUP = FALSE;

  //BASE_SCENE_ID = 1001;
  CLOCK_ENABLE = 1;
  CLOCK_DISABLE = 0;
  CLOCK_OUT = FALSE;
  CLOCK_IN = TRUE;

  FULL_PAGE_PROMO_TEMPLATE = 31;
  //FULL_PAGE_PROMO_SCENE = 7000;

implementation

end.
