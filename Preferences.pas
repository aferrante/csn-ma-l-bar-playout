unit Preferences;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, StBase, {StShBase, StBrowsr,} Spin, Mask;

type
  TPrefs = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel2: TPanel;
    Label2: TLabel;
    Edit2: TEdit;
    Edit3: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    Panel4: TPanel;
    RadioGroup4: TRadioGroup;
    Panel3: TPanel;
    RadioGroup1: TRadioGroup;
    Panel7: TPanel;
    StaticText3: TStaticText;
    Edit1: TEdit;
    RadioGroup2: TRadioGroup;
    RadioGroup3: TRadioGroup;
    Panel1: TPanel;
    RadioGroup5: TRadioGroup;
    Panel5: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    StationIDDesc: TEdit;
    Label3: TLabel;
    StationIDNum: TSpinEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Prefs: TPrefs;

implementation

{$R *.DFM}

end.
