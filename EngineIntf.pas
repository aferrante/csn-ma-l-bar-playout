// Rev: 03/09/06  18:40  M. Dilworth  Video Design Software Inc.
unit EngineIntf;

// Template sponsor types
// 1 = Sponsor with tagline
// 2 = Full-page sponsor
// 3 = Lower-right sponsor
// 4 = Clear lower-right sponsor

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OoMisc, ADTrmEmu, AdPacket, AdPort, AdWnPort, ExtCtrls, Globals, FileCtrl,
  ScktComp;

type
  TEngineInterface = class(TForm)
    AdTerminal1: TAdTerminal;
    PacketEnableTimer: TTimer;
    TickerCommandDelayTimer: TTimer;
    PacketIndicatorTimer: TTimer;
    TickerPacketTimeoutTimer: TTimer;
    JumpToNextTickerRecordTimer: TTimer;
    DisplayClockTimer: TTimer;
    FieldUpdateDelayTimer: TTimer;
    ACKReceivedLEDTimer: TTimer;
    EnginePort: TClientSocket;
    TickerCommandDelayTimer2: TTimer;
    FieldUpdateDelayTimer2: TTimer;
    JumpToNextTickerRecordTimer2: TTimer;
    TickerPacketTimeoutTimer2: TTimer;
    CrawlDataResponseSocket: TServerSocket;
    CrawlLEDTimer: TTimer;
    TopGraphicDwellTimer: TTimer;
    procedure PacketEnableTimerTimer(Sender: TObject);
    procedure TickerCommandDelayTimerTimer(Sender: TObject);
    procedure PacketIndicatorTimerTimer(Sender: TObject);
    procedure TickerPacketTimeoutTimerTimer(Sender: TObject);
    procedure JumpToNextTickerRecordTimerTimer(Sender: TObject);
    procedure WriteToErrorLog (ErrorString: String);
    procedure WriteToAsRunLog (AsRunString: String);
    procedure FormActivate(Sender: TObject);
    procedure ACKReceivedLEDTimerTimer(Sender: TObject);
    procedure EnginePortRead(Sender: TObject; Socket: TCustomWinSocket);
    procedure EnginePortConnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure EnginePortDisconnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure EnginePortError(Sender: TObject; Socket: TCustomWinSocket; ErrorEvent: TErrorEvent; var ErrorCode: Integer);
    procedure TickerCommandDelayTimer2Timer(Sender: TObject);
    procedure JumpToNextTickerRecordTimer2Timer(Sender: TObject);
    procedure TickerPacketTimeoutTimer2Timer(Sender: TObject);
    procedure CrawlDataResponseSocketClientRead(Sender: TObject; Socket: TCustomWinSocket);
    procedure CrawlLEDTimerTimer(Sender: TObject);
    procedure CrawlDataResponseSocketAccept(Sender: TObject; Socket: TCustomWinSocket);
    procedure CrawlDataResponseSocketClientConnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure TopGraphicDwellTimerTimer(Sender: TObject);
  private
    { Private declarations }
    function ProcessStyleChips(CmdStr: String): String;
  public
    { Public declarations }
    procedure StartTickerData;
    procedure SendTickerRecord(PlaylistID: SmallInt; NextRecord: Boolean; RecordIndex: SmallInt);
    //procedure SendTickerRecord2(PlaylistID: SmallInt; NextRecord: Boolean; RecordIndex: SmallInt);
    procedure TriggerGraphic (Mode: SmallInt);
    function GetMixedCase(InStr: String): String;
    function FormatTimeString (InStr: String): String;
    function GetSponsorLogoFileName(LogoName: String): String;
    function GetTeamMnemonic (LeagueCode: String; STTeamCode: String): String;
    function GetTeamBasename (LeagueCode: String; STTeamCode: String): String;
    function LoadTempTemplateFields(PlaylistID, TemplateID: SmallInt): TemplateDefsRec;
    //Function to run stored procedures to get game information
    procedure UpdateTickerRecordDataFromDB(FieldID: SmallInt; PlaylistID: Double; TickerRecordIndex: SmallInt; EventGUID: TGUID);
    function GetTeamDisplayInfo2(League: String; TeamMnemonic: String): TeamDisplayInfoRec;
    function GetLineupText(CurrentTickerEntryIndex: SmallInt; TickerMode: SmallInt): LineupText;
    function CheckForActiveSponsorLogo: Boolean;
    procedure WriteToCommandLog (CommandString: String);
    function GetAlternateTemplateID(CurrentTemplateID: SmallInt): SmallInt;
    function LastEntryIsSponsor: Boolean;
    procedure AddSponsorPageToTail;
    function TemplateIsFantasy(TemplateID: SmallInt): Boolean;
    function TemplateIsLeaders(TemplateID: SmallInt): Boolean;

    //For Channel Box
    procedure SendCommand(CommandString: String);
    procedure OpenScene(SceneID: LongInt; SendParams: Boolean; Params: ANSIString);
    procedure LoadScene(SceneID: LongInt; SendParams: Boolean; Params: ANSIString);
    procedure PlayScene(SceneID: LongInt);
    procedure StopScene(SceneID: LongInt);
    procedure CloseScene(SceneID: LongInt);
    procedure UpdateScene(SceneID: LongInt; Params: ANSIString);
    procedure QuerySceneStatus(SceneID: LongInt);
    procedure SetupInitialScenes;
    procedure ClearFieldPanels(Stop_Scenes: Boolean);

    //V1.2 Procedure to set the state of the clock at the top of the L-Bar
    procedure SetClockState(ClockIn: Boolean);

    //Version 2.0 Functions for top graphic region
    procedure RefreshTopGraphicCollection;
    procedure SetNextTopGraphic;

  end;

const
  SingleLineWinningLarge: String[1] = #180;
  SingleLineLosingLarge: String[1]  = #181;
  SingleLineWinningSmall: String[1]  = #182;
  SingleLineLosingSmall: String[1]  = #183;

var
  EngineInterface: TEngineInterface;
  DisableCommandTimer: Boolean;
  //Boolean to select whether or not the data packet is used to trigger the next graphic

implementation

uses Main, //Main form
     DataModule,
     GameDataFunctions; //Data module

{$R *.DFM}
//Imit
procedure TEngineInterface.FormActivate(Sender: TObject);
begin
end;

//These are the graphics engine control procedures
////////////////////////////////////////////////////////////////////////////////
// UTILITY FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Function to get a parsed token from a delited string
function GetItem(TokenNo: Integer; Data: string; Delimiter: Char = '|'): string;
var
  Token: string;
  StrLen, TEnd, TNum: Integer;
begin
  StrLen := Length(Data);
  //Init
  TNum := 1;
  TEnd := StrLen;
  //Walk through string and parse for delimiter
  while ((TNum <= TokenNo) and (TEnd <> 0)) do
  begin
    TEnd := Pos(Delimiter, Data);
    //Not last value
    if TEnd <> 0 then
    begin
      //Copy & delete data from input string; increment token counter
      Token := Copy(Data, 1, TEnd - 1);
      Delete(Data, 1, TEnd);
      Inc(TNum);
    end
    //Last value
    else begin
      Token := Data;
    end;
  end;
  //Return data
  if TNum >= TokenNo then
  begin
    Result := Token;
  end
  //Return error code
  else begin
    Result := #27;
  end;
end;

//Function to do style chip substitutions
function TEngineInterface.ProcessStyleChips(CmdStr: String): String;
var
  i,j: SmallInt;
  StyleChipRecPtr: ^StyleChipRec;
  OutStr: String;
  SwitchPos: SmallInt;
begin
  //Check for presence of style chips
  //NOTE: Conversion to mixed case/upper case done in function to get field contents
  OutStr := CmdStr;
  if (StyleChip_Collection.Count > 0) AND (Length(CmdStr) >= 4) then
  begin
    for i := 0 to StyleChip_Collection.Count-1 do
    begin
      StyleChipRecPtr := StyleChip_Collection.At(i);
      //Check style chip type and substiture accordingly
      //Type 1 = Use substitution string
      if (StyleChipRecPtr^.StyleChip_Type = 1) then
        OutStr := StringReplace(OutStr, StyleChipRecPtr^.StyleChip_Code,
                  StyleChipRecPtr^.StyleChip_String, [rfReplaceAll])
      //Type 2 = Use substitution font & character
      else if (StyleChipRecPtr^.StyleChip_Type = 2) then
        OutStr := StringReplace(OutStr, StyleChipRecPtr^.StyleChip_Code,
                  '[f ' + IntToStr(StyleChipRecPtr^.StyleChip_FontCode) + ']' +
                  Char(StyleChipRecPtr^.StyleChip_CharacterCode), [rfReplaceAll])
    end;
  end;
  ProcessStyleChips := OutStr;
end;

//Function to take the team mnmonic and return the team name
function TEngineInterface.GetTeamDisplayInfo2(LEague: String; TeamMnemonic: String): TeamDisplayInfoRec;
var
  i: SmallInt;
  TeamRecPtr: ^TeamRec; //Pointer to team record type
  OutRec: TeamDisplayInfoRec;
  FoundRecord: Boolean;
begin
  FoundRecord := FALSE;
  if (Team_Collection.Count > 0) then
  begin
    i := 0;
    repeat
      TeamRecPtr := Team_Collection.At(i);
//      if (Trim(TeamRecPtr^.NewSTTeamCode) = Trim(TeamMnemonic)) AND
//         (Trim(TeamRecPtr^.League) = Trim(League))then
      begin
//        OutRec.DisplayMnemonic := TeamRecPtr^.DisplayName1;
//        OutRec.Displayname := TeamRecPtr^.DisplayName2;
        FoundRecord := TRUE;
      end;
      Inc(i);
    until (i = Team_Collection.Count) OR (FoundRecord = TRUE);
  end;
  GetTeamDisplayInfo2 := OutRec;
end;

//Function to take the league code and Sportsticker team code, and return the team menmonic
function TEngineInterface.GetTeamMnemonic(LeagueCode: String; STTeamCode: String): String;
var
  i: SmallInt;
  TeamRecPtr: ^TeamRec; //Pointer to stat record type
  OutStr: String;
  FoundRec: Boolean;
begin
  OutStr := ' ';
  if (Team_Collection.Count > 0) then
  begin
    i := 0;
    FoundRec := FALSE;
    repeat
      TeamRecPtr := Team_Collection.At(i);
//      if (Trim(TeamRecPtr^.NewSTTeamCode) = Trim(STTeamCode)) AND (Trim(TeamRecPtr^.League) = LeagueCode) then
//      begin
//        OutStr := Trim(TeamRecPtr^.TeamMnemonic);
//        FoundRec := TRUE;
//      end;
      Inc(i);
    Until (FoundRec = TRUE) OR (i = Team_Collection.Count);
  end;
  GetTeamMnemonic := OutStr;
end;

//Function to take the league code and Sportsticker team code, and return the team base name
function TEngineInterface.GetTeamBaseName(LeagueCode: String; STTeamCode: String): String;
var
  i: SmallInt;
  TeamRecPtr: ^TeamRec; //Pointer to stat record type
  OutStr: String;
  FoundRec: Boolean;
begin
  OutStr := ' ';
  if (Team_Collection.Count > 0) then
  begin
    i := 0;
    FoundRec := FALSE;
    repeat
      TeamRecPtr := Team_Collection.At(i);
//      if (Trim(TeamRecPtr^.NewSTTeamCode) = Trim(STTeamCode)) AND (Trim(TeamRecPtr^.League) = Trim(LeagueCode)) then
//      begin
//        OutStr := Trim(TeamRecPtr^.BaseName);
//        FoundRec := TRUE;
//      end;
      Inc(i);
    until (FoundRec = TRUE) OR (i = Team_Collection.Count);
  end;
  GetTeamBaseName := OutStr;
end;

// Function to take upper case string & return mixed-case string
function TEngineInterface.GetMixedCase(InStr: String): String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := ANSILowerCase(InStr);
  if (Length(OutStr) > 0) then
  begin
    //Set first character to upper case if it's not a digit
    if (Ord(OutStr[1]) < 48) OR (Ord(OutStr[1]) > 57) then  OutStr[1] := Char(Ord(OutStr[1])-32);
    //Check if two words in state name, and set 2nd word to upper case
    if (Length(OutStr) > 2) then
    begin
      for i := 2 to Length(OutStr) do
      begin
        if (OutStr[i-1] = ' ') then OutStr[i] := Char(Ord(OutStr[i])-32);
      end;
    end;
  end;
  GetMixedCase := OutStr;
end;

//Function to take 4-digit clock time string from Datamart and format as "mm:ss"
function TEngineInterface.FormatTimeString (InStr: String): String;
var
  OutStr: String;
begin
  OutStr := '';
  InStr := Trim(InStr);
  Case Length(InStr) of
    0: Begin
         OutStr := '';
       end;
    1: Begin
         OutStr := ':0' + InStr[1];
       end;
    2: Begin
         OutStr := ':' + InStr[1] + InStr[2];
       end;
    3: Begin
         OutStr := InStr[1] + ':' + InStr[2] + InStr[3];
       end;
    4: Begin
         OutStr := InStr[1] + InStr[2] + ':' + InStr[3] + InStr[4];
       end;
  end;
  FormatTimeString := OutStr;
end;

//Function to get sponsor logo file name based on logo description
function TEngineInterface.GetSponsorLogoFilename(LogoName: String): String;
var
  i: SmallInt;
  OutStr: String;
  SponsorLogoPtr: ^SponsorLogoRec;
begin
  for i := 0 to SponsorLogo_Collection.Count-1 do
  begin
    SponsorLogoPtr := SponsorLogo_Collection.At(i);
    if (SponsorLogoPtr^.SponsorLogoName = LogoName) then
    begin
      OutStr := SponsorLogoPtr^.SponsorLogoFileName;
    end;
  end;
  GetSponsorLogoFilename := OutStr;
end;

//Procedure to load temporary collection for fields (for current template); also returns
//associated template information
function TEngineInterface.LoadTempTemplateFields(PlaylistID, TemplateID: SmallInt): TemplateDefsRec;
var
  i: SmallInt;
  TemplateFieldsRecPtr, NewTemplateFieldsRecPtr: ^TemplateFieldsRec;
  TemplateRecPtr: ^TemplateDefsRec;
  OutRec: TemplateDefsRec;
  FoundMatch: Boolean;
begin
  //Clear the existing collection
  Case PlaylistID of
    PLAYLIST_1: begin
      Temporary_Fields_Collection.Clear;
      Temporary_Fields_Collection.Pack;
    end;
    PLAYLIST_2: begin
      Temporary_Fields_Collection2.Clear;
      Temporary_Fields_Collection2.Pack;
    end;
  end;
  //First, get the engine template ID
  if (Template_Defs_Collection.Count > 0) then
  begin
    i := 0;
    FoundMatch := FALSE;
    repeat
      TemplateRecPtr := Template_Defs_Collection.At(i);
      if (TemplateID = TemplateRecPtr^.Template_ID) then
      begin
        OutRec.Template_ID := TemplateRecPtr^.Template_ID;
        OutRec.Template_Type := TemplateRecPtr^.Template_Type;
        OutRec.Template_Description := TemplateRecPtr^.Template_Description;
        OutRec.Record_Type := TemplateRecPtr^.Record_Type;
        OutRec.TemplateSponsorType := TemplateRecPtr^.TemplateSponsorType;
        OutRec.Scene_ID_1 := TemplateRecPtr^.Scene_ID_1;
        OutRec.Scene_ID_2 := TemplateRecPtr^.Scene_ID_2;
        OutRec.Transition_In := TemplateRecPtr^.Transition_In;
        OutRec.Transition_Out := TemplateRecPtr^.Transition_Out;
        OutRec.Default_Dwell := TemplateRecPtr^.Default_Dwell;
        OutRec.UsesGameData := TemplateRecPtr^.UsesGameData;
        OutRec.RequiredGameState := TemplateRecPtr^.RequiredGameState;
        OutRec.Use_Alert_Background := TemplateRecPtr^.Use_Alert_Background;
        FoundMatch := TRUE;
      end;
      Inc(i);
    until (FoundMatch) OR (i = Template_Defs_Collection.Count);
  end;
  //Now, get the fields for the template
  for i := 0 to Template_Fields_Collection.Count-1 do
  begin
    TemplateFieldsRecPtr := Template_Fields_Collection.At(i);
    if (TemplateFieldsRecPtr^.Template_ID = TemplateID) then
    begin
      GetMem (NewTemplateFieldsRecPtr, SizeOf(TemplateFieldsRec));
      With NewTemplateFieldsRecPtr^ do
      begin
        Template_ID := TemplateFieldsRecPtr^.Template_ID;
        Field_ID := TemplateFieldsRecPtr^.Field_ID;
        Field_Type := TemplateFieldsRecPtr^.Field_Type;
        Field_Is_Manual := TemplateFieldsRecPtr^.Field_Is_Manual;
        Field_Label := TemplateFieldsRecPtr^.Field_Label;
        Field_Contents := TemplateFieldsRecPtr^.Field_Contents;
        Field_Format_Prefix := TemplateFieldsRecPtr^.Field_Format_Prefix;
        Field_Format_Suffix := TemplateFieldsRecPtr^.Field_Format_Suffix;
        Field_Max_Length := TemplateFieldsRecPtr^.Field_Max_Length;
        Scene_Field_Name := TemplateFieldsRecPtr^.Scene_Field_Name;
      end;
      Case PlaylistID of
        PLAYLIST_1: begin
          if (Temporary_Fields_Collection.Count <= 100) then
          begin
            //Add to collection
            Temporary_Fields_Collection.Insert(NewTemplateFieldsRecPtr);
            Temporary_Fields_Collection.Pack;
          end;
        end;
        PLAYLIST_2: begin
          if (Temporary_Fields_Collection2.Count <= 100) then
          begin
            //Add to collection
            Temporary_Fields_Collection2.Insert(NewTemplateFieldsRecPtr);
            Temporary_Fields_Collection2.Pack;
          end;
        end;
      end;
    end;
  end;
  LoadTempTemplateFields := OutRec;
end;

//Utility function to strip off first character in string
function StripPrefix(InStr: String): String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := '';
  if (Length(InStr) > 0) then
  begin
    for i := 1 to Length(InStr) do
    begin
      if (Ord(InStr[i]) <=127) then OutStr := OutStr + InStr[i];
    end;
  end
  else OutStr := InStr;
  StripPrefix := OutStr;
end;

//FOR ODDS PAGES
const
  DELIMITER:STRING = Chr(160);

function GetLastName(FullName: String): String;
var
  i: SmallInt;
  OutStr: String;
  Cursor: SmallInt;
begin
  OutStr := FullName;
  Cursor := Pos(',', FullName);
  if (Cursor > 1) then
  begin
    OutStr := '';
    for i := 1 to Cursor-1 do
      OutStr := OutStr + FullName[i];
  end;
  GetLastName := OutStr;
end;

//Function to get & return text strings to be used for line-ups
function TEngineInterface.GetLineupText(CurrentTickerEntryIndex: SmallInt; TickerMode: SmallInt): LineupText;
var
  i,j: SmallInt;
  OutRec: LineupText;
  TickerRecPtrCurrent, TickerRecPtrPrevious,TickerRecPtrNext: ^TickerRec;
  PreviousEntryIndex, NextEntryIndex: SmallInt;
  CurrentEntryLeague, PreviousEntryLeague, NextEntryLeague: String;
  HeaderIndex, CollectionItemCounter: SmallInt;
  OneLeagueOnly: Boolean;
  FirstLeague: String;
  CurrentLineupTickerEntryIndex: SmallInt;
begin
  //Always set flag to do line-up
  OutRec.UseLineup := TRUE;

  //Get text for line-ups
  if (OutRec.UseLineup = TRUE) AND (CurrentTickerEntryIndex <> NOENTRYINDEX) then
  begin
    //Init
    HeaderIndex := 1;
    CollectionItemCounter := 1;

    //Get current entry types
    CurrentLineupTickerEntryIndex := CurrentTickerEntryIndex;
    TickerRecPtrCurrent := Ticker_Collection.At(CurrentLineupTickerEntryIndex);

    //Make sure record is enabled and within time window
    j := 0;
    While ((TickerRecPtrCurrent^.Enabled = FALSE) OR (TickerRecPtrCurrent^.StartEnableDateTime > Now) OR
          (TickerRecPtrCurrent^.EndEnableDateTime <= Now) OR (TickerRecPtrCurrent^.League = 'NONE')) AND
          (j < Ticker_Collection.Count) do
    begin
      Inc(CurrentLineupTickerEntryIndex);
      if (CurrentLineupTickerEntryIndex = Ticker_Collection.Count) then
      begin
        CurrentLineupTickerEntryIndex := 0;
      end;
      TickerRecPtrCurrent := Ticker_Collection.At(CurrentLineupTickerEntryIndex);
      Inc(j);
    end;

    //Special case to just show MLB for all MLB leagues
    CurrentEntryLeague :=  TickerRecPtrCurrent^.League;
    if (CurrentEntryLeague = 'AL') OR (CurrentEntryLeague = 'NL') OR
       (CurrentEntryLeague = 'ML') OR (CurrentEntryLeague = 'IL') then
      CurrentEntryLeague := 'MLB';

    NextEntryIndex := CurrentLineupTickerEntryIndex+1;
    //Get type of next entry
    if (NextEntryIndex = Ticker_Collection.Count) then NextEntryIndex := 0;

    //Set first entry
    OutRec.TextFields[HeaderIndex] := CurrentEntryLeague;

    //Get remaining entries
    repeat
      TickerRecPtrNext := Ticker_Collection.At(NextEntryIndex);
      NextEntryLeague := TickerRecPtrNext^.League;
      if (NextEntryLeague = 'AL') OR (NextEntryLeague = 'NL') OR
         (NextEntryLeague = 'ML') OR (NextEntryLeague = 'IL') then
        NextEntryLeague := 'MLB';

      //If new league or child to prior parent, set text field; otherwise, continue iterating through
      if (NextEntryLeague <> CurrentEntryLeague) AND
         (TickerRecPtrNext^.Enabled = TRUE) AND (TickerRecPtrNext^.StartEnableDateTime <= Now) AND
         (TickerRecPtrNext^.EndEnableDateTime > Now) AND (j < Ticker_Collection.Count) AND
         (TickerRecPtrNext^.League <> 'NONE') then
      begin
        Inc(HeaderIndex);
        //Use parent league for field and clear flag
        OutRec.TextFields[HeaderIndex] := NextEntryLeague;
        CurrentEntryLeague := NextEntryLeague;
      end;
      if (NextEntryIndex = Ticker_Collection.Count-1) then NextEntryIndex := 0
      else Inc(NextEntryIndex);
      Inc(CollectionItemCounter);
    until (HeaderIndex = 4) OR (CollectionItemCounter = Ticker_Collection.Count*3);

    //Repeat fields if not enough after iterating
    if (HeaderIndex = 1) then
    begin
      OutRec.TextFields[2] :=  OutRec.TextFields[1];
      OutRec.TextFields[3] :=  OutRec.TextFields[1];
      OutRec.TextFields[4] :=  OutRec.TextFields[1];
    end
    else if (HeaderIndex = 2) then
    begin
      OutRec.TextFields[3] :=  OutRec.TextFields[1];
      OutRec.TextFields[4] :=  OutRec.TextFields[2];
    end
    else if (HeaderIndex = 3) then
    begin
      OutRec.TextFields[4] :=  OutRec.TextFields[1];
    end;
  end
  else
    for i := 1 to 4 do OutRec.TextFields[i] := ' ';
  //Return
  for i := 1 to 4 do if (OutRec.TextFields[i] = '') then OutRec.TextFields[i] := ' ';
  GetLineupText := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
// ENGINE CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Procedure to trigger current graphic
procedure TEngineInterface.TriggerGraphic (Mode: SmallInt);
var
  CmdStr: String;
begin
  try
    //Build & send command string
    //CmdStr := SOT + 'TG' + ETX + IntToStr(Mode) + EOT;
    if (SocketConnected) AND (EngineParameters.Enabled = TRUE) then EnginePort.Socket.SendText(CmdStr);

    //Write to command log
    if (CommandLoggingEnabled) then WriteToCommandLog(CmdStr);

    //If aborting, reset packet and disable packet timeout timer
    if (Mode = 2) AND (SocketConnected) then
    begin
      //Disable packet timeout timer
      TickerPacketTimeoutTimer.Enabled := FALSE;
    end;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to trigger ticker on the graphics engine.');
    end;
  end;
end;

//Function to determine if the template is a fantasy template
function TEngineInterface.TemplateIsFantasy(TemplateID: SmallInt): Boolean;
var
  i: SmallInt;
  OutVal, FoundMatch: Boolean;
  TemplateDefsRecPtr: ^TemplateDefsRec;
begin
  OutVal := FALSE;
  FoundMatch := FALSE;
  if (Template_Defs_Collection.Count > 0) then
  begin
    i := 0;
    Repeat
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_ID = TemplateID) then
      begin
        FoundMatch := TRUE;
        OutVal := TemplateDefsRecPtr^.IsFantasyTemplate;
      end;
      inc(i);
    Until (FoundMatch) or (i = Template_Defs_Collection.Count);
  end;
  TemplateIsFantasy := OutVal;
end;

//Function to determine if the template is a leaders template
function TEngineInterface.TemplateIsLeaders(TemplateID: SmallInt): Boolean;
var
  i: SmallInt;
  OutVal, FoundMatch: Boolean;
  TemplateDefsRecPtr: ^TemplateDefsRec;
begin
  OutVal := FALSE;
  FoundMatch := FALSE;
  if (Template_Defs_Collection.Count > 0) then
  begin
    i := 0;
    Repeat
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_ID = TemplateID) then
      begin
        FoundMatch := TRUE;
        OutVal := TemplateDefsRecPtr^.IsLeadersTemplate;
      end;
      inc(i);
    Until (FoundMatch) or (i = Template_Defs_Collection.Count);
  end;
  TemplateIsLeaders := OutVal;
end;

//Procedure to update data for a specific record from the ticker playlist; called for each record
procedure TEngineInterface.UpdateTickerRecordDataFromDB(FieldID: SmallInt; PlaylistID: Double; TickerRecordIndex: SmallInt; EventGUID: TGUID);
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  SQLString: String;
begin
  try
    dmMain.Query3.Close;
    dmMain.Query3.SQL.Clear;
    SQLString := 'SELECT * FROM Ticker_Elements WHERE Playlist_ID = '+ FloatToStr(PlaylistID) +
      ' AND Event_GUID = ' + QuotedStr(GUIDToString(EventGUID));
    dmMain.Query3.SQL.Add (SQLString);
    dmMain.Query3.Open;
    if (dmMain.Query3.RecordCount > 0) then
    begin
      //Don't update if fantasy or leaders template - will overwrite stat index
      if (TemplateIsFantasy(dmMain.Query3.FieldByName('Template_ID').AsInteger)) or
         (TemplateIsLeaders(dmMain.Query3.FieldByName('Template_ID').AsInteger)) then
      begin
        Exit;
      end
      else begin
        Case FieldID of
          PLAYLIST_1: TickerRecPtr := Ticker_Collection.At(TickerRecordIndex);
          PLAYLIST_2: TickerRecPtr := Ticker_Collection2.At(TickerRecordIndex);
        end;
        With TickerRecPtr^ do
        begin
          Enabled := dmMain.Query3.FieldByName('Enabled').AsBoolean;
          League := dmMain.Query3.FieldByName('League').AsString;
          Subleague_Mnemonic_Standard := dmMain.Query3.FieldByName('Subleague_Mnemonic_Standard').AsString;
          Mnemonic_LiveEvent := dmMain.Query3.FieldByName('Mnemonic_LiveEvent').AsString;
          //Load the user-defined text fields
          for i := 1 to 50 do
            UserData[i] := dmMain.Query3.FieldByName('UserData_' + IntToStr(i)).AsString;
          StartEnableDateTime := dmMain.Query3.FieldByName('StartEnableTime').AsDateTime;
          EndEnableDateTime := dmMain.Query3.FieldByName('EndEnableTime').AsDateTime;
          DwellTime := dmMain.Query3.FieldByName('DwellTime').AsInteger;
        end;
      end;
    end;
    dmMain.Query3.Close;
  except
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred while trying to update data for playlist entry #' +
        IntToStr(TickerRecordIndex));
  end;
end;

//Function to check the ticker collection for any sponsor logo with a valid time window
function TEngineInterface.CheckForActiveSponsorLogo: Boolean;
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  OutVal: Boolean;
begin
  //Init
  OutVal := FALSE;
  //Check collection for any sponsor logo with a valid time window
  if (Ticker_Collection.Count > 0) then
  begin
    for i := 0 to Ticker_Collection.Count-1 do
    begin
      TickerRecPtr := Ticker_Collection.At(i);
      if ((TickerRecPtr^.Template_ID = ONE_LINE_SPONSOR_TAGLINE) OR (TickerRecPtr^.Template_ID = TWO_LINE_SPONSOR_TAGLINE) OR
          (TickerRecPtr^.Template_ID = ONE_LINE_SPONSOR_FULL_PAGE) OR (TickerRecPtr^.Template_ID = TWO_LINE_SPONSOR_FULL_PAGE)) AND
         (TickerRecPtr^.StartEnableDateTime <= Now) AND
         (TickerRecPtr^.EndEnableDateTime > Now) AND
         (TickerRecPtr^.Enabled = TRUE) then OutVal := TRUE;
    end;
  end;
  CheckForActiveSponsorLogo := OutVal;
end;

//Function to take a template ID and return the alternate template ID for 1-line vs.
//2-line mode
function TEngineInterface.GetAlternateTemplateID(CurrentTemplateID: SmallInt): SmallInt;
var
  i: SmallInt;
  TemplateDefsRecPtr: ^TemplateDefsRec;
  OutVal: SmallInt;
begin
  OutVal := -1;
  if (Template_Defs_Collection.Count > 0) then
  begin
    //Walk through templates to find alternate mode ID
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      //Check for match between ticker element template ID and entry in template defs
      if (TemplateDefsRecPtr^.Template_ID = CurrentTemplateID) then
      begin
        //Alternate template ID found, so re-assign template ID
        if (TemplateDefsRecPtr^.AlternateModeTemplateID > 0) then
        begin
          OutVal := TemplateDefsRecPtr^.AlternateModeTemplateID;
        end;
      end;
    end;
  end;
  GetAlternateTemplateID := OutVal;
end;

function TEngineInterface.LastEntryIsSponsor: Boolean;
var
  TickerRecPtr: ^TickerRec;
  OutVal: Boolean;
begin
  //Init
  OutVal := FALSE;
  //Check if last entry is sponsor
  TickerRecPtr := Ticker_Collection.At(Ticker_Collection.Count-1);
  if (TickerRecPtr^.Template_ID = ONE_LINE_SPONSOR_TAGLINE) OR (TickerRecPtr^.Template_ID = ONE_LINE_SPONSOR_FULL_PAGE) OR
     (TickerRecPtr^.Template_ID = TWO_LINE_SPONSOR_TAGLINE) OR (TickerRecPtr^.Template_ID = TWO_LINE_SPONSOR_FULL_PAGE) then
    OutVal := TRUE;
  //Return
  LastEntryIsSponsor := OutVal;
end;

//Procedure to add the sponsor logo to the tail of the ticker if not looping and
//TailSponsorPagesEnabled is True
procedure TEngineInterface.AddSponsorPageToTail;
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  TempGUID: TGUID;
begin
  try
    GetMem (TickerRecPtr, SizeOf(TickerRec));
    With TickerRecPtr^ do
    begin
      //Set values for collection record
      Event_Index := 9999;
      CreateGUID(TempGuid);
      Event_GUID := TempGUID;
      Is_Child := FALSE;
      Enabled := TRUE;
      League := 'NONE';
      Subleague_Mnemonic_Standard := '';
      Mnemonic_LiveEvent := '';
      Template_ID := CurrentSponsorInfo[1].CurrentSponsorTemplate;
      Record_Type := 1;
      GameID := '';
      SponsorLogo_Name := CurrentSponsorInfo[1].CurrentSponsorLogoName;
      SponsorLogo_Dwell := CurrentSponsorInfo[1].CurrentSponsorLogoDwell;
      StatStoredProcedure := '';
      StatType := 0;
      StatTeam := '';
      StatLeague := '';
      StatRecordNumber := 0;
      //Load the user-defined text fields
      UserData[1] := CurrentSponsorInfo[1].Tagline_Top;
      UserData[2] := CurrentSponsorInfo[1].Tagline_Bottom;
      for i := 3 to 50 do
        UserData[i] := '';
      StartEnableDateTime := Now - 10;
      EndEnableDateTime := Now + 10;
      DwellTime := CurrentSponsorInfo[1].CurrentSponsorLogoDwell;
      Description := 'Tail Sponsor Logo: ' + CurrentSponsorInfo[1].CurrentSponsorLogoName;
      Selected := FALSE;
      Comments := '';

      //Insert the object
      Ticker_Collection.Insert(TickerRecPtr);
      Ticker_Collection.Pack;

      //Refresh the grid
      MainForm.RefreshPlaylistGrid(FIELD_1, MainForm.PlaylistGrid);
    end;
  except
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred while trying to insert sponsor logo page at tail of playlist');
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TICKER CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Procedure to start ticker, bugs and applicable extra line
//Sets all initial data and sends first data record
procedure TEngineInterface.StartTickerData;
var
  i, j1, j2, k: SmallInt;
  TempStr, CmdStr, CmdStr2: String;
  TickerRecPtr, TickerRecPtr2: ^TickerRec;
  FoundGame, FoundGame2, FoundStat: Boolean;
  VisitorScore: SmallInt;
  HomeScore: SmallInt;
  VisitorColor: SmallInt;
  HomeColor: SmallInt;
  HomeRank: String;
  VisitorRank: String;
  HomeName: String;
  Visitorname: String;
  SponsorLogoToUse: String;
  NextStatStr: String;
  FieldStr: String;
  PhaseStr, TimeStr: String;
  VLeader, HLeader: Boolean;
  GameHasStarted, GameIsFinal, GameIsHalf: Boolean;
  GTIME: String;
  GPHASE: SmallInt;
  CurrentGamePhaseRec, CurrentGamePhaseRec2: GamePhaseRec;
  CurrentGameState, CurrentGameState2: SmallInt;
  RecordIsSponsor: Boolean;
  CurrentLeague: String;
  LineupData: LineupText;
  TemplateInfo, TemplateInfo2: TemplateDefsRec;
  TemplateFieldsRecPtr, TemplateFieldsRecPtr2: ^TemplateFieldsRec;
  BugClockPhaseStr: String;
  OkToGo: Boolean;
  UseFieldDelayTimer: Boolean;
  TokenCounter: SmallInt;
  SymbolValueData: SymbolValueRec;
  FirstTemplateIsField3Type: Boolean;
begin
  //Init flags
  GameHasStarted := FALSE;
  GameIsFinal:= FALSE;
  GameIsHalf := FALSE;
  RecordIsSponsor := FALSE;
  UseFieldDelayTimer := FALSE;
  TickerCommandDelayTimer.Enabled := FALSE;
  TickerPacketTimeoutTimer.Enabled := FALSE;
  MasterSegmentIndex := TICKER;
  CmdStr := '';
  CmdStr2 := '';
  FirstTemplateIsField3Type := FALSE;

  //Set looping mode as required
  Case TickerDisplayMode of
    //1x through for 1-lime, 2-line
    1,3: LoopTicker := FALSE;
    //Continuous loop for 1-lime, 2-line
    2,4: LoopTicker := TRUE;
  end;

  try
    //////////////////////////////////////////////////////////////////////////
    // START TICKER LOGIC
    //////////////////////////////////////////////////////////////////////////
    //Only proceed if at leat one story in Ticker
    if (Ticker_Collection.Count > 0) and (Ticker_Collection2.Count > 0) then
    begin
      //Make sure it;s a valid index
      if (CurrentTickerEntryIndex1 < 0) then CurrentTickerEntryIndex1 := 0;
      if (CurrentTickerEntryIndex2 < 0) then CurrentTickerEntryIndex2 := 0;
      if (CurrentBreakingNewsEntryIndex < 0) then CurrentBreakingNewsEntryIndex := 0;

      //Init FoundGame flag; used to prevent sending of commands if game not found
      OKToGo := TRUE;
      FoundGame := TRUE;
      FoundGame2 := TRUE;
      //Set enable
      PacketEnable := TRUE;

      //Send the first record for the Ticker; check to make sure it's a valid index
      if (CurrentTickerEntryIndex1 <= Ticker_Collection.Count-1) and (CurrentTickerEntryIndex2 <= Ticker_Collection2.Count-1) then
      begin
        //Init
        j1 := 0;
        j2 := 0;

        //Increment based on current master segment (Ticker = 1; Breaking News = 2)
        TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex1);
        TickerRecPtr2 := Ticker_Collection2.At(CurrentTickerEntryIndex2);
        UpdateTickerRecordDataFromDB(PLAYLIST_1, CurrentTickerPlaylistID, CurrentTickerEntryIndex1, TickerRecPtr^.Event_GUID);
        UpdateTickerRecordDataFromDB(PLAYLIST_2, CurrentTickerPlaylistID, CurrentTickerEntryIndex2, TickerRecPtr2^.Event_GUID);

        //Advance past disabled records
        //Playlist 1
        While ((TickerRecPtr^.Enabled = FALSE) OR NOT((TickerRecPtr^.StartEnableDateTime <= Now) AND
              (TickerRecPtr^.EndEnableDateTime > Now))) AND (j1 < Ticker_Collection.Count) do
        begin
          Inc (j1);
          Inc(CurrentTickerEntryIndex1);
          if (CurrentTickerEntryIndex1 = Ticker_Collection.Count) then
            CurrentTickerEntryIndex1 := 0;
          TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex1);
          //Update the data from the database - called for each record
          UpdateTickerRecordDataFromDB(PLAYLIST_1, CurrentTickerPlaylistID, CurrentTickerEntryIndex1, TickerRecPtr^.Event_GUID);
        end;
        //Set flag if no enabled records found
        if (j1 >= Ticker_Collection.Count) then OKToGo := FALSE;
        //Playlist 2
        While ((TickerRecPtr2^.Enabled = FALSE) OR NOT((TickerRecPtr2^.StartEnableDateTime <= Now) AND
              (TickerRecPtr2^.EndEnableDateTime > Now))) AND (j2 < Ticker_Collection.Count) do
        begin
          Inc (j2);
          Inc(CurrentTickerEntryIndex2);
          if (CurrentTickerEntryIndex2 = Ticker_Collection2.Count) then
            CurrentTickerEntryIndex2 := 0;
          TickerRecPtr2 := Ticker_Collection2.At(CurrentTickerEntryIndex2);
          //Update the data from the database - called for each record
          UpdateTickerRecordDataFromDB(PLAYLIST_2, CurrentTickerPlaylistID, CurrentTickerEntryIndex2, TickerRecPtr2^.Event_GUID);
        end;
        //Set flag if no enabled records found
        if (j2 >= Ticker_Collection2.Count) then OKToGo := FALSE;

        //Proceed if records found
        if (OKToGo) then
        begin
          //MAIN PROCESSING LOOP FOR TICKER FIELDS
          //Get template info and load the termporary fields collection
          try
            TemplateInfo := LoadTempTemplateFields(PLAYLIST_1, TickerRecPtr^.Template_ID);
            TemplateInfo2 := LoadTempTemplateFields(PLAYLIST_2, TickerRecPtr2^.Template_ID);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to load template fields');
          end;

          //Get game data if template requires it
          //Playlist 1
          if (TemplateInfo.UsesGameData) then
          begin
            try
              //Get game data if template requires it
              if (TemplateInfo.UsesGameData) then
              begin
                //Use full game data
                CurrentGameData := GetGameData(TRIM(TickerRecPtr^.League), TRIM(TickerRecPtr^.GameID));
              end;
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying to get game data for ticker');
            end;
            FoundGame := CurrentGameData.DataFound;
          end;
          //Playlist 2
          if (TemplateInfo2.UsesGameData) then
          begin
            try
              //Get game data if template requires it
              if (TemplateInfo2.UsesGameData) then
              begin
                //Use full game data
                CurrentGameData2 := GetGameData(TRIM(TickerRecPtr2^.League), TRIM(TickerRecPtr2^.GameID));
              end;
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying to get game data for ticker');
            end;
            FoundGame2 := CurrentGameData2.DataFound;
          end;

          //Check the game state vs. the state required by the template if not a match, increment
          //Also do check for enabled entries and check time window
          //Playlist 1
          if (TemplateInfo.RequiredGameState > 0) AND (CurrentGameData.StateForInProgressCheck <> TemplateInfo.RequiredGameState) AND
          //Special case for postponed, delayed or suspended MLB games
          (NOT((CurrentGameData.State = -1) AND (TemplateInfo.RequiredGameState = 3))) then
          Repeat
            Inc(CurrentTickerEntryIndex1);
            if (CurrentTickerEntryIndex1 = Ticker_Collection.Count) then
            begin
              MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
              CurrentTickerEntryIndex1 := 0;
            end;
            TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex1);
            TemplateInfo := LoadTempTemplateFields(PLAYLIST_1, TickerRecPtr^.Template_ID);
            if (TemplateInfo.UsesGameData) then
            begin
              try
                //Use full game data
                CurrentGameData := GetGameData(TRIM(TickerRecPtr^.League), TRIM(TickerRecPtr^.GameID));
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to get game data for ticker');
              end;
              FoundGame := CurrentGameData.DataFound;
            end
            else CurrentGameData.State := -1;
            //Update the data from the database - called for each record
            UpdateTickerRecordDataFromDB(PLAYLIST_1, CurrentTickerPlaylistID, CurrentTickerEntryIndex1, TickerRecPtr^.Event_GUID);
          //Version 2.1.0.0 Bug corrected that resulted in all templates being displayed for games that were delayed, cancelled, etc.
          Until ((TemplateInfo.RequiredGameState = 0) OR (CurrentGameData.StateForInProgressCheck = TemplateInfo.RequiredGameState) OR
                //Special case for postponed, delayed or suspended MLB games
                (((CurrentGameData.State = POSTPONED) OR (CurrentGameData.State = SUSPENDED) OR (CurrentGameData.State = DELAYED)) AND
                (TemplateInfo.RequiredGameState = FINAL))) AND
                ((TickerRecPtr^.Enabled = TRUE) AND
                (TickerRecPtr^.StartEnableDateTime <= Now) AND (TickerRecPtr^.EndEnableDateTime > Now));

          //Playlist 2
          if (TemplateInfo2.RequiredGameState > 0) AND (CurrentGameData2.StateForInProgressCheck <> TemplateInfo2.RequiredGameState) AND
          //Special case for postponed, delayed or suspended MLB games
          (NOT((CurrentGameData2.State = -1) AND (TemplateInfo2.RequiredGameState = 3))) then
          Repeat
            Inc(CurrentTickerEntryIndex2);
            if (CurrentTickerEntryIndex2 = Ticker_Collection2.Count) then
            begin
              MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
              CurrentTickerEntryIndex2 := 0;
            end;
            TickerRecPtr2 := Ticker_Collection.At(CurrentTickerEntryIndex2);
            TemplateInfo2 := LoadTempTemplateFields(PLAYLIST_2, TickerRecPtr2^.Template_ID);
            if (TemplateInfo2.UsesGameData) then
            begin
              try
                //Use full game data
                CurrentGameData2 := GetGameData(TRIM(TickerRecPtr2^.League), TRIM(TickerRecPtr2^.GameID));
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to get game data for ticker');
              end;
              FoundGame2 := CurrentGameData2.DataFound;
            end
            else CurrentGameData2.State := -1;
            //Update the data from the database - called for each record
            UpdateTickerRecordDataFromDB(PLAYLIST_2, CurrentTickerPlaylistID, CurrentTickerEntryIndex2, TickerRecPtr2^.Event_GUID);
          //Version 2.1.0.0 Bug corrected that resulted in all templates being displayed for games that were delayed, cancelled, etc.
          Until ((TemplateInfo2.RequiredGameState = 0) OR (CurrentGameData2.StateForInProgressCheck = TemplateInfo2.RequiredGameState) OR
                //Special case for postponed, delayed or suspended MLB games
                (((CurrentGameData2.State = POSTPONED) OR (CurrentGameData2.State = SUSPENDED) OR (CurrentGameData2.State = DELAYED)) AND
                (TemplateInfo2.RequiredGameState = FINAL))) AND
                ((TickerRecPtr2^.Enabled = TRUE) AND
                (TickerRecPtr2^.StartEnableDateTime <= Now) AND (TickerRecPtr2^.EndEnableDateTime > Now));

          //Set flag
          USEDATAPACKET := TRUE;
          //Set the default dwell time; set packet timeout interval

          //Setup dwell times - playlist 1
          if (TickerRecPtr^.SponsorLogo_Dwell > 0) then
          begin
            TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
            TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 60000;
          end
          //Setup for crawl
          else begin
            TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
            if (TickerRecPtr^.DwellTime = 1) then
              TickerPacketTimeoutTimer.Interval := 90000
            else
              TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 60000;
          end;

          //Setup dwell times - playlist 2
          if (TickerRecPtr2^.DwellTime > 2000) then
          begin
            TickerCommandDelayTimer2.Interval := TickerRecPtr2^.DwellTime;
            TickerPacketTimeoutTimer2.Interval := TickerRecPtr2^.DwellTime + 60000;
          end
          //Setup for crawl
          else begin
            TickerCommandDelayTimer2.Interval := TickerRecPtr2^.DwellTime;
            if (TickerRecPtr2^.DwellTime = 1) then
              TickerPacketTimeoutTimer2.Interval := 90000
            else
              TickerPacketTimeoutTimer2.Interval := TickerRecPtr2^.DwellTime + 60000;
          end;

          //Add the fields from the temporary fields collection
          if (Temporary_Fields_Collection.Count > 0) and (Temporary_Fields_Collection2.Count > 0) then
          begin
            //Playlist 1
            for j1 := 0 to Temporary_Fields_Collection.Count-1 do
            begin
              TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j1);
              //Version 2.1
              //Check for single fields (not compound fields required for 2-line, new look engine)
              if (TemplateFieldsRecPtr^.Field_Type > 0) then
              begin
                //Filter out fields ID values of -1 => League designator
                //if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
                begin
                  //Add region command
                  //If not a symbolic fields, send field contents directly
                  if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
                  begin
                    TempStr := TemplateFieldsRecPtr^.Field_Contents;
                    //Add prefix and/or suffix if applicable
                    if (TemplateFieldsRecPtr^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TemplateFieldsRecPtr^.Field_Format_Prefix + TempStr;
                    if (TemplateFieldsRecPtr^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TempStr + TemplateFieldsRecPtr^.Field_Format_Suffix;
                    if (Trim(TempStr) = '') then TempStr := ' ';

                    //Append field update to command string
                    CmdStr := CmdStr + TemplateFieldsRecPtr^.Scene_Field_Name + '�' + TempStr;
                    //Append command delimiter if not last field
                    if (j1 < Temporary_Fields_Collection.Count-1) then CmdStr := CmdStr + '\'
                  end
                  //It's a symbolic name, so get the field contents
                  else begin
                    try
                      TempStr := GetValueOfSymbol(PLAYLIST_1, TICKER, TemplateFieldsRecPtr^.Field_Contents, CurrentTickerEntryIndex1,
                        CurrentGameData, TickerDisplayMode, TickerRecPtr^.UserData[21]).SymbolValue;
                      //Add previx and/or suffix if applicable
                      if (TemplateFieldsRecPtr^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                        TempStr := TemplateFieldsRecPtr^.Field_Format_Prefix + TempStr;
                      if (TemplateFieldsRecPtr^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                        TempStr := TempStr + TemplateFieldsRecPtr^.Field_Format_Suffix;
                    except
                      if (ErrorLoggingEnabled = True) then
                        WriteToErrorLog('Error occurred while trying to get value for main ticker data field');
                    end;
                    if (Trim(TempStr) = '') then TempStr := ' ';

                    //Process the style chips
                    TempStr := ProcessStyleChips(TempStr);

                    //Append field update to command string
                    CmdStr := CmdStr + TemplateFieldsRecPtr^.Scene_Field_Name + '�' + TempStr;
                    //Append command delimiter if not last field
                    if (j1 < Temporary_Fields_Collection.Count-1) then CmdStr := CmdStr + '\'
                  end;
                end;
              end;
            end;

            //Playlist 2
            for j2 := 0 to Temporary_Fields_Collection2.Count-1 do
            begin
              TemplateFieldsRecPtr2 := Temporary_Fields_Collection2.At(j2);
              //Version 2.1
              //Check for single fields (not compound fields required for 2-line, new look engine)
              if (TemplateFieldsRecPtr2^.Field_Type > 0) then
              begin
                //Filter out fields ID values of -1 => League designator
                //if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
                begin
                  //Add region command
                  //If not a symbolic fields, send field contents directly
                  if (Pos('$', TemplateFieldsRecPtr2^.Field_Contents) = 0) then
                  begin
                    TempStr := TemplateFieldsRecPtr2^.Field_Contents;
                    //Add prefix and/or suffix if applicable
                    if (TemplateFieldsRecPtr2^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TemplateFieldsRecPtr2^.Field_Format_Prefix + TempStr;
                    if (TemplateFieldsRecPtr2^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TempStr + TemplateFieldsRecPtr2^.Field_Format_Suffix;
                    if (Trim(TempStr) = '') then TempStr := ' ';

                    //Append field update to command string
                    CmdStr2 := CmdStr2 + TemplateFieldsRecPtr2^.Scene_Field_Name + '�' + TempStr;
                    //Append command delimiter if not last field
                    if (j2 < Temporary_Fields_Collection2.Count-1) then CmdStr2 := CmdStr2 + '\'
                  end
                  //It's a symbolic name, so get the field contents
                  else begin
                    try
                      TempStr := GetValueOfSymbol(PLAYLIST_2, TICKER, TemplateFieldsRecPtr2^.Field_Contents, CurrentTickerEntryIndex2,
                        CurrentGameData2, TickerDisplayMode, TickerRecPtr2^.UserData[21]).SymbolValue;
                      //Add previx and/or suffix if applicable
                      if (TemplateFieldsRecPtr2^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                        TempStr := TemplateFieldsRecPtr2^.Field_Format_Prefix + TempStr;
                      if (TemplateFieldsRecPtr2^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                        TempStr := TempStr + TemplateFieldsRecPtr2^.Field_Format_Suffix;
                    except
                      if (ErrorLoggingEnabled = True) then
                        WriteToErrorLog('Error occurred while trying to get value for main ticker data field');
                    end;
                    if (Trim(TempStr) = '') then TempStr := ' ';

                    //Process the style chips
                    TempStr := ProcessStyleChips(TempStr);

                    //Append field update to command string
                    CmdStr2 := CmdStr2 + TemplateFieldsRecPtr2^.Scene_Field_Name + '�' + TempStr;
                    //Append command delimiter if not last field
                    if (j2 < Temporary_Fields_Collection2.Count-1) then CmdStr2 := CmdStr2 + '\'
                  end;
                end;
              end;
            end;
          end;

          //Set data row indicator in Ticker grid
          MainForm.PlaylistGrid.CurrentDataRow := CurrentTickerEntryIndex1+1;
          MainForm.PlaylistGrid.SetTopLeft(1, MainForm.PlaylistGrid.CurrentDataRow);
          MainForm.PlaylistGrid2.CurrentDataRow := CurrentTickerEntryIndex2+1;
          MainForm.PlaylistGrid2.SetTopLeft(1, MainForm.PlaylistGrid2.CurrentDataRow);
          //Update Authoring tool index
          AuthoringInterface.SendPlaylistIndex(PLAYLIST_1, CurrentTickerEntryIndex1);
          AuthoringInterface.SendPlaylistIndex(PLAYLIST_2, CurrentTickerEntryIndex2);

          //Transition out the prior scene
          //Transition out existing sponsor if new page is a sponsor logo
          //Do Playlist 1 - check if sponsor first
          if (TickerRecPtr^.SponsorLogo_Dwell <> 0) then
          begin
            CurrentSponsorInfo[1].CurrentSponsorLogoName := TickerRecPtr^.SponsorLogo_Name;
            WriteToAsRunLog('Started display of sponsor logo: ' + CurrentSponsorInfo[1].CurrentSponsorLogoName);
            EngineInterface.UpdateScene(SPONSOR_LOGO_SCENE_ID, SPONSOR_LOGO_OUT_COMMAND + '�' + 'T');
          end
          //Not a sponsor
          else begin
            if (LastTemplateInfo[PLAYLIST_1].Scene_ID <> 0) and
               (LastTemplateInfo[PLAYLIST_1].Scene_ID <> SPONSOR_LOGO_SCENE_ID) and
               (LastTemplateInfo[PLAYLIST_1].OutTransition_Command <> '') then
              EngineInterface.UpdateScene(LastTemplateInfo[PLAYLIST_1].Scene_ID, LastTemplateInfo[PLAYLIST_1].OutTransition_Command + '�' + 'T');
          end;
          //Do Playlist 2
          if (LastTemplateInfo[PLAYLIST_2].Scene_ID <> 0) and
             (LastTemplateInfo[PLAYLIST_2].OutTransition_Command <> '') then
            EngineInterface.UpdateScene(LastTemplateInfo[PLAYLIST_2].Scene_ID, LastTemplateInfo[PLAYLIST_2].OutTransition_Command + '�' + 'T');

          //Delay
          if (TickerRecPtr^.SponsorLogo_Dwell <> 0) then
          begin
            for k := 1 to 10 do
            begin
              Sleep (100);
              Application.ProcessMessages;
            end
          end
          else Sleep (200);

          //Transition the data in & store info on the last scene & transition
          //Playlist 1
          //Added for Version 1.1.0  10/20/11
          //Order of operations changed for V1.1.1 11/17/11 to play scene before sending data
          //Play the new Field 1 & 2 scenes
          PlayScene(TemplateInfo.Scene_ID_1);
          PlayScene(TemplateInfo2.Scene_ID_2);

          //Send the new data
          if (TemplateFieldUpdatesEnabled) then
          begin
            UpdateScene(TemplateInfo.Scene_ID_1, CmdStr);
            UpdateScene(TemplateInfo2.Scene_ID_2, CmdStr2);
          end;

          //Delay
          Sleep (100);

          //Check field state and change backplates if needed (Field 1&2 vs. Field 3)
          Case TemplateInfo.Template_Type of
            //Fields 1 & 2
            FIELDS_1_2: begin
              //Transition in Field 1 backplate if startup
              if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID = STARTUP) then
              begin
                EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_1_In_Command + '�' + 'T');
              end
              //Take out Field 3 if current state and bring in Field 1
              else if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID = FIELD_3) then
              begin
                //Take out Field 3 if not a full page promo
                if (LastTemplateInfo[PLAYLIST_1].Scene_ID <> Full_Page_Promo_Scene_ID) then
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_3_Out_Command + '�' + 'T');
                EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_1_In_Command + '�' + 'T');
              end;
            end;
            //Field 3
            FIELD_3: begin
              //Take out Fields 1 & 2 if current state and bring in Field 3
              if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID = FIELDS_1_2) then
              begin
                //Take out field 2 if it was in
                if (LastTemplateInfo[PLAYLIST_2].LastFieldTypeID = FIELDS_1_2) then
                begin
                  EngineInterface.UpdateScene(LastTemplateInfo[PLAYLIST_2].Scene_ID, LastTemplateInfo[PLAYLIST_2].OutTransition_Command + '�' + 'T');
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_2_Out_Command + '�' + 'T');
                  //Clear fields so Field 2 starts from scratch the next time around
                  LastTemplateInfo[PLAYLIST_2].LastFieldTypeID := STARTUP;
                  LastTemplateInfo[PLAYLIST_2].Scene_ID := 0;
                end;
                //Take out field 1
                EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_1_Out_Command + '�' + 'T');
                //Bring in Field 3 if not a full page promo
                if (TickerRecPtr^.Template_ID <> FULL_PAGE_PROMO_TEMPLATE) then
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_3_In_Command + '�' + 'T');
              end
              //Transition in Field 3 backplate if startup
              else if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID = STARTUP) then
              begin
                if (TickerRecPtr^.Template_ID <> FULL_PAGE_PROMO_TEMPLATE) then
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_3_In_Command + '�' + 'T');
              end
              //Added for Version 2.0
              else if (LastTemplateInfo[PLAYLIST_1].Scene_ID = Full_Page_Promo_Scene_ID) then
              begin
                EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_3_In_Command + '�' + 'T');
              end;
              //Set flag to prevent Field 2 from coming in right away
              FirstTemplateIsField3Type := TRUE;
            end;
          end;
          //Transition the new scene in
          EngineInterface.UpdateScene(TemplateInfo.Scene_ID_1, TemplateInfo.Transition_In + '�' + 'T');

          //Playlist 2
          if not (FirstTemplateIsField3Type) then
          begin
            //Take out Field 3 if current state and bring in Field 2
            if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID = FIELD_3) then
            begin
              //Take out the text from scene 1
              if (LastTemplateInfo[PLAYLIST_1].Scene_ID <> 0) and (LastTemplateInfo[PLAYLIST_1].OutTransition_Command <> '') then
                EngineInterface.UpdateScene(LastTemplateInfo[PLAYLIST_1].Scene_ID, LastTemplateInfo[PLAYLIST_1].OutTransition_Command + '�' + 'T');

              //Swap the backplates
              if (LastTemplateInfo[PLAYLIST_1].Scene_ID <> Full_Page_Promo_Scene_ID) then
                EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_3_Out_Command + '�' + 'T');
              //EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_2_In_Command + '�' + 'T');

              //Clear fields so Field 1 starts from scratch the next time around
              LastTemplateInfo[PLAYLIST_1].LastFieldTypeID := STARTUP;
              LastTemplateInfo[PLAYLIST_1].Scene_ID := 0;
            end;

            //Added for Version 1.1.0  10/20/11
            //Play the new Field 2 scene
            //PlayScene(TemplateInfo2.Scene_ID_2);

            //Update Field 2 data
            if (LastTemplateInfo[PLAYLIST_2].LastFieldTypeID = STARTUP) then
              EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_2_In_Command + '�' + 'T');

            //Transition the new scene in
            EngineInterface.UpdateScene(TemplateInfo2.Scene_ID_2, TemplateInfo2.Transition_In + '�' + 'T');
          end;

          //Store the last template info
          if (TickerRecPtr^.SponsorLogo_Dwell = 0) then
          begin
            LastTemplateInfo[PLAYLIST_1].Scene_ID := TemplateInfo.Scene_ID_1;
            LastTemplateInfo[PLAYLIST_1].OutTransition_Command := TemplateInfo.Transition_Out;
            LastTemplateInfo[PLAYLIST_1].LastFieldTypeID := TemplateInfo.Template_Type;
          end;

          //Store the last template info
          if not (FirstTemplateIsField3Type) then
          begin
            LastTemplateInfo[PLAYLIST_2].Scene_ID := TemplateInfo2.Scene_ID_2;
            LastTemplateInfo[PLAYLIST_2].OutTransition_Command := TemplateInfo2.Transition_Out;
            LastTemplateInfo[PLAYLIST_2].LastFieldTypeID := TemplateInfo2.Template_Type;
          end;

          //Set data row indicator in Ticker grid
          try
            MainForm.PlaylistGrid.CurrentDataRow := CurrentTickerEntryIndex1+1;
            MainForm.PlaylistGrid.SetTopLeft(1, MainForm.PlaylistGrid.CurrentDataRow);
            MainForm.PlaylistGrid2.CurrentDataRow := CurrentTickerEntryIndex2+1;
            MainForm.PlaylistGrid2.SetTopLeft(1, MainForm.PlaylistGrid2.CurrentDataRow);
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to reposition grid cursor to correct record');
          end;

          //Enable packet timeout timers
          TickerPacketTimeoutTimer.Enabled := FALSE;
          TickerPacketTimeoutTimer.Enabled := TRUE;
          TickerPacketTimeoutTimer2.Enabled := FALSE;
          TickerPacketTimeoutTimer2.Enabled := TRUE;
          //Start timer to send next record
          //if (USEDATAPACKET = FALSE) then
          if (USEDATAPACKET) then
          begin
            TickerCommandDelayTimer.Enabled := TRUE;
            //Don't fire Field 2 timer if starting with a Field 3 type template
            if not (FirstTemplateIsField3Type) then TickerCommandDelayTimer2.Enabled := TRUE;
          end
          else begin
            if (FoundGame = FALSE) then
            begin
              //Enable delay timer
              if (RunningTicker = TRUE) then JumpToNextTickerRecordTimer.Enabled := TRUE;
            end;
            if (FoundGame2 = FALSE) then
            begin
              //Enable delay timer
              if (RunningTicker = TRUE) then JumpToNextTickerRecordTimer2.Enabled := TRUE;
            end;
          end;
        end;
      end;
    end;
    //////////////////////////////////////////////////////////////////////////
    // END TICKER LOGIC
    //////////////////////////////////////////////////////////////////////////
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to send initial text and start ticker');
    end;
  end;
  //Set dwell label value
  MainForm.DwellValue.Caption := IntToStr(TickerCommandDelayTimer.Interval);
end;

//FUNCTIONS FOR ADVANCING RECORDS
//Handler for packet requesting more data for ticker
procedure TEngineInterface.EnginePortRead(Sender: TObject; Socket: TCustomWinSocket);
var
  Data: String;
  i: SmallInt;
  CurrentTemplateIDNum: SmallInt;
  CurrentTemplateIDNumStr: String;
  RequiredDelay: SmallInt;
  HeaderStrPos: SmallInt;
  SceneStatus: SmallInt;
  CommandStr: String;
  SceneID: LongInt;
  SceneIDStr: String;
  AllCommandsProcessed: Boolean;
  EOTLocation: SmallInt;
  SceneRecPtr: ^SceneRec;
begin
  AllCommandsProcessed := FALSE;

  //Get data
  Data := Trim(Socket.ReceiveText);

  //Parse out status if scene status info
  HeaderStrPos := Pos('B\QS\', Data);
  if (HeaderStrPos > 0) and (Pos(EOT_CB, Data) > 0) then
  repeat
    SceneIDStr := '';
    //Find start of command
    HeaderStrPos := Pos('B\QS\', Data);
    //Find end of command
    EOTLocation := Pos(EOT_CB, Data);
    //Copy from start to end of command & parse
    CommandStr := Trim(Copy(Data, HeaderStrPos, EOTLocation+1));

    SceneIDStr := '';
    i := 6;
    if (HeaderStrPos > 0) and (Length(CommandStr) > 5) then
    repeat
      SceneIDStr := SceneIDStr + CommandStr[i];
      inc(i);
    until (CommandStr[i] = CB_DELIMITER) or (CommandStr[i] = '\') or (i > Length(CommandStr));
    if (Length(SceneIDStr) > 0) then SceneID := StrToIntDef(SceneIDStr, 0);

    //Update status if valid scene ID
    if (SceneID > 0) and (Scene_Collection.Count > 0) then
    begin
      //Get status
      if (Pos('Opened', CommandStr) > 0) then SceneStatus := SCENE_OPENED
      else if (Pos('Loaded', CommandStr) > 0) then SceneStatus := SCENE_LOADED
      else if (Pos('Loading', CommandStr) > 0) then SceneStatus := SCENE_LOADING
      else if (Pos('Playing', CommandStr) > 0) then SceneStatus := SCENE_PLAYING
      else if (Pos('Stopped', CommandStr) > 0) then SceneStatus := SCENE_STOPPED
      else if (Pos('Closed', CommandStr) > 0) then SceneStatus := SCENE_CLOSED
      else SceneStatus := SCENE_NONEXISTENT;

      //Lookup scene in collection
      for i := 0 to Scene_Collection.Count-1 do
      begin
        SceneRecPtr := Scene_Collection.At(i);
        if (SceneRecPtr^.Scene_ID = SceneID) then
          SceneRecPtr^.Scene_Status := SceneStatus;
      end;
    end;

    //Delete command from data string for next iteration
    if (Pos(EOT_CB, Data) > 0) and (EOTLocation+1 < Length(Data)) then
      Data := Copy(Data, EOTLocation+2, Length(Data))
    else
      AllCommandsProcessed := TRUE;
  until (AllCommandsProcessed)

  //Handling for normal events - next ticker record
  else if (Trim(Data) = '*') and (USEDATAPACKET) and (RunningTicker) then
  begin
    MainForm.ApdStatusLight5.Lit := TRUE;
    //Light ACK received indicator
    ACKReceivedLEDTimer.Enabled := TRUE;
    //TickerCommandDelayTimer.Enabled := TRUE;
  end;
  //else TickerCommandDelayTimer.Enabled := FALSE;
end;

//Timer to extinguish ACK received indicator
procedure TEngineInterface.ACKReceivedLEDTimerTimer(Sender: TObject);
begin
  ACKReceivedLEDTimer.Enabled := FALSE;
  MainForm.ApdStatusLight5.Lit := FALSE;
end;

////////////////////////////////////////////////////////////////////////////////
// DISPLAY DWELL TIMERS
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// TICKER
////////////////////////////////////////////////////////////////////////////////
//Handler for timer to send next command
//Playlist 1
procedure TEngineInterface.TickerCommandDelayTimerTimer(Sender: TObject);
begin
  if (Ticker_Collection.Count > 0) then
  begin
    if (RunningTicker) then
    begin
      //Prevent retrigger
      TickerCommandDelayTimer.Enabled := FALSE;
      //Send next record
      SendTickerRecord(PLAYLIST_1, TRUE, 0);
    end;
  end;
end;
//Playlist 2
procedure TEngineInterface.TickerCommandDelayTimer2Timer(Sender: TObject);
begin
  if (Ticker_Collection2.Count > 0) then
  begin
    if (RunningTicker) then
    begin
      //Prevent retrigger
      TickerCommandDelayTimer2.Enabled := FALSE;
      //Send next record
      SendTickerRecord(PLAYLIST_2, TRUE, 0);
    end;
  end;
end;

//Handler for 1 mS timer used to send next command when jumping over stats headers
//Playlist 1
procedure TEngineInterface.JumpToNextTickerRecordTimerTimer(Sender: TObject);
begin
  if (Ticker_Collection.Count > 0) then
  begin
    if (RunningTicker) then
    begin
      //Prevent retrigger
      JumpToNextTickerRecordTimer.Enabled := FALSE;
      //Send next record
      SendTickerRecord(PLAYLIST_1, TRUE, 0);
    end;
  end;
end;
//Playlist 2
procedure TEngineInterface.JumpToNextTickerRecordTimer2Timer(Sender: TObject);
begin
  if (Ticker_Collection2.Count > 0) then
  begin
    if (RunningTicker) then
    begin
      //Prevent retrigger
      JumpToNextTickerRecordTimer2.Enabled := FALSE;
      //Send next record
      SendTickerRecord(PLAYLIST_2, TRUE, 0);
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TICKER
////////////////////////////////////////////////////////////////////////////////
//Procedure to send next record after receipt of packet
//Procedure to send next record after receipt of packet
procedure TEngineInterface.SendTickerRecord(PlaylistID: SmallInt; NextRecord: Boolean; RecordIndex: SmallInt);
var
  i,j, k: SmallInt;
  TempStr, CmdStr: String;
  TickerRecPtr, TickerRecPtrNew: ^TickerRec;
  FoundGame: Boolean;
  VisitorScore: SmallInt;
  HomeScore: SmallInt;
  VisitorColor: SmallInt;
  HomeColor: SmallInt;
  HomeRank: String;
  VisitorRank: String;
  HomeName: String;
  Visitorname: String;
  IsLast: Boolean;
  OverlayIndex, OverlayIndexNext: SmallInt;
  IsNewOverlay, PadText: Boolean;
  InGameStatsRequired: Boolean;
  FoundGameInDatamart: Boolean;
  PhaseStr, TimeStr: String;
  VLeader, HLeader: Boolean;
  GameHasStarted, GameIsFinal, GameIsHalf: Boolean;
  GTIME: String;
  GPHASE: SmallInt;
  CurrentGamePhaseRec: GamePhaseRec;
  CurrentGameState: SmallInt;
  RecordIsSponsor: Boolean;
  CurrentLeague: String;
  LineupData: LineupText;
  TemplateInfo: TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  OkToGo: Boolean;
  UseFieldDelayTimer: Boolean;
  TempTickerEntryIndex: SmallInt;
  TokenCounter: SmallInt;
  SymbolValueData: SymbolValueRec;
  //Added for Channel Box
  TickerEntryIndex: SmallInt;
  CollectionCount: SmallInt;
begin
  //Init flags
  USEDATAPACKET := TRUE;
  GameHasStarted := FALSE;
  GameIsFinal:= FALSE;
  GameIsHalf := FALSE;
  UseFieldDelayTimer := FALSE;
  //Init FoundGame flag - used to prevent inadverant commands from being sent if game not found
  FoundGame := TRUE;
  RecordIsSponsor := FALSE;

  //Turn on indicator
  PacketIndicatorTimer.Enabled := TRUE;
  MainForm.ApdStatusLight2.Lit := TRUE;

  //Init packet timeout timer to disabled
  TickerPacketTimeoutTimer.Enabled := FALSE;

  //Set looping mode as required
  //Hard-wired for L-Bar
  //Case TickerDisplayMode of
  //  //1x through for 1-lime, 2-line
  //  1,3: LoopTicker := FALSE;
  //  //Continuous loop for 1-lime, 2-line
  //  2,4: LoopTicker := TRUE;
  //end;
  LoopTicker := TRUE;

  //Disable packets and enable timer to re-enable packets
  //PacketEnable := FALSE;
  PacketEnableTimer.Enabled := TRUE;
  try
    //Init
    IsLast := FALSE;

    //Set temporary ticker rec pointer - used for segment mode breaking news
    Case PlaylistID of
      PLAYLIST_1: begin
        CollectionCount := Ticker_Collection.Count;
        TickerEntryIndex := CurrentTickerEntryIndex1;
      end;
      PLAYLIST_2: begin
        CollectionCount := Ticker_Collection2.Count;
        TickerEntryIndex := CurrentTickerEntryIndex2;
      end;
    end;

    //Make sure we haven't dumped out; also enable if in remote control mode from Authoring tool
    if ((TickerAbortFlag = FALSE) OR (AuthoringRemoteControlModeEnabled)) AND
       (CollectionCount > 0) then
    begin
      OKToGo := TRUE;
      //Check if flag set to just send next record
      if (NEXTRECORD = TRUE) then
      begin
        //Make sure there's at least one record left before sending more data;
        //If not, reset to top if in looping mode
        if (TickerEntryIndex >= CollectionCount-1) and (LoopTicker = TRUE) then
        begin
          try
            //Reload the playlist collection in case it has been modified; clear flag to indicate
            //it's not the first time through
            MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
            Case PlaylistID of
              PLAYLIST_1: begin
                if (Ticker_Collection.Count > 0) then TickerEntryIndex := 0;
                CurrentTickerEntryIndex1 := TickerEntryIndex;
                CollectionCount := Ticker_Collection.Count;
              end;
              PLAYLIST_2: begin
                if (Ticker_Collection2.Count > 0) then TickerEntryIndex := 0;
                CurrentTickerEntryIndex2 := TickerEntryIndex;
                CollectionCount := Ticker_Collection2.Count;
              end;
            end;
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to reload L-bar playlist collection');
          end;
          //Check time window and enable
          Case PlaylistID of
            PLAYLIST_1: TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex1);
            PLAYLIST_2: TickerRecPtr := Ticker_Collection2.At(CurrentTickerEntryIndex2);
          end;
          //Update the data from the database - called for each record
          UpdateTickerRecordDataFromDB(PlaylistID, CurrentTickerPlaylistID, TickerEntryIndex, TickerRecPtr^.Event_GUID);
          //Make sure record is enabled and within time window
          j := 0;
          While ((TickerRecPtr^.Enabled = FALSE) OR NOT ((TickerRecPtr^.StartEnableDateTime <= Now) AND
                (TickerRecPtr^.EndEnableDateTime > Now))) AND (j < CollectionCount) do
          begin
            Case PlaylistID of
              PLAYLIST_1: begin
                Inc(CurrentTickerEntryIndex1);
                TickerEntryIndex := CurrentTickerEntryIndex1;
                //Set pointer
                TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex1);
              end;
              PLAYLIST_2: begin
                Inc(CurrentTickerEntryIndex2);
                TickerEntryIndex := CurrentTickerEntryIndex2;
                //Set pointer
                TickerRecPtr := Ticker_Collection2.At(CurrentTickerEntryIndex2);
              end;
            end;
            //Update the data from the database - called for each record
            UpdateTickerRecordDataFromDB(PlaylistID, CurrentTickerPlaylistID, TickerEntryIndex, TickerRecPtr^.Event_GUID);
            if (TickerEntryIndex = CollectionCount) AND (LoopTicker = TRUE) then
            begin
              try
                MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
                Case PlaylistID of
                  PLAYLIST_1: begin
                    if (Ticker_Collection.Count > 0) then TickerEntryIndex := 0;
                    CurrentTickerEntryIndex1 := TickerEntryIndex;
                    CollectionCount := Ticker_Collection.Count;
                    TickerRecPtr := Ticker_Collection.At(TickerEntryIndex);
                  end;
                  PLAYLIST_2: begin
                    if (Ticker_Collection2.Count > 0) then TickerEntryIndex := 0;
                    CurrentTickerEntryIndex2 := TickerEntryIndex;
                    CollectionCount := Ticker_Collection2.Count;
                    TickerRecPtr := Ticker_Collection2.At(TickerEntryIndex);
                  end;
                end;
                //Update the data from the database - called for each record
                UpdateTickerRecordDataFromDB(PlaylistID, CurrentTickerPlaylistID, TickerEntryIndex, TickerRecPtr^.Event_GUID);
                j := 0;
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to reload main playlist collection');
              end;
            end
            else if (TickerEntryIndex = CollectionCount) AND (Loopticker = FALSE) then
            begin
              //Abort the ticker and exit this procedure
              MainForm.AbortCurrentEvent(IS_NOT_STARTUP);
              MainForm.ResetTickerToTop;
              PacketEnableTimer.Enabled := TRUE;
              RunningTicker := FALSE;
              //Dump out
              Exit;
            end;
            Inc(j);
          end;
          //Set flag if no enabled records found
          if (j >= CollectionCount) then
          begin
            OKToGo := FALSE;
            //Abort the ticker and exit this procedure
            MainForm.AbortCurrentEvent(IS_NOT_STARTUP);
            MainForm.ResetTickerToTop;
            PacketEnableTimer.Enabled := TRUE;
            RunningTicker := FALSE;
            //Dump out
            Exit;
          end
        end
        //Not last record, so increment Ticker playout collection object index
        else if (TickerEntryIndex < CollectionCount-1) then
        begin
          //Make sure record is enabled and within time window
          j := 0;
          Repeat
            Case PlaylistID of
              PLAYLIST_1: begin
                Inc(CurrentTickerEntryIndex1);
                TickerEntryIndex := CurrentTickerEntryIndex1;
                //Set pointer
                TickerRecPtr := Ticker_Collection.At(CurrentTickerEntryIndex1);
              end;
              PLAYLIST_2: begin
                Inc(CurrentTickerEntryIndex2);
                TickerEntryIndex := CurrentTickerEntryIndex2;
                //Set pointer
                TickerRecPtr := Ticker_Collection2.At(CurrentTickerEntryIndex2);
              end;
            end;
            if (TickerEntryIndex = CollectionCount) AND (LoopTicker = TRUE) then
            begin
              try
                MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
                Case PlaylistID of
                  PLAYLIST_1: begin
                    if (Ticker_Collection.Count > 0) then TickerEntryIndex := 0;
                    CurrentTickerEntryIndex1 := TickerEntryIndex;
                    CollectionCount := Ticker_Collection.Count;
                    TickerRecPtr := Ticker_Collection.At(TickerEntryIndex);
                  end;
                  PLAYLIST_2: begin
                    if (Ticker_Collection2.Count > 0) then TickerEntryIndex := 0;
                    CurrentTickerEntryIndex2 := TickerEntryIndex;
                    CollectionCount := Ticker_Collection2.Count;
                    TickerRecPtr := Ticker_Collection2.At(TickerEntryIndex);
                  end;
                end;
                //Update the data from the database - called for each record
                UpdateTickerRecordDataFromDB(PlaylistID, CurrentTickerPlaylistID, TickerEntryIndex, TickerRecPtr^.Event_GUID);
                j := 0;
              except
                if (ErrorLoggingEnabled = True) then
                  WriteToErrorLog('Error occurred while trying to reload main ticker collection');
              end;
            end
            //Abort ticker if not looping
            else if (TickerEntryIndex = CollectionCount) AND (Loopticker = FALSE) then
            begin
              //Abort the ticker and exit this procedure
              MainForm.AbortCurrentEvent(IS_NOT_STARTUP);
              MainForm.ResetTickerToTop;
              PacketEnableTimer.Enabled := TRUE;
              RunningTicker := FALSE;
              //Dump out
              Exit;
            end;
            Case PlaylistID of
              PLAYLIST_1: TickerRecPtr := Ticker_Collection.At(TickerEntryIndex);
              PLAYLIST_2: TickerRecPtr := Ticker_Collection2.At(TickerEntryIndex);
            end;
            Inc (j);
            //Update the data from the database - called for each record
            UpdateTickerRecordDataFromDB(PlaylistID, CurrentTickerPlaylistID, TickerEntryIndex, TickerRecPtr^.Event_GUID);
          Until ((TickerRecPtr^.Enabled = TRUE) AND (TickerRecPtr^.StartEnableDateTime <= Now) AND
                (TickerRecPtr^.EndEnableDateTime > Now)) OR (j >= Ticker_Collection.Count);
          //Set flag if no enabled records found
          if (j >= CollectionCount) then
          begin
            OKToGo := FALSE;
            //Abort the ticker and exit this procedure
            MainForm.AbortCurrentEvent(IS_NOT_STARTUP);
            MainForm.ResetTickerToTop;
            PacketEnableTimer.Enabled := TRUE;
            RunningTicker := FALSE;
            //Dump out
            Exit;
          end;
        end
        //At end and not looping, so clear screen
        else if (TickerEntryIndex >= CollectionCount-1) AND
                (LoopTicker = FALSE) then
        begin
          MainForm.AbortCurrentEvent(IS_NOT_STARTUP);
          MainForm.ResetTickerToTop;
          PacketEnableTimer.Enabled := TRUE;
          RunningTicker := FALSE;
          //Dump out
          Exit;
        end;
        //Set last segment heading - used for segment mode breaking news
        LastSegmentHeading := TickerRecPtr^.League;

        //Clear ticker if at last collection obejct and not looping
        if (TickerEntryIndex > CollectionCount-1) AND
           (LoopTicker = FALSE) then
        begin
          MainForm.AbortCurrentEvent(IS_NOT_STARTUP);
          MainForm.ResetTickerToTop;
          RunningTicker := FALSE;
          PacketEnableTimer.Enabled := TRUE;
          //Dump out
          Exit;
        end;
        //Disable command timer
        DisableCommandTimer := FALSE;
      end
      //Triggering specific record, so set current entry index
      else begin
        Case PlaylistID of
          PLAYLIST_1: begin
            CurrentTickerEntryIndex1 := RecordIndex;
            TickerEntryIndex := RecordIndex;
            CollectionCount := Ticker_Collection.Count;
          end;
          PLAYLIST_2: begin
            CurrentTickerEntryIndex2 := RecordIndex;
            TickerEntryIndex := RecordIndex;
            CollectionCount := Ticker_Collection2.Count;
          end;
        end;
        //Disble command timer
        DisableCommandTimer := TRUE;
      end;

      //Proceed if not at end or in looping mode and at beggining
      if (TickerEntryIndex <= CollectionCount-1) and (OKToGo) then
      begin
        Case PlaylistID of
          //Get pointer to current record
          PLAYLIST_1: TickerRecPtr := Ticker_Collection.At(TickerEntryIndex);
          PLAYLIST_2: TickerRecPtr := Ticker_Collection2.At(TickerEntryIndex);
        end;
        //Update the data from the database - called for each record
        UpdateTickerRecordDataFromDB(PlaylistID, CurrentTickerPlaylistID, TickerEntryIndex, TickerRecPtr^.Event_GUID);

        ////////////////////////////////////////////////////////////////////////
        //MAIN PROCESSING LOOP FOR TICKER FIELDS
        ////////////////////////////////////////////////////////////////////////
        //Get template info and load the termporary fields collection
        //Always load into temporary fields collection for Playlist 1 in this function
        //TemplateInfo := LoadTempTemplateFields(PlaylistID, TickerRecPtr^.Template_ID);
        TemplateInfo := LoadTempTemplateFields(PLAYLIST_1, TickerRecPtr^.Template_ID);

        //Get game data if template requires it and in ticker mode
        if (TemplateInfo.UsesGameData) then
        begin
          try
            //Use full game data
            CurrentGameData := GetGameData(TRIM(TickerRecPtr^.League), TRIM(TickerRecPtr^.GameID));
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to get game data for ticker');
          end;
          FoundGame := CurrentGameData.DataFound;
        end;

        //Check the game state vs. the state required by the template if not a match, increment
        //Also do check for enabled entries and check time window
        if (TemplateInfo.RequiredGameState > 0) AND (CurrentGameData.StateForInProgressCheck <> TemplateInfo.RequiredGameState) AND
        //Special case for postponed, delayed or suspended MLB games
          (NOT(((CurrentGameData.State = POSTPONED) OR (CurrentGameData.State = SUSPENDED) OR (CurrentGameData.State = DELAYED)) AND
           (TemplateInfo.RequiredGameState = 3))) AND
           (NEXTRECORD = TRUE) then
        Repeat
          Case PlaylistID of
            PLAYLIST_1: begin
              Inc(CurrentTickerEntryIndex1);
              TickerEntryIndex := CurrentTickerEntryIndex1;
            end;
            PLAYLIST_2: begin
              Inc(CurrentTickerEntryIndex2);
              TickerEntryIndex := CurrentTickerEntryIndex2;
            end;
          end;
          if (TickerEntryIndex = CollectionCount) AND (Loopticker = TRUE) then
          begin
            MainForm.LoadPlaylistCollection(TICKER, 0, CurrentTickerPlaylistID);
            Case PlaylistID of
              PLAYLIST_1: begin
                CurrentTickerEntryIndex1 := 0;
                TickerEntryIndex := CurrentTickerEntryIndex1;
              end;
              PLAYLIST_2: begin
                CurrentTickerEntryIndex2 := 0;
                TickerEntryIndex := CurrentTickerEntryIndex2;
              end;
            end;
          end
          else if (TickerEntryIndex = CollectionCount) AND (Loopticker = FALSE) then
          begin
            //Abort the ticker and exit this procedure
            MainForm.AbortCurrentEvent(IS_NOT_STARTUP);
            MainForm.ResetTickerToTop;
            PacketEnableTimer.Enabled := TRUE;
            RunningTicker := FALSE;
            //Dump out
            Exit;
          end;
          Case PlaylistID of
            PLAYLIST_1: TickerRecPtr := Ticker_Collection.At(TickerEntryIndex);
            PLAYLIST_2: TickerRecPtr := Ticker_Collection2.At(TickerEntryIndex);
          end;
          //Update the data from the database - called for each record
          UpdateTickerRecordDataFromDB(PlaylistID, CurrentTickerPlaylistID, TickerEntryIndex, TickerRecPtr^.Event_GUID);
          TemplateInfo := LoadTempTemplateFields(PlaylistID, TickerRecPtr^.Template_ID);
          if (TemplateInfo.UsesGameData) then
          begin
            try
              //Use full game data
              CurrentGameData := GetGameData(TRIM(TickerRecPtr^.League), TRIM(TickerRecPtr^.GameID));
            except
              if (ErrorLoggingEnabled = True) then
                WriteToErrorLog('Error occurred while trying to get game data for playlist entry');
            end;
            FoundGame := CurrentGameData.DataFound;
          end
          else CurrentGameData.State := UNDEFINED;
        //Version 2.1.0.0 Bug corrected that resulted in all templates being displayed for games that were delayed, cancelled, etc.
        Until ((TemplateInfo.RequiredGameState = 0) OR (CurrentGameData.StateForInProgressCheck = TemplateInfo.RequiredGameState) OR
              (((CurrentGameData.State = POSTPONED) OR (CurrentGameData.State = DELAYED) OR (CurrentGameData.State = SUSPENDED)) AND
               (TemplateInfo.RequiredGameState = FINAL))) AND
              ((TickerRecPtr^.Enabled = TRUE) AND
              (TickerRecPtr^.StartEnableDateTime <= Now) AND (TickerRecPtr^.EndEnableDateTime > Now));

        //Set flag
        USEDATAPACKET := TRUE;

        //Process display of record - set timeouts
        //Check for sponsor first
        if (TickerRecPtr^.SponsorLogo_Dwell > 0) then
        begin
          TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
          TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 60000;
        end
        else begin
          TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
          if (TickerRecPtr^.DwellTime = 1) then
            TickerPacketTimeoutTimer.Interval := 90000
          else
            TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 60000;
        end;

        //Add the fields from the temporary fields collection
        if (Temporary_Fields_Collection.Count > 0) then
        begin
          for j := 0 to Temporary_Fields_Collection.Count-1 do
          begin
            TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
            //Version 2.1
            //Check for single fields (not compound fields required for 2-line, new look engine)
            if (TemplateFieldsRecPtr^.Field_Type > 0) then
            begin
              //Filter out fields ID values of -1 => League designator
              //if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
              begin
                //Add region command
                //If not a symbolic fields, send field contents directly
                if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
                begin
                  TempStr := TemplateFieldsRecPtr^.Field_Contents;
                  //Add prefix and/or suffix if applicable
                  if (TemplateFieldsRecPtr^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                    TempStr := TemplateFieldsRecPtr^.Field_Format_Prefix + TempStr;
                  if (TemplateFieldsRecPtr^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                    TempStr := TempStr + TemplateFieldsRecPtr^.Field_Format_Suffix;
                  if (Trim(TempStr) = '') then TempStr := ' ';

                  //Append field update to command string
                  CmdStr := CmdStr + TemplateFieldsRecPtr^.Scene_Field_Name + '�' + TempStr;
                  //Append command delimiter if not last field
                  if (j < Temporary_Fields_Collection.Count-1) then CmdStr := CmdStr + '\'
                end
                //It's a symbolic name, so get the field contents
                 else begin
                  try
                    TempStr := GetValueOfSymbol(PlaylistID, TICKER, TemplateFieldsRecPtr^.Field_Contents, TickerEntryIndex,
                      CurrentGameData, TickerDisplayMode, TickerRecPtr^.UserData[21]).SymbolValue;
                    //Add previx and/or suffix if applicable
                    if (TemplateFieldsRecPtr^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TemplateFieldsRecPtr^.Field_Format_Prefix + TempStr;
                    if (TemplateFieldsRecPtr^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TempStr + TemplateFieldsRecPtr^.Field_Format_Suffix;
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to get value for main ticker data field');
                  end;
                  if (Trim(TempStr) = '') then TempStr := ' ';

                  //Process the style chips
                  TempStr := ProcessStyleChips(TempStr);

                  //Append field update to command string
                  CmdStr := CmdStr + TemplateFieldsRecPtr^.Scene_Field_Name + '�' + TempStr;
                  //Append command delimiter if not last field
                  if (j < Temporary_Fields_Collection.Count-1) then CmdStr := CmdStr + '\'
                end;
              end;
            end;
          end;
       end;

        //Transition out the prior scene
        //Transition out existing sponsor if new page is a sponsor logo
        if (TickerRecPtr^.SponsorLogo_Dwell <> 0) then
        begin
          CurrentSponsorInfo[1].CurrentSponsorLogoName := TickerRecPtr^.SponsorLogo_Name;
          WriteToAsRunLog('Started display of sponsor logo: ' + CurrentSponsorInfo[1].CurrentSponsorLogoName);
          EngineInterface.UpdateScene(SPONSOR_LOGO_SCENE_ID, SPONSOR_LOGO_OUT_COMMAND + '�' + 'T');
        end
        //Not a sponsor
        else begin
          if (LastTemplateInfo[PlaylistID].Scene_ID <> 0) and
             //(LastTemplateInfo[PlaylistID].Scene_ID <> SPONSOR_LOGO_SCENE_ID) and
             (LastTemplateInfo[PlaylistID].OutTransition_Command <> '') then
            EngineInterface.UpdateScene(LastTemplateInfo[PlaylistID].Scene_ID, LastTemplateInfo[PlaylistID].OutTransition_Command + '�' + 'T');
        end;

        //Added for Version 1.1.0  10/20/11
        //Play the new scene
        Case PlaylistID of
          PLAYLIST_1: PlayScene(TemplateInfo.Scene_ID_1);
          PLAYLIST_2: PlayScene(TemplateInfo.Scene_ID_2);
        end;

        //Delay
        if (TickerRecPtr^.SponsorLogo_Dwell <> 0) then
        begin
          for k := 1 to 10 do
          begin
            Sleep (100);
            //Application.ProcessMessages;
          end;
        end
        else Sleep (200);
//TEST
        //else Sleep (500);

        //Send the new data
        if (TemplateFieldUpdatesEnabled) then
        begin
          Case PlaylistID of
            PLAYLIST_1: UpdateScene(TemplateInfo.Scene_ID_1, CmdStr);
            PLAYLIST_2: UpdateScene(TemplateInfo.Scene_ID_2, CmdStr);
          end;
        end;

        //Delay
        Sleep (100);

        //Transition the data in & store info on the last scene & transition
        Case PlaylistID of
          //Playlist for Field 1
          PLAYLIST_1: begin
            //Check field state and change backplates if needed (Field 1&2 vs. Field 3)
            Case TemplateInfo.Template_Type of
              //Fields 1 & 2
              FIELDS_1_2: begin
                //Transition in Field 1 backplate if startup
                if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID = STARTUP) then
                begin
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_1_In_Command + '�' + 'T');
                end
                //Take out Field 3 if current state and bring in Field 1
                else if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID = FIELD_3) then
                begin
                  //Take out Field 3 if not a full page promo
                  if (LastTemplateInfo[PLAYLIST_1].Scene_ID <> Full_Page_Promo_Scene_ID) then
                    EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_3_Out_Command + '�' + 'T');
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_1_In_Command + '�' + 'T');

                  //Automatically fire next template for Field 2 region - would have been held off to prevent
                  //premature removal of Field Type 3 template in auto-sequence mode
                  if (RunningTicker) then JumpToNextTickerRecordTimer2.Enabled := TRUE;
                end;
              end;
              //Field 3
              FIELD_3: begin
                //Take out Fields 1 & 2 if current state and bring in Field 3
                if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID = FIELDS_1_2) then
                begin
                  //Take out field 2 if it was in
                  if (LastTemplateInfo[PLAYLIST_2].LastFieldTypeID = FIELDS_1_2) then
                  begin
                    EngineInterface.UpdateScene(LastTemplateInfo[PLAYLIST_2].Scene_ID, LastTemplateInfo[PLAYLIST_2].OutTransition_Command + '�' + 'T');
                    EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_2_Out_Command + '�' + 'T');
                    //Clear fields so Field 2 starts from scratch the next time around
                    LastTemplateInfo[PLAYLIST_2].LastFieldTypeID := STARTUP;
                    LastTemplateInfo[PLAYLIST_2].Scene_ID := 0;
                  end;
                  //Take out field 1
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_1_Out_Command + '�' + 'T');
                  //Bring in Field 3 if not a full page promo
                  if (TickerRecPtr^.Template_ID <> FULL_PAGE_PROMO_TEMPLATE) then
                    EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_3_In_Command + '�' + 'T');
                end
                //Transition in Field 3 backplate if startup
                else if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID = STARTUP) then
                begin
                  if (LastTemplateInfo[PLAYLIST_1].Scene_ID <> Full_Page_Promo_Scene_ID) then
                    EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_3_In_Command + '�' + 'T');
                end
                //Added for Version 2.0
                else if (LastTemplateInfo[PLAYLIST_1].Scene_ID = Full_Page_Promo_Scene_ID) then
                begin
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_3_In_Command + '�' + 'T');
                end;
                //Disable the timer for Field Region #2 to prevent premature removal of Field 3 template
                if (RunningTicker) then TickerCommandDelayTimer2.Enabled := FALSE;
              end;
            end;
            //Transition the new scene in
            EngineInterface.UpdateScene(TemplateInfo.Scene_ID_1, TemplateInfo.Transition_In + '�' + 'T');

            //Stop the prior scene if it's a different scene number
            //Added for Version 1.1.0  10/20/11
            if (TickerRecPtr^.SponsorLogo_Dwell = 0) and (TemplateInfo.Scene_ID_1 <> LastTemplateInfo[PLAYLIST_1].Scene_ID) then
              StopScene(LastTemplateInfo[PLAYLIST_1].Scene_ID);

            //Store the last template info if not a sponsor
            if (TickerRecPtr^.SponsorLogo_Dwell = 0) then
            begin
              LastTemplateInfo[PLAYLIST_1].Scene_ID := TemplateInfo.Scene_ID_1;
              LastTemplateInfo[PLAYLIST_1].OutTransition_Command := TemplateInfo.Transition_Out;
              LastTemplateInfo[PLAYLIST_1].LastFieldTypeID := TemplateInfo.Template_Type;
            end;
          end;

          //Playlist for Field 2
          PLAYLIST_2: begin
            //Check field state and change backplates if needed (Field 1&2 vs. Field 3)
            Case TemplateInfo.Template_Type of
              //Fields 1 & 2
              FIELDS_1_2: begin
                //Take out Field 3 if current state and bring in Field 2
                if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID = FIELD_3) then
                begin
                  //Take out the text from scene 1
                  if (LastTemplateInfo[PLAYLIST_1].Scene_ID <> 0) and (LastTemplateInfo[PLAYLIST_1].OutTransition_Command <> '') then
                    EngineInterface.UpdateScene(LastTemplateInfo[PLAYLIST_1].Scene_ID, LastTemplateInfo[PLAYLIST_1].OutTransition_Command + '�' + 'T');

                  //Swap the backplates
                  if (LastTemplateInfo[PLAYLIST_1].Scene_ID <> Full_Page_Promo_Scene_ID) then
                    EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_3_Out_Command + '�' + 'T');

                  //Clear fields so Field 1 starts from scratch the next time around
                  LastTemplateInfo[PLAYLIST_1].LastFieldTypeID := STARTUP;
                  LastTemplateInfo[PLAYLIST_1].Scene_ID := 0;
                end;
                //Bring in Field 2 backplate
                if (LastTemplateInfo[PLAYLIST_2].LastFieldTypeID = STARTUP) then
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_2_In_Command + '�' + 'T');
              end;
            end;
            //Transition the new scene in
            EngineInterface.UpdateScene(TemplateInfo.Scene_ID_2, TemplateInfo.Transition_In + '�' + 'T');

            //Stop the prior scene if it's a different scene number
            //Added for Version 1.1.0  10/20/11
            if (TickerRecPtr^.SponsorLogo_Dwell = 0) and (TemplateInfo.Scene_ID_2 <> LastTemplateInfo[PLAYLIST_2].Scene_ID) then
              StopScene(LastTemplateInfo[PLAYLIST_2].Scene_ID);

            //Store the last template info
            LastTemplateInfo[PLAYLIST_2].Scene_ID := TemplateInfo.Scene_ID_2;
            LastTemplateInfo[PLAYLIST_2].OutTransition_Command := TemplateInfo.Transition_Out;
            LastTemplateInfo[PLAYLIST_2].LastFieldTypeID := TemplateInfo.Template_Type;
          end;
        end;

        //Set data row indicator in Ticker grid
        if (NEXTRECORD) then
        begin
          try
            Case PlaylistID of
              PLAYLIST_1: begin
                MainForm.PlaylistGrid.CurrentDataRow := CurrentTickerEntryIndex1+1;
                MainForm.PlaylistGrid.SetTopLeft(1, MainForm.PlaylistGrid.CurrentDataRow);
                //Update Authoring tool index
                AuthoringInterface.SendPlaylistIndex(PLAYLIST_1, CurrentTickerEntryIndex1);
              end;
              PLAYLIST_2: begin
                MainForm.PlaylistGrid2.CurrentDataRow := CurrentTickerEntryIndex2+1;
                MainForm.PlaylistGrid2.SetTopLeft(1, MainForm.PlaylistGrid2.CurrentDataRow);
                //Update Authoring tool index
                AuthoringInterface.SendPlaylistIndex(PLAYLIST_2, CurrentTickerEntryIndex2);
              end;
            end;
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to reposition grid cursor to correct record');
          end;
        end;

        Case PlaylistID of
          PLAYLIST_1: begin
            //Enable packet timeout timers
            TickerPacketTimeoutTimer.Enabled := FALSE;
            TickerPacketTimeoutTimer.Enabled := TRUE;
            //Start timer to send next record
            //if (USEDATAPACKET = FALSE) then
            if (USEDATAPACKET) then
            begin
              TickerCommandDelayTimer.Enabled := TRUE;
            end
            else begin
              if (FoundGame = FALSE) then
              begin
                //Enable delay timer
                if (RunningTicker = TRUE) then JumpToNextTickerRecordTimer.Enabled := TRUE;
              end;
            end;
          end;
          PLAYLIST_2: begin
            TickerPacketTimeoutTimer2.Enabled := FALSE;
            TickerPacketTimeoutTimer2.Enabled := TRUE;
            //Start timer to send next record
            //if (USEDATAPACKET = FALSE) then
            if (USEDATAPACKET) then
            begin
              TickerCommandDelayTimer2.Enabled := TRUE;
            end
            else begin
              if (FoundGame = FALSE) then
              begin
                //Enable delay timer
                if (RunningTicker = TRUE) then JumpToNextTickerRecordTimer2.Enabled := TRUE;
              end;
            end;
          end;
        end;
      end;
    end;
    //Send status to Authoring tool
    AuthoringInterface.SendAutoSequenceStatus(RunningTicker);
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to send additional records to ticker');
    end;
  end;
  //Set dwell label value
  MainForm.DwellValue.Caption := IntToStr(TickerCommandDelayTimer.Interval);
end;

//Procedure to clear the field panels and data; called when triggered out via GPI or under remote control
procedure TEngineInterface.ClearFieldPanels(Stop_Scenes: Boolean);
var
  i, j: SmallInt;
  SceneRecPtr: ^SceneRec;
begin
  //Transition out the data for field 1
  if (LastTemplateInfo[PLAYLIST_1].Scene_ID <> 0) and
     (LastTemplateInfo[PLAYLIST_1].Scene_ID <> SPONSOR_LOGO_SCENE_ID) and
     (LastTemplateInfo[PLAYLIST_1].OutTransition_Command <> '') then
       EngineInterface.UpdateScene(LastTemplateInfo[PLAYLIST_1].Scene_ID, LastTemplateInfo[PLAYLIST_1].OutTransition_Command + '�' + 'T');

  //Transition out the data for field 2
  if (LastTemplateInfo[PLAYLIST_2].LastFieldTypeID = FIELDS_1_2) then
    EngineInterface.UpdateScene(LastTemplateInfo[PLAYLIST_2].Scene_ID, LastTemplateInfo[PLAYLIST_2].OutTransition_Command + '�' + 'T');

  //Transition out the backplates
  if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID = FIELD_3) then
    EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_3_Out_Command + '�' + 'T')
  else begin
    if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID = FIELDS_1_2) then
      EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_1_Out_Command + '�' + 'T');
    if (LastTemplateInfo[PLAYLIST_2].LastFieldTypeID = FIELDS_1_2) then
      EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_2_Out_Command + '�' + 'T');
  end;

  //Transition out the top marketing image region
  if (TopRegionToggleState = 1) then
  begin
    //Trigger current graphic #1 out
    UpdateScene(BaseScene_SceneID, 'Image_1_Out_Btn' + '�' + 'T');
  end
  else if (TopRegionToggleState = 2) then
  begin
    //Trigger current graphic #2 out
    UpdateScene(BaseScene_SceneID, 'Image_2_Out_Btn' + '�' + 'T');
  end;

  //Set values so panels will fly in with next trigger
  LastTemplateInfo[PLAYLIST_1].LastFieldTypeID := STARTUP;
  LastTemplateInfo[PLAYLIST_1].Scene_ID := 0;
  LastTemplateInfo[PLAYLIST_1].OutTransition_Command := '';
  LastTemplateInfo[PLAYLIST_2].LastFieldTypeID := STARTUP;
  LastTemplateInfo[PLAYLIST_2].Scene_ID := 0;
  LastTemplateInfo[PLAYLIST_2].OutTransition_Command := '';
  TopRegionToggleState := -1;

  //Stop all text/image template scenes
  if (Stop_Scenes) then
  begin
    //Set status bar color
    MainForm.StatusBar.Color := clYellow;
    //Process scenes
    for i := 0 to Scene_Collection.Count-1 do
    begin
      //Get status ad play scene if needed
      SceneRecPtr := Scene_Collection.At(i);
      QuerySceneStatus(SceneRecPtr^.Scene_ID);

      //If seen not flagged to play at startup, stop it
      if (SceneRecPtr^.Load_At_Startup) and (SceneRecPtr^.Play_At_Startup = FALSE) and ((SceneRecPtr^.Scene_Status = SCENE_PLAYING) or (AlwaysPlayAllScenesAtStartup)) then
      begin
        StopScene(SceneRecPtr^.Scene_ID);
        MainForm.StatusBar.SimpleText := ' STOPPING SCENE ID: ' + IntToStr(SceneRecPtr^.Scene_ID);
        j := 0;
        //Wait for scene to be loaded
        Repeat
          //for j := 1 to 10 do
          //begin
            Sleep(100);
            Application.ProcessMessages;
            QuerySceneStatus(SceneRecPtr^.Scene_ID);
          //end;
          inc(j);
        //Timeout set for 100 * 100mS = 10 seconds
        Until (SceneRecPtr^.Scene_Status = SCENE_STOPPED) or (SceneRecPtr^.Scene_Status = SCENE_LOADED) or (j >= 100);
        //Check for timeout
        if (j >= 100) then
        begin
          MainForm.StatusBar.Color := clRed;
          MainForm.StatusBar.SimpleText := ' TIMEOUT OCCURRED WHILE TRYING TO STOP SCENE ID: ' + IntToStr(SceneRecPtr^.Scene_ID);
          Exit;
        end;
      end;
    end;
    //Set status on main form
    MainForm.StatusBar.Color := clBtnFace;
    MainForm.StatusBar.SimpleText := ' ';
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// SOCKET EVENT HANDLERS
////////////////////////////////////////////////////////////////////////////////
//Handler for packet re-enable timer
procedure TEngineInterface.PacketEnableTimerTimer(Sender: TObject);
begin
  //Disable to prevent retrigger
  PacketEnableTimer.Enabled := FALSE;
  //Re-enable packets
  PacketEnable := TRUE;
end;

//Handler to turn off packet recevied indicator after 250 mS
procedure TEngineInterface.PacketIndicatorTimerTimer(Sender: TObject);
begin
  PacketIndicatorTimer.Enabled := FALSE;
  MainForm.ApdStatusLight2.Lit := FALSE;
end;

////////////////////////////////////////////////////////////////////////////////
// PACKET TIMEOUT TIMERS
////////////////////////////////////////////////////////////////////////////////
//Handler for ticker packet timeout timer - sends next command if status command not
//received
//Playlist 1
procedure TEngineInterface.TickerPacketTimeoutTimerTimer(Sender: TObject);
begin
  //Disable timer to prevent retriggering
  TickerPacketTimeoutTimer.Enabled := FALSE;
  //Enable delay timer
  if (RunningTicker = TRUE) then
    //Send next record
    SendTickerRecord(PLAYLIST_1, TRUE, 0);
  //Log the timeout
  if (ErrorLoggingEnabled = True) then
  begin
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('A timeout occurred while waiting for a Field 1 command return status from the graphics engine.');
  end;
end;
//Playlist 2
procedure TEngineInterface.TickerPacketTimeoutTimer2Timer(Sender: TObject);
begin
  //Disable timer to prevent retriggering
  TickerPacketTimeoutTimer2.Enabled := FALSE;
  //Enable delay timer
  if (RunningTicker = TRUE) then
    //Send next record
    SendTickerRecord(PLAYLIST_2, TRUE, 0);
  //Log the timeout
  if (ErrorLoggingEnabled = True) then
  begin
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('A timeout occurred while waiting for a Field 2 command return status from the graphics engine.');
  end;
end;

//Handler to light indicator
procedure TEngineInterface.EnginePortConnect(Sender: TObject; Socket: TCustomWinSocket);
begin
  MainForm.ApdStatusLight1.Lit := TRUE;
  SocketConnected := TRUE;
end;

//Handler to turn off indicator
procedure TEngineInterface.EnginePortDisconnect(Sender: TObject; Socket: TCustomWinSocket);
begin
  MainForm.ApdStatusLight1.Lit := FALSE;
  SocketConnected := FALSE;
  MessageDlg('The connection to the graphics engine was lost. ' +
             'You will not be able to control the graphics engine. If you wish to try ' +
             'reconnecting to the engine, please select Utilities | Reconnect to Graphics Engine ' +
             'from the main program menu.', mtError, [mbOK], 0);
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    MainForm.Label5.Caption := 'ERROR';
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred with connection to ticker graphics engine. Please verify that the engine is running. This may also ' +
        'be due to problems with your network');
    if (RunningTicker) then
    begin
      //Abort
      MainForm.AbortCurrentEvent(IS_NOT_STARTUP);
    end;
  end;
end;

//Handler for socket error
procedure TEngineInterface.EnginePortError(Sender: TObject; Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  MainForm.ApdStatusLight1.Lit := FALSE;
  SocketConnected := FALSE;
  MessageDlg('The connection to the graphics engine was lost. ' +
             'You will not be able to control the graphics engine. If you wish to try ' +
             'reconnecting to the engine, please select Utilities | Reconnect to Graphics Engine ' +
             'from the main program menu.', mtError, [mbOK], 0);
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    MainForm.Label5.Caption := 'ERROR';
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred with connection to ticker graphics engine. Please verify that the engine is running. This may also ' +
        'be due to problems with your network');
    if (RunningTicker) then
    begin
      //Abort
      MainForm.AbortCurrentEvent(IS_NOT_STARTUP);
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// CHANNEL BOX SPECIFIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Procedure to send a command to the Playout Controller application
procedure TEngineInterface.SendCommand(CommandString: String);
begin
  EnginePort.Socket.SendText(CommandString + #13#10);
  //Write to command log
  if (CommandLoggingEnabled) then WriteToCommandLog(CommandString);
end;

//Open the specified scene
procedure TEngineInterface.OpenScene(SceneID: LongInt; SendParams: Boolean; Params: ANSIString);
var
  CmdStr: String;
begin
  CmdStr := 'B\OP\' + IntToStr(SceneId);
  if (SendParams) then CmdStr := CmdStr + '\' + Params;
  CmdStr := CmdStr + '\\';
  SendCommand(CmdStr);
end;

//Load the specified scene
procedure TEngineInterface.LoadScene(SceneID: LongInt; SendParams: Boolean; Params: ANSIString);
var
  CmdStr: String;
begin
  CmdStr := 'B\LO\' + IntToStr(SceneId);
  if (SendParams) then CmdStr := CmdStr + '\' + Params;
  CmdStr := CmdStr + '\\';
  SendCommand(CmdStr);
end;

//Play the specified scene
procedure TEngineInterface.PlayScene(SceneID: LongInt);
var
  CmdStr: String;
begin
  CmdStr := 'B\PL\' + IntToStr(SceneId) + '\\';
  SendCommand(CmdStr);
end;

//Stop the specified scene
procedure TEngineInterface.StopScene(SceneID: LongInt);
var
  CmdStr: String;
begin
  CmdStr := 'B\ST\' + IntToStr(SceneId) + '\\';
  SendCommand(CmdStr);
end;

//Close the specified scene
procedure TEngineInterface.CloseScene(SceneID: LongInt);
var
  CmdStr: String;
begin
  CmdStr := 'B\CL\' + IntToStr(SceneId) + '\\';
  SendCommand(CmdStr);
end;

//Update the specified scene with the specified name/value pairs
procedure TEngineInterface.UpdateScene(SceneID: LongInt; Params: ANSIString);
var
  CmdStr: String;
begin
  CmdStr := 'B\UP\' + IntToStr(SceneId) + '\' + Params + '\\';
  SendCommand(CmdStr);
end;

//Query the status of the specified scene
procedure TEngineInterface.QuerySceneStatus(SceneID: LongInt);
var
  CmdStr: String;
begin
  CmdStr := 'B\QS\' + IntToStr(SceneId) + '\\';
  SendCommand(CmdStr);
end;

//Setup the initial scenes - Load & Play
procedure TEngineInterface.SetupInitialScenes;
var
  i, j: LongInt;
  SceneRecPtr: ^SceneRec;
begin
  //Set status on main form
  MainForm.StatusBar.Color := clYellow;
  MainForm.StatusBar.SimpleText := ' PLEASE WAIT WHILE THE CHANNEL BOX SCENES ARE LOADED.';

  if (Scene_Collection.Count > 0) then
  begin
    //Load the scenes
    for i := 0 to Scene_Collection.Count-1 do
    begin
      //Get status & load scene if needed
      SceneRecPtr := Scene_Collection.At(i);
      //If seen not stopped, loaded or playing, load it
      if (SceneRecPtr^.Load_At_Startup) and (SceneRecPtr^.Scene_Status <> SCENE_STOPPED) and
         (SceneRecPtr^.Scene_Status <> SCENE_LOADED) and (SceneRecPtr^.Scene_Status <> SCENE_PLAYING) then
      begin
        LoadScene(SceneRecPtr^.Scene_ID, FALSE, '');
        MainForm.StatusBar.SimpleText := ' LOADING SCENE ID: ' + IntToStr(SceneRecPtr^.Scene_ID);
        //Wait for scene to be loaded
        j := 0;
        Repeat
          //for j := 1 to 5 do
          //begin
            Sleep(500);
            Application.ProcessMessages;
            QuerySceneStatus(SceneRecPtr^.Scene_ID);
          //end;
          inc(j);
        //Timeout set for 40*500mS = 20 seconds
        Until (SceneRecPtr^.Scene_Status = SCENE_LOADED) or (j >= 40);
        //Check for timeout
        if (j >= 40) then
        begin
          MainForm.StatusBar.Color := clRed;
          MainForm.StatusBar.SimpleText := ' TIMEOUT OCCURRED WHILE TRYING TO LOAD SCENE ID: ' + IntToStr(SceneRecPtr^.Scene_ID);
          Exit;
        end;
      end;
    end;

    //Delay
    for j := 1 to 20 do
    begin
      Sleep(100);
      Application.ProcessMessages;
    end;

    //Play the scenes
    for i := 0 to Scene_Collection.Count-1 do
    begin
      //Get status ad play scene if needed
      SceneRecPtr := Scene_Collection.At(i);
      QuerySceneStatus(SceneRecPtr^.Scene_ID);

      //If seen not playing, loaded or playing, load it
      if (SceneRecPtr^.Load_At_Startup) and (SceneRecPtr^.Play_At_Startup = TRUE) and ((SceneRecPtr^.Scene_Status <> SCENE_PLAYING) or (AlwaysPlayAllScenesAtStartup)) then
      begin
        PlayScene(SceneRecPtr^.Scene_ID);
        MainForm.StatusBar.SimpleText := ' PLAYING SCENE ID: ' + IntToStr(SceneRecPtr^.Scene_ID);
        //Wait for scene to be loaded
        j := 0;
        Repeat
          //for j := 1 to 10 do
          //begin
            Sleep(100);
            Application.ProcessMessages;
            QuerySceneStatus(SceneRecPtr^.Scene_ID);
          //end;
          inc (j);
        //Timeout set for 100*100mS = 10 seconds
        Until (SceneRecPtr^.Scene_Status = SCENE_PLAYING) or (j >= 100);
        //Check for timeout
        if (j >= 100) then
        begin
          MainForm.StatusBar.Color := clRed;
          MainForm.StatusBar.SimpleText := ' TIMEOUT OCCURRED WHILE TRYING TO PLAY SCENE ID: ' + IntToStr(SceneRecPtr^.Scene_ID);
          Exit;
        end;
      end

      //If seen not flagged to play at startup, stop it
      else if (SceneRecPtr^.Load_At_Startup) and (SceneRecPtr^.Play_At_Startup = FALSE) and ((SceneRecPtr^.Scene_Status = SCENE_PLAYING) or (AlwaysPlayAllScenesAtStartup)) then
      begin
        StopScene(SceneRecPtr^.Scene_ID);
        MainForm.StatusBar.SimpleText := ' STOPPING SCENE ID: ' + IntToStr(SceneRecPtr^.Scene_ID);
        //Wait for scene to be loaded
        j := 0;
        Repeat
          //for j := 1 to 10 do
          //begin
            Sleep(100);
            Application.ProcessMessages;
            QuerySceneStatus(SceneRecPtr^.Scene_ID);
          //end;
          inc (j);
        //Timeout set for 100*100mS = 10 seconds
        Until (SceneRecPtr^.Scene_Status = SCENE_STOPPED) or (SceneRecPtr^.Scene_Status = SCENE_LOADED) or (j >= 100);
        //Check for timeout
        if (j >= 100) then
        begin
          MainForm.StatusBar.Color := clRed;
          MainForm.StatusBar.SimpleText := ' TIMEOUT OCCURRED WHILE TRYING TO STOP SCENE ID: ' + IntToStr(SceneRecPtr^.Scene_ID);
          Exit;
        end;
      end;
    end;

    //Delay
    for j := 1 to 20 do
    begin
      Sleep(100);
      Application.ProcessMessages;
    end;

    //Send any initial commands
    for i := 0 to Scene_Collection.Count-1 do
    begin
      SceneRecPtr := Scene_Collection.At(i);
      if (SceneRecPtr^.Run_Startup_Command = TRUE) then
      begin
        UpdateScene(SceneRecPtr^.Scene_ID, SceneRecPtr^.Startup_Command);
      end;
    end;
  end;

  //Set status on main form
  MainForm.StatusBar.Color := clBtnFace;
  MainForm.StatusBar.SimpleText := ' ';

  //Set flag
  InitialSceneLoadComplete := TRUE;
end;

////////////////////////////////////////////////////////////////////////////////
// TRAFFIC CRAWL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Command received to send more data
procedure TEngineInterface.CrawlDataResponseSocketClientRead(Sender: TObject; Socket: TCustomWinSocket);
var
  Data: String;
begin
  //Get data
  Data := Trim(Socket.ReceiveText);
  //Send more data
  if (Trim(Data) = '<EOT>') then
  begin
    //If still data left, send next record; otherwise clear crawl
    if (CurrentTrafficDataIndex < Traffic_Data_Collection.Count-1) then
    begin
      Inc(CurrentTrafficDataIndex);
      //Send next record
      MainForm.SendNextCrawlRecord;
      MainForm.TrafficDataRequestLED.Lit := TRUE;
      CrawlLEDTimer.Enabled := TRUE;
    end
    else begin
      //Clear crawl
      MainForm.ClearTrafficCrawl;
    end;
  end;
end;

procedure TEngineInterface.CrawlLEDTimerTimer(Sender: TObject);
begin
  CrawlLEDTimer.Enabled := FALSE;
  MainForm.TrafficDataRequestLED.Lit := FALSE;
end;

procedure TEngineInterface.CrawlDataResponseSocketAccept(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  MainForm.HostModeSocketLED.Lit := TRUE;
end;

procedure TEngineInterface.CrawlDataResponseSocketClientConnect(
  Sender: TObject; Socket: TCustomWinSocket);
begin
  MainForm.HostModeSocketLED.Lit := TRUE;
end;

////////////////////////////////////////////////////////////////////////////////
// CLOCK FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//V1.2 Procedure to set the state of the clock at the top of the L-Bar
procedure TEngineInterface.SetClockState(ClockIn: Boolean);
begin
  if (SocketConnected) then
  begin
    //Clock is currently in so bring it out
    if (ClockIn = CLOCK_OUT) then
    begin
      //Clear graphic with clock
      if (TopRegionToggleState = 1) then
      begin
        //Trigger current graphic #1 out
        UpdateScene(BaseScene_SceneID, 'Image_1_Out_Btn' + '�' + 'T');
      end
      else begin
        //Trigger current graphic #2 out
        UpdateScene(BaseScene_SceneID, 'Image_2_Out_Btn' + '�' + 'T');
      end;
      //Take out logo only & bring clock in
      UpdateScene(BaseScene_SceneID, 'Clock_Logo_Out_Btn�T');
      ClockDisplayEnabled := FALSE;
    end
    //Clock is currently out so bring it in
    else if (ClockIn = CLOCK_IN) then
    begin
      //Clear graphic with clock
      if (TopRegionToggleState = 1) then
      begin
        //Trigger current graphic #1 out
        UpdateScene(BaseScene_SceneID, 'Mktg_1_No_Clock_Out_Btn' + '�' + 'T');
      end
      else begin
        //Trigger current graphic #2 out
        UpdateScene(BaseScene_SceneID, 'Mktg_2_No_Clock_Out_Btn' + '�' + 'T');
      end;
      //Take out clock + logo & bring logo only in
      UpdateScene(BaseScene_SceneID, 'Clock_Logo_In_Btn�T');
      ClockDisplayEnabled := TRUE;
    end;
    //Set flag to prevent double out transition
    ClockStateChanged := TRUE;
  end;
end;

//Version 2.0 Functions for top graphic region
////////////////////////////////////////////////////////////////////////////////
// TOP REGION FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Send next update
procedure TEngineInterface.TopGraphicDwellTimerTimer(Sender: TObject);
begin
  SetNextTopGraphic;
end;

//Refresh the collection from the database
procedure TEngineInterface.RefreshTopGraphicCollection;
var
  i: SmallInt;
  TopGraphicRecPtr: ^TopGraphicRec;
begin
  with dmMain.Query5 do
  try
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM Top_Region_Image_Wheel');
    Open;
    if (RecordCount > 0) then
    begin
      First;
      repeat
        //Add item to teams collection
        GetMem (TopGraphicRecPtr, SizeOf(TopGraphicRec));
        With TopGraphicRecPtr^ do
        begin
          GraphicIndex := FieldByName('GraphicIndex').AsInteger;
          GraphicFilePath := FieldByName('GraphicFilePath').AsString;
          DwellTime := FieldByName('DwellTime').AsInteger;
          if (Top_Graphic_Collection.Count <= 50) then
          begin
            Top_Graphic_Collection.Insert(TopGraphicRecPtr);
            Top_Graphic_Collection.Pack;
          end;
        end;
        //Got to next record
        Next;
      until (EOF = TRUE); //Repeat until end of dataset
    end;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      //WriteToErrorLog
      WriteToErrorLog('Error occurred while trying refresh top region wheel from database');
    end;
  end;
end;

//Send the next graphic to the region
procedure TEngineInterface.SetNextTopGraphic;
var
  TopGraphicRecPtr: ^TopGraphicRec;
begin
  if (Top_Graphic_Collection.Count = 0) then RefreshTopGraphicCollection;

  if (Top_Graphic_Collection.Count > 0) then
  begin
    //Set values and transition in new graphic based on toggle state
    TopGraphicRecPtr := Top_Graphic_Collection.At(0);

    if (ClockDisplayEnabled) then
    begin
      //Check for start of playlist
      if (TopRegionToggleState = -1) then //Bring Image #1 in
      begin
        //Set next value for Image #1 based on toggle state
        UpdateScene(BaseScene_SceneID, 'Top_Image_File_1' + '�' + TopGraphicRecPtr^.GraphicFilePath);

        //Trigger next graphic #1 in
        UpdateScene(BaseScene_SceneID, 'Image_1_In_Btn' + '�' + 'T');
      end
      //Last displayed was image region #1; display image #2
      else if (TopRegionToggleState = 1) then //Bring Image #2 in
      begin
        //Set next value for Image #1 based on toggle state
        UpdateScene(BaseScene_SceneID, 'Top_Image_File_2' + '�' + TopGraphicRecPtr^.GraphicFilePath);

        //Trigger current graphic #1 out if not a result of a clock state change
        if not (ClockStateChanged) then
          UpdateScene(BaseScene_SceneID, 'Image_1_Out_Btn' + '�' + 'T');

        //Trigger next graphic #2 in
        UpdateScene(BaseScene_SceneID, 'Image_2_In_Btn' + '�' + 'T');
      end
      //Last displayed was image region #2; display image #1
      else begin //Bring image #2 in
        //Set next value for Image #2 based on toggle state
        UpdateScene(BaseScene_SceneID, 'Top_Image_File_1' + '�' + TopGraphicRecPtr^.GraphicFilePath);

        //Trigger current graphic #2 out if not a result of a clock state change
        if not (ClockStateChanged) then
          UpdateScene(BaseScene_SceneID, 'Image_2_Out_Btn' + '�' + 'T');

        //Trigger next graphic #1 in
        UpdateScene(BaseScene_SceneID, 'Image_1_In_Btn' + '�' + 'T');
      end;
    end
    //Clock is out
    else begin
      //Check for start of playlist
      if (TopRegionToggleState = -1) then //Bring Image #1 in
      begin
        //Set next value for Image #1 based on toggle state
        UpdateScene(BaseScene_SceneID, 'Top_Image_File_1_No_Clock' + '�' + TopGraphicRecPtr^.GraphicFilePath);

        //Trigger next graphic #1 in
        UpdateScene(BaseScene_SceneID, 'Mktg_1_No_Clock_In_Btn' + '�' + 'T');
      end
      //Last displayed was image region #1; display image #2
      else if (TopRegionToggleState = 1) then //Bring Image #2 in
      begin
        //Set next value for Image #1 based on toggle state
        UpdateScene(BaseScene_SceneID, 'Top_Image_File_2_No_Clock' + '�' + TopGraphicRecPtr^.GraphicFilePath);

        //Trigger current graphic #1 out if not a result of a clock state change
        if not (ClockStateChanged) then
          UpdateScene(BaseScene_SceneID, 'Mktg_1_No_Clock_Out_Btn' + '�' + 'T');

        //Trigger next graphic #2 in
        UpdateScene(BaseScene_SceneID, 'Mktg_2_No_Clock_In_Btn' + '�' + 'T');
      end
      //Last displayed was image region #2; display image #1
      else begin //Bring image #2 in
        //Set next value for Image #2 based on toggle state
        UpdateScene(BaseScene_SceneID, 'Top_Image_File_1_No_Clock' + '�' + TopGraphicRecPtr^.GraphicFilePath);

        //Trigger current graphic #2 out if not a result of a clock state change
        if not (ClockStateChanged) then
        UpdateScene(BaseScene_SceneID, 'Mktg_2_No_Clock_Out_Btn' + '�' + 'T');

        //Trigger next graphic #1 in
        UpdateScene(BaseScene_SceneID, 'Mktg_1_No_Clock_In_Btn' + '�' + 'T');
      end;
    end;

    //Set timer dwell value and trigger
    TopGraphicDwellTimer.Enabled := FALSE;
    TopGraphicDwellTimer.Interval := TopGraphicRecPtr^.DwellTime*1000;
    TopGraphicDwellTimer.Enabled := TRUE;

    //Delete the top entry in the collection
    Top_Graphic_Collection.AtDelete(0);
    Top_Graphic_Collection.Pack;
  end;

  //Refresh if needed
  if (Top_Graphic_Collection.Count = 0) then
    RefreshTopGraphicCollection;

  //Toggle the value for next iteration
  if (TopRegionToggleState = -1) or (TopRegionToggleState = 2) then TopRegionToggleState := 1
  else TopRegionToggleState := 2;

  //Clear flag
  ClockStateChanged := FALSE;
end;

////////////////////////////////////////////////////////////////////////////////
// LOGGING FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TEngineInterface.WriteToErrorLog (ErrorString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   ErrorLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
   DirectoryStr: String;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists('c:\Comcast_Error_LogFiles') = FALSE) then
   begin
     CreateDir('c:\Comcast_Error_LogFiles');
     DirectoryStr := 'c:\Comcast_Error_LogFiles\';
   end;
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   ErrorLogStr := TimeStr + '    ' + DateStr + '    ' + ErrorString;
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := DirectoryStr + 'ComcastErrorLog' + DayStr + '.txt';
   AssignFile (ErrorLogFile, FileName);
   //Write error information to one line in file
   //If file doesn't exist create a new one. Otherwise, check to see if it's one
   //month old. If so, delete & create a new one. Otherwise, append error to file
   if (Not (FileExists(FileName))) then
   begin
      ReWrite (ErrorLogFile);
      try
         Writeln (ErrorLogFile, ErrorLogStr);      finally         CloseFile (ErrorLogFile);      end;   end   else begin      {Check file to see if it's a month old. If so, erase and create a new one}      Reset (ErrorLogFile);      try         Readln (ErrorLogFile, TestStr);      finally         CloseFile (ErrorLogFile);      end;      If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then begin         {Date in 1st entry in error log does not match today's date, so erase}         Erase (ErrorLogFile);         {Create a new logfile}         ReWrite (ErrorLogFile);         try
            Writeln (ErrorLogFile, ErrorLogStr);         finally            CloseFile (ErrorLogFile);         end;      end      else begin         {File exists and not a month old, so append to existing log file}         Append (ErrorLogFile);         try
            Writeln (ErrorLogFile, ErrorLogStr);         finally            CloseFile (ErrorLogFile);         end;      end;   end;end;

// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TEngineInterface.WriteToAsRunLog (AsRunString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   AsRunLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
   DirectoryStr: String;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists(AsRunLogFileDirectoryPath) = FALSE) then
   begin
     if (DirectoryExists('c:\Comcast_AsRun_LogFiles') = FALSE) then
       CreateDir('c:\Comcast_AsRun_LogFiles');
     DirectoryStr := 'c:\Comcast_AsRun_LogFiles\';
   end
   else DirectoryStr := AsRunLogFileDirectoryPath;
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   AsRunLogStr := TimeStr + '    ' + DateStr + '    ' + AsRunString;
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := DirectoryStr + 'ComcastAsRunLog' + DayStr + '.txt';
   AssignFile (AsRunLogFile, FileName);
   {Write error information to one line in file}
   {If file doesn't exist create a new one. Otherwise, check to see if it's one }
   {month old. If so, delete & create a new one. Otherwise, append error to file}
   if (Not (FileExists(FileName))) then begin
      ReWrite (AsRunLogFile);
      try
         Writeln (AsRunLogFile, AsRunLogStr);      finally         CloseFile (AsRunLogFile);      end;   end   else begin      {Check file to see if it's a month old. If so, erase and create a new one}      Reset (AsRunLogFile);      try         Readln (AsRunLogFile, TestStr);      finally         CloseFile (AsRunLogFile);      end;      If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then      begin         {Date in 1st entry in error log does not match today's date, so erase}         Erase (AsRunLogFile);         {Create a new logfile}         ReWrite (AsRunLogFile);         try
            Writeln (AsRunLogFile, AsRunLogStr);         finally            CloseFile (AsRunLogFile);         end;      end      else begin         {File exists and not a month old, so append to existing log file}         Append (AsRunLogFile);         try
            Writeln (AsRunLogFile, AsRunLogStr);         finally            CloseFile (AsRunLogFile);         end;      end;   end;end;

// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TEngineInterface.WriteToCommandLog (CommandString: String);
var
   FileName: String;
   TimeStr: String;
   CommandLogStr: String;
   Present: TDateTime;
   Hour, Min, Sec, MSec: Word;
   DirectoryStr: String;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists(CommandLogFileDirectoryPath) = FALSE) then
   begin
     if (DirectoryExists('c:\Comcast_Command_LogFiles') = FALSE) then
       CreateDir('c:\Comcast_Command_LogFiles');
     DirectoryStr := 'c:\Comcast_Command_LogFiles\';
   end
   else DirectoryStr := CommandLogFileDirectoryPath;
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   //Get time in mSec
   TimeStr := IntToStr(mSec + (Sec*1000) + (Min*60*1000) + (Hour*60*60*1000));
   CommandLogStr := TimeStr + ':' + CommandString;
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := DirectoryStr + 'ComcastCommandLog' + '.txt';
   AssignFile (CommandLogFile, FileName);
   {Write error information to one line in file}
   {If file doesn't exist create a new one. Otherwise, check to see if it's one }
   {month old. If so, delete & create a new one. Otherwise, append error to file}
   if (Not (FileExists(FileName))) then
   begin
      ReWrite (CommandLogFile);
      try
         Writeln (CommandLogFile, CommandLogStr);      finally         CloseFile (CommandLogFile);      end;   end   else begin     {File exists and not a month old, so append to existing log file}     Append (CommandLogFile);     try
        Writeln (CommandLogFile, CommandLogStr);     finally        CloseFile (CommandLogFile);     end;   end;end;

end.


