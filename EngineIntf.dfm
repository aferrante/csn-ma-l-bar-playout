object EngineInterface: TEngineInterface
  Left = 428
  Top = 206
  Width = 328
  Height = 378
  Caption = 'Engine Interface'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object AdTerminal1: TAdTerminal
    Left = 66
    Top = 8
    Width = 145
    Height = 73
    Active = False
    CaptureFile = 'APROTERM.CAP'
    Scrollback = False
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clSilver
    Font.Height = -12
    Font.Name = 'Terminal'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 0
  end
  object PacketEnableTimer: TTimer
    Enabled = False
    Interval = 10
    OnTimer = PacketEnableTimerTimer
    Left = 24
    Top = 56
  end
  object TickerCommandDelayTimer: TTimer
    Enabled = False
    OnTimer = TickerCommandDelayTimerTimer
    Left = 24
    Top = 94
  end
  object PacketIndicatorTimer: TTimer
    Enabled = False
    Interval = 250
    OnTimer = PacketIndicatorTimerTimer
    Left = 96
    Top = 177
  end
  object TickerPacketTimeoutTimer: TTimer
    Enabled = False
    Interval = 10000
    OnTimer = TickerPacketTimeoutTimerTimer
    Left = 24
    Top = 132
  end
  object JumpToNextTickerRecordTimer: TTimer
    Enabled = False
    Interval = 10
    OnTimer = JumpToNextTickerRecordTimerTimer
    Left = 99
    Top = 94
  end
  object DisplayClockTimer: TTimer
    Enabled = False
    Interval = 10000
    Left = 56
    Top = 176
  end
  object FieldUpdateDelayTimer: TTimer
    Enabled = False
    Left = 64
    Top = 94
  end
  object ACKReceivedLEDTimer: TTimer
    Enabled = False
    Interval = 500
    OnTimer = ACKReceivedLEDTimerTimer
    Left = 59
    Top = 134
  end
  object EnginePort: TClientSocket
    Active = False
    ClientType = ctNonBlocking
    Port = 0
    OnConnect = EnginePortConnect
    OnDisconnect = EnginePortDisconnect
    OnRead = EnginePortRead
    OnError = EnginePortError
    Left = 24
    Top = 16
  end
  object TickerCommandDelayTimer2: TTimer
    Enabled = False
    OnTimer = TickerCommandDelayTimer2Timer
    Left = 160
    Top = 94
  end
  object FieldUpdateDelayTimer2: TTimer
    Enabled = False
    Left = 200
    Top = 94
  end
  object JumpToNextTickerRecordTimer2: TTimer
    Enabled = False
    Interval = 10
    OnTimer = JumpToNextTickerRecordTimer2Timer
    Left = 235
    Top = 94
  end
  object TickerPacketTimeoutTimer2: TTimer
    Enabled = False
    Interval = 10000
    OnTimer = TickerPacketTimeoutTimer2Timer
    Left = 160
    Top = 132
  end
  object CrawlDataResponseSocket: TServerSocket
    Active = False
    Port = 0
    ServerType = stNonBlocking
    OnAccept = CrawlDataResponseSocketAccept
    OnClientConnect = CrawlDataResponseSocketClientConnect
    OnClientRead = CrawlDataResponseSocketClientRead
    Left = 24
    Top = 224
  end
  object CrawlLEDTimer: TTimer
    Enabled = False
    Interval = 500
    OnTimer = CrawlLEDTimerTimer
    Left = 56
    Top = 225
  end
  object TopGraphicDwellTimer: TTimer
    Enabled = False
    OnTimer = TopGraphicDwellTimerTimer
    Left = 24
    Top = 288
  end
end
