unit AuthoringInterfaceModule;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OoMisc, ADTrmEmu, AdPacket, AdPort, AdWnPort, ScktComp, Globals;

type
  TAuthoringInterface = class(TForm)
    AuthoringControlPort: TServerSocket;
    procedure AuthoringControlPortAccept(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure AuthoringControlPortClientConnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure AuthoringControlPortClientDisconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure AuthoringControlPortClientError(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure AuthoringControlPortClientRead(Sender: TObject;
      Socket: TCustomWinSocket);
    function ParseReceivedData(CommandIn: String): CommandRec;
    procedure SendPlaylistIndex(PlaylistID, PlaylistIndex: SmallInt);
    procedure SendAutoSequenceStatus(PlaylistRunning: Boolean);
  private

  public

  end;

var
  AuthoringConnected: Boolean;

implementation

{$R *.dfm}

uses Main, EngineIntf;

//LED handlers
//Global Control connection
procedure TAuthoringInterface.AuthoringControlPortAccept(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  MainForm.AuthoringConnectLED.Lit := TRUE;
end;

procedure TAuthoringInterface.AuthoringControlPortClientConnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  MainForm.AuthoringConnectLED.Lit := TRUE;
end;

procedure TAuthoringInterface.AuthoringControlPortClientDisconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  MainForm.AuthoringConnectLED.Lit := FALSE;
end;

procedure TAuthoringInterface.AuthoringControlPortClientError(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  MainForm.AuthoringConnectLED.Lit := FALSE;
  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

//General function to parse data received from Playout Controller
function TAuthoringInterface.ParseReceivedData(CommandIn: String): CommandRec;
var
  Tokens: Array[1..5] of String;
  i, Cursor, TokenIndex: SmallInt;
  ParamVal: SmallInt;
  OutRec: CommandRec;
begin
  //Parse the string for the command and any parameters
  //First, check for valid command framing; should contain at least one SOH, one ETX and one EOT;
  //If not, send back error code 1 = Invalid command syntax received
  //Need to at least have framing characters; also check for SOH at start and EOT at end
  if (Length(CommandIn) > 3) AND (CommandIn[1] = SOH) AND (CommandIn[Length(CommandIn)] = EOT) then
  begin
    //Strip off SOT and EOT                      T
    CommandIn := Copy (CommandIn, 2, Length(CommandIn)-2);
    //Parse for tokens
    for i := 1 to 5 do Tokens[i] := '';
    Cursor := 1;
    TokenIndex :=1;
    repeat
      if (CommandIn[Cursor] = ETX) then
      begin
        Inc(Cursor);
        Inc(TokenIndex);
      end;
      Tokens[TokenIndex] := Tokens[TokenIndex] + CommandIn[Cursor];
      Inc(Cursor);
    until (Cursor > Length(CommandIn)) OR (TokenIndex = 5);
  end;
  //Set output record
  OutRec.Command := Tokens[1];
  OutRec.Param[1] := Tokens[2];
  OutRec.Param[2] := Tokens[3];
  OutRec.Param[3] := Tokens[4];
  OutRec.Param[4] := Tokens[5];
  ParseReceivedData := OutRec;
end;

//Handler for command received from Authoring control port
procedure TAuthoringInterface.AuthoringControlPortClientRead(Sender: TObject; Socket: TCustomWinSocket);
var
  Data: String;
  EOTLocation: SmallInt;
  CommandStr: String;
  CommandData: CommandRec;
  AllCommandsProcessed: Boolean;
  ErrorCode: SmallInt;
  Tokens: Array[1..3] of String;
  i, Cursor, TokenIndex: SmallInt;
  ParamVal, ParamVal2: SmallInt;
  ParamValFloat: Double;
  TickerType: SmallInt;
  UseLocalPlaylist: Boolean;
  TempStr: String;
  StatusString: String;
  SaveIndex, SaveIndex2: SmallInt;
begin
  //Get the data
  Data := Socket.ReceiveText;

  //Only act if Authoring tool interface enabled
  //Check command
  ErrorCode := 0;

  //Parse the string for the command and any parameters
  //First, check for valid command framing; should contain at least one SOH, one ETX and one EOT;
  //If not, send back error code 1 = Invalid command syntax received

  //Only act if automation interface enabled
  if (AuthoringControlInterfaceEnabled) then
  begin
    //Check command
    ErrorCode := 0;
    //Init
    AllCommandsProcessed := FALSE;
    //Process commands
    //Need to at least have framing characters; also check for SOH at start and EOT at end
    if (Length(Data) > 3) AND (Data[1] = SOH) AND (Data[Length(Data)] = EOT) then
    repeat
      //Copy command from data string & parse
      //Find end of command
      EOTLocation := Pos(EOT, Data);
      //Copy from start to end of command & parse
      CommandStr := Copy(Data, 1, EOTLocation);
      CommandData := ParseReceivedData(CommandStr);
      //Delete command from data string for next iteration
      if (EOTLocation < Length(Data)) then
        Data := Copy(Data, EOTLocation+1, Length(Data))
      else
        AllCommandsProcessed := TRUE;

      /////////////////////////////////////////////////////////////////////////
      //Call functions based on command
      //////////////////////////////////////////////////////////////////////////
      //Playout slave mode enable/disable (SM command)
      if (CommandData.Command = 'SM') then
      begin
        //Call function to trigger ticker; pass in -1 if invalid integer
        ParamVal := StrToIntDef(CommandData.Param[1], -1);
        if (ParamVal >= 0) then
        begin
          //Set state of Authoring Remote Control Enable flag
          if (ParamVal = 1) then
          begin
            //Set to remote/manual mode
            //Disable schedule monitoring
            ScheduleMonitoringEnabled := FALSE;
            MainForm.AuthoringControlLED.Lit := TRUE;
            MainForm.AuthoringControlLabel.Color := clLime;
            MainForm.AuthoringControlLabel.Caption := 'ONLINE MODE';
            MainForm.Statusbar.Color := clYellow;
            MainForm.Statusbar.Font.Style := [fsBold];
            MainForm.Statusbar.SimpleText := ' REMOTE CONTROL FROM AUTHORING TOOL ENABLED';
            AuthoringRemoteControlModeEnabled := TRUE;
            //Disable GPIs
            GPIsOKToFire := FALSE;
            //Clear ticker abort flag
            TickerAbortFlag := FALSE;
          end
          else begin
            //Exit remote manual mode
            MainForm.AuthoringControlLED.Lit := FALSE;
            MainForm.AuthoringControlLabel.Color := clSilver;
            MainForm.AuthoringControlLabel.Caption := 'OFFLINE MODE';
            MainForm.Statusbar.Color := clBtnFace;
            MainForm.Statusbar.SimpleText := '';
            AuthoringRemoteControlModeEnabled := FALSE;
            //Re-enable GPIs
            GPIsOKToFire := TRUE;

            //Set state of schedule monitoring
            if (MainForm.ScheduleMonitoringEnabledCheckBox.Checked = TRUE) then
            begin
              ScheduleMonitoringEnabled := TRUE;
              MainForm.BitBtn4.Visible := FALSE;
              MainForm.Label6.Visible := FALSE;
            end
            //In manual mode
            else begin
              //Clear flag; disble controls; show label
              ScheduleMonitoringEnabled := FALSE;
              MainForm.BitBtn4.Visible := TRUE;
              MainForm.Label6.Visible := TRUE;
            end;
          end;
        end
        //Set error code for incorrect remote control mode specified
        else ErrorCode := 2;
      end

      //Auto-play mode enable/disable (AP command)
      else if (CommandData.Command = 'AP') then
      begin
        //Call function to trigger ticker; pass in -1 if invalid integer
        ParamVal := StrToIntDef(CommandData.Param[1], -1);
        if (ParamVal >= 0) then
        begin
          //Set state of Auto-Sequence Enable flag
          if (ParamVal = 1) then
          begin
            AutoSequenceEnabled := TRUE;
            //Update status
            MainForm.Statusbar.Color := clYellow;
            MainForm.Statusbar.Font.Style := [fsBold];
            MainForm.Statusbar.SimpleText := ' AUTO-SEQUENCE MODE ENABLED, SCHEDULE MONITORING DISABLED';
          end
          else begin
            AutoSequenceEnabled := FALSE;
            //Update status
            MainForm.Statusbar.Color := clYellow;
            MainForm.Statusbar.Font.Style := [fsBold];
            MainForm.Statusbar.SimpleText := ' AUTO-SEQUENCE MODE DISABLED';
          end;
        end
        //Set error code for incorrect auto-sequence mode specified
        else ErrorCode := 3;
      end

      //Load playlist (@ specified ID) (PL command)
      else if (CommandData.Command = 'PL') then
      begin
        if not (RunningTicker) then
        begin
          ParamValFloat := StrToFloatDef(CommandData.Param[1], -1);
          if (ParamValFloat >= 0) then
          begin
            //Load specified playlist
            TempStr := MainForm.LoadPlaylistCollection (TICKER, CLEAR_AND_LOAD_PLAYLIST_MODE, ParamValFloat);
            //Set current playlist ID value
            CurrentTickerPlaylistID := ParamValFloat;
            //Update status
            MainForm.Statusbar.Color := clYellow;
            MainForm.Statusbar.Font.Style := [fsBold];
            MainForm.Statusbar.SimpleText := ' PLAYLIST LOADED REMOTELY: ' + TempStr;
            //Update main form status labels
            MainForm.TickerPlaylistName.Caption := TempStr;
            MainForm.TickerStartTimeLabel.Caption := 'N/A';
            MainForm.TickerEndTimeLabel.Caption := 'N/A';
            //Reset indices to top
            if (Ticker_Collection.Count > 0) then
            begin
              CurrentTickerEntryIndex1 := 0;
              MainForm.PlaylistGrid.CurrentDataRow := CurrentTickerEntryIndex1+1;
              MainForm.PlaylistGrid.SetTopLeft(1, MainForm.PlaylistGrid.CurrentDataRow);
              SendPlaylistIndex(PLAYLIST_1, 0);
            end;
            if (Ticker_Collection2.Count > 0) then
            begin
              CurrentTickerEntryIndex2 := 0;
              MainForm.PlaylistGrid2.CurrentDataRow := CurrentTickerEntryIndex2+1;
              MainForm.PlaylistGrid2.SetTopLeft(1, MainForm.PlaylistGrid2.CurrentDataRow);
              SendPlaylistIndex(PLAYLIST_2, 0);
            end;
          end
          //Set error code for invalid playlist ID
          else ErrorCode := 4;
        end;
      end

      //Unload current playlist (PU command)
      else if (CommandData.Command = 'PU') then
      begin
        if not (RunningTicker) then
        begin
          //Clear ticker collection and grid
          Ticker_Collection.Clear;
          Ticker_Collection.Pack;
          Ticker_Collection2.Clear;
          Ticker_Collection2.Pack;
          MainForm.RefreshPlaylistGrid(FIELD_1, MainForm.PlaylistGrid);
          MainForm.RefreshPlaylistGrid(FIELD_2, MainForm.PlaylistGrid2);
          //Update status & labels
          MainForm.Statusbar.Color := clYellow;
          MainForm.Statusbar.Font.Style := [fsBold];
          MainForm.Statusbar.SimpleText := ' PLAYLIST UNLOADED REMOTELY';
          //Update main form status labels
          MainForm.TickerPlaylistName.Caption := 'N/A';
          MainForm.TickerStartTimeLabel.Caption := 'N/A';
          MainForm.TickerEndTimeLabel.Caption := 'N/A';
        end;
      end

      //Refresh current playlist (RP command)
      else if (CommandData.Command = 'RP') then
      begin
        if not (RunningTicker) then
        begin
          ParamValFloat := StrToFloatDef(CommandData.Param[1], -1);
          if (ParamValFloat >= 0) then
          begin
            //Save the index
            SaveIndex := CurrentTickerEntryIndex1;
            SaveIndex2 := CurrentTickerEntryIndex2;
            //Load specified playlist
            TempStr := MainForm.LoadPlaylistCollection (TICKER, CLEAR_AND_LOAD_PLAYLIST_MODE, ParamValFloat);
            //Restore ticker indexes
            if (SaveIndex >= Ticker_Collection.Count-1) then
              CurrentTickerEntryIndex1 := SaveIndex
            else
              CurrentTickerEntryIndex1 := Ticker_Collection.Count-1;
            if (SaveIndex2 >= Ticker_Collection2.Count-1) then
              CurrentTickerEntryIndex2 := SaveIndex2
            else
              CurrentTickerEntryIndex2 := Ticker_Collection2.Count-1;
            MainForm.PlaylistGrid.CurrentDataRow := CurrentTickerEntryIndex1+1;
            MainForm.PlaylistGrid.SetTopLeft(1, MainForm.PlaylistGrid.CurrentDataRow);
            MainForm.PlaylistGrid2.CurrentDataRow := CurrentTickerEntryIndex2+1;
            MainForm.PlaylistGrid2.SetTopLeft(1, MainForm.PlaylistGrid2.CurrentDataRow);
            //Update status
            MainForm.Statusbar.Color := clYellow;
            MainForm.Statusbar.Font.Style := [fsBold];
            MainForm.Statusbar.SimpleText := ' PLAYLIST LOADED REMOTELY: ' + TempStr;
            //Update main form status labels
            MainForm.TickerPlaylistName.Caption := TempStr;
            MainForm.TickerStartTimeLabel.Caption := 'N/A';
            MainForm.TickerEndTimeLabel.Caption := 'N/A';
          end
          //Set error code for invalid playlist ID
          else ErrorCode := 4;
        end;
      end

      //Play next page for specified playlist (PN command)
      else if (CommandData.Command = 'PN') then
      begin
        if (AuthoringRemoteControlModeEnabled) and not (RunningTicker) then
        begin
          ParamVal := StrToIntDef(CommandData.Param[1], -1);
          //Play next record
          EngineInterface.SendTickerRecord(ParamVal, NEXT_RECORD, 0);
          //Update status
          MainForm.Statusbar.Color := clYellow;
          MainForm.Statusbar.Font.Style := [fsBold];
          MainForm.Statusbar.SimpleText := ' PLAY NEXT RECORD COMMAND RECEIVED: PLAYLIST = ' + IntToStr(ParamVal);

          //Echo back current ticker entry index to Authoring tool
          Case ParamVal of
            PLAYLIST_1: StatusString := SOH + 'PI' + ETX + IntToStr(ParamVal) + ETX + IntToStr(CurrentTickerEntryIndex1) + EOT;
            PLAYLIST_2: StatusString := SOH + 'PI' + ETX + IntToStr(ParamVal) + ETX + IntToStr(CurrentTickerEntryIndex2) + EOT;
          end;
          AuthoringControlPort.Socket.Connections[0].SendText(StatusString);
        end;
      end

      //Play specified page (@ specified index) for specified playlist (@ playlist)(PS command)
      else if (CommandData.Command = 'PS') and not (RunningTicker) then
      begin
        ParamVal := StrToIntDef(CommandData.Param[1], -1);
        ParamVal2 := StrToIntDef(CommandData.Param[2], -1);
        if (ParamVal >= 0) then
        begin
          if (AuthoringRemoteControlModeEnabled) then
          begin
            //Play specified record
            Case ParamVal of
              PLAYLIST_1: begin
                CurrentTickerEntryIndex1 := ParamVal2;
                MainForm.PlaylistGrid.CurrentDataRow := ParamVal2+1;
              end;
              PLAYLIST_2: begin
                CurrentTickerEntryIndex2 := ParamVal2;
                MainForm.PlaylistGrid2.CurrentDataRow := ParamVal2+1;
              end;
            end;
            EngineInterface.SendTickerRecord(ParamVal, SPECIFIED_RECORD, ParamVal2);
            //Update status
            MainForm.Statusbar.Color := clYellow;
            MainForm.Statusbar.Font.Style := [fsBold];
            MainForm.Statusbar.SimpleText := ' PLAY SPECIFIELD RECORD COMMAND RECEIVED: PLAYLIST = ' + IntToStr(ParamVal) +
              ', INDEX = ' + IntToStr(ParamVal2);

            //Echo back current ticker entry index to Authoring tool
            StatusString := SOH + 'PI' + ETX + IntToStr(ParamVal) + ETX + IntToStr(ParamVal2) + EOT;
            AuthoringControlPort.Socket.Connections[0].SendText(StatusString);
          end;
        end
        //Set error code for invalid ticker entry index specified
        else ErrorCode := 5;
      end

      //Trigger playlist/ticker (TS command)
      else if (CommandData.Command = 'TS') then
      begin
        if (AuthoringRemoteControlModeEnabled) and (AutoSequenceEnabled) then
        begin
          MainForm.TriggerCurrentTicker;
          //Update status
          MainForm.Statusbar.Color := clYellow;
          MainForm.Statusbar.Font.Style := [fsBold];
          MainForm.Statusbar.SimpleText := ' TRIGGER TICKER COMMAND RECEIVED';
        end;
      end

      //Pause playlist/ticker (TP command)
      else if (CommandData.Command = 'TP') then
      begin
        if (RunningTicker) and (AuthoringRemoteControlModeEnabled) and (AutoSequenceEnabled)then
        begin
          //Abort
          MainForm.AbortCurrentEvent(IS_NOT_STARTUP);
          MainForm.Statusbar.Color := clYellow;
          MainForm.Statusbar.Font.Style := [fsBold];
          MainForm.Statusbar.SimpleText := ' PAUSE/ABORT TICKER COMMAND RECEIVED';
        end;
      end

      //Reset playlist to top (TR command)
      else if (CommandData.Command = 'TR') then
      begin
        if (not(RunningTicker)) and (AuthoringRemoteControlModeEnabled) and (AutoSequenceEnabled) then
        begin
          MainForm.ResetTickerToTop;
          MainForm.Statusbar.Color := clYellow;
          MainForm.Statusbar.Font.Style := [fsBold];
          MainForm.Statusbar.SimpleText := ' RESET TICKER TO TOP COMMAND RECEIVED';
        end;
      end

      //Clear all graphics (CG command)
      else if (CommandData.Command = 'CG') then
      begin
        if (not(RunningTicker)) and (AuthoringRemoteControlModeEnabled) then
        begin
          MainForm.ClearAllTemplates;
          MainForm.Statusbar.Color := clYellow;
          MainForm.Statusbar.Font.Style := [fsBold];
          MainForm.Statusbar.SimpleText := ' CLEAR ALL TEMPLATES COMMAND RECEIVED';
        end;
      end

      //Clear data panels (CD command)
      else if (CommandData.Command = 'CD') then
      begin
        if (AuthoringRemoteControlModeEnabled) then
        begin
          //Clear the data panels
          EngineInterface.ClearFieldPanels(STOP_SCENES);
          MainForm.Statusbar.Color := clYellow;
          MainForm.Statusbar.Font.Style := [fsBold];
          MainForm.Statusbar.SimpleText := ' CLEAR DATA PANELS COMMAND RECEIVED';
        end;
      end

      //Added for V1.2 Set Clock Mode command
      else if (CommandData.Command = 'SC') then
      begin
        ParamVal := StrToIntDef(CommandData.Param[1], -1);
        if (ParamVal >= 0) then
        begin
          //if (AuthoringRemoteControlModeEnabled) then
          begin
            //Play specified record
            Case ParamVal of
              CLOCK_DISABLE: begin
                ClockDisplayEnabled := FALSE;
                MainForm.ClockEnableLED.Lit := FALSE;
                MainForm.ClockDisableLED.Lit := TRUE;

                //Command the clock
                EngineInterface.SetClockState(CLOCK_OUT);

                //Added for Version 2.0.1
                EngineInterface.SetNextTopGraphic;
              end;

              CLOCK_ENABLE: begin
                ClockDisplayEnabled := TRUE;
                MainForm.ClockEnableLED.Lit := TRUE;
                MainForm.ClockDisableLED.Lit := FALSE;

                //Command the clock
                EngineInterface.SetClockState(CLOCK_IN);

                //Added for Version 2.0.1
                EngineInterface.SetNextTopGraphic;
              end;
            end;
          end;
        end;
      end

      //Heartbeat (from Playout to Authoring) (HB command)
      else if (CommandData.Command = 'HB') then
      begin

      end
      //Unidentified error - invalid command received, command could not be processed
      else ErrorCode := -1;

      //Send response; 0 = Success; > 0 => Error Code
      AuthoringControlPort.Socket.Connections[0].SendText(#1 + IntToStr(ErrorCode) + #4);
    until (AllCommandsProcessed)
    //Improperly framed command so set error code
    else ErrorCode := 1;

    //Send status code back to Authoring tool
    StatusString := SOH + 'CS' + ETX + IntToStr(ErrorCode) + EOT;
    AuthoringControlPort.Socket.Connections[0].SendText(StatusString);
  end;
end;

//Pocedure to send the current bug name to the Master Control application
procedure TAuthoringInterface.SendPlaylistIndex(PlaylistID, PlaylistIndex: SmallInt);
begin
  if (AuthoringControlPort.Active) AND (AuthoringControlPort.Socket.ActiveConnections > 0) then
    AuthoringControlPort.Socket.Connections[0].SendText(SOH + 'PI' + ETX + IntToStr(PlaylistID) + ETX +
      IntToStr(PlaylistIndex) + EOT);
end;

//Send the auto-sequence playlist status
procedure TAuthoringInterface.SendAutoSequenceStatus(PlaylistRunning: Boolean);
var
  StateStr: String;
begin
  if (PlaylistRunning) then StateStr := '1'
  else StateStr := '0';
  if (AuthoringControlPort.Active) and (AuthoringControlPort.Socket.ActiveConnections > 0) then
  begin
    //Modified to always send status in case Authoring was shut down and Playout was cycling
    //if (AuthoringRemoteControlModeEnabled) and (AutoSequenceEnabled) then
      AuthoringControlPort.Socket.Connections[0].SendText(SOH + 'PS' + ETX + StateStr + EOT)
    //else
    //  AuthoringControlPort.Socket.Connections[0].SendText(SOH + 'PS' + ETX + '-1' + EOT);
  end;
end;

end.
