////////////////////////////////////////////////////////////////////////////////
// Comcast L-Bar Playout Controller - Original Release
// V1.0.0  08/25/11  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Enhanced command logging functions
// V1.0.1  10/11/11  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Added INI option to turn off template (text/image) updating
// V1.0.2  10/18/11  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to play scenes only when needed (to address performance issues)
// V1.1.0  10/20/11  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to play scenes before sending data when first starting ticker.
// V1.1.1  11/17/11  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to add INI setting for enabling/disabling crawl feedback port
// V1.1.2  04/13/12  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to add support for clock in/out controls.
// V1.2.0  08/26/12  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to suport new headshot/logo composition scheme, automated scores &
// stats and other misc. modifications.
// V2.0.0  03/28/14  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to tighten up timing on transitions for top area market bug.
// V2.0.1  04/15/14  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
//Record types

//Playlist Types
// 0 L-Bar
// 1 Breaking News

unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Spin, Buttons, ExtCtrls, Menus, Mask, ComCtrls, StBase,
  TSImageList, Grids_ts, TSGrid, TSDBGrid, DBCtrls, StColl, MPlayer, OleCtrls,
  OleServer, Grids, PBJustOne, ClipBrd, TSMask, INIFiles, DBGrids, OoMisc, AdStatLt,
  Globals, AdPacket, AdPort,
  AuthoringInterfaceModule;

{H+} //ANSI strings
//Record type defs
type
  //Main progam object def
  TMainForm = class(TForm)
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Exit1: TMenuItem;
    Prefs1: TMenuItem;
    SetPrefs1: TMenuItem;
    N1: TMenuItem;
    Help1: TMenuItem;
    About1: TMenuItem;
    tsImageList: TtsImageList;
    tsImageList1: TtsImageList;
    tsImageList2: TtsImageList;
    Login1: TMenuItem;
    LogintoSystem1: TMenuItem;
    N2: TMenuItem;
    LogoutfromSystem1: TMenuItem;
    PBJustOne1: TPBJustOne;
    Database1: TMenuItem;
    RepopulateTeamsCollection1: TMenuItem;
    N3: TMenuItem;
    PurgeScheduledEvents1DayOld1: TMenuItem;
    tsMaskDefs1: TtsMaskDefs;
    Panel9: TPanel;
    Label69: TLabel;
    NumEntries: TLabel;
    PlaylistGrid: TtsGrid;
    Panel8: TPanel;
    Image2: TImage;
    Panel5: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    LastCommandLabel: TLabel;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    StaticText38: TStaticText;
    Label1: TLabel;
    TickerStartTimeLabel: TLabel;
    TickerEndTimeLabel: TLabel;
    StaticText6: TStaticText;
    StaticText8: TStaticText;
    Panel3: TPanel;
    Label15: TLabel;
    Label5: TLabel;
    ApdStatusLight1: TApdStatusLight;
    ApdStatusLight2: TApdStatusLight;
    StaticText13: TStaticText;
    StaticText15: TStaticText;
    StaticText16: TStaticText;
    StaticText27: TStaticText;
    StaticText12: TStaticText;
    Panel2: TPanel;
    StaticText17: TStaticText;
    AbortPlaylistButton: TBitBtn;
    TriggerTickerButton: TBitBtn;
    Panel4: TPanel;
    TickerStatusLabel: TLabel;
    ResetTickerButton: TBitBtn;
    BitBtn4: TBitBtn;
    ScheduleMonitoringEnabledCheckBox: TCheckBox;
    Panel11: TPanel;
    Label23: TLabel;
    TimeOfDayTimer: TTimer;
    ErrorFlashTimer: TTimer;
    GPIResetTimer: TTimer;
    GPIInitTimer: TTimer;
    N4: TMenuItem;
    DisableErrorChecking1: TMenuItem;
    EnableErrorChecking1: TMenuItem;
    ResetError1: TMenuItem;
    N5: TMenuItem;
    SetGraphicsEngineConnectionPreferences1: TMenuItem;
    N6: TMenuItem;
    ReconnecttoGraphicsEngine1: TMenuItem;
    PlaylistRefreshTimer: TTimer;
    TickerPlaylistName: TLabel;
    GPIComPort: TApdComPort;
    ComPortDataPacket: TApdDataPacket;
    ApdStatusLight3: TApdStatusLight;
    StaticText5: TStaticText;
    StaticText7: TStaticText;
    ApdStatusLight4: TApdStatusLight;
    StaticText1: TStaticText;
    DwellValue: TLabel;
    ApdStatusLight5: TApdStatusLight;
    StaticText4: TStaticText;
    OneTimeLoopModeLabel: TPanel;
    Label7: TLabel;
    N7: TMenuItem;
    EnableCommandLogging1: TMenuItem;
    DisableCommandLogging1: TMenuItem;
    TickerModePanel: TPanel;
    TickerMode: TLabel;
    Panel6: TPanel;
    StaticText9: TStaticText;
    StationIDLabel: TLabel;
    StationIDNumLabel: TLabel;
    ReloadINIfile1: TMenuItem;
    Options1: TMenuItem;
    UseRankingsforNCAAB: TMenuItem;
    UseSeedforNCAAB: TMenuItem;
    StaticText10: TStaticText;
    AuthoringConnectLED: TApdStatusLight;
    AuthoringControlLED: TApdStatusLight;
    StaticText11: TStaticText;
    StatusBar: TStatusBar;
    PlaylistGrid2: TtsGrid;
    Label8: TLabel;
    NumEntries2: TLabel;
    Panel10: TPanel;
    Label10: TLabel;
    StartupTimer: TTimer;
    N8: TMenuItem;
    ClearAllGraphics1: TMenuItem;
    ReloadAllScenes1: TMenuItem;
    Label6: TLabel;
    AuthoringControlLabel: TPanel;
    Label4: TLabel;
    TrafficOnBtn: TBitBtn;
    TrafficOffBtn: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn5: TBitBtn;
    StaticText14: TStaticText;
    StaticText18: TStaticText;
    ClearFieldDataPanels1: TMenuItem;
    TrafficDataRequestLED: TApdStatusLight;
    StaticText19: TStaticText;
    HostModeSocketLED: TApdStatusLight;
    TrafficEnableLED: TApdStatusLight;
    TrafficDisableLED: TApdStatusLight;
    SchoolClosingsEnableLED: TApdStatusLight;
    SchoolClosingsDisableLED: TApdStatusLight;
    StaticText20: TStaticText;
    ClockEnableLED: TApdStatusLight;
    ClockOnBtn: TBitBtn;
    ClockOffBtn: TBitBtn;
    ClockDisableLED: TApdStatusLight;
    //General program functions
    procedure FormActivate(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    //Dialog functions
    procedure About1Click(Sender: TObject);
    procedure SetPrefs1Click(Sender: TObject);
    procedure StorePrefs1Click(Sender: TObject);
    procedure LoadPrefs;
    //Playlist operations
    procedure RefreshPlaylistGrid(GridID: SmallInt; var DataGrid: TtsGrid);
    procedure DeleteZipperPlaylist (PlaylistType: SmallInt; Playlist_ID: Double; Playlist_Description: String);
    //Utility functions
    procedure ReloadDataFromDatabase;
    function GetPlaylistCount(PlaylistType: SmallInt; SearchStr: String): SmallInt;
    function ScrubText (InText: String) : String;
    function LoadPlaylistCollection(PlaylistType: SmallInt; AppendMode: SmallInt; PlaylistID: Double): String;
    procedure RepopulateTeamsCollection1Click(Sender: TObject);
    function GetLeagueName(League_ID: SmallInt): String;
    function GetGamePhaseRec(League: String; GPhase: SmallInt): GamePhaseRec;
    function GetGameTimeString(GTimeStr: String): String;
    function GetStatInfo (StoredProcedureName: String): StatRec;
    function GetGameState(GTIME: String; GPHASE: SmallInt): Integer;
    function GetTemplateFieldsCount(Template_ID: SmallInt): SmallInt;
    function GetRecordTypeFromTemplateID(Template_ID: SmallInt): SmallInt;
    procedure TriggerTickerButtonClick(Sender: TObject);
    procedure TriggerCurrentTicker;
    procedure TimeOfDayTimerTimer(Sender: TObject);
    procedure ErrorFlashTimerTimer(Sender: TObject);
    procedure GPIResetTimerTimer(Sender: TObject);
    procedure GPIInitTimerTimer(Sender: TObject);
    procedure AbortCurrentEvent(IsStartup: Boolean);
    procedure SearchForNextScheduledPlaylist(LoopingTicker: Boolean);
    procedure AbortPlaylistButtonClick(Sender: TObject);
    procedure ResetTickerButtonClick(Sender: TObject);
    procedure EnableErrorChecking1Click(Sender: TObject);
    procedure DisableErrorChecking1Click(Sender: TObject);
    procedure ResetError1Click(Sender: TObject);
    procedure DatabaseConnectError(ExceptionString: String);
    procedure DatabaseEngineError(ExceptionString: String);
    procedure FormCreate(Sender: TObject);
    procedure StorePrefs;
    procedure SetGraphicsEngineConnectionPreferences1Click(Sender: TObject);
    procedure ReconnecttoGraphicsEngine1Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure PlaylistRefreshTimerTimer(Sender: TObject);
    procedure ScheduleMonitoringEnabledCheckBoxClick(Sender: TObject);
    function GetRecordTypeDescription (Playlist_Type: SmallInt; Record_Type: SmallInt): String;
    procedure ResetTickerToTop;
    function GetTemplateInformation(TemplateID: SmallInt): TemplateDefsRec;
    function GetAutomatedLeagueDisplayMnemonic (League: String): String;
    procedure ComPortDataPacketStringPacket(Sender: TObject; Data: String);
    procedure EnableCommandLogging1Click(Sender: TObject);
    procedure DisableCommandLogging1Click(Sender: TObject);
    procedure ReloadINIfile1Click(Sender: TObject);
    procedure UseRankingsforNCAABClick(Sender: TObject);
    procedure UseSeedforNCAABClick(Sender: TObject);
    procedure StartupTimerTimer(Sender: TObject);

    //Channel Box functions
    procedure RunStartupCommands;
    procedure ClearAllTemplates;
    procedure ClearAllGraphics1Click(Sender: TObject);
    procedure ReloadAllScenes1Click(Sender: TObject);
    procedure ClearFieldDataPanels1Click(Sender: TObject);

    //For traffic
    procedure ReloadTrafficData;
    procedure ClearTrafficCrawl;
    procedure SendNextCrawlRecord;
    procedure TrafficOnBtnClick(Sender: TObject);
    procedure TriggerTrafficCrawl;
    procedure TrafficOffBtnClick(Sender: TObject);
    procedure ClockOnBtnClick(Sender: TObject);
    procedure ClockOffBtnClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;
  AuthoringInterface: TAuthoringInterface;

implementation

uses Preferences,     //Prefs dialog
     AboutBox,        //About box
     DataModule,      //Data module
     SearchDataEntry, //Dialog used to enter text to search for
     GeneralFunctions,//General purpose functions
     PlaylistSelect,  //Dialog for selecting playlist to load
     PlaylistGraphicsViewer,  //Viewer for Zipper playlists
     ZipperEntryEditor,  //Editor dialog for playlist entries
     ScheduleEntryTimeEditor, //Editor for schedule entry times
     NoteEntryEditor, //Dialog for game note entry & editing
     NCAAManualGameEntry, //Dialog for manual NCAA entries
     ZipperEntry, //Dialog for basic data entry
     EngineIntf, //Ticker engine interface module
     EngineConnectionPreferences, //Ticker engine connnection prefs dialog
     CheckTickerSchedule,
     GameDataFunctions;

{$R *.DFM}
{$H+}
//Procedures to delete data pointers in each collection node
procedure Ticker_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TickerRec));
end;
procedure Temp_Ticker_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TickerRec));
end;
procedure Team_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TeamRec));
end;
procedure Ticker_Playout_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TickerRec));
end;
procedure SponsorLogo_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(SponsorLogoRec));
end;
procedure PromoLogo_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(PromoLogoRec));
end;
procedure Stat_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(StatRec));
end;
procedure Temp_Stat_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(StatRec));
end;
procedure Game_Phase_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(GamePhaseRec));
end;
procedure Template_Defs_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TemplateDefsRec));
end;
procedure Temporary_Fields_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TemplateFieldsRec));
end;
procedure Template_Fields_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TemplateFieldsRec));
end;
procedure Categories_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(CategoryRec));
end;
procedure Category_Templates_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(CategoryTemplatesRec));
end;
procedure Sports_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(SportRec));
end;
procedure RecordType_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(RecordTypeRec));
end;
procedure StyleChip_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(StyleChipRec));
end;
procedure LeagueCode_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(LeagueCodeRec));
end;
procedure Automated_League_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(AutomatedLeagueRec));
end;
procedure Scene_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(SceneRec));
end;
procedure LBar_Weather_Icon_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(LBarWeatherIconRec));
end;
procedure Traffic_Data_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TrafficDataRec));
end;
procedure Top_Graphic_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TopGraphicRec));
end;

////////////////////////////////////////////////////////////////////////////////
// GENERAL PROGRAM PROCEDURES, FUNCTIONS AND HANDLERS
////////////////////////////////////////////////////////////////////////////////
//Handler for main form creation
procedure TMainForm.FormCreate(Sender: TObject);
begin
  {Init for continuous loop}
  LoopTicker := TRUE;

  {Init error condition flag}
  Error_Condition := FALSE;

  //oolean to indciate error logging is enabled; default = false; will be
  //enabled after one second timer expires
  ErrorLoggingEnabled := True;

  //Init StartUp flag for display of splash-screen
  InStartUp := True;

  //Make label to indicate error checking disabled invisible
  StaticText27.Visible := False;
end;

//Handler for form activiation - inits done here
procedure TMainForm.FormActivate(Sender: TObject);
begin
  //Set debug flag
  Debug := FALSE;

  InitialSceneLoadComplete := FALSE;

  MainForm.Height := 922;
  MainForm.Width := 1176;

  //Init flags & handles
  RunningThread := FALSE;
  LoadingPlaylist := FALSE;
  LogoClockEnabled := TRUE;
  CurrentBreakingNewsPlaylistID := -1;
  LastSegmentHeading := '';

  //Set defaults
  LastPageIndex := 0;
  DBConnectionString := 'Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Data Source=TSTN';
  //Init for 2-line ticker, looping
  TickerDisplayMode := 3;

  //Set data dictionary directory
  SpellCheckerDictionaryDir := 'C:\Program Files\VDS\TSTN\Dictionaries';

  //Set day of week (used for roll-over of week number)
  LastDayOfWeek := DayOfWeek(Now);

  if (InStartup = True) then
  begin
    //Clear flag
    InStartup := False;

    LastPageIndex := 0;
    EngineControlMode := 0; //Run locally
    SocketConnected := FALSE;

    //Set default engine prefs
    with EngineParameters do
    begin
      Enabled := FALSE;
      IPAddress := '127.0.0.1';
      Port := '7795';
    end;

    //Disable GPIs to fire
    GPIsEnabled := FALSE;
    GPIsOKToFire := FALSE;

    //Reset flag
    RefreshingGrid := FALSE;

    LogFilePath := 'C:\Logfiles\';
    LogFileName := LogFilePath + 'NFL';

    //Load pereferences file
    LoadPrefs;

    //For command logging
    if (CommandLoggingEnabled) then
    begin
      EnableCommandLogging1.Checked := TRUE;
      DisableCommandLogging1.Checked := FALSE;
    end
    else begin
      EnableCommandLogging1.Checked := FALSE;
      DisableCommandLogging1.Checked := TRUE;
    end;

    //Setup crawl data response port
    EngineInterface.CrawlDataResponseSocket.Port := CrawlDataSocketPort;
    if (CrawlFeedbackPortEnabled) then EngineInterface.CrawlDataResponseSocket.Active := TRUE;

    //Set menu options for seeding
    if (UseSeedingForNCAABGames) then
    begin
      UseRankingsforNCAAB.Checked := FALSE;
      UseSeedforNCAAB.Checked := TRUE;
    end
    else begin
      UseRankingsforNCAAB.Checked := TRUE;
      UseSeedforNCAAB.Checked := FALSE;
    end;

    //Setup GPIs
    //Connect COM port if GPIs enabled
    if (GPIsEnabled) then
    begin
      GPIComPort.AutoOpen := FALSE;
      GPIComPort.Open := FALSE;
      GPIComPort.ComNumber := GPICOMPortNum;
      GPIComPort.Baud := GPIBaudRate;
      GPIComPort.Open := TRUE;
      GPIComPort.AutoOpen := TRUE;
    end
    else begin
      GPIComPort.AutoOpen := FALSE;
      GPIComPort.Open := FALSE;
    end;

    //Create collections
    Ticker_Collection := TStCollection.Create(1500);
    Ticker_Collection2 := TStCollection.Create(1500);
    Team_Collection := TStCollection.Create(5000);
    Ticker_Playout_Collection := TStCollection.Create(1500);
    Overlay_Collection := TStCollection.Create(250);
    SponsorLogo_Collection := TStCollection.Create(100);
    PromoLogo_Collection := TStCollection.Create(100);
    Stat_Collection := TStCollection.Create(250);
    Temp_Stat_Collection := TStCollection.Create(250);
    Game_Phase_Collection := TStCollection.Create(500);
    Template_Defs_Collection := TStCollection.Create(500);
    Template_Fields_Collection := TStCollection.Create(5000);
    Temporary_Fields_Collection := TStCollection.Create(100);
    Temporary_Fields_Collection2 := TStCollection.Create(100);
    Categories_Collection := TStCollection.Create(50);
    Category_Templates_Collection := TStCollection.Create(500);
    RecordType_Collection := TStCollection.Create(250);
    StyleChip_Collection := TStCollection.Create(100);
    LeagueCode_Collection := TStCollection.Create(1000);
    Automated_League_Collection := TStCollection.Create(50);
    Scene_Collection := TStCollection.Create(100);
    LBar_Weather_Icon_Collection := TStCollection.Create(100);
    Traffic_Data_Collection := TStCollection.Create(500);
    //Added for Version 2.0
    Top_Graphic_Collection := TStCollection.Create(50);

    Ticker_Collection.DisposeData := Ticker_Collection_DisposeData;
    Ticker_Collection2.DisposeData := Ticker_Collection_DisposeData;
    Team_Collection.DisposeData := Team_Collection_DisposeData;
    Ticker_Playout_Collection.DisposeData := Ticker_Playout_Collection_DisposeData;
    SponsorLogo_Collection.DisposeData := SponsorLogo_Collection_DisposeData;
    PromoLogo_Collection.DisposeData := PromoLogo_Collection_DisposeData;
    Stat_Collection.DisposeData := Stat_Collection_DisposeData;
    Temp_Stat_Collection.DisposeData := Temp_Stat_Collection_DisposeData;
    Game_Phase_Collection.DisposeData := Game_Phase_Collection_DisposeData;
    Template_Defs_Collection.DisposeData := Template_Defs_Collection_DisposeData;
    Template_Fields_Collection.DisposeData := Template_Fields_Collection_DisposeData;
    Temporary_Fields_Collection.DisposeData := Temporary_Fields_Collection_DisposeData;
    Temporary_Fields_Collection2.DisposeData := Temporary_Fields_Collection_DisposeData;
    Categories_Collection.DisposeData := Categories_Collection_DisposeData;
    Category_Templates_Collection.DisposeData := Category_Templates_Collection_DisposeData;
    RecordType_Collection.DisposeData := RecordType_Collection_DisposeData;
    StyleChip_Collection.DisposeData := StyleChip_Collection_DisposeData;
    LeagueCode_Collection.DisposeData := LeagueCode_Collection_DisposeData;
    Automated_League_Collection.DisposeData := Automated_League_Collection_DisposeData;
    Scene_Collection.DisposeData := Scene_Collection_DisposeData;
    LBar_Weather_Icon_Collection.DisposeData := LBar_Weather_Icon_Collection_DisposeData;
    Traffic_Data_Collection.DisposeData := Traffic_Data_Collection_DisposeData;
    //Added for Version 2.0
    Top_Graphic_Collection.DisposeData := Top_Graphic_Collection_DisposeData;


    //Activate database & tables
    With dmMain do
    begin
      dbCSNTicker.Connected := FALSE;
      dbCSNTicker.ConnectionString := DBConnectionString;
      dbCSNTicker.Connected := TRUE;
      tblTicker_Groups.Active := TRUE;
      tblTicker_Elements.Active := TRUE;
      tblScheduled_Ticker_Groups.Active := TRUE;
      tblBreakingNews_Groups.Active := TRUE;
      tblBreakingNews_Elements.Active := TRUE;
      //tblScheduled_BreakingNews_Groups.Active := TRUE;
      tblSponsor_Logos.Active := TRUE;
      dbSportbase.Connected := FALSE;
      dbSportbase.ConnectionString := DBConnectionString2;
      dbSportbase.Connected := TRUE;
      dbTraffic.Connected := FALSE;
      dbTraffic.ConnectionString := DBConnectionString3;
      dbTraffic.Connected := TRUE;
    end;

    //Call procedure to load data from database
    ReloadDataFromDatabase;

    //Connect to the graphics engine if it's enabled
    try
      EngineParameters.Enabled := TRUE;
      EngineInterface.EnginePort.Address := EngineParameters.IPAddress;
      EngineInterface.EnginePort.Port := StrToIntDef(EngineParameters.Port, 7795);
      EngineInterface.EnginePort.Active := TRUE;
    except
      if (ErrorLoggingEnabled = True) then
      begin
        Error_Condition := True;
        //WriteToErrorLog
        EngineInterface.WriteToErrorLog('Error occurred while trying connect to database engine');
      end;
    end;
    //Init the playlists IDs so new ones are found
    CurrentTickerPlaylistID := -1;
    CurrentBreakingNewsPlaylistID := -1;

    //For GPIs
    GPIResetTimer.Interval := GPIDebounceInterval;
  end;

  //Spawn thread to check ticker playlist schedule
//  CheckTickerScheduleThread := TCheckTickerSchedule.Create(TRUE);
//  CheckTickerScheduleThread.Priority := tpLowest;
//  CheckTickerScheduleThread.FreeOnTerminate := TRUE;
//  //Execute the thread
//  try
//    CheckTickerScheduleThread.Resume;
//  except
//    CheckTickerScheduleThread.Free;
//    if (ErrorLoggingEnabled = True) then
//      EngineInterface.WriteToErrorLog('Exception occurred in thread for checking ticker schedule');
//  end;

    PlaylistRefreshTimer.Enabled := TRUE;

    //Set state of schedule monitoring
    if (ScheduleMonitoringEnabled = TRUE) then
    begin
      ScheduleMonitoringEnabledCheckBox.Checked := TRUE;
      BitBtn4.Visible := FALSE;
      Label6.Visible := FALSE;
      //Enable timer to check for scheduled playlist
      //PlaylistRefreshTimer.Enabled := TRUE;
    end
    //In manual mode
    else begin
      //Clear flag; disble controls; show label
      ScheduleMonitoringEnabledCheckBox.Checked := FALSE;
      BitBtn4.Visible := TRUE;
      Label6.Visible := TRUE;
      //Disable timer to check for scheduled playlist
      //PlaylistRefreshTimer.Enabled := FALSE;
    end;

    //Make sure graphic is cleared from engine
    AbortCurrentEvent(IS_STARTUP);

   //Init Authoring Interface port
   With AuthoringInterface do
   begin
     //Open monitor ports
     AuthoringControlPort.Port := StrToInt(AuthoringControlPortNum);
     AuthoringControlPort.Open;
   end;

  //Setup status bar
  StatusBar.Font.Style := [fsBold];

  //Setup timer to call function that loads & plays scenes at startup
  StartupTimer.Enabled := TRUE;
end;

//Startup timer event handler
procedure TMainForm.StartupTimerTimer(Sender: TObject);
var
  i: SmallInt;
  SceneRecPtr: ^SceneRec;
begin
  //Disable to prevent re-triggering
  StartupTimer.Enabled := FALSE;

  //Query scenes for initial status
  if (Scene_Collection.Count > 0) then
  begin
    for i := 0 to Scene_Collection.Count-1 do
    begin
      SceneRecPtr := Scene_Collection.At(i);
      //Clear status
      SceneRecPtr^.Scene_Status := SCENE_NOSTATUS;
      //Request current status
      EngineInterface.QuerySceneStatus(SceneRecPtr^.Scene_ID);
    end;
  end;

  //Delay 2 seconds to get data back
  for i := 1 to 200 do
  begin
    Sleep(10);
    Application.ProcessMessages;
  end;

  //Load scenes; set flag right away if not loading scenes at startup
  if (LoadScenesAtStartup) then EngineInterface.SetupInitialScenes
  else InitialSceneLoadComplete := TRUE;
  //Run startup commands
  RunStartupCommands;
  //Clear all graphics
  ClearAllTemplates;
end;

//Function to run scene setup commands at startup
procedure TMainForm.RunStartupCommands;
var
  i: SmallInt;
  SponsorLogoRecPtr: ^SponsorLogoRec;
  SponsorLogoPath: String;
begin
  //Load in startup command information
  with dmMain.Query1 do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM Startup_Commands');
    Open;
    if (RecordCount > 0) then
    begin
      First;
      repeat
        //Send command
        EngineInterface.UpdateScene(FieldByName('SceneID').AsInteger, FieldByName('Command').AsString);
        //Dwell
        Sleep (FieldByName('Dwell').AsInteger);
        //Go to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Load the default sponsor logo
    if (SponsorLogo_Collection.Count > 0) then
    begin
      for i := 0 to SponsorLogo_Collection.Count-1 do
      begin
        SponsorLogoRecPtr := SponsorLogo_Collection.At(i);
        //If it's the default logo, update the sponsor logo region
        if (SponsorLogoRecPtr^.IsDefaultLogo) then
        begin
          SponsorLogoPath := Sponsor_Logo_Base_Path + '\' + SponsorLogoRecPtr^.SponsorLogoFilename;
          SponsorLogoPath := StringReplace(SponsorLogoPath, '\', '/', [rfReplaceAll]);
          EngineInterface.UpdateScene(SPONSOR_LOGO_SCENE_ID, SPONSOR_LOGO_FILE_UPDATE_COMMAND + '�' + SponsorLogoPath);
          EngineInterface.UpdateScene(SPONSOR_LOGO_SCENE_ID, SPONSOR_LOGO_IN_COMMAND + '�' + 'T');
        end;
      end;
    end;
  end;
end;

//Handler for program exit from main menu
procedure TMainForm.Exit1Click(Sender: TObject);
begin
  Close;
end;

//Handler for main program form close
procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //Confirm with operator}
  if (MessageDlg('Are you sure you want to exit the Comcast L-Bar Playout application?',
                  mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
  begin
    //Set action for main form
    Action := caFree;
    //Disable timers
    TimeOfDayTimer.Enabled := FALSE;
    GPIResetTimer.Enabled := FALSE;
    PlaylistRefreshTimer.Enabled := FALSE;
    //Make sure packet timeout timer is disabled
    EngineInterface.TickerPacketTimeoutTimer.Enabled := FALSE;

    //Shutdown GPI COM Port
    ComPortDataPacket.AutoEnable := FALSE;
    ComPortDataPacket.Enabled := FALSE;
    Sleep(100);
    GPIComPort.AutoOpen := FALSE;
    GPIComPort.Open := FALSE;
    Sleep(100);

    //Shut down crawl host message port
    EngineInterface.CrawlDataResponseSocket.Active := FALSE;

    //Clear the zipper if it's running
    AbortCurrentEvent(IS_NOT_STARTUP);

    //Terminate the thread
    CheckTickerScheduleThread.Terminate;
  end
  else
     Action := caNone;
end;

//Procedure to store user preferences
procedure TMainForm.StorePrefs1Click(Sender: TObject);
begin
  StorePrefs;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES FOR LAUNCHING VARIOUS PROGRAM DIALOGS
////////////////////////////////////////////////////////////////////////////////
//Display about box
procedure TMainForm.About1Click(Sender: TObject);
var
  Modal: TAbout;
begin
  Modal := TAbout.Create(Application);
  try
    Modal.ShowModal;
  finally
    Modal.Free
  end;
end;

//Handler to display dialog for setting user preferences
procedure TMainForm.SetPrefs1Click(Sender: TObject);
var
  Modal: TPrefs;
  Control: Word;
begin
  Modal := TPrefs.Create(Application);
  try
    Modal.Edit1.Text := AsRunLogFileDirectoryPath;
    Modal.Edit2.Text := DBConnectionString;
    Modal.Edit3.Text := DBConnectionString2;
    if (GPIsEnabled) then Modal.RadioGroup1.ItemIndex := 1
    else Modal.RadioGroup1.ItemIndex := 0;
    Modal.RadioGroup2.ItemIndex := GPIComPortNum-1;
    Case GPIBaudRate of
      9600: Modal.RadioGroup3.ItemIndex := 0;
     19200: Modal.RadioGroup3.ItemIndex := 1;
    end;
    if (ScheduleMonitoringEnabled) then Modal.RadioGroup4.ItemIndex := 1
    else Modal.RadioGroup4.ItemIndex := 0;
    if (FullSponsorPagesEnabled) then Modal.RadioGroup5.ItemIndex := 1
    else Modal.RadioGroup5.ItemIndex := 0;
    Modal.StationIDNum.Value := StationID;
    Modal.StationIDDesc.Text := StationDescription;
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    //Set new values if user didn't cancel out
    if (Control = mrOK) then
    begin
      AsRunLogFileDirectoryPath := Modal.Edit1.Text;
      DBConnectionString := Modal.Edit2.Text;
      DBConnectionString2 := Modal.Edit3.Text;
      if (Modal.RadioGroup1.ItemIndex = 1) then GPIsEnabled := TRUE
      else GPIsEnabled := FALSE;
      GPIComPortNum := Modal.RadioGroup2.ItemIndex+1;
      if (Modal.RadioGroup3.ItemIndex = 0) then GPIBaudRate := 9600
      else GPIBaudRate := 19200;
      if (Modal.RadioGroup4.ItemIndex = 1) then ScheduleMonitoringEnabled := TRUE
      else ScheduleMonitoringEnabled := FALSE;
      if (Modal.RadioGroup5.ItemIndex = 1) then FullSponsorPagesEnabled := TRUE
      else FullSponsorPagesEnabled := FALSE;

      //Setup GPIs
      //Connect COM port if GPIs enabled
      if (GPIsEnabled) then
      begin
        GPIComPort.AutoOpen := FALSE;
        GPIComPort.Open := FALSE;
        GPIComPort.ComNumber := GPICOMPortNum;
        GPIComPort.Baud := GPIBaudRate;
        GPIComPort.Open := TRUE;
        GPIComPort.AutoOpen := TRUE;
      end
      else begin
        GPIComPort.AutoOpen := FALSE;
        GPIComPort.Open := FALSE;
      end;

      //Set controls for schedule monitoring
      if (Modal.RadioGroup4.ItemIndex = 1) then
      begin
        ScheduleMonitoringEnabledCheckBox.Checked := TRUE;
        ScheduleMonitoringEnabled := TRUE;
        BitBtn4.Visible := FALSE;
        Label6.Visible := FALSE;
        //Enable timer to check for scheduled playlist
        //PlaylistRefreshTimer.Enabled := TRUE;
      end
      else begin
        ScheduleMonitoringEnabledCheckBox.Checked := FALSE;
        ScheduleMonitoringEnabled := FALSE;
        BitBtn4.Visible := TRUE;
        Label6.Visible := TRUE;
        //Disable timer to check for scheduled playlist
        //PlaylistRefreshTimer.Enabled := FALSE;
      end;

      //Set station ID info
      StationID := Modal.StationIDNum.Value;
      StationDescription := Modal.StationIDDesc.Text;
      //Set label
      StationIDNumLabel.Caption := IntToStr(StationID);
      StationIDLabel.Caption := StationDescription;

      //Activate database & tables
      With dmMain do
      begin
        dbCSNTicker.Connected := FALSE;
        dbCSNTicker.ConnectionString := DBConnectionString;
        dbCSNTicker.Connected := TRUE;
        tblTicker_Elements.Active := TRUE;
        tblTicker_Groups.Active := TRUE;
        tblScheduled_Ticker_Groups.Active := TRUE;
        tblBreakingNews_Elements.Active := TRUE;
        tblBreakingNews_Groups.Active := TRUE;
        //tblScheduled_BreakingNews_Groups.Active := TRUE;
        tblSponsor_Logos.Active := TRUE;
        dbSportbase.Connected := FALSE;
        dbSportbase.ConnectionString := DBConnectionString2;
        dbSportbase.Connected := TRUE;
      end;
      //Store the preferences
      StorePrefs;
    end;
    Modal.Free
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// UTILITY FUNCTIONS
// Various functions used for collection lookup, data processing, etc.
////////////////////////////////////////////////////////////////////////////////
//Handler for menu entry to re-load INI file
procedure TMainForm.ReloadINIfile1Click(Sender: TObject);
begin
  LoadPrefs;
end;

//Handler to reload collections
procedure TMainForm.RepopulateTeamsCollection1Click(Sender: TObject);
begin
  ReloadDataFromDatabase;
end;
//General procedure to cache in data from database into collections
procedure TMainForm.ReloadDataFromDatabase;
var
  TeamPtr: ^TeamRec; //Pointer to team collection object
  SponsorLogoPtr: ^SponsorLogoRec; //Pointer to sponsor logo object
  StatPtr: ^StatRec; //Pointer to stat object
  GamePhasePtr: ^GamePhaseRec;
  TemplateDefRecPtr: ^TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CategoryRecPtr: ^CategoryRec;
  CategoryTemplatesRecPtr: ^CategoryTemplatesRec;
  SportRecPtr: ^SportRec;
  RecordTypeRecPtr: ^RecordTypeRec;
  StyleChipRecPtr: ^StyleChipRec;
  LeagueCodeRecPtr: ^LeagueCodeRec;
  AutomatedLeagueRecPtr: ^AutomatedLeagueRec;
  SceneRecPtr: ^SceneRec;
  LBarWeatherIconRecPtr: ^LBarWeatherIconRec;
begin
  try
    //Load collections for users and block subcategories
    Team_Collection.Clear;
    Team_Collection.Pack;
    //Load in teams information
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Teams');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to teams collection
        GetMem (TeamPtr, SizeOf(TeamRec));
        With TeamPtr^ do
        begin
          League := dmMain.Query1.FieldByName('League').AsString;
          //OldSTTeamCode := dmMain.Query1.FieldByName('OldSTTeamCode').AsString;
          NewSTTeamCode := dmMain.Query1.FieldByName('NewSTTeamCode').AsString;
          StatsIncID := dmMain.Query1.FieldByName('StatsIncID').AsFloat;
          LongName := dmMain.Query1.FieldByName('LongName').AsString;
          ShortName := dmMain.Query1.FieldByName('ShortName').AsString;
          BaseName := dmMain.Query1.FieldByName('BaseName').AsString;
          DisplayName1 := dmMain.Query1.FieldByName('DisplayName1').AsString;
          DisplayName2 := dmMain.Query1.FieldByName('DisplayName2').AsString;
          CamioTeamLogoPath := dmMain.Query1.FieldByName('CamioTeamLogoPath').AsString;
          If (Team_Collection.Count <= 5000) then
          begin
            Team_Collection.Insert(TeamPtr);
            Team_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end
    else begin
      MessageDlg('No data found in Teams database table!', mtError, [mbOk], 0);
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the categories table; iterate for all records in table
    Categories_Collection.Clear;
    Categories_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Categories');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (CategoryRecPtr, SizeOf(CategoryRec));
        With CategoryRecPtr^ do
        begin
          Category_Type := dmMain.Query1.FieldByName('Category_Type').AsInteger;
          Category_ID := dmMain.Query1.FieldByName('Category_ID').AsInteger;
          Category_Name := dmMain.Query1.FieldByName('Category_Name').AsString;
          Category_Label := dmMain.Query1.FieldByName('Category_Label').AsString;
          Category_Description := dmMain.Query1.FieldByName('Category_Description').AsString;
          Category_Is_Sport := dmMain.Query1.FieldByName('Category_Is_Sport').AsBoolean;
          Category_Sport := dmMain.Query1.FieldByName('Category_Sport').AsString;
          Sport_Games_Table_Name := dmMain.Query1.FieldByName('Sport_Games_Table_Name').AsString;
          CamioFolderName := dmMain.Query1.FieldByName('CamioFolderName').AsString;
          If (Categories_Collection.Count <= 50) then
          begin
            //Add to collection
            Categories_Collection.Insert(CategoryRecPtr);
            Categories_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end
    else begin
      MessageDlg('No data found in Ticker Categories database table!', mtError, [mbOk], 0);
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the game phase codes table; iterate for all records in table
    Game_Phase_Collection.Clear;
    Game_Phase_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Game_Phase_Codes');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (GamePhasePtr, SizeOf(GamePhaseRec));
        With GamePhasePtr^ do
        begin
          League := dmMain.Query1.FieldByName('League').AsString;
          ST_Phase_Code := dmMain.Query1.FieldByName('ST_Phase_Code').AsInteger;
          SI_Phase_Code := dmMain.Query1.FieldByName('SI_Phase_Code').AsInteger;
          Display_Period := dmMain.Query1.FieldByName('Display_Period').AsString;
          End_Is_Half := dmMain.Query1.FieldByName('End_Is_Half').AsBoolean;
          Display_Half := dmMain.Query1.FieldByName('Display_Half').AsString;
          Display_Final := dmMain.Query1.FieldByName('Display_Final').AsString;
          Display_Extended1 := dmMain.Query1.FieldByName('Display_Extended1').AsString;
          Display_Extended2 := dmMain.Query1.FieldByName('Display_Extended2').AsString;
          If (Game_Phase_Collection.Count <= 500) then
          begin
            //Add to collection
            Game_Phase_Collection.Insert(GamePhasePtr);
            Game_Phase_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end
    else begin
      MessageDlg('No data found in Game Phase database table!', mtError, [mbOk], 0);
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the stats info table; iterate for all records in table
    Stat_Collection.Clear;
    Stat_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Stat_Definitions');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (StatPtr, SizeOf(StatRec));
        With StatPtr^ do
        begin
          StatLeague := dmMain.Query1.FieldByName('StatLeague').AsString;
          StatType := dmMain.Query1.FieldByName('StatType').AsInteger;
          StatDescription := dmMain.Query1.FieldByName('StatDescription').AsString;
          StatHeader := dmMain.Query1.FieldByName('StatHeader').AsString;
          StatStoredProcedure := dmMain.Query1.FieldByName('StatStoredProcedure').AsString;
          StatDataField := dmMain.Query1.FieldByName('StatDataField').AsString;
          StatAbbreviation := dmMain.Query1.FieldByName('StatAbbreviation').AsString;
          UseStatQualifier := dmMain.Query1.FieldByName('UseStatQualifier').AsBoolean;
          StatQualifier := dmMain.Query1.FieldByName('StatQualifier').AsString;
          StatQualifierValue := dmMain.Query1.FieldByName('StatQualifierValue').AsInteger;
          If (Stat_Collection.Count <= 100) then
          begin
            //Add to collection
            Stat_Collection.Insert(StatPtr);
            Stat_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the sponsor logos table; iterate for all records in table
    SponsorLogo_Collection.Clear;
    SponsorLogo_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Sponsor_Logos');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (SponsorLogoPtr, SizeOf(SponsorLogoRec));
        With SponsorLogoPtr^ do
        begin
          SponsorLogoIndex := dmMain.Query1.FieldByName('LogoIndex').AsInteger;
          SponsorLogoName := dmMain.Query1.FieldByName('LogoName').AsString;
          SponsorLogoFilename := dmMain.Query1.FieldByName('LogoFilename').AsString;
          IsDefaultLogo := dmMain.Query1.FieldByName('IsDefaultLogo').AsBoolean;
          If (SponsorLogo_Collection.Count <= 100) then
          begin
            //Add to collection
            SponsorLogo_Collection.Insert(SponsorLogoPtr);
            SponsorLogo_Collection.Pack;
            //Add to comboboxes
            //ComboBox4.Items.Add(SponsorLogoName);
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the template definitions table; iterate for all records in table
    Template_Defs_Collection.Clear;
    Template_Defs_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Template_Defs');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (TemplateDefRecPtr, SizeOf(TemplateDefsRec));
        With TemplateDefRecPtr^ do
        begin
          Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
          Template_Type := dmMain.Query1.FieldByName('Template_Type').AsInteger;
          AlternateModeTemplateID := dmMain.Query1.FieldByName('AlternateModeTemplateID').AsInteger;
          Template_Description := dmMain.Query1.FieldByName('Template_Description').AsString;
          Template_Has_Children := dmMain.Query1.FieldByName('Template_Has_Children').AsBoolean;
          Template_Is_Child := dmMain.Query1.FieldByName('Template_Is_Child').AsBoolean;
          Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
          TemplateSponsorType := dmMain.Query1.FieldByName('TemplateSponsorType').AsInteger;
          Scene_ID_1 := dmMain.Query1.FieldByName('Scene_ID_1').AsInteger;
          Scene_ID_2 := dmMain.Query1.FieldByName('Scene_ID_2').AsInteger;
          Transition_In := dmMain.Query1.FieldByName('Transition_In').AsString;
          Transition_Out := dmMain.Query1.FieldByName('Transition_Out').AsString;
          Default_Dwell := dmMain.Query1.FieldByName('Default_Dwell').AsInteger;
          ManualLeague := dmMain.Query1.FieldByName('ManualLeague').AsBoolean;
          EnableForOddsOnly := dmMain.Query1.FieldByName('EnableForOddsOnly').AsBoolean;
          UsesGameData := dmMain.Query1.FieldByName('UsesGameData').AsBoolean;
          RequiredGameState := dmMain.Query1.FieldByName('RequiredGameState').AsInteger;
          IsFantasyTemplate := dmMain.Query1.FieldByName('OptionalFlag1').AsBoolean;
          IsLeadersTemplate := dmMain.Query1.FieldByName('OptionalFlag2').AsBoolean;
          //Special feature for SNY
          if (AllowAlertBackgroundsForNews) then
            Use_Alert_Background := dmMain.Query1.FieldByName('Use_Alert_Background').AsBoolean;
          StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
          EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
          If (Template_Defs_Collection.Count <= 500) then
          begin
            //Add to collection
            Template_Defs_Collection.Insert(TemplateDefRecPtr);
            Template_Defs_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the template fieldss table; iterate for all records in table
    Template_Fields_Collection.Clear;
    Template_Fields_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Template_Fields');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (TemplateFieldsRecPtr, SizeOf(TemplateFieldsRec));
        With TemplateFieldsRecPtr^ do
        begin
          Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
          Field_ID := dmMain.Query1.FieldByName('Field_ID').AsInteger;
          Field_Type := dmMain.Query1.FieldByName('Field_Type').AsInteger;
          Field_Is_Manual := dmMain.Query1.FieldByName('Field_Is_Manual').AsBoolean;
          Field_Label := dmMain.Query1.FieldByName('Field_Label').AsString;
          Field_Contents := dmMain.Query1.FieldByName('Field_Contents').AsString;
          Field_Format_Prefix := dmMain.Query1.FieldByName('Field_Format_Prefix').AsString;
          Field_Format_Suffix := dmMain.Query1.FieldByName('Field_Format_Suffix').AsString;
          Field_Max_Length := dmMain.Query1.FieldByName('Field_Max_Length').AsInteger;
          Scene_Field_Name := dmMain.Query1.FieldByName('Scene_Field_Name').AsString;
          If (Template_Fields_Collection.Count <= 5000) then
          begin
            //Add to collection
            Template_Fields_Collection.Insert(TemplateFieldsRecPtr);
            Template_Fields_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the record types table; iterate for all records in table
    RecordType_Collection.Clear;
    RecordType_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Record_Types');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to scripts collection
        GetMem (RecordTypeRecPtr, SizeOf(RecordTypeRec));
        With RecordTypeRecPtr^ do
        begin
          Playlist_Type := dmMain.Query1.FieldByName('Playlist_Type').AsInteger;
          Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
          Record_Description := dmMain.Query1.FieldByName('Record_Description').AsString;
          If (RecordType_Collection.Count <= 250) then
          begin
            //Add to collection
            RecordType_Collection.Insert(RecordTypeRecPtr);
            RecordType_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the style chips table; iterate for all records in table
    StyleChip_Collection.Clear;
    StyleChip_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Style_Chips');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to style chip collection
        GetMem (StyleChipRecPtr, SizeOf(StyleChipRec));
        With StyleChipRecPtr^ do
        begin
          StyleChip_Index := dmMain.Query1.FieldByName('StyleChip_Index').AsInteger;
          StyleChip_Description := dmMain.Query1.FieldByName('StyleChip_Description').AsString;
          StyleChip_Code := dmMain.Query1.FieldByName('StyleChip_Code').AsString;
          StyleChip_Type := dmMain.Query1.FieldByName('StyleChip_Type').AsInteger;
          StyleChip_String := dmMain.Query1.FieldByName('StyleChip_String').AsString;
          StyleChip_FontCode := dmMain.Query1.FieldByName('StyleChip_FontCode').AsInteger;
          StyleChip_CharacterCode := dmMain.Query1.FieldByName('StyleChip_CharacterCode').AsInteger;
          If (StyleChip_Collection.Count <= 100) then
          begin
            //Add to collection
            StyleChip_Collection.Insert(StyleChipRecPtr);
            StyleChip_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the data from the automated leagues table; iterate for all records in table
    Automated_League_Collection.Clear;
    Automated_League_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Automated_Leagues');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to league codes collection
        GetMem (AutomatedLeagueRecPtr, SizeOf(AutomatedLeagueRec));
        With AutomatedLeagueRecPtr^ do
        begin
          SI_LeagueCode := dmMain.Query1.FieldByName('SI_LeagueCode').AsString;
          ST_LeagueCode := dmMain.Query1.FieldByName('ST_LeagueCode').AsString;
          Display_Mnemonic := dmMain.Query1.FieldByName('Display_Mnemonic').AsString;
          If (Automated_League_Collection.Count <= 50) then
          begin
            //Add to collection
            Automated_League_Collection.Insert(AutomatedLeagueRecPtr);
            Automated_League_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the scenes collection
    Scene_Collection.Clear;
    Scene_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Scene_Defs');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to league codes collection
        GetMem (SceneRecPtr, SizeOf(SceneRec));
        With SceneRecPtr^ do
        begin
          Scene_ID := dmMain.Query1.FieldByName('Scene_ID').AsInteger;
          Scene_Description := dmMain.Query1.FieldByName('Scene_Description').AsString;
          Is_Base_Scene := dmMain.Query1.FieldByName('Is_Base_Scene').AsBoolean;
          Load_At_Startup := dmMain.Query1.FieldByName('Load_At_Startup').AsBoolean;
          Play_At_Startup := dmMain.Query1.FieldByName('Play_At_Startup').AsBoolean;
          Run_Startup_Command := dmMain.Query1.FieldByName('Run_Startup_Command').AsBoolean;
          Startup_Command := dmMain.Query1.FieldByName('Startup_Command').AsString;
          if (Scene_Collection.Count <= 100) then
          begin
            //Add to collection
            Scene_Collection.Insert(SceneRecPtr);
            Scene_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;

    //Load in the weather icon collection
    LBar_Weather_Icon_Collection.Clear;
    LBar_Weather_Icon_Collection.Pack;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Weather_Icons_Info');
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then
    begin
      dmMain.Query1.First;
      repeat
        //Add item to league codes collection
        GetMem (LBarWeatherIconRecPtr, SizeOf(LBarWeatherIconRec));
        With LBarWeatherIconRecPtr^ do
        begin
          IconID := dmMain.Query1.FieldByName('IconID').AsInteger;
          IconDescription := dmMain.Query1.FieldByName('IconDescription').AsString;
          IconFilename := dmMain.Query1.FieldByName('IconFilename').AsString;
          IconCharacterValue := dmMain.Query1.FieldByName('IconCharacterValue').AsInteger;
          if (LBar_Weather_Icon_Collection.Count <= 100) then
          begin
            //Add to collection
            LBar_Weather_Icon_Collection.Insert(LBarWeatherIconRecPtr);
            LBar_Weather_Icon_Collection.Pack;
          end;
        end;
        //Go to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close query
    dmMain.Query1.Active := FALSE;
  except
    EngineInterface.WriteToErrorLog('Error occurred while trying to reload data from database');
  end;
end;

//Function to scrub a string of text to remove control characters
function TMainForm.ScrubText (InText: ANSIString) : ANSIString;
var
  i: SmallInt;
  OutStr: ANSIString;
begin
  OutStr := '';
  //Remove all except printable ASCII characters
  for i := 1 to Length(InText) do
    if (Ord(InText[i]) >= 32) AND (Ord(InText[i]) <= 126) then OutStr := OutStr + InText[i];
  ScrubText := Trim(OutStr);
end;

//Function to return number of zipper playlists in the database corresponding to
//the specified Cart ID
function TMainForm.GetPlaylistCount(PlaylistType: SmallInt; SearchStr: String): SmallInt;
var
  OutCount: SmallInt;
begin
  try
    dmMain.Query1.Close;
    dmMain.Query1.SQL.Clear;
    Case PlaylistType of
     0: dmMain.Query1.SQL.Add('SELECT * FROM Ticker_Groups WHERE Playlist_Description ' +
                          'LIKE ' + QuotedStr('%' + SearchStr + '%'));
     1: dmMain.Query1.SQL.Add('SELECT * FROM Bug_Groups WHERE Playlist_Description ' +
                          'LIKE ' + QuotedStr('%' + SearchStr + '%'));
     2: dmMain.Query1.SQL.Add('SELECT * FROM ExtraLine_Groups WHERE Playlist_Description ' +
                          'LIKE ' + QuotedStr('%' + SearchStr + '%'));
    end;
    dmMain.Query1.Open;
    //Get the count
    OutCount := dmMain.Query1.RecordCount;
    //Close the query and return
    dmMain.Query1.Close;
  except
    if (ErrorLoggingEnabled = True) then
      EngineInterface.WriteToErrorLog('Error occurred while trying to get playlist count');
  end;
  GetPlaylistCount := OutCount;
end;

//Function to take league ID and return league name
function TMainForm.GetLeagueName(League_ID: SmallInt): String;
var
  LeagueRecPtr: ^LeagueRec;
  OutStr: String;
begin
  OutStr := ' ';
  if (League_Collection.Count >= League_ID) then
  begin
    LeagueRecPtr := League_Collection.At(League_ID-1);
    OutStr := LeagueRecPtr^.LeagueNameShort;
  end;
  GetLeagueName := OutStr;
end;

//Function to take a template ID & return its description
function TMainForm.GetTemplateInformation(TemplateID: SmallInt): TemplateDefsRec;
var
  i: SmallInt;
  TemplateDefsRecPtr: ^TemplateDefsRec;
  OutRec: TemplateDefsRec;
begin
  if (Template_Defs_Collection.Count > 0) then
  begin
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_ID = TemplateID) then
      with OutRec do
      begin
        Template_ID := TemplateDefsRecPtr^.Template_ID;
        Template_Type := TemplateDefsRecPtr^.Template_Type;
        Template_Description := TemplateDefsRecPtr^.Template_Description;
        Record_Type := TemplateDefsRecPtr^.Record_Type;
        Scene_ID_1 := TemplateDefsRecPtr^.Scene_ID_1;
        Scene_ID_2 := TemplateDefsRecPtr^.Scene_ID_2;
        Transition_In := TemplateDefsRecPtr^.Transition_In;
        Transition_Out := TemplateDefsRecPtr^.Transition_Out;
        Default_Dwell := TemplateDefsRecPtr^.Default_Dwell;
        ManualLeague := TemplateDefsRecPtr^.ManualLeague;
        EnableForOddsOnly := TemplateDefsRecPtr^.EnableForOddsOnly;
        UsesGameData := TemplateDefsRecPtr^.UsesGameData;
        Template_Has_Children := TemplateDefsRecPtr^.Template_Has_Children;
        Template_Is_Child := TemplateDefsRecPtr^.Template_Is_Child;
        StartEnableDateTime := TemplateDefsRecPtr^.StartEnableDateTime;
        EndEnableDateTime := TemplateDefsRecPtr^.EndEnableDateTime;
      end;
    end
  end;
  GetTemplateInformation := OutRec;
end;

//Function to take a league and a game phase code, and return a game phase
//record
function TMainForm.GetGamePhaseRec(League: String; GPhase: SmallInt): GamePhaseRec;
var
  i: SmallInt;
  GamePhaseRecPtr: ^GamePhaseRec; //Pointer to game phase record type
  OutRec: GamePhaseRec;
  FoundRecord: Boolean;
begin
  FoundRecord := FALSE;
  OutRec.League := ' ';
  OutRec.ST_Phase_Code := 0;
  OutRec.SI_Phase_Code := 0;
  OutRec.Label_Period := ' ';
  OutRec.Display_Period := ' ';
  OutRec.Display_Half := ' ';
  OutRec.Display_Final := ' ';
  OutRec.Display_Extended1 := ' ';
  OutRec.Display_Extended2 := ' ';
  OutRec.Game_OT := FALSE;
  if (Game_Phase_Collection.Count > 0) then
  begin
    i := 0;
    repeat
      GamePhaseRecPtr := Game_Phase_Collection.At(i);
      if (GamePhaseRecPtr^.League = League) AND (GamePhaseRecPtr^.SI_Phase_Code = GPhase) then
      begin
        OutRec.League := League;
        OutRec.SI_Phase_Code := GPhase;
        OutRec.Display_Period := GamePhaseRecPtr^.Display_Period;
        OutRec.End_Is_Half := GamePhaseRecPtr^.End_Is_Half;
        OutRec.Display_Half := GamePhaseRecPtr^.Display_Half;
        OutRec.Display_Final := GamePhaseRecPtr^.Display_Final;
        OutRec.Display_Extended1 := GamePhaseRecPtr^.Display_Extended1;
        OutRec.Display_Extended2 := GamePhaseRecPtr^.Display_Extended2;
        OutRec.Game_OT := GamePhaseRecPtr^.Game_OT;
        FoundRecord := TRUE;
      end;
      Inc(i);
    until (i = Game_Phase_Collection.Count) OR (FoundRecord = TRUE);
  end;
  GetGamePhaseRec := OutRec;
end;

//Function to get game time string from GT Server time value
function TMainForm.GetGameTimeString(GTimeStr: String): String;
var
  Min, Sec: String;
begin
  //Blank out case where time = 9999 - for MLB
  if (GTimeStr = '9999') then Result := ' '
  //Actual clock time
  else if (StrToIntDef(GTimeStr, -1) <> -1) AND (Length(GTimeStr) = 4) then
  begin
    Min := GTimeStr[1] + GTimeStr[2];
    Sec := GTimeStr[3] + GTimeStr[4];
    Min := IntToStr(StrToInt(GTimeStr[1]+GTimeStr[2]));
    Result := Min + ':' + Sec;
  end
  else if (GTimeStr = 'END-') then Result := 'End'
  else if (GTimeStr = 'POST') then Result := 'PPD'
  else if (GTimeStr = 'SUSP') then Result := 'PPD'
  else if (GTimeStr = 'DELA') then Result := 'DLY'
  else if (GTimeStr = 'RAIN') then Result := 'RD'
  else if (GTimeStr = 'CANC') then Result := 'PPD'
  else Result := ' ';
end;

//Function to take the stat stored procedure name and return the stat information record
function TMainForm.GetStatInfo (StoredProcedureName: String): StatRec;
var
  i: SmallInt;
  StatRecPtr: ^StatRec; //Pointer to stat record type
  OutRec: StatRec;
begin
  OutRec.StatLeague := ' ';
  OutRec.StatType := 0;
  OutRec.StatDescription := ' ';
  OutRec.StatHeader := ' ';
  OutRec.StatStoredProcedure := ' ';
  OutRec.StatDataField := ' ';
  OutRec.StatAbbreviation := ' ';
  OutRec.UseStatQualifier := FALSE;
  OutRec.StatQualifier := ' ';
  OutRec.StatQualifierValue := 0;
  if (Stat_Collection.Count > 0) then
  begin
    for i := 0 to Stat_Collection.Count-1 do
    begin
      StatRecPtr := Stat_Collection.At(i);
      if (Trim(StatRecPtr^.StatStoredProcedure) = Trim(StoredProcedureName)) then
      begin
        OutRec.StatLeague := StatRecPtr^.StatLeague;
        OutRec.StatType := StatRecPtr^.StatType;
        OutRec.StatDescription := StatRecPtr^.StatDescription;
        OutRec.StatHeader := StatRecPtr^.StatHeader;
        OutRec.StatStoredProcedure := StatRecPtr^.StatStoredProcedure;
        OutRec.StatDataField := StatRecPtr^.StatDataField;
        OutRec.StatAbbreviation := StatRecPtr^.StatAbbreviation;
        OutRec.UseStatQualifier := StatRecPtr^.UseStatQualifier;
        OutRec.StatQualifier := StatRecPtr^.StatQualifier;
        OutRec.StatQualifierValue := StatRecPtr^.StatQualifierValue;
      end;
    end;
  end;
  GetStatInfo := OutRec;
end;

//Function to get game state
function TMainForm.GetGameState(GTIME: String; GPHASE: SmallInt): Integer;
begin
  if (GTIME = '9999') OR (GPHASE = 0) then
    //Game not started
    Result := 0
  else if (GTIME = 'FINA') OR (GTIME = 'SUM-') then
    //Game is final
    Result := 3
  else if (GTIME = 'END-') then
    //End of period/quarter
    Result := 2
  else if (GPHASE <> 0) AND (StrToIntDef(GTIME, -1) <> -1) then
    //Game in progress
    Result := 1
  else
    //All other conditions
    Result := -1; // Game is suspended due to rain or cancelled etc. (GTIME = 'RAIN' , GTIME = 'SUSP', GTIME = 'CANC')
end;

//Function to get the number of fields in a template
function TMainForm.GetTemplateFieldsCount(Template_ID: SmallInt): SmallInt;
var
  i: SmallInt;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  OutVal: SmallInt;
begin
  OutVal := 0;
  If (Template_Fields_Collection.Count > 0) then
  begin
    for i := 0 to Template_Fields_Collection.Count-1 do
    begin
      TemplateFieldsRecPtr := Template_Fields_Collection.At(i);
      if (TemplateFieldsRecPtr^.Template_ID = Template_ID) then Inc(OutVal);
    end;
  end;
  GetTemplateFieldsCount := OutVal;
end;

//Function to get the record type based on the template ID
function TMainForm.GetRecordTypeFromTemplateID(Template_ID: SmallInt): SmallInt;
var
  i: SmallInt;
  TemplateDefsRecPtr: ^TemplateDefsRec;
  OutVal: SmallInt;
begin
  OutVal := 0;
  if (Template_Defs_Collection.Count > 0) then
  begin
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_ID = Template_ID) then
        OutVal := TemplateDefsRecPtr^.Record_Type;
    end;
  end;
  GetRecordTypeFromTemplateID := OutVal;
end;

//Function to return a record type description base on playlist type and record type code
function TMainForm.GetRecordTypeDescription (Playlist_Type: SmallInt; Record_Type: SmallInt): String;
var
  i: SmallInt;
  RecordTypeRecPtr: ^RecordTypeRec;
  OutStr: String;
begin
  OutStr := '';
  if (RecordType_Collection.Count > 1) then
  begin
    for i := 0 to RecordType_Collection.Count-1 do
    begin
      RecordTypeRecPtr := RecordType_Collection.At(i);
      if (RecordTypeRecPtr^.Playlist_Type = Playlist_Type) AND (RecordTypeRecPtr^.Record_Type = Record_Type) then
        OutStr := RecordTypeRecPtr^.Record_Description;
    end;
  end;
  GetRecordTypeDescription := OutStr;
end;

function TMainForm.GetAutomatedLeagueDisplayMnemonic (League: String): String;
var
  i: SmallInt;
  AutomatedLeagueRecPtr: ^AutomatedLeagueRec; //Pointer to team record type
  OutStr: String;
  FoundRecord: Boolean;
begin
  //Init to passed in variable; will return if no substitution found
  OutStr := League;
  if (Automated_League_Collection.Count > 0) then
  begin
    FoundRecord := FALSE;
    i := 0;
    Repeat
      AutomatedLeagueRecPtr := Automated_League_Collection.At(i);
      if (Trim(AutomatedLeagueRecPtr^.SI_LeagueCode) = League) then
      begin
        OutStr := AutomatedLeagueRecPtr^.Display_Mnemonic;
      end;
      Inc(i);
    Until (FoundRecord = TRUE) OR (i = Automated_League_Collection.Count);
  end;
  GetAutomatedLeagueDisplayMnemonic := OutStr;
end;

////////////////////////////////////////////////////////////////////////////////
// HANDLERS FOR GENERAL PROGRAM OPERATIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES AND HANDLERS FOR ZIPPER PLAYLIST CREATION AND EDITING
////////////////////////////////////////////////////////////////////////////////
//General procedure to clear and refresh the trial playlist grid
procedure TMainForm.RefreshPlaylistGrid(GridID: SmallInt; var DataGrid: TtsGrid);
var
  i: SmallInt;
  TickerPtr: ^TickerRec;
  CollectionCount: SmallInt;
  AutoLeagueMnemonic: String;
  CurrentRow, CurrentTopRow: SmallInt;
begin
  //Get current row
  CurrentRow := DataGrid.CurrentDataRow;
  CurrentTopRow := DataGrid.TopRow;

  //Clear grid values
  if (DataGrid.Rows > 0) then
    DataGrid.DeleteRows (1, DataGrid.Rows);

  case GridID of
    FIELD_1: CollectionCount := Ticker_Collection.Count;
    FIELD_2: CollectionCount := Ticker_Collection2.Count;
   end;

  //Setup grid
  if (CollectionCount > 0) then
  begin
    DataGrid.StoreData := TRUE;
    DataGrid.Cols := 7;
    DataGrid.Rows := CollectionCount;
    //Populate the grid
    for i := 0 to CollectionCount-1 do
    begin
      case GridID of
        FIELD_1: TickerPtr := Ticker_Collection.At(i);
        FIELD_2: TickerPtr := Ticker_Collection2.At(i);
      end;
      DataGrid.Cell[1,i+1] := IntToStr(i+1); //Index
      DataGrid.Cell[2,i+1] := TickerPtr^.League; //Record enable
      DataGrid.Cell[3,i+1] := TickerPtr^.Enabled; //Record enable
      DataGrid.Cell[4,i+1] :=
        GetRecordTypeDescription(TICKER, TickerPtr^.Record_Type);
      if (TickerPtr^.Record_Type = 2) then
        DataGrid.Cell[5,i+1] := TickerPtr^.SponsorLogo_Name
      else if (GetTemplateInformation(TickerPtr^.Template_ID).UsesGameData = TRUE) then
        DataGrid.Cell[5,i+1] := TickerPtr^.Description
      else if (TickerPtr^.Record_Type = 3) or (TickerPtr^.Record_Type = 4) or (TickerPtr^.Record_Type = 5) then
        DataGrid.Cell[5,i+1] := TickerPtr^.UserData[21]
      else
        DataGrid.Cell[5,i+1] := TickerPtr^.UserData[1];
        if (Trim(TickerPtr^.UserData[2]) <> '') then
          DataGrid.Cell[5,i+1] := DataGrid.Cell[5,i+1] + ' | ' + TickerPtr^.UserData[2];
        if (Trim(TickerPtr^.UserData[3]) <> '') then
          DataGrid.Cell[5,i+1] := DataGrid.Cell[5,i+1] + ' | ' + TickerPtr^.UserData[3];
        if (Trim(TickerPtr^.UserData[4]) <> '') then
          DataGrid.Cell[5,i+1] := DataGrid.Cell[5,i+1] + ' | ' + TickerPtr^.UserData[4];
        if (Trim(TickerPtr^.UserData[5]) <> '') then
          DataGrid.Cell[5,i+1] := DataGrid.Cell[5,i+1] + ' | ' + TickerPtr^.UserData[5];
        if (Trim(TickerPtr^.UserData[6]) <> '') then
          DataGrid.Cell[5,i+1] := DataGrid.Cell[5,i+1] + ' | ' + TickerPtr^.UserData[6];
      DataGrid.Cell[6,i+1] := TickerPtr^.Comments; //Record enable
      //By default, show item over white background except if child template and added as group
      //Modified to always show child templates in gray or silver
      if (GetTemplateInformation(TickerPtr^.Template_ID).Template_Is_Child = TRUE) AND
         (TickerPtr^.Is_Child = TRUE) then
        DataGrid.RowColor[i+1] := clGray
      else if (GetTemplateInformation(TickerPtr^.Template_ID).Template_Is_Child = TRUE) then
        DataGrid.RowColor[i+1] := clMedGray
      else
        DataGrid.RowColor[i+1] := clWindow;
      //Update number fo entries label
      NumEntries.Caption := IntToStr(Ticker_Collection.Count);
      NumEntries2.Caption := IntToStr(Ticker_Collection2.Count);
    end;
  end
  else begin
    NumEntries.Caption := IntToStr(CollectionCount);
  end;
  //Refresh grid
  if (CurrentTopRow > 0) AND (CollectionCount > 0) then
  begin
    if (CurrentTopRow > CollectionCount) then CurrentTopRow := CollectionCount;
    DataGrid.TopRow := CurrentTopRow;
  end;
  if (CurrentRow > 0) AND (CollectionCount > 0) then
  begin
    if (CurrentRow > CollectionCount) then CurrentRow := CollectionCount;
    DataGrid.CurrentDataRow := CurrentRow;
    DataGrid.RowSelected[CurrentRow] := TRUE;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES AND HANDLERS FOR ZIPPER PLAYLIST STORAGE,, LOADING & DELETION
////////////////////////////////////////////////////////////////////////////////
//Procedure to delete a specified zipper playlist from the database
procedure TMainForm.DeleteZipperPlaylist (PlaylistType: SmallInt; Playlist_ID: Double; Playlist_Description: String);
begin
  Case PlaylistType of
      //Ticker
   0: begin
        try
          if (dmMain.tblTicker_Groups.RecordCount > 0) then
          begin
            dmMain.tblTicker_Groups.First;
            While(dmMain.tblTicker_Groups.EOF = FALSE) do
            begin
              if (dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblTicker_Groups.Delete;
              end
              else dmMain.tblTicker_Groups.Next;
            end;
          end;

          //Delete any associated elements
          if (dmMain.tblTicker_Elements.RecordCount > 0) then
          begin
            dmMain.tblTicker_Elements.First;
            While(dmMain.tblTicker_Elements.EOF = FALSE) do
            begin
              if (dmMain.tblTicker_Elements.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblTicker_Elements.Delete;
              end
              else dmMain.tblTicker_Elements.Next;
            end;
          end;
        except
          //Alert operator
          MessageDlg('An error occurred while trying to delete the existing copy of the ' +
                     'ticker playlist from the database.', mtError, [mbOK], 0)
        end;
      end;
      //Breaking News
   1: begin
        try
          if (dmMain.tblBreakingNews_Groups.RecordCount > 0) then
          begin
            dmMain.tblBreakingNews_Groups.First;
            While(dmMain.tblBreakingNews_Groups.EOF = FALSE) do
            begin
              if (dmMain.tblBreakingNews_Groups.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblBreakingNews_Groups.Delete;
              end
              else dmMain.tblBreakingNews_Groups.Next;
            end;
          end;

          //Delete any associated elements
          if (dmMain.tblBreakingNews_Elements.RecordCount > 0) then
          begin
            dmMain.tblBreakingNews_Elements.First;
            While(dmMain.tblBreakingNews_Elements.EOF = FALSE) do
            begin
              if (dmMain.tblBreakingNews_Elements.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblBreakingNews_Elements.Delete;
              end
              else dmMain.tblBreakingNews_Elements.Next;
            end;
          end;
        except
          //Alert operator
          MessageDlg('An error occurred while trying to delete the existing copy of the ' +
                     'Breaking News playlist from the database.', mtError, [mbOK], 0)
        end;
      end;
  end;
end;

//Procedure to load default zipper collection from database
function TMainForm.LoadPlaylistCollection (PlaylistType: SmallInt; AppendMode: SmallInt; PlaylistID: Double): String;
var
  i, j: SmallInt;
  InsertPoint: SmallInt;
  TickerRecPtr: ^TickerRec;
  SelectedPlaylistID: Double;
  ExpansionIndex: SmallInt;
  PlaylistName: String;
  FoundPlaylist: Boolean;
begin
  Case AppendMode of
      //Clear and load new collection
   CLEAR_AND_LOAD_PLAYLIST_MODE: begin
        Case PlaylistType of
            //Ticker
         TICKER: begin
              //Set controls to values from selected block
              SelectedPlaylistID := PlaylistID;
              try
                //Set flag
                LoadingPlaylist := TRUE;
                dmMain.tblTicker_Groups.Active := FALSE;
                dmMain.tblTicker_Groups.Active := TRUE;
                FoundPlaylist := dmMain.tblTicker_Groups.Locate('Playlist_ID', SelectedPlaylistID, []);

                if (FoundPlaylist) then
                  PlaylistName := dmMain.tblTicker_Groups.FieldByName('Playlist_Description').AsString
                else
                  PlaylistName := 'PLAYLIST NOT FOUND';

                if (FoundPlaylist) then
                begin
                  //Load the collection from the database
                  //Query for the elements table for those that match the playlist ID
                  dmMain.Query1.Close;
                  dmMain.Query1.SQL.Clear;
                  dmMain.Query1.SQL.Add('SELECT * FROM Ticker_Elements ' +
                                           'WHERE Playlist_ID = ' + FloatToStr(SelectedPlaylistID));
                  dmMain.Query1.Open;
                  if (dmMain.Query1.RecordCount > 0) then
                  begin
                    //Clear the collection - only if there are entries in the playlist
                    Ticker_Collection.Clear;
                    Ticker_Collection.Pack;
                    Ticker_Collection2.Clear;
                    Ticker_Collection2.Pack;
                    //Iterate through all records in the table
                    repeat
                      //Check for Fantasy Stats; if so, auto-expand to include specified entries for each stat
                      if (EngineInterface.TemplateIsFantasy(dmMain.Query1.FieldByName('Template_ID').AsInteger)) or
                         (EngineInterface.TemplateIsleaders(dmMain.Query1.FieldByName('Template_ID').AsInteger))then
                      begin
                        //If for SNY, use 2 per page - only need 5 pages
                        if (EngineInterface.TemplateIsFantasy(dmMain.Query1.FieldByName('Template_ID').AsInteger)) then
                        begin
                          if (FormatForSNY) and (dmMain.Query1.FieldByName('Template_ID').AsInteger > 1000) and
                             (dmMain.Query1.FieldByName('Template_ID').AsInteger < 10000) then
                            ExpansionIndex := FantasyExpansionPageCount DIV 2
                          else
                            ExpansionIndex := FantasyExpansionPageCount;
                        end
                        else if (EngineInterface.TemplateIsleaders(dmMain.Query1.FieldByName('Template_ID').AsInteger))then
                        begin
                          if (FormatForSNY) and (dmMain.Query1.FieldByName('Template_ID').AsInteger > 1000) and
                             (dmMain.Query1.FieldByName('Template_ID').AsInteger < 10000) then
                            ExpansionIndex := LeadersExpansionPageCount DIV 2
                          else
                            ExpansionIndex := LeadersExpansionPageCount;
                        end;

                        for j := 1 to ExpansionIndex do
                        begin
                          GetMem (TickerRecPtr, SizeOf(TickerRec));
                          With TickerRecPtr^ do
                          begin
                            //Set values for collection record
                            Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                            Field_ID := dmMain.Query1.FieldByName('Field_ID').AsInteger;
                            Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                            Is_Child := dmMain.Query1.FieldByName('Is_Child').AsBoolean;
                            Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                            League := dmMain.Query1.FieldByName('League').AsString;
                            Subleague_Mnemonic_Standard := dmMain.Query1.FieldByName('Subleague_Mnemonic_Standard').AsString;
                            Mnemonic_LiveEvent := dmMain.Query1.FieldByName('Mnemonic_LiveEvent').AsString;
                            Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                            Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                            GameID := dmMain.Query1.FieldByName('GameID').AsString;
                            SponsorLogo_Name := dmMain.Query1.FieldByName('SponsorLogoName').AsString;
                            SponsorLogo_Dwell := dmMain.Query1.FieldByName('SponsorLogoDwell').AsInteger;
                            StatStoredProcedure := dmMain.Query1.FieldByName('StatStoredProcedure').AsString;
                            StatType := dmMain.Query1.FieldByName('StatType').AsInteger;
                            StatTeam := dmMain.Query1.FieldByName('StatTeam').AsString;
                            StatLeague := dmMain.Query1.FieldByName('StatLeague').AsString;
                            StatRecordNumber := dmMain.Query1.FieldByName('StatRecordNumber').AsInteger;

                            //Store the index for the stat (1 - 10) in the first user data field
                            //If for SNY, use 2 per page - only need 5 pages
                            if (FormatForSNY) and (dmMain.Query1.FieldByName('Template_ID').AsInteger > 1000) and
                               (dmMain.Query1.FieldByName('Template_ID').AsInteger < 10000) then
                              UserData[1] := IntToStr(j + (j-1))
                            else
                              UserData[1] := IntToStr(j);

                            //Load nulls into all other user-defined text fields
                            for i := 2 to 50 do UserData[i] := '';
                            StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                            EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                            DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                            Description := dmMain.Query1.FieldByName('Description').AsString;
                            Selected := FALSE;
                            Comments := dmMain.Query1.FieldByName('Comments').AsString;
                            //Insert the object based on the field ID
                            Case dmMain.Query1.FieldByName('Field_ID').AsInteger of
                              FIELD_1: begin
                                Ticker_Collection.Insert(TickerRecPtr);
                                Ticker_Collection.Pack;
                              end;
                              FIELD_2: begin
                                Ticker_Collection2.Insert(TickerRecPtr);
                                Ticker_Collection2.Pack;
                              end;
                            end;
                          end;
                        end;
                      end
                      //Not a fantasy or leaders stat, so just add to collection
                      else begin
                        GetMem (TickerRecPtr, SizeOf(TickerRec));
                        With TickerRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                          Field_ID := dmMain.Query1.FieldByName('Field_ID').AsInteger;
                          Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                          Is_Child := dmMain.Query1.FieldByName('Is_Child').AsBoolean;
                          Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                          League := dmMain.Query1.FieldByName('League').AsString;
                          Subleague_Mnemonic_Standard := dmMain.Query1.FieldByName('Subleague_Mnemonic_Standard').AsString;
                          Mnemonic_LiveEvent := dmMain.Query1.FieldByName('Mnemonic_LiveEvent').AsString;
                          Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                          Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                          GameID := dmMain.Query1.FieldByName('GameID').AsString;
                          SponsorLogo_Name := dmMain.Query1.FieldByName('SponsorLogoName').AsString;
                          SponsorLogo_Dwell := dmMain.Query1.FieldByName('SponsorLogoDwell').AsInteger;
                          StatStoredProcedure := dmMain.Query1.FieldByName('StatStoredProcedure').AsString;
                          StatType := dmMain.Query1.FieldByName('StatType').AsInteger;
                          StatTeam := dmMain.Query1.FieldByName('StatTeam').AsString;
                          StatLeague := dmMain.Query1.FieldByName('StatLeague').AsString;
                          StatRecordNumber := dmMain.Query1.FieldByName('StatRecordNumber').AsInteger;
                          //Load the user-defined text fields
                          for i := 1 to 50 do
                            UserData[i] := dmMain.Query1.FieldByName('UserData_' + IntToStr(i)).AsString;
                          StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                          EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                          DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                          Description := dmMain.Query1.FieldByName('Description').AsString;
                          Selected := FALSE;
                          Comments := dmMain.Query1.FieldByName('Comments').AsString;
                          //Insert the object based on the field ID
                          Case dmMain.Query1.FieldByName('Field_ID').AsInteger of
                            FIELD_1: begin
                              Ticker_Collection.Insert(TickerRecPtr);
                              Ticker_Collection.Pack;
                            end;
                            FIELD_2: begin
                              Ticker_Collection2.Insert(TickerRecPtr);
                              Ticker_Collection2.Pack;
                            end;
                          end;
                        end;
                      end;
                      //Go to next record in query
                      dmMain.Query1.Next;
                    until (dmMain.Query1.EOF = TRUE);
                      //Refresh the playlist grids
                      RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
                      RefreshPlaylistGrid(FIELD_2, PlaylistGrid2);
                  end;
                  //Clear flags
                  LoadingPlaylist := FALSE;
                end;
              except
                if (ErrorLoggingEnabled = True) then
                  EngineInterface.WriteToErrorLog('Error occurred while trying to load main ticker playlist');
              end;
            end;
        end;
      end;
      //Append to existing collection
   APPEND_PLAYLIST_MODE: begin
        Case PlaylistType of
            //Ticker
         1: begin
              if (dmMain.tblTicker_Groups.RecordCount > 0) then
              begin
                //Load the collection from the database
                //Query for the elements table for those that match the playlist ID
                try
                  //Set flag
                  LoadingPlaylist := TRUE;
                  //Set controls to values from selected block
                  SelectedPlaylistID := PlaylistID;
                  dmMain.tblTicker_Groups.Locate('Playlist_ID', SelectedPlaylistID, []);

                  dmMain.Query1.Close;
                  dmMain.Query1.SQL.Clear;
                  dmMain.Query1.SQL.Add('SELECT * FROM Ticker_Elements ' +
                                           'WHERE Playlist_ID = ' + FloatToStr(SelectedPlaylistID));
                  dmMain.Query1.Open;
                  if (dmMain.Query1.RecordCount > 0) then

                  //Iterate through all records in the table
                  repeat
                    //Check for Fantasy Stats; if so, auto-expand to include specified entries for each stat
                    if (EngineInterface.TemplateIsFantasy(dmMain.Query3.FieldByName('Template_ID').AsInteger)) or
                       (EngineInterface.TemplateIsLeaders(dmMain.Query3.FieldByName('Template_ID').AsInteger)) then
                    begin
                      //If for SNY, use 2 per page - only need 5 pages
                      if (EngineInterface.TemplateIsFantasy(dmMain.Query1.FieldByName('Template_ID').AsInteger)) then
                      begin
                        if (FormatForSNY) and (dmMain.Query1.FieldByName('Template_ID').AsInteger > 1000) and
                           (dmMain.Query1.FieldByName('Template_ID').AsInteger < 10000) then
                          ExpansionIndex := FantasyExpansionPageCount DIV 2
                        else
                          ExpansionIndex := FantasyExpansionPageCount;
                      end
                      else if (EngineInterface.TemplateIsleaders(dmMain.Query1.FieldByName('Template_ID').AsInteger))then
                      begin
                        if (FormatForSNY) and (dmMain.Query1.FieldByName('Template_ID').AsInteger > 1000) and
                           (dmMain.Query1.FieldByName('Template_ID').AsInteger < 10000) then
                          ExpansionIndex := LeadersExpansionPageCount DIV 2
                        else
                          ExpansionIndex := LeadersExpansionPageCount;
                      end;

                      for j := 1 to ExpansionIndex do
                      begin
                        GetMem (TickerRecPtr, SizeOf(TickerRec));
                        With TickerRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                          Field_ID := dmMain.Query1.FieldByName('Field_ID').AsInteger;
                          Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                          Is_Child := dmMain.Query1.FieldByName('Is_Child').AsBoolean;
                          Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                          League := dmMain.Query1.FieldByName('League').AsString;
                          Subleague_Mnemonic_Standard := dmMain.Query1.FieldByName('Subleague_Mnemonic_Standard').AsString;
                          Mnemonic_LiveEvent := dmMain.Query1.FieldByName('Mnemonic_LiveEvent').AsString;
                          Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                          Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                          GameID := dmMain.Query1.FieldByName('GameID').AsString;
                          SponsorLogo_Name := dmMain.Query1.FieldByName('SponsorLogoName').AsString;
                          SponsorLogo_Dwell := dmMain.Query1.FieldByName('SponsorLogoDwell').AsInteger;
                          StatStoredProcedure := dmMain.Query1.FieldByName('StatStoredProcedure').AsString;
                          StatType := dmMain.Query1.FieldByName('StatType').AsInteger;
                          StatTeam := dmMain.Query1.FieldByName('StatTeam').AsString;
                          StatLeague := dmMain.Query1.FieldByName('StatLeague').AsString;
                          StatRecordNumber := dmMain.Query1.FieldByName('StatRecordNumber').AsInteger;

                          //Store the index for the stat (1 - 10) in the first user data field
                          //If for SNY, use 2 per page - only need 5 pages
                          if (FormatForSNY) and (dmMain.Query1.FieldByName('Template_ID').AsInteger > 1000) and
                             (dmMain.Query1.FieldByName('Template_ID').AsInteger < 10000) then
                            UserData[1] := IntToStr(j + (j-1))
                          else
                            UserData[1] := IntToStr(j);

                          //Load nulls into all other user-defined text fields
                          for i := 2 to 50 do UserData[i] := '';
                          StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                          EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                          DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                          Description := dmMain.Query1.FieldByName('Description').AsString;
                          Selected := FALSE;
                          Comments := dmMain.Query1.FieldByName('Comments').AsString;
                          //Insert the object based on the field ID
                          Case dmMain.Query1.FieldByName('Field_ID').AsInteger of
                            FIELD_1: begin
                              if (PlaylistGrid.CurrentDataRow < 1) then PlaylistGrid.CurrentDataRow := 1;
                              InsertPoint := PlaylistGrid.CurrentDataRow-1;
                              if (AppendMode = 1) then
                                Ticker_Collection.Insert(TickerRecPtr)
                              else if (AppendMode = 2) then
                                Ticker_Collection.AtInsert(InsertPoint, TickerRecPtr);
                              Ticker_Collection.Pack;
                            end;
                            FIELD_2: begin
                              if (PlaylistGrid2.CurrentDataRow < 1) then PlaylistGrid2.CurrentDataRow := 1;
                              InsertPoint := PlaylistGrid2.CurrentDataRow-1;
                              if (AppendMode = 1) then
                                Ticker_Collection2.Insert(TickerRecPtr)
                              else if (AppendMode = 2) then
                                Ticker_Collection2.AtInsert(InsertPoint, TickerRecPtr);
                              Ticker_Collection2.Pack;
                            end;
                          end;
                        end;
                      end;
                    end
                    //Not a fantasy or leaders stat, so just add to collection
                    else begin
                      GetMem (TickerRecPtr, SizeOf(TickerRec));
                      With TickerRecPtr^ do
                      begin
                        //Set values for collection record
                        Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                        Field_ID := dmMain.Query1.FieldByName('Field_ID').AsInteger;
                        Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                        Is_Child := dmMain.Query1.FieldByName('Is_Child').AsBoolean;
                        Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                        League := dmMain.Query1.FieldByName('League').AsString;
                        Subleague_Mnemonic_Standard := dmMain.Query1.FieldByName('Subleague_Mnemonic_Standard').AsString;
                        Mnemonic_LiveEvent := dmMain.Query1.FieldByName('Mnemonic_LiveEvent').AsString;
                        Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                        Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                        GameID := dmMain.Query1.FieldByName('GameID').AsString;
                        SponsorLogo_Name := dmMain.Query1.FieldByName('SponsorLogoName').AsString;
                        SponsorLogo_Dwell := dmMain.Query1.FieldByName('SponsorLogoDwell').AsInteger;
                        StatStoredProcedure := dmMain.Query1.FieldByName('StatStoredProcedure').AsString;
                        StatType := dmMain.Query1.FieldByName('StatType').AsInteger;
                        StatTeam := dmMain.Query1.FieldByName('StatTeam').AsString;
                        StatLeague := dmMain.Query1.FieldByName('StatLeague').AsString;
                        StatRecordNumber := dmMain.Query1.FieldByName('StatRecordNumber').AsInteger;
                        //Load the user-defined text fields
                        for i := 1 to 50 do
                          UserData[i] := dmMain.Query1.FieldByName('UserData_' + IntToStr(i)).AsString;
                        StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                        EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                        DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                        Description := dmMain.Query1.FieldByName('Description').AsString;
                        Selected := FALSE;
                        Comments := dmMain.Query1.FieldByName('Comments').AsString;
                        //Insert the object based on the field ID
                        Case dmMain.Query1.FieldByName('Field_ID').AsInteger of
                          FIELD_1: begin
                            if (PlaylistGrid.CurrentDataRow < 1) then PlaylistGrid.CurrentDataRow := 1;
                            InsertPoint := PlaylistGrid.CurrentDataRow-1;
                            if (AppendMode = 1) then
                              Ticker_Collection.Insert(TickerRecPtr)
                            else if (AppendMode = 2) then
                              Ticker_Collection.AtInsert(InsertPoint, TickerRecPtr);
                            Ticker_Collection.Pack;
                          end;
                          FIELD_2: begin
                            if (PlaylistGrid2.CurrentDataRow < 1) then PlaylistGrid2.CurrentDataRow := 1;
                            InsertPoint := PlaylistGrid2.CurrentDataRow-1;
                            if (AppendMode = 1) then
                              Ticker_Collection2.Insert(TickerRecPtr)
                            else if (AppendMode = 2) then
                              Ticker_Collection2.AtInsert(InsertPoint, TickerRecPtr);
                            Ticker_Collection2.Pack;
                          end;
                        end;
                      end;
                    end;
                    //Go to next record in query
                    dmMain.Query1.Next;
                  until (dmMain.Query1.EOF = TRUE);
                  //Refresh the grids
                  RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
                  RefreshPlaylistGrid(FIELD_2, PlaylistGrid2);
                  //Clear flag
                  LoadingPlaylist := FALSE;
               except
                  if (ErrorLoggingEnabled = True) then
                    EngineInterface.WriteToErrorLog('Error occurred while trying to load main ticker playlist');
                end;
              end;
            end;
            //Breaking News
         2: begin
            end;
        end;
      end;
  end;
  //Return
  LoadPlaylistCollection := PlaylistName;
end;

////////////////////////////////////////////////////////////////////////////////
// TRIGGER CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Handler to trigger the ticker
procedure TMainForm.TriggerTickerButtonClick(Sender: TObject);
begin
  TriggerCurrentTicker;
end;

////////////////////////////////////////////////////////////////////////////////
//Procedure to trigger Currently scheduled ticker
////////////////////////////////////////////////////////////////////////////////
procedure TMainForm.TriggerCurrentTicker;
begin
  //Make sure there is data in the zipper collection
  if ((Ticker_Collection.Count > 0) or (Ticker_Collection2.Count > 0)) AND (RunningTicker = FALSE) AND (InitialSceneLoadComplete) then
  begin
    //Init for top image (marketing) area
    TopRegionToggleState := -1;

    //Set flag to indicate we're running the default zipper; also prevents search for
    //new playlist in schedule while running stored procedures
    RunningTicker := TRUE;

    //Disable reset to top button
    ResetTickerButton.Enabled := FALSE;

    //Make sure packet timeout timer is disabled
    EngineInterface.TickerPacketTimeoutTimer.Enabled := FALSE;

    //Reset sponsor logo dwell counter
    SponsorLogoDwellCounter := 9999;

    //Set labels
    TickerStatusLabel.Color := clLime;
    TickerStatusLabel.Caption := 'PLAYLIST RUNNING';
    LastCommandLabel.Caption := 'Trigger Current Ticker';

    //Clear the abort flag
    TickerAbortFlag := FALSE;
    //Refresh the zipper grid
    RefreshPlaylistGrid(FIELD_1, MainForm.PlaylistGrid);
    //Set the data
    EngineInterface.StartTickerData;

    //Added for Version 2.0
    EngineInterface.SetNextTopGraphic;

    //If Current Entry index is 0, where at the top, so restart the wheel timer
    if (CurrentTickerEntryIndex1 = 0) then ElapsedTimeThisWheel := 0;

    //Echo back status to Authoring
    AuthoringInterface.SendAutoSequenceStatus(TRUE);

    //Bring in traffic crawl if enabled
    if (TrafficCrawlEnabled) then
    begin
      Sleep(500);
      TriggerTrafficCrawl;
    end;
  end;
end;

//Handler for abort ticker event
procedure TMainForm.AbortPlaylistButtonClick(Sender: TObject);
begin
  if (RunningTicker) then
  begin
    //Abort
    AbortCurrentEvent(IS_NOT_STARTUP);
  end;
end;

//Procedure for aborting ticker
procedure TMainForm.AbortCurrentEvent(IsStartup: Boolean);
var
  SavePos: SmallInt;
begin
  //Clear crawl if it's in
  if (TrafficCrawlDataIn) then
  begin
    ClearTrafficCrawl;
    TrafficCrawlDataIn := FALSE;
  end;

  //Clear flags & handles
  LastSegmentHeading := '';
  RunningTicker := FALSE;

  //Make sure packet timeout timer is disabled
  EngineInterface.TickerPacketTimeoutTimer.Enabled := FALSE;
  EngineInterface.TickerPacketTimeoutTimer2.Enabled := FALSE;

  //Disable next command delay timers
  EngineInterface.TickerCommandDelayTimer.Enabled := FALSE;
  EngineInterface.TickerCommandDelayTimer2.Enabled := FALSE;

  //Added for Version 2.0 - disable dwell time for top image (marketing) region
  EngineInterface.TopGraphicDwellTimer.Enabled := FALSE;

  //Enable reset to top button
  ResetTickerButton.Enabled := TRUE;

  //Set labels
  TickerStatusLabel.Color := clRed;
  TickerStatusLabel.Caption := 'PLAYLIST STOPPED';
  LastCommandLabel.Caption := 'Playlist Stopped';

  //Set the abort flag & reset zipper entry index
  TickerAbortFlag := TRUE;

  //Set controls to abort current event locally
  //EngineInterface.TriggerGraphic(2);

  //Clear data and field backplate panels if enabled in INI file
  if (ClearDataPanelsOnAbort) then
  begin
    if (IsStartup) then
      EngineInterface.ClearFieldPanels(DONT_STOP_SCENES)
    else
      EngineInterface.ClearFieldPanels(STOP_SCENES);
  end;

  //Search to see if there's a new playlist scheduled immediately
  //ticker mode
  SearchForNextScheduledPlaylist(FALSE);

  //Echo back status to Authoring
  AuthoringInterface.SendAutoSequenceStatus(FALSE);
end;

procedure TMainForm.TimeOfDayTimerTimer(Sender: TObject);
var
  Hour, Min, Sec: Word;
  TimeStr: String;
  //For National Instruments card
//  iDevice: i16;
//  iStatus: i16;
//  iPort: i16;
//  iLine: i16;
//  iState: i16;
begin
  //Increment sponsor logo dwell counter if ticker is running
  if (RunningTicker = TRUE) then
  begin
    Inc(SponsorLogoDwellCounter);
    //Update the elapsed time counter
    Inc(ElapsedTimeThisWheel);
    Hour := ElapsedTimeThisWheel DIV (60*60);
    Min :=  ElapsedTimeThisWheel DIV 60 - (Hour*60);
    Sec :=  ElapsedTimeThisWheel - ((Hour*60*60) + (Min*60));
    TimeStr := Format('%2.2d:%2.2d:%2.2d', [Hour, Min, Sec]);

    //Process GPI for standard/live event mode
    //Init DIO card variables
//    iDevice := 1;
    //Only proceed if GPIs enabled
    if (GPIsEnabled = TRUE) AND (GPIsOKToFire) then
    begin
      //Start sampling GPIs; setup port #0 for input
//      iStatus := DIG_Prt_Config(iDevice, 0, 0, 0);
      //Do bit #0; Go to live event mode
//      iLine := 0;
//      iStatus := DIG_In_Line(iDevice, iPort, iLine, @iState);
      //Go to live event mode if in standard mode and GPI is active
//      if (iState = 0) AND (CurrentTickerDisplayMode = -1) then
//      begin
        //Process GPI
        //Disable GPIs and enable GPI reset timer
//        GPIsOKToFire := FALSE;
//        GPIResetTimer.Enabled := TRUE;
//      end
//      //Go to standard mode if in live event mode and GPI is not active
//      else if (iState = 1) AND (CurrentTickerDisplayMode = 1) then
//      begin
        //Process GPI
        //Disable GPIs and enable GPI reset timer
//        GPIsOKToFire := FALSE;
//        GPIResetTimer.Enabled := TRUE;
//      end;
    end;
  end;
  //Update clock labels
  Label2.Caption := DateToStr(Now);
  Label3.Caption := TimeToStr(Now);
end;

//Handler to flash the error panel red if an error occurs
procedure TMainForm.ErrorFlashTimerTimer(Sender: TObject);
begin
  //If error condition flag is set, start flashing status box red
  If (Error_Condition = True) then
  begin
     if (Panel3.Color = clBtnFace) then Panel3.Color := clRed
     else Panel3.Color := clBtnFace;
  end;
end;

//Handler for check to new scheduled playlist
procedure TMainForm.PlaylistRefreshTimerTimer(Sender: TObject);
begin
  //Not for looping ticker mode
  SearchForNextScheduledPlaylist(FALSE);
end;
//Spawn thread to check for new schedule
procedure TMainForm.SearchForNextScheduledPlaylist(LoopingTicker: Boolean);
var
  SaveTickerEntryIndex: SmallInt;
  SaveRow: SmallInt;
begin
  //Modified to allow thread to run if ticker is running so it picks up alerts
  //if (RunningThread = FALSE) AND (RunningTicker = FALSE) then
  if (RunningThread = FALSE) then
  begin
    //Check for schedule monitoring enable
    if (ScheduleMonitoringEnabled) then
    begin
      //Spawn thread to check ticker playlist schedule
      CheckTickerScheduleThread := TCheckTickerSchedule.Create(TRUE);
      CheckTickerScheduleThread.Priority := tpLower;
      CheckTickerScheduleThread.FreeOnTerminate := TRUE;
      //Execute the thread
      try
        CheckTickerScheduleThread.Resume;
      except
        CheckTickerScheduleThread.Free;
        if (ErrorLoggingEnabled = True) then
          EngineInterface.WriteToErrorLog('Exception occurred in thread for checking ticker schedule');
      end;
    end
    //Manual mode - only update if ticker not running and NOT in remote control mode from Authroing tool
    else if not (AuthoringRemoteControlModeEnabled) then
    begin
      //Spawn thread - will check for Breaking News only if ticker is not running
      CheckTickerScheduleThread := TCheckTickerSchedule.Create(TRUE);
      CheckTickerScheduleThread.Priority := tpLower;
      CheckTickerScheduleThread.FreeOnTerminate := TRUE;
      //Execute the thread
      try
        CheckTickerScheduleThread.Resume;
      except
        CheckTickerScheduleThread.Free;
        if (ErrorLoggingEnabled = True) then
          EngineInterface.WriteToErrorLog('Exception occurred in thread for checking ticker schedule');
      end;
      //Reload playlist if ticker not running
      if (RunningTicker = FALSE) then
      begin
        //Save current index so it can be restored
        SaveTickerEntryIndex := CurrentTickerEntryIndex1;
        SaveRow := PlaylistGrid.CurrentDataRow;
        //Load the manual collection
        LoadPlaylistCollection(1, 0, CurrentTickerPlaylistID);
        RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
        if (CurrentTickerEntryIndex1 <= Ticker_Collection.Count-1) then
        begin
          CurrentTickerEntryIndex1 := SaveTickerEntryIndex;
          if (SaveRow <= Ticker_Collection.Count) then PlaylistGrid.CurrentDataRow := SaveRow;
        end
        else begin
          CurrentTickerEntryIndex1 := 0;
          if (Ticker_Collection.Count > 0) then
            PlaylistGrid.CurrentDataRow := 1;
        end;
      end;
    end;
  end;
end;
//GPI reset timer
procedure TMainForm.GPIResetTimerTimer(Sender: TObject);
begin
  //Disable timer to prevent retrigger
  GPIResetTimer.Enabled := FALSE;
  //Re-enable GPIs
  GPIsOKToFire := TRUE;
  //Turn off LEDs
  ApdStatusLight3.Lit := FALSE;
  ApdStatusLight4.Lit := FALSE;
end;

////////////////////////////////////////////////////////////////////////////////
// This procedure sets up a timer to trigger 1 second after program startup.
// When triggered, database queries and the input com port are enabled.
////////////////////////////////////////////////////////////////////////////////
procedure TMainForm.GPIInitTimerTimer(Sender: TObject);
begin
  //Disable timer to prevent retrigger
  GPIInitTimer.Enabled := FALSE;
  //Enable GPIs to fire
  GPIsOKToFire := TRUE;
end;

//Handler for reset ticker to top
procedure TMainForm.ResetTickerButtonClick(Sender: TObject);
begin
  ResetTickerToTop;
end;
procedure TMainForm.ResetTickerToTop;
begin
  //Set the zipper story to start with
  CurrentTickerEntryIndex1 := 0;
  CurrentTickerEntryIndex2 := 0;
  CurrentBreakingNewsEntryIndex := 0;
  //Set data row indicator in grids
  MainForm.PlaylistGrid.CurrentDataRow := CurrentTickerEntryIndex1+1;
  MainForm.PlaylistGrid.SetTopLeft(1, MainForm.PlaylistGrid.CurrentDataRow);
  MainForm.PlaylistGrid2.CurrentDataRow := CurrentTickerEntryIndex2+1;
  MainForm.PlaylistGrid2.SetTopLeft(1, MainForm.PlaylistGrid2.CurrentDataRow);
  //Update indices on Authoring
  AuthoringInterface.SendPlaylistIndex(PLAYLIST_1, CurrentTickerEntryIndex1);
  AuthoringInterface.SendPlaylistIndex(PLAYLIST_2, CurrentTickerEntryIndex2);
end;

//Handler to enable error logging
procedure TMainForm.EnableErrorChecking1Click(Sender: TObject);
begin
  ErrorLoggingEnabled := True;
  EnableErrorChecking1.Checked := True;
  DisableErrorChecking1.Checked := False;
  StaticText27.Visible := False;
end;

//Handler to disable error logging
procedure TMainForm.DisableErrorChecking1Click(Sender: TObject);
begin
  ErrorLoggingEnabled := False;
  EnableErrorChecking1.Checked := False;
  DisableErrorChecking1.Checked := True;
  StaticText27.Visible := True;
  Panel3.Color := clBtnFace;
end;

//Handler to reset error
procedure TMainForm.ResetError1Click(Sender: TObject);
begin
  //Reset error flag and status panel color
  Error_Condition := False;

  //Reset captions for labels on status panel
  Label5.Caption := 'OK';
  Panel3.Color := clBtnFace;
end;

////////////////////////////////////////////////////////////////////////////////
// This procedure processes exceptions due to database connection problems
////////////////////////////////////////////////////////////////////////////////
procedure TMainForm.DatabaseConnectError(ExceptionString: String);
begin
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    Label15.Caption := 'ERROR';

    //WriteToErrorLog procedure defined in Engine Interface unit
    EngineInterface.WriteToErrorLog (ExceptionString);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// This procedure processes exceptions due to database engine problems
////////////////////////////////////////////////////////////////////////////////
procedure TMainForm.DatabaseEngineError(ExceptionString: String);
begin
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    Label15.Caption := 'ERROR';

    //WriteToErrorLog procedure defined in Engine Interface unit
    EngineInterface.WriteToErrorLog (ExceptionString);
  end;
end;

procedure TMainForm.SetGraphicsEngineConnectionPreferences1Click(
  Sender: TObject);
var
  Modal: TEnginePrefsDlg;
  Control: Word;
begin
  Modal := TEnginePrefsDlg.Create (Application);
  //Pre-load with values read from prefs file; if no prefs file is
  //found, the default values are loaded
  Modal.Edit2.Text := EngineParameters.IPAddress;
  Modal.Edit3.Text := EngineParameters.Port;
  //Note: Service information setup dynamically during dialog creation V3.0
  try
    Control := Modal.ShowModal;
  finally
    //If confirmed, set new values and store prefs
    if (Control = mrOK) then
    begin
      EngineParameters.IPAddress := Trim(Modal.Edit2.Text);
      EngineParameters.Port := Trim(Modal.Edit3.Text);
      //Store the preferences
      StorePrefs;
    end;
    Modal.Free;
  end;
end;

//Handler to reconnect to graphics engine
procedure TMainForm.ReconnecttoGraphicsEngine1Click(Sender: TObject);
begin
  //Connect to the graphics engine if it's enabled
  try
      EngineInterface.EnginePort.Active := FALSE;
      EngineInterface.EnginePort.Address := EngineParameters.IPAddress;
      EngineInterface.EnginePort.Port := StrToIntDef(EngineParameters.Port, 7795);
      EngineInterface.EnginePort.Active := TRUE;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      //WriteToErrorLog
      EngineInterface.WriteToErrorLog('Error occurred while trying connect to database engine');
    end;
  end;
end;

//Handler for selecting playlist for manual load
procedure TMainForm.BitBtn4Click(Sender: TObject);
var
  Modal: TPlaylistSelectDlg;
  Control: Word;
begin
  if (RunningTicker) then
    MessageDlg('You cannot load a playlist while the L-Bar is running!', mtWarning, [mbOK], 0)
  else begin
    //Display dialog for playlist selection
    Modal := TPlaylistSelectDlg.Create (Application);
    try
      //Refresh playlists table
      dmMain.tblTicker_Groups.Active := FALSE;
      dmMain.tblTicker_Groups.Active := TRUE;
      dmMain.tblTicker_Groups.First;
      //Set flag to prevent thread from updating playlist
      RunningThread := TRUE;
      Control := Modal.ShowModal;
    finally
      //If confirmed, set new values and store prefs
      if (Control = mrOK) then
      begin
        //Load the collection; set flag to indicate it's the first time through
        LoadPlaylistCollection(1, 0, dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsFloat);

        //Reset ticker to top
        ResetTickerToTop;

        //Refresh the zipper grid and status labels
        //Label5Caption := dmMain.tblTicker_Groups.FieldByName('Playlist_Description').AsString;
        RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
        //Set the current playlist ID - allows for refresh of collection of more than one iteration
        CurrentTickerPlaylistID := dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsFloat;

        //Update data fields
        ScheduledPlaylistInfo[1].Playlist_Description := dmMain.tblTicker_Groups.FieldByName('Playlist_Description').AsString;
        ScheduledPlaylistInfo[1].Playlist_ID := dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsFloat;
        ScheduledPlaylistInfo[1].Station_ID := dmMain.tblTicker_Groups.FieldByName('Station_ID').AsInteger;
        ScheduledPlaylistInfo[1].Ticker_Mode := dmMain.tblTicker_Groups.FieldByName('Ticker_Mode').AsInteger;

        MainForm.TickerPlaylistName.Caption := ScheduledPlaylistInfo[1].Playlist_Description;
        MainForm.TickerStartTimeLabel.Caption := 'N/A';
        MainForm.TickerEndTimeLabel.Caption := 'N/A';

        //Set the ticker mode and refresh the controls
        TickerDisplayMode := dmMain.tblTicker_Groups.FieldByName('Ticker_Mode').AsInteger;
        Case TickerDisplayMode of
             //1-line, 1-time
          1: begin
               //MainForm.OneTimeLoopModeLabel.Visible := TRUE;
               MainForm.TickerMode.Caption := '1-LINE MODE';
             end;
             //1-line, loop
          2: begin
               //MainForm.OneTimeLoopModeLabel.Visible := FALSE;
               MainForm.TickerMode.Caption := '1-LINE MODE';
             end;
             //2-line, 1-time
          3: begin
               //MainForm.OneTimeLoopModeLabel.Visible := TRUE;
               MainForm.TickerMode.Caption := '2-LINE MODE';
             end;
             //2-line, loop
          4: begin
               //MainForm.OneTimeLoopModeLabel.Visible := FALSE;
               MainForm.TickerMode.Caption := '2-LINE MODE';
             end;
        end;
      end;
      Modal.Free;
      //Set flag to prevent thread from updating playlist
      RunningThread := FALSE;
    end;
  end;
end;

procedure TMainForm.ScheduleMonitoringEnabledCheckBoxClick(Sender: TObject);
begin
  //Set state of schedule monitoring
  if (ScheduleMonitoringEnabledCheckBox.Checked = TRUE) then
  begin
    ScheduleMonitoringEnabledCheckBox.Caption := 'Scheduled Playlist Enabled';
    ScheduleMonitoringEnabledCheckBox.Color := clBtnFace;
    ScheduleMonitoringEnabled := TRUE;
    BitBtn4.Visible := FALSE;
    Label6.Visible := FALSE;
    //Enable timer to check for scheduled playlist
    //PlaylistRefreshTimer.Enabled := TRUE;
  end
  //In manual mode
  else begin
    ScheduleMonitoringEnabledCheckBox.Caption := 'Scheduled Playlist Disabled';
    ScheduleMonitoringEnabledCheckBox.Color := clYellow;
    //Clear flag; disble controls; show label
    ScheduleMonitoringEnabled := FALSE;
    BitBtn4.Visible := TRUE;
    Label6.Visible := TRUE;
    //Disable timer to check for scheduled playlist
    //PlaylistRefreshTimer.Enabled := FALSE;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES TO LOAD & STORE PREFS HERE BEACUSAE OF WEIRD DEBUGGER ISSUES -
// BREAKPOINTS NOT IN CORRECT PLACE
////////////////////////////////////////////////////////////////////////////////
//Procedure to load user preferences
procedure TMainForm.LoadPrefs;
var
  PrefsINI : TINIFile;
begin
  try
    //Create the INI file
    PrefsINI := TINIFile.Create(ExtractFilePath(Application.EXEName) + 'Comcast_Playout.ini');
    //Read the settings
    ForceUpperCase := PrefsINI.ReadBool('General Settings', 'Force Upper Case', FALSE);
    GPIsEnabled := PrefsINI.ReadBool('General Settings', 'GPIs Enabled', FALSE);
    GPICOMPortNum := PrefsINI.ReadInteger('General Settings', 'GPI COM Port', 1);
    GPIBaudRate:= PrefsINI.ReadInteger('General Settings', 'GPI Baud Rate', 19200);
    ScheduleMonitoringEnabled := PrefsINI.ReadBool('General Settings', 'Schedule Monitoring Enabled', TRUE);
    FullSponsorPagesEnabled := PrefsINI.ReadBool('General Settings', 'Full Sponsor Pages Enabled', FALSE);
    TailSponsorPagesEnabled := PrefsINI.ReadBool('General Settings', 'Tail Sponsor Pages Enabled', FALSE);
    //Get game start time offset - expressed in hours, so divide by 24 to get value
    GameStartTimeOffset := (PrefsINI.ReadInteger('General Settings', 'Game Start Time Offset from ET (hours)', 0))/24;
    TimeZoneSuffix := PrefsINI.ReadString('General Settings', 'Time Zone Suffix', 'ET');
    StationID := PrefsINI.ReadInteger('General Settings', 'Station ID Number', 1);
    StationDescription := PrefsINI.ReadString('General Settings', 'Station Description', 'SF BAY AREA');
    EnableTemplateFieldFormatting := PrefsINI.ReadBool('General Settings', 'Enable Template Field Formatting', TRUE);
    EnablePersistentSponsorLogo := PrefsINI.ReadBool('General Settings', 'Enable Persistent Sponsor Logo', TRUE);
    BlankOutLineupForSponsors := PrefsINI.ReadBool('General Settings', 'Blank Out Lineup For Sponsors', TRUE);
    AllowAlertBackgroundsForNews := PrefsINI.ReadBool('General Settings', 'Allow Alert Backgrounds for News', FALSE);

    //For fantasy stats - used to trigger expansion into multiple templates
    FantasyExpansionPageCount := PrefsINI.ReadInteger('General Settings', 'Fantasy Expansion Page Count (must be even)', 10);
    //For leader stats - used to trigger expansion into multiple templates
    LeadersExpansionPageCount := PrefsINI.ReadInteger('General Settings', 'Leaders Expansion Page Count (must be even)', 10);

    ShowTimeAndDay := PrefsINI.ReadBool('General Settings', 'Show Time and Day for Schedule', FALSE);

    FormatForSNY := PrefsINI.ReadBool('General Settings', 'Format for SNY', FALSE);

    UseManualRankings_NCAAB := PrefsINI.ReadBool('General Settings', 'Use Manual Rankings (NCCAB)', FALSE);
    UseManualRankings_NCAAF := PrefsINI.ReadBool('General Settings', 'Use Manual Rankings (NCCAF)', FALSE);
    UseManualRankings_NCAAW := PrefsINI.ReadBool('General Settings', 'Use Manual Rankings (NCCAW)', FALSE);
    UseSeedingForNCAABGames := PrefsINI.ReadBool('General Settings', 'Use Seeding for NCAAB Games', FALSE);

    AsRunLogFileDirectoryPath := PrefsINI.ReadString('General Settings', 'Sponsor As-Run Logfile Directory Path', 'c:\Comcast_AsRun_LogFiles\');

    LoadScenesAtStartup := PrefsINI.ReadBool('General Settings', 'Load Scenes at Startup', FALSE);

    ClearDataPanelsOnAbort := PrefsINI.ReadBool('General Settings', 'Clear Data Panels on Abort', TRUE);

    AlwaysPlayAllScenesAtStartup := PrefsINI.ReadBool('General Settings', 'Always Play All Scenes at Startup', TRUE);

    CommandLoggingEnabled := PrefsINI.ReadBool('General Settings', 'Enable Command Logging', TRUE);

    TemplateFieldUpdatesEnabled := PrefsINI.ReadBool('General Settings', 'Enable Template Field Updates', TRUE);

    CrawlFeedbackPortEnabled := PrefsINI.ReadBool('General Settings', 'Crawl Feedback Port Enabled', TRUE);

    //Set labels
    StationIDNumLabel.Caption := IntToStr(StationID);
    StationIDLabel.Caption := StationDescription;
    DBConnectionString := PrefsINI.ReadString('Database Connections', 'Ticker Database Connection String',
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;User ID=sa;' +
      'Initial Catalog=SNYTicker;Data Source=(local)');
    DBConnectionString2 := PrefsINI.ReadString('Database Connections', 'Sportbase Database Connection String',
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;User ID=sa;' +
      'Initial Catalog=Sportbase;Data Source=(local)');
    DBConnectionString3 := PrefsINI.ReadString('Database Connections', 'Traffic Database Connection String',
      'Provider=SQLOLEDB.1;Persist Security Info=True;User ID=sa;Password=Vds@dmin1;' +
      'Initial Catalog=Metro_Traffic;Data Source=(local)');

    EngineParameters.IPAddress := PrefsINI.ReadString('Ticker Engine Connection', 'IP Address',
      '127.0.0.1');
    EngineParameters.Port := PrefsINI.ReadString('Ticker Engine Connection', 'Port', '7795');
    //Game Final Glyph variables
    UseGameFinalGlyph := PrefsINI.ReadBool('Format Settings', 'Enable Game Final Glyph', FALSE);
    GameFinalGlyphCharacterID  := PrefsINI.ReadInteger('Format Settings', 'Game Final Glyph Character ID', 239);

    //For remote connection to Authoring tool
    AuthoringControlPortNum := PrefsINI.ReadString('Authoring Control Connection', 'Port', '3390');
    AuthoringControlInterfaceEnabled := PrefsINI.ReadBool('Authoring Control Connection', 'Enable', TRUE);

    //For image paths
    League_Logo_Mapped_Drive := PrefsINI.ReadString('Image Path Settings', 'League Logo Mapped Drive', 'E:');
    League_Logo_Host_Drive := PrefsINI.ReadString('Image Path Settings', 'League Logo Host Drive', 'C:\VDS_Projects');
    League_Logo_Base_Path := PrefsINI.ReadString('Image Path Settings', 'League Logo Base Path', 'VDS_Projects\Comcast_2011\CSNMA_LBar\Logos');
    Team_Logo_Mapped_Drive := PrefsINI.ReadString('Image Path Settings', 'Team Logo Mapped Drive', 'E:');
    Team_Logo_Host_Drive := PrefsINI.ReadString('Image Path Settings', 'Team Logo Host Drive', 'C:\VDS_Projects');
    Team_Logo_Base_Path := PrefsINI.ReadString('Image Path Settings', 'Team Logo Base Path', 'VDS_Projects\Comcast_2011\CSNMA_LBar\Logos');
    Player_Headshot_Mapped_Drive := PrefsINI.ReadString('Image Path Settings', 'Player Headshot Mapped Drive', 'E:');
    Player_Headshot_Host_Drive := PrefsINI.ReadString('Image Path Settings', 'Player Headshot Host Drive', 'C:\VDS_Projects');
    Player_Headshot_Base_Path := PrefsINI.ReadString('Image Path Settings', 'Player Headshot Base Path', 'VDS_Projects\Comcast_2011\CSNMA_LBar\Headshots');
    Weather_Icon_Base_Path := PrefsINI.ReadString('Image Path Settings', 'Weather Icon Base Path', 'c:\VDS_Projects\Comcast_2011\CSNMA_LBar\Weather_Icons');
    Sponsor_Logo_Base_Path := PrefsINI.ReadString('Image Path Settings', 'Sponsor Logo Base Path', 'c:\VDS_Projects\Comcast_2011\CSNMA_LBar\Sponsors');
    //Added for Version 2.0
    Full_Promo_Graphic_Mapped_Drive := PrefsINI.ReadString('Image Path Settings', 'Full Promo Graphic Mapped Drive', 'E:');
    Full_Promo_Graphic_Host_Drive := PrefsINI.ReadString('Image Path Settings', 'Full Promo Graphic Host Drive', 'C:\VDS_Projects');
    Full_Promo_Graphic_Base_Path := PrefsINI.ReadString('Image Path Settings', 'Full Promo Graphic Base Path', 'VDS_Projects\Comcast_2011\CSNMA_LBar\Promos');
    Top_Region_Image_Mapped_Drive := PrefsINI.ReadString('Image Path Settings', 'Top Region Image Mapped Drive', 'E:');
    Top_Region_Image_Base_Path := PrefsINI.ReadString('Image Path Settings', 'Top Region Image Host Drive', 'C:\VDS_Projects');
    Top_Region_Image_Host_Drive := PrefsINI.ReadString('Image Path Settings', 'Top Region Image Base Path', 'VDS_Projects\Comcast_2011\CSNMA_LBar\Promos');
    Base_Element_Path := PrefsINI.ReadString('Image Path Settings', 'Base Element Path', 'M:\NBCSN\');
    End_Element_Path := PrefsINI.ReadString('Image Path Settings', 'End Element Path', 'ELEMENTS');
    //Added for Version 2.0 - Image filenames
    Headshot_Backplate_Filename := PrefsINI.ReadString('Image Filename', 'Headshot Backplate Filename', 'ELEMENT 022.png');
    Team_Logo_Headshot_Filename := PrefsINI.ReadString('Image Filename', 'Team Logo Headshot Filename', 'ELEMENT 038.png');
    Team_Chip_A_Filename := PrefsINI.ReadString('Image Filename', 'Team Chip A Filename', 'ELEMENT 053.png');
    Team_Chip_B_Filename := PrefsINI.ReadString('Image Filename', 'Team Chip B Filename', 'ELEMENT 060.png');
    Team_Color_Filename := PrefsINI.ReadString('Image Filename', 'Team Color Filename', 'ELEMENT 085.png');
    Flag_Logo_Headshot_Filename := PrefsINI.ReadString('Image Filename', 'Flag Logo Headshot Filename', 'ELEMENT 013.png');

    //For base scene info
    BaseScene_SceneID := PrefsINI.ReadInteger('Base Scene Info', 'Base Scene ID', 10001001);
    BaseScene_Field_1_In_Command := PrefsINI.ReadString('Base Scene Info', 'Field 1 In Command', 'Field_1_In_Btn');
    BaseScene_Field_1_Out_Command := PrefsINI.ReadString('Base Scene Info', 'Field 1 Out Command', 'Field_1_Out_Btn');
    BaseScene_Field_2_In_Command := PrefsINI.ReadString('Base Scene Info', 'Field 2 In Command', 'Field_2_In_Btn');
    BaseScene_Field_2_Out_Command := PrefsINI.ReadString('Base Scene Info', 'Field 2 Out Command', 'Field_2_Out_Btn');
    BaseScene_Field_3_In_Command := PrefsINI.ReadString('Base Scene Info', 'Field 3 In Command', 'Field_3_In_Btn');
    BaseScene_Field_3_Out_Command := PrefsINI.ReadString('Base Scene Info', 'Field 3 Out Command', 'Field_3_Out_Btn');
    Sponsor_Logo_Scene_ID := PrefsINI.ReadInteger('Sponsor Logo Scene Info', 'Sponsor Logo Scene ID', 10001002);
    Full_Page_Promo_Scene_ID := PrefsINI.ReadInteger('Full Page Promo Scene Info', 'Full Page Promo Scene ID', 10007000);
    Full_Page_Promo_Template := PrefsINI.ReadInteger('Full Page Promo Scene Info', 'Full Page Promo Template', 31);

    //For GPIs
    GPIDebounceInterval := PrefsINI.ReadInteger('GPI Info', 'GPI De-Bounce Interval (mS)', 1000);
    GPIInfo[1].GPI_String := PrefsINI.ReadString('GPI Info', 'GPI 1 String', '*02');
    GPIInfo[1].GPI_Delay := PrefsINI.ReadInteger('GPI Info', 'GPI 1 Delay (mS)', 0);
    GPIInfo[2].GPI_String := PrefsINI.ReadString('GPI Info', 'GPI 2 String', '*01');
    GPIInfo[2].GPI_Delay := PrefsINI.ReadInteger('GPI Info', 'GPI 2 Delay (mS)', 0);
    GPIInfo[3].GPI_String := PrefsINI.ReadString('GPI Info', 'GPI 3 String', '*03');
    GPIInfo[3].GPI_Delay := PrefsINI.ReadInteger('GPI Info', 'GPI 3 Delay (mS)', 0);
    GPIInfo[4].GPI_String := PrefsINI.ReadString('GPI Info', 'GPI 4 String', '*04');
    GPIInfo[4].GPI_Delay := PrefsINI.ReadInteger('GPI Info', 'GPI 4 Delay (mS)', 0);
    GPIInfo[5].GPI_String := PrefsINI.ReadString('GPI Info', 'GPI 5 String', '*05');
    GPIInfo[5].GPI_Delay := PrefsINI.ReadInteger('GPI Info', 'GPI 5 Delay (mS)', 0);

    //For crawl data
    CrawlDataSocketPort := PrefsINI.ReadInteger('Crawl Data Response Socket Info', 'Port', 49201);

  finally
    //Free up the object
    PrefsINI.Free;
  end;
end;

//Procedure to store usee preferences
procedure TMainForm.StorePrefs;
var
  PrefsINI : TINIFile;
begin
  try
    //Create the INI file
    PrefsINI := TINIFile.Create(ExtractFilePath(Application.EXEName) + 'Comcast_Playout.ini');
    //Save the settings
    PrefsINI.WriteBool('General Settings', 'Force Upper Case', ForceUpperCase);
    PrefsINI.WriteBool('General Settings', 'GPIs Enabled', GPIsEnabled);
    PrefsINI.WriteInteger('General Settings', 'GPI COM Port', GPICOMPortNum);
    PrefsINI.WriteInteger('General Settings', 'GPI Baud Rate', GPIBaudRate);
    PrefsINI.WriteBool('General Settings', 'Schedule Monitoring Enabled', ScheduleMonitoringEnabled);
    PrefsINI.WriteBool('General Settings', 'Full Sponsor Pages Enabled', FullSponsorPagesEnabled);
    PrefsINI.WriteBool('General Settings', 'Tail Sponsor Pages Enabled', TailSponsorPagesEnabled);
    PrefsINI.WriteInteger('General Settings', 'Game Start Time Offset from ET (hours)', Trunc(GameStartTimeOffset*24));
    PrefsINI.WriteString('General Settings', 'Time Zone Suffix', TimeZoneSuffix);
    PrefsINI.WriteInteger('General Settings', 'Station ID Number', StationID);
    PrefsINI.WriteString('General Settings', 'Station Description', StationDescription);
    PrefsINI.WriteBool('General Settings', 'Enable Template Field Formatting', EnableTemplateFieldFormatting);
    PrefsINI.WriteBool('General Settings', 'Enable Persistent Sponsor Logo', EnablePersistentSponsorLogo);
    PrefsINI.WriteBool('General Settings', 'Blank Out Lineup For Sponsors', BlankOutLineupForSponsors);
    PrefsINI.WriteBool('General Settings', 'Allow Alert Backgrounds for News', AllowAlertBackgroundsForNews);
    PrefsINI.WriteBool('General Settings', 'Use Seeding for NCAAB Games', UseSeedingForNCAABGames);

    //For fantasy stats - used to trigger expansion into multiple templates
    PrefsINI.WriteInteger('General Settings', 'Fantasy Expansion Page Count (must be even)', FantasyExpansionPageCount);
    //For leader stats - used to trigger expansion into multiple templates
    PrefsINI.WriteInteger('General Settings', 'Leaders Expansion Page Count (must be even)', LeadersExpansionPageCount);

    PrefsINI.WriteBool('General Settings', 'Show Time and Day for Schedule', ShowTimeAndDay);

    PrefsINI.WriteBool('General Settings', 'Format for SNY', FormatForSNY);

    PrefsINI.WriteBool('General Settings', 'Use Manual Rankings (NCCAB)', UseManualRankings_NCAAB);
    PrefsINI.WriteBool('General Settings', 'Use Manual Rankings (NCCAF)', UseManualRankings_NCAAF);
    PrefsINI.WriteBool('General Settings', 'Use Manual Rankings (NCCAW)', UseManualRankings_NCAAW);
    PrefsINI.WriteBool('General Settings', 'Use Seeding for NCAAB Games', UseSeedingForNCAABGames);

    PrefsINI.WriteString('General Settings', 'Sponsor As-Run Logfile Directory Path', AsRunLogFileDirectoryPath);

    PrefsINI.WriteBool('General Settings', 'Load Scenes at Startup', LoadScenesAtStartup);

    PrefsINI.WriteBool('General Settings', 'Clear Data Panels on Abort', ClearDataPanelsOnAbort);

    PrefsINI.WriteBool('General Settings', 'Always Play All Scenes at Startup', AlwaysPlayAllScenesAtStartup);

    PrefsINI.WriteBool('General Settings', 'Enable Command Logging', CommandLoggingEnabled);

    PrefsINI.WriteBool('General Settings', 'Enable Template Field Updates', TemplateFieldUpdatesEnabled);

    PrefsINI.WriteBool('General Settings', 'Crawl Feedback Port Enabled', CrawlFeedbackPortEnabled);

    PrefsINI.WriteString('Database Connections', 'Ticker Database Connection String', DBConnectionString);
    PrefsINI.WriteString('Database Connections', 'Sportbase Database Connection String', DBConnectionString2);
    PrefsINI.WriteString('Database Connections', 'Traffic Database Connection String', DBConnectionString3);

    PrefsINI.WriteString('Ticker Engine Connection', 'IP Address', EngineParameters.IPAddress);
    PrefsINI.WriteString('Ticker Engine Connection', 'Port', EngineParameters.Port);
    //Game Final Glyph variables
    PrefsINI.WriteBool('Format Settings', 'Enable Game Final Glyph', UseGameFinalGlyph);
    PrefsINI.WriteInteger('Format Settings', 'Game Final Glyph Character ID', GameFinalGlyphCharacterID);

    PrefsINI.WriteString('Authoring Control Connection', 'Port', AuthoringControlPortNum);
    PrefsINI.WriteBool('Authoring Control Connection', 'Enable', AuthoringControlInterfaceEnabled);

    //For image paths
    PrefsINI.WriteString('Image Path Settings', 'League Logo Mapped Drive', League_Logo_Mapped_Drive);
    PrefsINI.WriteString('Image Path Settings', 'League Logo Host Drive', League_Logo_Host_Drive);
    PrefsINI.WriteString('Image Path Settings', 'League Logo Base Path', League_Logo_Base_Path);
    PrefsINI.WriteString('Image Path Settings', 'Team Logo Mapped Drive', Team_Logo_Mapped_Drive);
    PrefsINI.WriteString('Image Path Settings', 'Team Logo Host Drive', Team_Logo_Host_Drive);
    PrefsINI.WriteString('Image Path Settings', 'Team Logo Base Path', Team_Logo_Base_Path);
    PrefsINI.WriteString('Image Path Settings', 'Player Headshot Mapped Drive', Player_Headshot_Mapped_Drive);
    PrefsINI.WriteString('Image Path Settings', 'Player Headshot Host Drive', Player_Headshot_Host_Drive);
    PrefsINI.WriteString('Image Path Settings', 'Player Headshot Base Path', Player_Headshot_Base_Path);
    PrefsINI.WriteString('Image Path Settings', 'Weather Icon Base Path', Weather_Icon_Base_Path);
    PrefsINI.WriteString('Image Path Settings', 'Sponsor Logo Base Path', Sponsor_Logo_Base_Path);
    //Added for Version 2.0
    PrefsINI.WriteString('Image Path Settings', 'Full Promo Graphic Mapped Drive', Full_Promo_Graphic_Mapped_Drive);
    PrefsINI.WriteString('Image Path Settings', 'Full Promo Graphic Host Drive', Full_Promo_Graphic_Host_Drive);
    PrefsINI.WriteString('Image Path Settings', 'Full Promo Graphic Base Path', Full_Promo_Graphic_Base_Path);
    PrefsINI.WriteString('Image Path Settings', 'Top Region Image Mapped Drive', Top_Region_Image_Mapped_Drive);
    PrefsINI.WriteString('Image Path Settings', 'Top Region Image Host Drive', Top_Region_Image_Base_Path);
    PrefsINI.WriteString('Image Path Settings', 'Top Region Image Base Path', Top_Region_Image_Host_Drive);
    PrefsINI.WriteString('Image Path Settings', 'Base Element Path', Base_Element_Path);
    PrefsINI.WriteString('Image Path Settings', 'End Element Path', End_Element_Path);
    //Added for Version 2.0 - Image filenames
    PrefsINI.WriteString('Image Filename', 'Headshot Backplate Filename', Headshot_Backplate_Filename);
    PrefsINI.WriteString('Image Filename', 'Team Logo Headshot Filename', Team_Logo_Headshot_Filename);
    PrefsINI.WriteString('Image Filename', 'Team Chip A Filename', Team_Chip_A_Filename);
    PrefsINI.WriteString('Image Filename', 'Team Chip B Filename', Team_Chip_B_Filename);
    PrefsINI.WriteString('Image Filename', 'Team Color Filename', Team_Color_Filename);
    PrefsINI.WriteString('Image Filename', 'Flag Logo Headshot Filename', Flag_Logo_Headshot_Filename);

    //For base scene info
    PrefsINI.WriteInteger('Base Scene Info', 'Base Scene ID', BaseScene_SceneID);
    PrefsINI.WriteString('Base Scene Info', 'Field 1 In Command', BaseScene_Field_1_In_Command);
    PrefsINI.WriteString('Base Scene Info', 'Field 1 Out Command', BaseScene_Field_1_Out_Command);
    PrefsINI.WriteString('Base Scene Info', 'Field 2 In Command', BaseScene_Field_2_In_Command);
    PrefsINI.WriteString('Base Scene Info', 'Field 2 Out Command', BaseScene_Field_2_Out_Command);
    PrefsINI.WriteString('Base Scene Info', 'Field 3 In Command', BaseScene_Field_3_In_Command);
    PrefsINI.WriteString('Base Scene Info', 'Field 3 Out Command', BaseScene_Field_3_Out_Command);

    //For GPIs
    PrefsINI.WriteInteger('GPI Info', 'GPI De-Bounce Interval (mS)', GPIDebounceInterval);
    PrefsINI.WriteString('GPI Info', 'GPI 1 String', GPIInfo[1].GPI_String);
    PrefsINI.WriteInteger('GPI Info', 'GPI 1 Delay (mS)', GPIInfo[1].GPI_Delay);
    PrefsINI.WriteString('GPI Info', 'GPI 2 String', GPIInfo[2].GPI_String);
    PrefsINI.WriteInteger('GPI Info', 'GPI 2 Delay (mS)', GPIInfo[2].GPI_Delay);
    PrefsINI.WriteString('GPI Info', 'GPI 3 String', GPIInfo[3].GPI_String);
    PrefsINI.WriteInteger('GPI Info', 'GPI 3 Delay (mS)', GPIInfo[3].GPI_Delay);
    PrefsINI.WriteString('GPI Info', 'GPI 4 String', GPIInfo[4].GPI_String);
    PrefsINI.WriteInteger('GPI Info', 'GPI 4 Delay (mS)', GPIInfo[4].GPI_Delay);
    PrefsINI.WriteString('GPI Info', 'GPI 5 String', GPIInfo[5].GPI_String);
    PrefsINI.WriteInteger('GPI Info', 'GPI 5 Delay (mS)', GPIInfo[5].GPI_Delay);
    PrefsINI.WriteInteger('Sponsor Logo Scene Info', 'Sponsor Logo Scene ID', Sponsor_Logo_Scene_ID);
    PrefsINI.WriteInteger('Full Page Promo Scene Info', 'Full Page Promo Scene ID', Full_Page_Promo_Scene_ID);
    PrefsINI.WriteInteger('Full Page Promo Scene Info', 'Full Page Promo Template', Full_Page_Promo_Template);

    //For crawl data
    PrefsINI.ReadInteger('Crawl Data Response Socket Info', 'Port', CrawlDataSocketPort);
  finally
    //Free up the object
    PrefsINI.Free;
  end;
end;

//Handler for GPI packet
procedure TMainForm.ComPortDataPacketStringPacket(Sender: TObject; Data: String);
var
  i: SmallInt;
begin
  //Only proceed if GPIs enabled
  if (GPIsOKToFire = TRUE) AND (GPIsEnabled = TRUE) AND (Length(Data) >= 3) then
  begin
    //Check for GPI #1 - Trigger Current Ticker
    if (Data = GPIInfo[1].GPI_String) then
    begin
      //Delay if required
      if ((GPIInfo[1].GPI_Delay DIV 10) > 0) then
      begin
        for i := 1 to (GPIInfo[1].GPI_Delay DIV 10) do
        begin
          Sleep(10);
          Application.ProcessMessages;
        end;
      end;
      //Call procedure to trigger next event
      TriggerCurrentTicker;
      //Set indicator
      ApdStatusLight3.Lit := TRUE;
      //Disable GPIs and enable GPI reset timer
      GPIsOKToFire := FALSE;
      GPIResetTimer.Enabled := TRUE;
      //Write to log
      EngineInterface.WriteToAsRunLog('Received GPI trigger to start/resume ticker');
    end
    else ApdStatusLight3.Lit := FALSE;
    //Check for GPI #2 - Abort Ticker
    if (Data = GPIInfo[2].GPI_String) then
    begin
      //Delay if required
      if ((GPIInfo[2].GPI_Delay DIV 10) > 0) then
      begin
        for i := 1 to (GPIInfo[2].GPI_Delay DIV 10) do
        begin
          Sleep(10);
          Application.ProcessMessages;
        end;
      end;
      //Call procedure to key bug in
      AbortCurrentEvent(IS_NOT_STARTUP);
      //Set indicator
      ApdStatusLight4.Lit := TRUE;
      //Disable GPIs and enable GPI reset timer
      GPIsOKToFire := FALSE;
      GPIResetTimer.Enabled := TRUE;
      //Write to log
      EngineInterface.WriteToAsRunLog('Received GPI trigger to stop ticker');
    end
    else ApdStatusLight4.Lit := FALSE;
    //Check for GPI #3 - Reset Ticker to Top and Trigger
    if (Data = GPIInfo[3].GPI_String) then
    begin
      //Delay if required
      if ((GPIInfo[3].GPI_Delay DIV 10) > 0) then
      begin
        for i := 1 to (GPIInfo[3].GPI_Delay DIV 10) do
        begin
          Sleep(10);
          Application.ProcessMessages;
        end;
      end;
      //Reset ticker to top
      ResetTickerToTop;
      //Call procedure to trigger next event
      TriggerCurrentTicker;
      //Set indicator
      ApdStatusLight3.Lit := TRUE;
      //Disable GPIs and enable GPI reset timer
      GPIsOKToFire := FALSE;
      GPIResetTimer.Enabled := TRUE;
      //Write to log
      EngineInterface.WriteToAsRunLog('Received GPI trigger to reset & start ticker');
    end
    else ApdStatusLight3.Lit := TRUE;
  end
  //Not enabled; make sure all lights not lit
  else begin
    ApdStatusLight3.Lit := FALSE;
    ApdStatusLight4.Lit := FALSE;
  end;
end;

//Handler to enable command logging
procedure TMainForm.EnableCommandLogging1Click(Sender: TObject);
begin
  CommandLoggingEnabled := TRUE;
  EnableCommandLogging1.Checked := TRUE;
  DisableCommandLogging1.Checked := FALSE;
end;

//Handler to disable command logging
procedure TMainForm.DisableCommandLogging1Click(Sender: TObject);
begin
  CommandLoggingEnabled := FALSE;
  EnableCommandLogging1.Checked := FALSE;
  DisableCommandLogging1.Checked := TRUE;
end;

//Handler to use NCAAB Ranks
procedure TMainForm.UseRankingsforNCAABClick(Sender: TObject);
begin
  UseRankingsforNCAAB.Checked := TRUE;
  UseSeedforNCAAB.Checked := FALSE;
  UseSeedingForNCAABGames := FALSE;
  StorePrefs;
end;

//Handler to use NCAAB Seeds
procedure TMainForm.UseSeedforNCAABClick(Sender: TObject);
begin
  UseRankingsforNCAAB.Checked := FALSE;
  UseSeedforNCAAB.Checked := TRUE;
  UseSeedingForNCAABGames := TRUE;
  StorePrefs;
end;

////////////////////////////////////////////////////////////////////////////////
// CHANNEL BOX SPECIFIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Handler to clear all templates
procedure TMainForm.ClearAllGraphics1Click(Sender: TObject);
begin
  ClearAllTemplates;
end;

//Procedure to clear all graphics; sends transition out to all templates; removes backplates and
//resets last template variables for all regions
procedure TMainForm.ClearAllTemplates;
var
  i: SmallInt;
  TemplateDefsRecPtr: ^TemplateDefsRec;
begin
  if (Template_Defs_Collection.Count > 0) then
  begin
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_Type > 0) then
      begin
        EngineInterface.UpdateScene(TemplateDefsRecPtr^.Scene_ID_1, TemplateDefsRecPtr^.Transition_Out + '�' + 'T');
        EngineInterface.UpdateScene(TemplateDefsRecPtr^.Scene_ID_2, TemplateDefsRecPtr^.Transition_Out + '�' + 'T');
      end;
    end;
  end;
  //Clear backplates
  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_1_Out_Command + '�' + 'T');
  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_2_Out_Command + '�' + 'T');
  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_3_Out_Command + '�' + 'T');
  //Clear out the last template info
  LastTemplateInfo[PLAYLIST_1].LastFieldTypeID := STARTUP;
  LastTemplateInfo[PLAYLIST_1].Scene_ID := 0;
  LastTemplateInfo[PLAYLIST_2].LastFieldTypeID := STARTUP;
  LastTemplateInfo[PLAYLIST_2].Scene_ID := 0;
end;

//Handler to reload all scenes
procedure TMainForm.ReloadAllScenes1Click(Sender: TObject);
begin
  //Load scenes
  if (LoadScenesAtStartup) then EngineInterface.SetupInitialScenes;
  //Run startup commands
  RunStartupCommands;
  //Clear all graphics
  ClearAllTemplates;
end;

procedure TMainForm.ClearFieldDataPanels1Click(Sender: TObject);
begin
  EngineInterface.ClearFieldPanels(STOP_SCENES);
end;

////////////////////////////////////////////////////////////////////////////////
// TRAFFIC DATA
////////////////////////////////////////////////////////////////////////////////
//Trigger traffic crawl
procedure TMainForm.TrafficOnBtnClick(Sender: TObject);
begin
  TriggerTrafficCrawl;
end;
procedure TMainForm.TriggerTrafficCrawl;
begin
  if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID <> STARTUP) then
  begin
    //Reload the traffic data
    ReloadTrafficData;
    //Set crawl header
    EngineInterface.UpdateScene(CRAWL_SCENE, CRAWL_HEADER + '�' + ' TRAFFIC');

    //Send command to engine to transition crawl backplate in
    EngineInterface.UpdateScene(CRAWL_SCENE, CRAWL_IN + '�' + 'T');

    //Send first data record
    SendNextCrawlRecord;

    //Set flag
    TrafficCrawlDataIn := TRUE;

  end;
  //Set crawl enabled flag
  TrafficCrawlEnabled := TRUE;
  //Set indicators
  TrafficEnableLED.Lit := TRUE;
  TrafficDisableLED.Lit := FALSE;
end;

//Clear traffic crawl
procedure TMainForm.TrafficOffBtnClick(Sender: TObject);
begin
  ClearTrafficCrawl;
end;
procedure TMainForm.ClearTrafficCrawl;
begin
  if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID <> STARTUP) then
  begin
    //Send command to engine to clear crawl backplate
    EngineInterface.UpdateScene(CRAWL_SCENE, CRAWL_OUT + '�' + 'T');
    //Clear flag to prevent additional crawl data from being sent

    //Clear flag
    TrafficCrawlDataIn := FALSE;
  end;
  //Clear crawl enabled flag
  TrafficCrawlEnabled := FALSE;
  //Set indicators
  TrafficEnableLED.Lit := FALSE;
  TrafficDisableLED.Lit := TRUE;
end;

//Procedure to send the crawl data for the current index in the traffic collection
procedure TMainForm.SendNextCrawlRecord;
var
  TrafficDataRecPtr: ^TrafficDataRec;
begin
  if (Traffic_Data_Collection.Count > 0) then
  begin
    //Get data
    TrafficDataRecPtr := Traffic_Data_Collection.At(CurrentTrafficDataIndex);
    //Send data; append action keyword
    //Pad spaces if first enry
    if (CurrentTrafficDataIndex = 0) then
      EngineInterface.UpdateScene(CRAWL_SCENE, CRAWL_TEXT + '�' +
        '                                          ' + TrafficDataRecPtr^.DataStr + '\\action')
    //Pad spaces if last enry
    else if (CurrentTrafficDataIndex = Traffic_Data_Collection.Count-1) then
      EngineInterface.UpdateScene(CRAWL_SCENE, CRAWL_TEXT + '�' + TrafficDataRecPtr^.DataStr +
        '                                                                                       \\action')
    else
      EngineInterface.UpdateScene(CRAWL_SCENE, CRAWL_TEXT + '�' + TrafficDataRecPtr^.DataStr + '\\action');
  end;
end;

//Procedure to relaod the traffic data
procedure TMainForm.ReloadTrafficData;
var
  i: SmallInt;
  TrafficDataRecPtr: ^TrafficDataRec;
  TempStr: String;
  ColorStr: String;
  TravelTime: Double;
  SpeedLimitTravelTime: Double;
  DelayPercentage: Double;
begin
  try
    //Load collections for users and block subcategories
    Traffic_Data_Collection.Clear;
    Traffic_Data_Collection.Pack;
    //Close the query
    dmMain.TrafficQuery.Close;
    //Load in teams information
    dmMain.TrafficQuery.SQL.Clear;
    //Only pull in enabled data
    dmMain.TrafficQuery.SQL.Add('SELECT * FROM Traffic_Times WHERE EnabledForAir = 1');
    dmMain.TrafficQuery.Open;
    if (dmMain.TrafficQuery.RecordCount > 0) then
    begin
      //Add header
      //TempStr := 'TRAVEL TIMES: ';
      //ColorStr := TRAFFIC_WHITE;
      //Append color string
      //GetMem (TrafficDataRecPtr, SizeOf(TrafficDataRec));
      //With TrafficDataRecPtr^ do DataStr := ColorStr + TempStr;
      //Add to collection
      //If (Traffic_Data_Collection.Count <= 500) then
      //begin
      //  Traffic_Data_Collection.Insert(TrafficDataRecPtr);
      //  Traffic_Data_Collection.Pack;
      //end;

      //Add traffic times data
      dmMain.TrafficQuery.First;
      repeat
        //Add item to teams collection
        GetMem (TrafficDataRecPtr, SizeOf(TrafficDataRec));
        With TrafficDataRecPtr^ do
        begin
          //Get segment name
          TempStr := UpperCase(dmMain.TrafficQuery.FieldByName('SegmentName').AsString);
          TravelTime := dmMain.TrafficQuery.FieldByName('TravelTime').AsFloat;
          SpeedLimitTravelTime := dmMain.TrafficQuery.FieldByName('SpeedLimitTravelTime').AsFloat;
          //Prevent division by 0
          if (SpeedLimitTravelTime > 0) then
            DelayPercentage := (TravelTime-SpeedLimitTravelTime)/SpeedLimitTravelTime
          else DelayPercentage := -1;
          if (DelayPercentage >= 0.4) then ColorStr := TRAFFIC_RED
          else if (DelayPercentage < 0.4) and (DelayPercentage >= 0.2) then ColorStr := TRAFFIC_YELLOW
          else ColorStr := TRAFFIC_GREEN;
          //Append color string
          DataStr := ColorStr + TempStr;
          //Add to collection
          if (Traffic_Data_Collection.Count <= 500) and (DelayPercentage <> -1) then
          begin
            Traffic_Data_Collection.Insert(TrafficDataRecPtr);
            Traffic_Data_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.TrafficQuery.Next;
      until (dmMain.TrafficQuery.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close the query
    dmMain.TrafficQuery.Close;
    dmMain.TrafficQuery.SQL.Clear;

    //Now, get any traffic incidents that are enabled
    //Only pull in enabled data where the impact Type >= 3
    dmMain.TrafficQuery.SQL.Add('SELECT * FROM Traffic_Incidents WHERE (EnabledForAir = 1) AND (ImpactType >= 3)');
    dmMain.TrafficQuery.Open;
    if (dmMain.TrafficQuery.RecordCount > 0) then
    begin
      dmMain.TrafficQuery.First;
      //Add header
      //TempStr := '    INCIDENTS: ';
      //ColorStr := TRAFFIC_WHITE;
      //Append color string
      //GetMem (TrafficDataRecPtr, SizeOf(TrafficDataRec));
      //Append color string
      //With TrafficDataRecPtr^ do DataStr := ColorStr + TempStr;
      //Insert into collection
      //If (Traffic_Data_Collection.Count <= 500) then
      //begin
      //  Traffic_Data_Collection.Insert(TrafficDataRecPtr);
      //  Traffic_Data_Collection.Pack;
      //end;
      repeat
        //Add item to teams collection
        GetMem (TrafficDataRecPtr, SizeOf(TrafficDataRec));
        With TrafficDataRecPtr^ do
        begin
          //Get segment name
          TempStr := UpperCase(dmMain.TrafficQuery.FieldByName('IncidentDescription').AsString);
          ColorStr := TRAFFIC_WHITE;
          //Append color string
          DataStr := ColorStr + TempStr;
          //Add to collection
          If (Traffic_Data_Collection.Count <= 500) then
          begin
            Traffic_Data_Collection.Insert(TrafficDataRecPtr);
            Traffic_Data_Collection.Pack;
          end;
        end;
        //Got to next record
        dmMain.TrafficQuery.Next;
      until (dmMain.TrafficQuery.EOF = TRUE); //Repeat until end of dataset
    end;
    //Close the query
    dmMain.TrafficQuery.Close;
    //Reset index
    CurrentTrafficDataIndex := 0;
  except

  end;
end;

////////////////////////////////////////////////////////////////////////////////
//CLOCK COMMANDS
////////////////////////////////////////////////////////////////////////////////
//Enable clock
procedure TMainForm.ClockOnBtnClick(Sender: TObject);
begin
  if (ClockDisplayEnabled = CLOCK_OUT) then
  begin
    //Set indicators
    //ClockDisplayEnabled := TRUE;
    ClockEnableLED.Lit := TRUE;
    ClockDisableLED.Lit := FALSE;
    //Command the clock
    EngineInterface.SetClockState(CLOCK_IN);

    //Added for Version 2.0.1
    EngineInterface.SetNextTopGraphic;
  end;
end;

//Disable clock
procedure TMainForm.ClockOffBtnClick(Sender: TObject);
begin
  if (ClockDisplayEnabled = CLOCK_IN) then
  begin
    //Set indicators
    //ClockDisplayEnabled := FALSE;
    ClockEnableLED.Lit := FALSE;
    ClockDisableLED.Lit := TRUE;
    //Command the clock
    EngineInterface.SetClockState(CLOCK_OUT);

    //Added for Version 2.0.1
    EngineInterface.SetNextTopGraphic;
  end;
end;

end.

